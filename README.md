# FrontendAppProject

#Best practises following Angular 2 Styleguide
@https://angular.io/styleguide#!#04-12

  - 400 lines of code by file as maximum
  - 75 lines by function as maximum
  - Separate third party components from core 
  - Consider creating sub-folders when a folder reaches seven or more files.
  - Consider configuring the IDE tng new codelyzero hide distracting, irrelevant files such as generated .js and .js.map files.
  - Do place properties up top followed by methods.
  - Do place private members after public members, alphabetized.
