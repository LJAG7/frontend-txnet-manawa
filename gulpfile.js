var gulp = require('gulp'),
  sass = require('gulp-sass'),
  minify = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  gutil = require('gulp-util'),
  argv = require('minimist')(process.argv),
  gulpif = require('gulp-if'),
  prompt = require('gulp-prompt'),
  rsync = require('gulp-rsync'),
  path = require('path');



//TODO

gulp.task("deploy", function () {
  var rsyncPaths = [path.dist, 'lang', 'lib', 'templates', './*.php', './style.css'],
    rsyncConf = {
      progress: true,
      incremental: true,
      relative: true,
      emptyDirectories: true,
      recursive: true,
      clean: true,
      exclude: []
    };

  if (argv.staging) {

    rsyncConf.hostname = ''; // hostname
    rsyncConf.username = ''; // ssh username
    rsyncConf.destination = ''; // path where uploaded files go
  } else if (argv.production) {

    rsyncConf.hostname = ''; // hostname
    rsyncConf.username = ''; // ssh username
    rsyncConf.destination = ''; // path where uploaded files go
  } else {
    throwError('deploy', gutil.colors.red('Missing or invalid target'));
  }

  // Use gulp-rsync to sync the files
  return gulp.src(rsyncPaths)
    .pipe(gulpif(
      argv.production,
      prompt.confirm({
        message: 'Heads Up! Are you SURE you want to push to PRODUCTION?',
        default: false
      })
    ))
    .pipe(rsync(rsyncConf));
});

// Compile component SASS
gulp.task('styles', function () {
  gulp.src(['src/app/**/*.scss', 'src/modules/**/*.scss', 'src/app/*.scss'], {base: process.cwd()})
    .pipe(sass().on('error', sass.logError))
    .pipe(rename(function (path) {
      path.basename += ".component";
      path.extname = ".css";
    }))
    .pipe(gulp.dest('./'));
});

// Watch components task
gulp.task('serve', function () {
  gulp.watch('src/app/**/*.scss', ['styles']);
});

// Compile primeng SASS
gulp.task('styles-primeng', function () {
  gulp.src(['src/assets/sass/override.scss'], {base: process.cwd()})
    .pipe(sass().on('error', sass.logError))
    .pipe(rename(function (path) {
      path.extname = ".css";
    }))
    .pipe(gulp.dest(''));
});

// Watch primeng task
gulp.task('serve-primeng', function () {
  gulp.watch('src/assets/sass/override.scss', ['styles-primeng']);
});


function throwError(taskName, msg) {
  throw new gutil.PluginError({
    plugin: taskName,
    message: msg
  });
}
