'use strict';

import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';

import {TranslatePipe, TranslateService} from 'ng2-translate/ng2-translate';

import {AuthSrvService} from '../shared/services/auth.service';
import {LoaderService} from '../shared/services/loader.service';

const _ = require('lodash');

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css'],
  providers: [
    TranslateService,
    TranslatePipe,
  ]
})

export class AdminPanelComponent implements OnInit {

  public tabs: SelectItem[];
  public selectedTab: number;

  constructor(private _authSrv: AuthSrvService,
              private _loaderSrv: LoaderService,
              private _router: Router) {

    this._authSrv.verifyViewPermission('admin');
    this._loaderSrv.general();
  }

  ngOnInit() {
    if (this.isAllowed()) {

      const permissionConfig = this._authSrv.getViewConfig('admin');
      this.tabs = permissionConfig['tabs'];
      this.selectedTab = permissionConfig['selectedTab'];

      const selectedTab = localStorage.getItem('adminPanelSelectedTab');
      if (!_.isNil(selectedTab)) {
        if (this._authSrv.getPersonLogged().isAdmin() && Number(selectedTab) === 0) {
          this.onTabSelected({option: {value: 1 }});
        } else {
          this.selectedTab = Number(selectedTab);
        }
      }

      setTimeout( () => {
        this._loaderSrv.stop();
      }, 500);
    }

  }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('admin', tag);
  }

  /**************************** EVENTS DOM ****************************/
  /**
   * Return to list of team type
   */
  public goBackTo(): void {
    this._router.navigateByUrl('/');
  }

  /**
   * On tab selected
   * @param event
   */
  public onTabSelected(event): void {
    localStorage.setItem('adminPanelSelectedTab', event['option']['value'].toString());
  }
}
