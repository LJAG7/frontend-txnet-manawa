import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';

/*MODULE ASSETS*/
import {AdminPanelComponent} from './admin-panel.component';

import {TabDomainComponent} from './tabs/tab-domain/tab-domain.component';

import {TabUsersComponent} from './tabs/tab-users/tab-users.component';
import {TabAdminGestorScopeComponent} from './subviews/gestor-scope/gestor-scope.component';
import {TeamManagedCellComponent} from './components/team-managed/team-managed-cell.component';
import {TeamUnmanagedCellComponent} from './components/team-unmanaged/team-unmanaged-cell.component';
import {TabAdminGestorAddUserComponent} from './subviews/add-user/add-user.component';

import {TabEntityComponent} from './tabs/tab-entity/tab-entity.component';
import {EditMasterClassComponent} from './tabs/tab-entity/edit-master-class/edit-master-class.component';
import {AddMasterClassComponent} from './tabs/tab-entity/edit-master-class/add-master-class/add-master-class.component';
import {EditTeamGroupComponent} from './tabs/tab-entity/edit-team-group/edit-team-group.component';
import {AddTeamGroupComponent} from './tabs/tab-entity/edit-team-group/add-team-group/add-team-group.component';
import {EditRoleComponent} from './tabs/tab-entity/edit-role/edit-role.component';
import {AddRoleComponent} from './tabs/tab-entity/edit-role/add-role/add-role.component';
import {EditRoleCategoryComponent} from './tabs/tab-entity/edit-role-category/edit-role-category.component';
import {AddRoleCategoryComponent} from './tabs/tab-entity/edit-role-category/add-role-category/add-role-category.component';

/*SERVICES*/
import {HttpSrvService} from '../shared/services/http.service';
import {ComponentsModule} from '../shared/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    ComponentsModule
  ],
  exports: [
  ],
  declarations: [
    AdminPanelComponent,
    TabDomainComponent,
    TabUsersComponent,
    TabAdminGestorScopeComponent,
    TeamManagedCellComponent,
    TeamUnmanagedCellComponent,
    TabAdminGestorAddUserComponent,
    TabEntityComponent,
    EditMasterClassComponent,
    AddMasterClassComponent,
    EditTeamGroupComponent,
    AddTeamGroupComponent,
    EditRoleCategoryComponent,
    AddRoleCategoryComponent,
    EditRoleComponent,
    AddRoleComponent
  ],
  providers: [
    HttpSrvService
  ]
})

export class AdminPanelModule {}
