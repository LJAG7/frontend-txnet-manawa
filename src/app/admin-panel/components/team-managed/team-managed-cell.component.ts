import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from '@angular/core';

import {isNull} from 'util';

const _ = require('lodash');

@Component({
  selector: 'app-team-managed-cell-component',
  templateUrl: './team-managed-cell.component.html',
  styleUrls: ['./team-managed-cell.component.css']
})
export class TeamManagedCellComponent implements OnInit, OnChanges {

  @ViewChild('teamBox') public teamBoxComponent: ElementRef;
  @Input() public team: object;
  @Input() public isCascade: boolean;
  @Input() public hidden = false;
  @Input() public externalConfig: object;
  @Output() public outputEvent = new EventEmitter();

  private config = {
    id: 'team-box_component_n'.concat(Math.floor(Math.random() * 100000).toString()),
    root: true,
    level: 1
  };

  // *************** STATICS METHODS ********************************** //
  public static isValidObjectProperty(item: object, property: string): boolean {
    return item
      && item.hasOwnProperty(property)
      && item[property];
  }

  constructor() {
  }

  ngOnInit() {
    _.merge(this.config, this.externalConfig);
  }

  ngOnChanges(changes: SimpleChanges): void {
    _.merge(this.config, this.externalConfig);
  }


  // **************** EMITTERS / EVENTS METHODS ****************//

  private emitOutputEvent(event: string,
                          params: object = null): void {

    const emit = {
      id: this.config.id,
      team: this.team,
      event: event
    };

    if (!isNull(params)) {
      _.merge(emit, params);
    }

    this.outputEvent.emit(emit);
  }


  // ********** MODEL METHODS ********** //
  public getId(): string {
    return this.config.id;
  }

  public getTeamId(): number {
    return TeamManagedCellComponent.isValidObjectProperty(this.team, 'id')
      ? Number(this.team['id'])
      : -1;
  }

  public getTeamName(): string {
    return TeamManagedCellComponent.isValidObjectProperty(this.team, 'name') && !_.isEmpty(this.team['name'])
      ? this.team['name']
      : '(sin nombre)';
  }

  public getTeamLevel(): number {
    return TeamManagedCellComponent.isValidObjectProperty(this.team, 'level')
      ? this.team['level']
      : 0;
  }

  public getTeamLocation(): Array<any> {
    return TeamManagedCellComponent.isValidObjectProperty(this.team, 'team_locations')
      ? this.team['team_locations']
      : [];
  }

  public getTeamSubTeams(): Array<any> {
    return TeamManagedCellComponent.isValidObjectProperty(this.team, 'child_teams')
      ? this.team['child_teams']
      : [];
  }


  public generateSubTeamConfig(): object {
    return {
      root: false,
      level: this.config.level + 1
    };
  }

  private getTeamSubTeamsLength(): number {
    return this.getTeamSubTeams().length;
  }

  // ********** DOM METHODS ********** //
  public deleteTeam(): void {
    if (!_.isNil(this.getTeamId()) && this.getTeamId() > 0) {
      this.emitOutputEvent('delete');
    }
  }


  public getTeamLevelToShow(): string {
    return this.getTeamLevel() > 0
      ? 'niv. ' + this.getTeamLevel()
      : '';
  }

  public getTeamLocationToShow(): string {
    const locations = [];

    for (const location of this.getTeamLocation()) {
      if (!_.isNil(location) && location.hasOwnProperty('name') && !_.isNil(location['name'])) {
        locations.push(location['name']);
      }
    }

    return _.isEmpty(locations)
      ? 'sin localización'
      : locations.join(', ');
  }


  public isRootNode(): boolean {
    return this.config.root;
  }

  public isMultiBox(): boolean {
    return this.hasTeamSubTeams() || this.isCascade;
  }

  public getBoxAreaLevelCss(): string {
    return this.isRootNode()
      ? ''
      : 'box-subteam box-n' + this.config.level;
  }

  public getBoxMainCss(): string {
    const css = [];

    if (this.isRootNode() || this.hasTeamSubTeams()) {
      css.push('node-child');
    }

    if (this.isCascade) {
      css.push('team-cascade');
    }

    return css.join(' ');
  }

  public hasTeamSubTeams(): boolean {
    return this.getTeamSubTeamsLength() > 0;
  }

  public getTeamSubTeamsDescription(): string {
    return this.isCascade
      ? this.hasTeamSubTeams()
        ? 'supervisa también todos los subequipos' + this.getTeamSubTeamsLength()
        : 'este equipo no tiene subequipos<br>supervisa también todos los equipos futuros'
      : '';
  }


  /**
   * Return if box is expanded or not
   * @returns {boolean}
   */
  protected isExpandedChild(): boolean {
    return this.isRootNode()
      ? this.team && this.team.hasOwnProperty('expanded')
        ? this.team['expanded']
        : false
      : true;
  }

  /**
   * Output event to parent when one element from list is clicked
   */
  public onClickToogleEvent(): void {
    if (this.hasTeamSubTeams()) {
      this.team['expanded'] =  this.team && this.team.hasOwnProperty('expanded')
        ? !this.team['expanded']
        : true;
    }
  }


  /**
   * Apply css fadeOut to animation. Notify status deleted to parent component to call actions
   */
  public fadeOut(): void {
    this.teamBoxComponent.nativeElement.classList.add('delete');
  }
}
