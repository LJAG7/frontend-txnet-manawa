import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from '@angular/core';

const _ = require('lodash');
import {isNull} from 'util';

@Component({
  selector: 'app-team-unmanaged-cell-component',
  templateUrl: './team-unmanaged-cell.component.html',
  styleUrls: ['./team-unmanaged-cell.component.css']
})
export class TeamUnmanagedCellComponent implements OnInit, OnChanges {

  @ViewChild('unmanagedTeam') public unmanagedTeam: ElementRef;
  @Input() public team: object;
  @Input() public hidden = false;
  @Input() public externalConfig: object;
  @Output() public outputEvent = new EventEmitter();

  private config = {
    id: 'team-box_component_n'.concat(Math.floor(Math.random() * 100000).toString()),
    root: true,
    level: 1
  };

  // *************** STATICS METHODS ********************************** //
  public static isValidObjectProperty(item: object, property: string): boolean {
    return item
      && item.hasOwnProperty(property)
      && item[property];
  }

  constructor() {
  }

  ngOnInit() {
    _.merge(this.config, this.externalConfig);
  }

  ngOnChanges(changes: SimpleChanges): void {
    _.merge(this.config, this.externalConfig);
  }


  // **************** EMITTERS / EVENTS METHODS ****************//

  private emitOutputEvent(event: string,
                          params: object = null): void {

    const emit = {
      id: this.config.id,
      team: this.team,
      event: event
    };

    if (!isNull(params)) {
      _.merge(emit, params);
    }

    this.outputEvent.emit(emit);
  }


  // ********** MODEL METHODS ********** //
  public getId(): string {
    return this.config.id;
  }

  public getTeamId(): number {
    return TeamUnmanagedCellComponent.isValidObjectProperty(this.team, 'id')
      ? Number(this.team['id'])
      : -1;
  }

  public getTeamName(): string {
    return TeamUnmanagedCellComponent.isValidObjectProperty(this.team, 'name') && !_.isEmpty(this.team['name'])
      ? this.team['name']
      : '(sin nombre)';
  }

  public getTeamLevel(): number {
    return TeamUnmanagedCellComponent.isValidObjectProperty(this.team, 'level')
      ? this.team['level']
      : 0;
  }

  public getTeamLocation(): Array<any> {
    return TeamUnmanagedCellComponent.isValidObjectProperty(this.team, 'location')
      ? this.team['location']
      : [];
  }

  // ********** DOM METHODS ********** //

  public getTeamLevelToShow(): string {
    return this.getTeamLevel() > 0
      ? 'niv. ' + this.getTeamLevel()
      : 'sin nivel';
  }

  public getTeamLocationToShow(): string {
    const locations = [];

    for (const location of this.getTeamLocation()) {
      if (!_.isNil(location) && location.hasOwnProperty('name') && !_.isNil(location['name'])) {
        locations.push(location['name']);
      }
    }

    return _.isEmpty(locations)
      ? 'sin localización'
      : locations.join(', ');
  }


  public isManagedTeam(): boolean {
    return !this.isManaging()
    && TeamUnmanagedCellComponent.isValidObjectProperty(this.team, 'selected')
    && this.team['selected'] === true;
  }

  public isManaging(): boolean {
    return TeamUnmanagedCellComponent.isValidObjectProperty(this.team, 'managing') && this.team['managing'];
  }

  public onClickTeam(): void {
    this.emitOutputEvent(this.isManagedTeam() ? 'delete' : 'add');
  }
}
