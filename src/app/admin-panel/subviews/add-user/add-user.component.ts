'use strict';

import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import {Router} from '@angular/router';

import {ROLESYSTEMS} from '../../../shared/const/rolesystems.const';
import {BaseComponentModel} from '../../../shared/models/base-component/base-component.class';

import {AuthSrvService} from '../../../shared/services/auth.service';
import {HttpSrvService} from '../../../shared/services/http.service';

import {REGEXPATTERNSHTML} from '../../../shared/const/system.const';

const _ = require('lodash');

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css'],
  providers: [HttpSrvService]
})
export class TabAdminGestorAddUserComponent extends BaseComponentModel implements OnInit {

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() public outputEvent = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('confirmDialogComponent') public confirmDialogComponent;
  @ViewChild('noSendInvitationsDialogComponent') public noSendInvitationsDialogComponent;

  /************************ COMPONENT VARS  ************************/

  // Static vars for static methods

  public isValidPerson = TabAdminGestorAddUserComponent.isValidPerson;
  public onInvalidEmail = TabAdminGestorAddUserComponent.onInvalidEmail;
  public onInvalidName = TabAdminGestorAddUserComponent.onInvalidName;
  public onInvalidSurname = TabAdminGestorAddUserComponent.onInvalidSurname;

  // Component vars

  public systemRoles: Array<object | any> = [];

  private _persons: Array<object | any> = [];

  private _defaultPerson = {
    name: null,
    surname: null,
    email: undefined,
    role: ROLESYSTEMS.ROLE_USER,
    validate: false
  };

  /************************ CONFIG VARS  ************************/

  private _confirmDialogConfig = {
    header: 'Las personas que no tengan todos los campos informados y validados no serán invitadas.',
    message: '¿Desea enviar el resto de invitaciones?',
    accept: {
      action: 'sendInvitations',
      button: {
        text: 'Si'
      }
    }
  };

  private _emailInputValidatorConfig = {
    field: {
      placeholder: 'Email',
      css: {
        empty: true
      },
      validations: {
        required: {
          active: true,
          value: true,
          message: 'Campo obligatorio.'
        },
        maxlength: {
          active: true,
          value: 255,
          message: 'No puede tener más de 255 caracteres.'
        },
        pattern: {
          active: true,
          value: REGEXPATTERNSHTML.email,
          message: 'Email no válido.'
        }
      },
      validationsApi: {
        url: 'persons/InputValidator/email',
        message: 'El email ya existe.'
      },
      style: 'lowerCase'
    }
  };

  private _nameInputValidatorConfig = {
    field: {
      placeholder: 'Nombre',
      capitalize: true,
      css: {
        empty: true
      },
      validations: {
        required: {
          active: true,
          value: true,
          message: 'Campo obligatorio.'
        },
        maxlength: {
          active: true,
          value: 255,
          message: 'Máximo 255 caracteres.'
        },
        minlength: {
          active: true,
          value: 3,
          message: 'Mínimo 3 caracteres.'
        }
      }
    }
  };

  private _noSendInvitationsDialogConfig = {
    header: 'Ninguna persona de las indicadas tiene todos los campos validados.',
    message: 'No se enviará ninguna invitación.',
    accept: {
      action: null,
      button: {
        text: 'Aceptar',
        classes: {
          main: 'btn-secondary',
          extra: 'btn-regular'
        }
      }
    },
    reject: {
      show: false
    }
  };

  private _surnameInputValidatorConfig = {
    field: {
      placeholder: 'Apellidos',
      capitalize: true,
      css: {
        empty: true
      },
      validations: {
        required: {
          active: true,
          value: true,
          message: 'Campo obligatorio.'
        },
        maxlength: {
          active: true,
          value: 255,
          message: 'Máximo 255 caracteres.'
        },
        minlength: {
          active: true,
          value: 3,
          message: 'Mínimo 3 caracteres.'
        }
      }
    }
  };

  /************************ STATIC METHODS  ************************/

  /**
   * Check if mate has complete all fields
   * @param {Object} person
   */
  private static isValidPerson(person: object): boolean {
    return person['validate'];
  }

  /**
   * On invalid email setter
   * @param {Object | any} event
   * @param {Object} person
   */
  private static onInvalidEmail(event: object | any,  person: object): void {
    if (event.hasOwnProperty('valid')
      && !event['valid']) {
      person['email'] = null;
    }
  }

  /**
   * On invalid name setter
   * @param {Object | any} event
   * @param {Object} person
   */
  private static onInvalidName(event: object | any,  person: object): void {
    if (event.hasOwnProperty('valid')
      && !event['valid']) {
      person['name'] = null;
    }
  }

  /**
   * On invalid surname setter
   * @param {Object | any} event
   * @param {Object} person
   */
  private static onInvalidSurname(event: object | any,  person: object): void {
    if (event.hasOwnProperty('valid')
      && !event['valid']) {
      person['surname'] = null;
    }
  }

  /**
   * Check if mate has complete all fields
   * @param {Object} person
   */
  private static setValidPerson(person: object): void {
    person['validate'] = !_.isNil(person['name'])
      && !_.isNil(person['surname'])
      && !_.isNil(person['email'])
      && !_.isNil(person['role']);
  }

  /************************ NATIVE METHODS  ************************/

  constructor(private _authSrv: AuthSrvService,
              private _http: HttpSrvService,
              private _router: Router) {
    super();
    this._authSrv.verifyViewPermission('admin/user');
    this.config.id = 'tab_add_user_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnInit() {
    if (this.isAllowed()) {
      this.getAllAvailableSystemRoles();
      this._persons.push({...this._defaultPerson});
    }

  }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('admin/user', tag);
  }

  /************************ SETUP METHODS ************************/

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getConfirmDialogSetup(): object {
    return this._confirmDialogConfig;
  }

  /**
   * Get email input validator widgetConfig
   * @returns {Object}
   */
  public getEmailInputValidatorConfig(): object {
    return this._emailInputValidatorConfig;
  }

  /**
   * Get name input validator widgetConfig
   * @returns {Object}
   */
  public getNameInputValidatorConfig(): object {
    return this._nameInputValidatorConfig;
  }

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getNoSendInvitationsDialogSetup(): object {
    return this._noSendInvitationsDialogConfig;
  }

  /**
   * Get surname input validator widgetConfig
   * @returns {Object}
   */
  public getSurnameInputValidatorConfig(): object {
    return this._surnameInputValidatorConfig;
  }

  /************************ EVENTS METHODS ************************/

  /**
   * Check if must show new add user row
   */
  public checkAddUser(): void {
    let result = true;
    this._persons.forEach( person => {
      if (result && !person['validate']) {
        result = false;
      }
    });

    if (result === true) {
      this._persons.push({...this._defaultPerson});
    }
  }

  /**
   * Delete user
   * @param {Object} person
   */
  public deleteUser(person: object): void {
    this._persons.splice(this._persons.indexOf(person), 1);
    if (this._persons.length === 0) {
      this._persons.push({...this._defaultPerson});
    }
  }

  /**
   * Get mate array
   * @returns {Array<Object | any>}
   */
  public getPersons(): Array<object | any> {
    return this._persons;
  }

  /**
   * Return to admni gestor mate view
   */
  public goBackTo(): void {
    this._router.navigateByUrl('/admin');
  }

  /**
   * On valid email setter
   * @param {Object | any} event
   * @param {Object} person
   */
  public onValidEmail(event: object | any,  person: object): void {
    if (event.hasOwnProperty('valid') && event['valid']
      && event.hasOwnProperty('value') && event['value']) {
      person['email'] = event['value'];
      TabAdminGestorAddUserComponent.setValidPerson(person);
      this.checkAddUser();
    }
  }

  /**
   * On valid name setter
   * @param {Object | any} event
   * @param {Object} person
   */
  public onValidName(event: object | any,  person: object): void {
    if (event.hasOwnProperty('valid') && event['valid']
      && event.hasOwnProperty('value') && event['value']) {
      person['name'] = event['value'];
      TabAdminGestorAddUserComponent.setValidPerson(person);
      this.checkAddUser();
    }
  }

  /**
   * On valid surname setter
   * @param {Object | any} event
   * @param {Object} person
   */
  public onValidSurname(event: object | any,  person: object): void {
    if (event.hasOwnProperty('valid') && event['valid']
      && event.hasOwnProperty('value') && event['value']) {
      person['surname'] = event['value'];
      TabAdminGestorAddUserComponent.setValidPerson(person);
      this.checkAddUser();
    }
  }

  /**
   * Show confirm dialog for send invitations to selected persons
   */
  public showConfirmDialog(): void {
    if (this.checkPersons()) {
      this.confirmDialogComponent.showDialog(event);
    } else if (!this.checkPersons()) {
      this.noSendInvitationsDialogComponent.showDialog(event);
    }
  }

  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']]();
    }
  }

  /************************ API METHODS  ************************/

  /**
   * Get all avalible system roles
   */
  private getAllAvailableSystemRoles(): void {

    this._http.get(
      'domains/self/roleSystems/'
    ).subscribe(
      (success) => this.onSuccessGetAllAvailableSystemRoles(success),
      (fail) => this.onFailApiResponse(fail)
    );
  }

  /**
   * On success get all avalible system roles
   * @param {Object | any} response
   */
  private onSuccessGetAllAvailableSystemRoles(response: object | any): void {
    if (response['success']
      && response['object']) {
      this.systemRoles = [];
      response['object'].forEach(role => {
        if (role['tag'] !== ROLESYSTEMS.ROLE_SUPER_ADMIN && !this._authSrv.hasRoleSystemsList([role['tag']])) {
          this.systemRoles.push({label: role['name'], value: role['tag']});
        }
      });
    }
  }

  /**
   * Send invitations
   */
  private sendInvitations(): void {
    const sendPersons = [];
    this._persons.forEach( person => {
      if (person['validate'] === true) {
        const personToSend = {
          name: person['name'],
          surname: person['surname'],
          email: person['email'],
          role_systems: person['role']
        };

        sendPersons.push(personToSend);
      }
    });

    if (!_.isNil(sendPersons) && !_.isEmpty(sendPersons)) {
      this._http.post(
        'persons/batch/',
        {
          new_persons: sendPersons
        }
      ).subscribe(
        (success) => this.onSuccessSendInvitations(success),
        (fail) => this.onFailApiResponse(fail)
      );
    } else {

    }
  }

  /**
   * On success send invitations
   * @param {Object} response
   */
  private onSuccessSendInvitations(response: object): void {
    if (response['success']) {
      this.goBackTo();
    } else {
      this.onFailApiResponse(response);
    }
  }

  /**
   * On fail api response
   * @param response
   */
  private onFailApiResponse(response): void {

  }

  /************************ VALIDATION METHODS  ************************/

  private checkPersons(): boolean {
    let validate = false;
    for (let i = 0; i < this._persons.length; i++) {
      if ( this._persons[i]['validate'] === true) {
        validate = true;
        break;
      }
    }
    return validate;
  }
}
