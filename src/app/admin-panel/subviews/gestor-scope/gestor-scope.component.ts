'use strict';

import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  ViewChild,
  ViewChildren,
  QueryList
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';

import {ValidationObjectPipe} from '../../../shared/pipes/validation.pipe';

import {TeamManagedCellComponent} from '../../components/team-managed/team-managed-cell.component';
import {TeamUnmanagedCellComponent} from '../../components/team-unmanaged/team-unmanaged-cell.component';

import {AuthSrvService} from '../../../shared/services/auth.service';
import {HttpSrvService} from '../../../shared/services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-subview-gestor-scope',
  templateUrl: './gestor-scope.component.html',
  styleUrls: ['./gestor-scope.component.css'],
  providers: [HttpSrvService, ValidationObjectPipe],
})
export class TabAdminGestorScopeComponent implements OnInit, OnChanges {

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('confirmDialogComponent') public confirmDialogComponent;
  @ViewChildren('teamsComponent') public teamsComponent: QueryList<TeamManagedCellComponent>;
  @ViewChildren('unmamagedComponent') public unmamagedComponent: QueryList<TeamUnmanagedCellComponent>;

  /************************ COMPONENT VARS  ************************/

  public getConfirmDialogSetup = TabAdminGestorScopeComponent.getConfirmDialogSetup;

  /************************ COMPONENT DATA  ************************/
  private _user = null;
  private _teams = {
    data: [],
    additionalData: [],
    total_count: 0,
    page: 1
  };

  /************************ CONFIG VARS  ************************/

  public teamsWizard = false;
  public loading = true;

  public dropdownOptions: SelectItem[] = [
    {label: 'Todos', value: 0},
    {label: 'Nivel 1', value: 1},
    {label: 'Sin finalizar', value: 2}
  ];
  public selectedDropdownOption: number;

  public searchTeamOptions = {
    name: '',
    location: '',
    filter: 0
  };

  private _unmanagedTableEnable = false;
  private _hasManagedChanges = false;
  private _searchTeamOptionsDefault = {
    name: '',
    location: '',
    filter: 0
  };

  private _unmanagedTeamList = {
    data: [],
    total_count: 0,
    minimum: true,
    timer: null
  };

  private _minSearchUnmanaged = 5;

  private _timeoutKeyup = {
    first: 500,
    last: 1000
  };

  public allTeamsMode = {
    value: false,
    label: 'Todos los equipos'
  };


  // *************** STATICS METHODS ********************************** //

  public static getConfirmDialogSetup(): object {
    return {
      header: 'El equipo va a quedar sin gestor',
      message: 'Continuar?',
      accept: {
        action: 'doSomethingWithGestorTeamFromApi',
        button: {
          text: 'Si'
        }
      }
    };
  }

  constructor(private _http: HttpSrvService,
              private _authSrv: AuthSrvService,
              private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _validator: ValidationObjectPipe) {

    this._authSrv.verifyViewPermission('admin/scope');

    if (this.isAllowed()) {
      this.getTeamsGestorFromApi();

      if (!_.isNil(localStorage.getItem('gestor_email'))) {
        this._user = localStorage.getItem('gestor_email');
      }
    }

  }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges): void { }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('admin/scope', tag);
  }


  // **************** SETUP METHODS ******************** //

  /**
   * Reset an input text of search combo
   * @param event
   * @param {string} input
   */
  public resetSearchInput(event, input: string): void {
    event.stopPropagation();
    event.preventDefault();

    clearTimeout(this._unmanagedTeamList.timer);
    this.searchTeamOptions[input] = '';
    this.onSearchUnmanagedTeams();
  }

  /**
   * Reset all options of search header
   */
  private resetSearchOptions(): void {
    this.searchTeamOptions = _.cloneDeep(this._searchTeamOptionsDefault);
  }




  // **************** MODEL METHODS **************** //
  /**
   * reset the unmanaged team list
   */
  private resetUnmanagedTeamList(): void {
    this._unmanagedTeamList = {
      data: [],
      total_count: 0,
      minimum: true,
      timer: null
    };
  }

  /**
   * reset the managed team list
   */
  private resetManagedTeamList(): void {
    this._teams = {
      data: [],
      additionalData: [],
      total_count: 0,
      page: 1
    };
  }


  public getGestorId(): number {
    return this._activatedRoute.params['value'].hasOwnProperty('id')
      ? Number(this._activatedRoute.params['value']['id'])
      : -1;
  }

  /**
   * Copy domain data in class attributes. Make a backup in local memory
   * @param {Object | any} teamsData
   */
  private updateTeamsGestor(teamsData: object | any = null): void {
    if (teamsData.hasOwnProperty('paginate_data') && teamsData['paginate_data']
      && teamsData['paginate_data'].hasOwnProperty('items') && teamsData['paginate_data']['items']) {
      this._teams = {
        data: _.cloneDeep(teamsData['paginate_data']['items']),
        total_count: teamsData['paginate_data']['total_count'],
        page: 1,
        additionalData: teamsData.hasOwnProperty('addicional_data')
        && teamsData['addicional_data']
        && teamsData['addicional_data'].hasOwnProperty('teams_ids_cascade')
        && teamsData['addicional_data']['teams_ids_cascade']
          ? _.cloneDeep(teamsData['addicional_data']['teams_ids_cascade'])
          : []
      };

    } else {
      this.resetManagedTeamList();
    }


    if (_.isEmpty(this._teams.data) && !this.teamsWizard) {
      this.teamsWizard = true;
      this.onSearchUnmanagedTeams();
    }
  }

  public getUserEmail(): string {
    return !_.isNil(this._user) && !_.isEmpty(this._user)
      ? this._user
      : '';
  }


  private deleteTeamGestor(teamId: number): void {
    _.remove(this._teams.data, (team) => {
      return Number(team['id']) === teamId;
    });
  }


  // **************** DOM METHODS **************** //

  /**
   * Return to main section
   */
  public goBackTo(): void {
    localStorage.removeItem('gestor_email');
    this._router.navigateByUrl('/admin');
  }

  /**
   * Return if has team to show in searchTable
   * @returns {boolean}
   */
  public hasUnmanagedTeamsToShow(): boolean {
    return this._unmanagedTableEnable;
  }

  public toggleSearchCover(): void {
    this._unmanagedTableEnable = false;
    clearTimeout(this._unmanagedTeamList.timer);
    this.resetSearchOptions();
    this.allTeamsMode.value = false;
    this.resetUnmanagedTeamList();

    if (this._hasManagedChanges) {
      this.getTeamsGestorFromApi();
    }
  }

  /**
   * Return de team searched list
   * @returns {Array<Object>}
   */
  public getUnmanagedTeams(): Array<object> {
    return this._unmanagedTeamList.data;
  }

  public hasTeamUnmanaged(): boolean {
    return this._unmanagedTeamList.data.length > 0;
  }

  /**
   * Return the total count of team unmanaged by user
   * @returns {number}
   */
  public getTeamUnmanagedTotalCount(): number {
    return this._unmanagedTeamList.total_count;
  }

  public showScrollPaginator(): boolean {
    return this._unmanagedTeamList.data.length !== this._unmanagedTeamList.total_count
      &&  this._unmanagedTeamList.data.length >= this._minSearchUnmanaged
      && this._unmanagedTeamList.minimum;
  }

  /**
   * Get team
   * @returns {Array<Object>}
   */
  public getManagedTeams(): Array<object> {
    return !this.allTeamsMode.value
      ? this._teams.data
      : this._teams.data.filter((team) => {
          return this.isTeamManagedCascade(team);
        });
  }

  /**
   * Return if has team managed by user
   * @returns {boolean}
   */
  public hasTeamsManaged(): boolean {
    return this.getManagedTeams().length > 0;
  }


  /**
   * Return the total count of team managed by user
   * @returns {number}
   */
  public getTeamManagedTotalCount(): number {
    return this._teams.total_count;
  }

  /**
   * Return if a team is visible in the view
   * @param {Object} team
   * @returns {boolean}
   */
  public isAvailableTeam(team: object): boolean {
    return this.allTeamsMode.value && team['level'] === 1;
  }

  /**
   * Determine if a team is managed cascade or not
   * @param {Object} team
   * @returns {boolean}
   */
  public isTeamManagedCascade(team: object): boolean {
    return !_.isNil(team) && team.hasOwnProperty('id') && team['id']
      ? this._teams.additionalData.indexOf(team['id']) !== -1
      : false;
  }

  /**
   * Get the childView associated by id
   * @param {number} id
   * @returns {Object}
   */
  public findTeamBoxManaged(id: number): object {
    return this.teamsComponent.find((teamBox) => {
      return teamBox.getTeamId() === id;
    });
  }
  // **************** EMITTERS / EVENTS METHODS ****************//


  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNil(event['callBack'])) {
      this[event['callBack']](event['params']['team'], event['params']['event']);
    }
  }


  /**
   * Common event to child components in view
   * @param {Object | any} event
   * @param {boolean} showDialog
   */
  public onChildComponentEvent(event: object | any, showDialog: boolean = false): void {
    if (showDialog) {
      this.confirmDialogComponent.showDialog(event);
    } else {
      this.doSomethingWithGestorTeamFromApi(event['team'], event['event']);
    }
  }

  public onChangeAllTeams(event: boolean): void {
    // console.log(event);
    this.allTeamsMode.value = event;

    if (this.hasUnmanagedTeamsToShow()) {
      this.searchTeamsUnmanagedFromApi();
    }
  }

  /**
   * Show and filter the unmanaged team list.
   */
  public filterUnmanagedTeams(): void {
    if (!_.isEmpty(this.searchTeamOptions.name)
    || !_.isEmpty(this.searchTeamOptions.location)
    || this.searchTeamOptions.filter !== 0
    || this.getTeamUnmanagedTotalCount() === 0) {

      clearTimeout(this._unmanagedTeamList.timer);

      this._unmanagedTeamList.timer = setTimeout(() => {
        this.onSearchUnmanagedTeams();
      }, _.isEmpty(this._unmanagedTeamList.data)
        ? this._timeoutKeyup.first
        : this._timeoutKeyup.last);
    }
  }

  /**
   * Show or hide the close input button
   * @param {string} input
   * @returns {boolean}
   */
  public showCloseOptionSelected(input: string): boolean {
    return !_.isEmpty(this.searchTeamOptions[input]);
  }

  /**
   * Dropdown filter event
   * @param {Object | any} event
   */
  public onClickDropdownTeam(event: object | any): void {
    this.searchTeamOptions.filter = event;
    this.onSearchUnmanagedTeams();
  }


  private onSearchUnmanagedTeams(): void {
    this._unmanagedTableEnable = true;
    this.searchTeamsUnmanagedFromApi();
  }


  public onClickScrollInfiniteSearchTable(): void {
    clearTimeout(this._unmanagedTeamList.timer);
    this._unmanagedTeamList.minimum = false;
    this.onSearchUnmanagedTeams();
  }

  // **************** API  METHODS **************** //

  /**
   * Get managed team by this user
   */
  private getTeamsGestorFromApi(): void {
    this.loading = true;
    this._hasManagedChanges = false;

    this._http
      .get(
        'persons/' + this.getGestorId() + '/teams/managedBy/'
      )
      .subscribe(
      success => this.onGetTeamsGestorFromApi(success),
        () => {this.loading = false; }
    );
  }

  /**
   * On success get all managed team by this user
   * @param {Object} response
   */
  private onGetTeamsGestorFromApi(response: object): void {
    this.loading = false;

    if (this._validator.isValidObjectProperty(response, 'success')
      && this._validator.isValidResponse(response)) {
      this.updateTeamsGestor(response['object']);
    }
  }


  /**
   * Call to search unmanaged team
   */
  private searchTeamsUnmanagedFromApi(): void {
    this.loading = true;

    const queryString = {
      managed_by: 'null',
      page: 1,
      elements: this._unmanagedTeamList.minimum
        ? this._minSearchUnmanaged
        : this.getTeamUnmanagedTotalCount() > 0
          ? this.getTeamUnmanagedTotalCount()
          : this._minSearchUnmanaged
    };

    if (!_.isEmpty(this.searchTeamOptions.name)) {
      queryString['name'] = '%' + this.searchTeamOptions.name + '%';
    }

    if (!_.isEmpty(this.searchTeamOptions.location)) {
      queryString['location'] = '%' + this.searchTeamOptions.location + '%';
    }

    if (this.searchTeamOptions.filter === 1 || this.allTeamsMode.value === true) {
      queryString['level'] = 1;
    }

    if (this.searchTeamOptions.filter === 2) {
      queryString['status'] = 1;
    }

    this._http
      .get(
      'teams/',
        queryString
      )
      .subscribe(
      success => this.onSuccessSearchTeamsUnmanagedFromApi(success),
      fail => this.onFailSearchTeamsUnmanagedFromApi(fail)
    );
  }

  /**
   * On success to call
   * @param response
   */
  private onSuccessSearchTeamsUnmanagedFromApi(response): void {
    this.loading = false;

    // @TODO: cuando endpoint de equipos sin gestor -> eliminar filtro en este método.
    if (this._validator.isSuccessResponse(response) && this._validator.isValidResponseItemList(response)) {
      this._unmanagedTeamList.total_count = response['object']['total_count'];
      this._unmanagedTeamList.data = _.cloneDeep(response['object']['items'].filter((team) => {
        return !team.hasOwnProperty('managed_by') || _.isNil(team['managed_by']);
      }));

    } else {
      this.resetUnmanagedTeamList();
    }
  }

  /**
   * Fail on success to search unmanaged team
   * @param response
   */
  private onFailSearchTeamsUnmanagedFromApi(response): void {
    this.loading = false;
    this.resetUnmanagedTeamList();
  }

  /**
   * API call to add or delete gestor in a team
   * @param {Object} team
   * @param {string} action
   */
  private doSomethingWithGestorTeamFromApi(team: object,
                                           action: string): void {
    if (!_.isNil(team['id']) && team['id'] > 0) {
      team['managing'] = true;

      action = this.allTeamsMode.value === false || action === 'delete' ? action : 'addCascade';

      this._http
        .patch(
          'teams/' + team['id'] + '/relationships/managedBy/',
        {
            person_id: this.getGestorId(),
            action: action
          }
        )
        .subscribe(
        success => this.onSuccessDoSomethingWithGestorTeamFromApi(success, team, action)
      );
    }
  }

  /**
   * On success to api call
   * @param {Object} response
   * @param {Object} team
   * @param {string} action
   */
  private onSuccessDoSomethingWithGestorTeamFromApi(response: object,
                                                    team: object,
                                                    action: string): void {
    team['managing'] = false;

    if (response['success']) {
      localStorage.setItem('GeneralDomainWidgetContainerComponent', 'gestor');
      team['selected'] = (action === 'add' || (this.allTeamsMode.value && action === 'addCascade'));
      this._hasManagedChanges = true;

      if (action === 'delete' && this.findTeamBoxManaged(team['id']) !== undefined) {
        this.teamsComponent.find((teamBox) => {
          return teamBox.getTeamId() === Number(team['id']);
        }).fadeOut();

        setTimeout(() => {
          this.deleteTeamGestor(team['id']);
          this._teams.total_count--;
        }, 500);
      }

    }
  }
}
