'use strict';

import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {Router} from '@angular/router';
import {NgModel} from '@angular/forms';

import {ValidationObjectPipe} from '../../../shared/pipes/validation.pipe';

import {REGEXPATTERNSHTML} from '../../../shared/const/system.const';
import {ROLESYSTEMS} from '../../../shared/const/rolesystems.const';

import {DomainModel} from '../../../shared/models/domain.model';
import {PersonModel} from '../../../shared/models/person.model';

import {AlertService} from '../../../shared/services/alert.service';
import {AuthSrvService} from '../../../shared/services/auth.service';
import {HttpSrvService} from '../../../shared/services/http.service';


const _ = require('lodash');

@Component({
  selector: 'app-tab-domain-component',
  templateUrl: './tab-domain.component.html',
  styleUrls: ['./tab-domain.component.css'],
  providers: [
    HttpSrvService,
    ValidationObjectPipe
  ]
})

export class TabDomainComponent implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() public externalConfig: object;
  @Input() public visible = true;
  @Input() public css = '';
  @Input() public enableLoadData = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() public outputEvent = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('domainForm') private domainForm;

  /************************ COMPONENT VARS  ************************/

  public apiCalled = false;
  public domain: DomainModel = new DomainModel();
  public postalCodePattern;

  private _temporalDomain;
  private _countryCode = 'ESP';

  /************************ CONFIG VARS  ************************/

  public adminsList = {
    data: [],
    restart: 0
  };

  public config = {
    id: 'tab_domain_component_n'.concat(Math.floor(Math.random() * 100000).toString()),
  };

  private _originalDomain;

  // @TODO: no eliminar metodo sin uso. Se utilizará a futuro
  // private _searchAdminConfig = {
  //   title: 'fullname',
  //   subtitle: 'email',
  //   keyToFilter: 'name',
  //   alternativeKeysToFilter: ['email', 'surname'],
  //   titleOption: 'fulldata',
  //   flags: {
  //     hasWhiteBackground: true,
  //     showResultList: true,
  //     showResultListUnderClick: true,
  //     showResultListUnderUnselect: false
  //   },
  //   error: {
  //     show: false
  //   },
  //   icon: {
  //     hasIconRight: false,
  //     hasSearchIcon: false,
  //     hideIconOnSearch: true,
  //     showUnselectIcon: true
  //   },
  //   input: {
  //     placeholder: 'selecciona un admin...'
  //   },
  //   css: {
  //     extraClasses: 'form-style'
  //   }
  // };

  private _updateFirstTry = false;

  /************************ STATIC METHODS  ************************/

  /**
   * Generate search keys for admins
   * @param {Object} admin
   * @returns {Object}
   */
  private static createAdministeredBySearchKeys (admin: PersonModel | object): object {
    return {
      fullname: _.isNil(admin) ? '' : admin['name'] + ' ' + admin['surname'],
      fulldata: _.isNil(admin) ? '' : admin['name'] + ' ' + admin['surname'] + ' - ' + admin['email']
    };
  }

  /************************ NATIVE METHODS  ************************/

  constructor(private _alertSrv: AlertService,
              private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _router: Router,
              private _validator: ValidationObjectPipe) {
  }

  ngOnInit() {
    _.merge(this.config, this.externalConfig);
    this.postalCodePattern =  REGEXPATTERNSHTML.postalCode[this._countryCode];

    window.scrollTo(0, 0);
    if (this._authSrv.hasDomain() && this.enableLoadData) {
      this.getDomainFromApi();
      this.getAdminsFromApi();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    _.merge(this.config, this.externalConfig);
  }

  /************************ SETUP METHODS ************************/

  /**
   * Restart admin list
   * @returns {number}
   */
  // public restartAdminsList(): number {
  //   return this.adminsList.restart++;
  // }

  // @TODO: no eliminar metodo sin uso. Se utilizará a futuro
  // public getSearchAdminConfig(): object {
  //   return this._searchAdminConfig;
  // }

  // @TODO: no eliminar metodo sin uso. Se utilizará a futuro
  // public getAdminsList(): Array<any> {
  //   return this.adminsList.data;
  // }

  // @TODO: no eliminar metodo sin uso. Se utilizará a futuro
  // public getAdminsListDefaultValue(): object {
  //   return this.domain.hasAdministeredBy()
  //     ? _.cloneDeep(_.assign(this.domain.administered_by,
  //      TabDomainComponent.createAdministeredBySearchKeys(this.domain.administered_by)))
  //     : null;
  // }

  /************************ EVENTS METHODS ************************/

  /**
   * Get error input
   * @param {NgModel} input
   * @returns {string}
   */
  public getErrorInput(input: NgModel): string {
    if (this.hasErrorInput(input)) {
      if (input.getError('required')) {
        return 'Requerido';
      }
      if (input.getError('minlength')) {
        return 'Mínimo ' + input.errors.minlength.requiredLength + ' caracteres';
      }
      if (input.getError('maxlength')) {
        return 'Máximo ' + input.errors.maxlength.requiredLength + ' caracteres';
      }
      if (input.getError('pattern')) {
        return 'Formato inválido';
      }
    }
    return '';
  }

  /**
   * Get domain location name and address
   * @returns {string}
   */
  public getDomainNameAndAddress(): string {
    return this.domain.getMainLocationNameAndAddress();
  }

  /**
   * Get domain location postal code
   * @returns {string}
   */
  public getDomainPostalCode(): string {
    return this.domain.main_location.postal_code;
  }

  /**
   * Get domain province and country
   * @returns {string}
   */
  public getDomainProvinceCountry(): string {
    return this.domain.main_location.province + ', ' + this.domain.main_location.country;
  }

  /**
   * Get url domain
   * @returns {string}
   */
  public getUrlDomain(): string {
    return this.domain.url_domain;
  }

  /**
   * Go to edit domain location
   */
  public goToEditDomainLocation(): void {
    localStorage.setItem('EditLocationComponent', 'isDomainLocation');

    this._router.navigateByUrl('/locations/edit/'.concat(this.domain.hasMainLocation()
        ? this.domain.main_location.id.toString()
        : ''
      )
    );
  }

  // @TODO: no eliminar metodo sin uso. Se utilizará a futuro
  // /**
  //  * Output event for admin search.
  //  * @param {Object | any} event
  //  */
  // public onClickSearchAdmin(event: object | any): void {
  //   if (event['event'] === 'blur' && !event['itHasACorrectValue']) {
  //     if (_.isEmpty(event['currentText'])) {
  //       this.domain.administered_by = null;
  //     }
  //     this.restartAdminsList();
  //
  //
  //   } else if (event['event'] === 'unselect') {
  //     this.domain.administered_by = null;
  //     this.restartAdminsList();
  //   }
  // }

  // @TODO: no eliminar metodo sin uso. Se utilizará a futuro
  // public onSelectedAdmin(event: object | any): void {
  //   this.domain.administered_by = new PersonModel(event['element']);
  // }

  /**
   * Show message to form error
   * @returns {boolean}
   */
  public showErrorFromMessage(): boolean {
    return this._updateFirstTry && this.hasErrorForm();
  }

  /**
   * Update domain
   */
  public updateDomain() {
    this._updateFirstTry = true;

    if (!this.hasErrorForm()) {
      if (this.hasChangesInDomainForm()) {
        this.onUpdateDomainName();
      } else {
        this._alertSrv.info('No hay cambios que guardar');
      }
    } else {
      this.checkFormFields();
    }
  }

  /************************ MODEL METHODS ************************/

  /**
   * Save domain data in local memory
   */
  private backupDomainModel(): void {
    this._originalDomain = _.cloneDeep(this.domain);
  }

  /**
   * Restore original domain data
   */
  private restoreDomainModel(): void {
    this.domain = _.cloneDeep(this._originalDomain);
  }

  /**
   * Copy domain data in class attributes. Make a backup in local memory
   * @param {Object} domain
   */
  private updateDomainModel(domain?: object): void {

    if (!_.isNil(domain)) {
      this.domain = new DomainModel(domain);
      this._temporalDomain = null;
    }

    this.backupDomainModel();
  }

  /************************ API METHODS  ************************/

  /**
   * Get domain from api
   */
  private getDomainFromApi(): void {
    this._httpSrv.get(
      'domains/' + this._authSrv.getDomainId(),
      undefined,
      'Dominio'
    ).subscribe(
      success => this.onSuccessGetDomain(success),
      fail => this.onFailApiResponse(fail)
    );
  }

  /**
   * On success get domain from api
   * @param {Object} response
   */
  private onSuccessGetDomain(response: object): void {
    if (this._validator.isSuccessResponse(response)
      && this._validator.isValidResponse(response) && !_.isNil(response['object'])) {
      this.updateDomainModel(response['object']);
    } else {
      this.onFailApiResponse(response);
    }
  }

  /**
   * Return a list of admins cancidates to manage domain
   */
  private getAdminsFromApi(): void {
    this._httpSrv.get(
      'persons/rolesystemtype/' + ROLESYSTEMS.ROLE_ADMIN,
      {
        page: 1,
        elements: 100
      },
      'Listado admins'
    ).subscribe(
      success => this.onSuccessGetAdminsFromApi(success),
      fail => this.onFailApiResponse(fail)
    );
  }

  /**
   * On success get admins from api
   * @param response
   */
  private onSuccessGetAdminsFromApi(response): void {
    if (this._validator.isValidValueObjectProperty(response, 'success')
      && this._validator.isValidValueObjectProperty(response, 'object')
      && response['object'].hasOwnProperty('items')) {
      this.adminsList.data = _.cloneDeep(
        response['object']['items'].map((item) => {
          return _.assign(
            item,
            TabDomainComponent.createAdministeredBySearchKeys(item));
          }
        )
      );
    } else {
      this.onFailApiResponse(response);
    }
  }

  /**
   * On update domain name
   */
  private onUpdateDomainName() {
    if (this.hasChangesDomainName()) {
      this._httpSrv.patch(
        'domains/' + this.domain.id,
        this.domain,
        'Dominio'
      ).subscribe(
        success => this.onSuccessUpdateDomain(success), // this.onSuccessUpdateDomain(success, 'onUpdateDomainAdministeredBy'),
        fail => this.onFailApiResponse(fail, true)
      );
    } else {
      // this.onUpdateDomainAdministeredBy();
    }
  }


  /**
   * On update domain administered by
   */
  // @TODO: no eliminar metodo sin uso. Se utilizará a futuro
  // private onUpdateDomainAdministeredBy() {
  //   if (this.hasChangeInAdministeredDomain()) {
  //     const endOptions = !this.domain.hasAdministeredBy()
  //       ? {person_id: this._originalDomain.administered_by.id, action: 'delete'}
  //       : {person_id: this.domain.administered_by.id, action: 'add'};
  //
  //     this._httpSrv.patch(
  //       'domains/' + this.domain.id + '/relationships/administeredBy/',
  //       endOptions,
  //       'Dominio - superadmin'
  //     ).subscribe(
  //       success => this.onSuccessUpdateDomain(success),
  //       fail => this.onFailApiResponse(fail, true)
  //     );
  //
  //   } else {
  //     this.onSuccessUpdateDomain();
  //   }
  // }

  /**
   * On succes update domain
   * @param response
   * @param {string} callBack
   */
  private onSuccessUpdateDomain(response?, callBack?: string): void {
    if (!_.isNil(response)) {
      if (this._validator.isSuccessResponse(response)) {
        this._temporalDomain = response['object'];
        this._authSrv.updateSession();
      } else {
        this.onFailApiResponse(response, true);
      }
    }

    if (!_.isNil(callBack)) {
      this[callBack]();
    } else {
      this.updateDomainModel(this._temporalDomain);
    }
  }

  /**
   * On fail api response
   * @param {Object | any} response
   * @param {boolean} reset
   */
  private onFailApiResponse(response: object | any,
                            reset = false): void {
    if (reset) {
      this.restoreDomainModel();
    }
  }

  /************************ VALIDATION METHODS ************************/

  /**
   * Touch all field for launch validation process
   */
  private checkFormFields(): void {
    _.forIn(this.domainForm.control.controls, (input) => {
      if (input.invalid === true) {
        input.markAsTouched();
      }
    });
  }

  /**
   * Return if domain name has been changed
   * @returns {boolean}
   */
  private hasChangesDomainName(): boolean {
    return !this._validator.equal(this._originalDomain.name, this.domainForm.value['name']);
  }

  /**
   * Detect change in administered_by field
   * @returns {boolean}
   */
  private hasChangeInAdministeredDomain(): boolean {
    const administeredBy = this.domain.hasAdministeredBy();
    const originalAdministeredBy = this._originalDomain.hasAdministeredBy();

    return (originalAdministeredBy && !administeredBy) || (!originalAdministeredBy && administeredBy)
      || administeredBy && originalAdministeredBy && this.domain.administered_by.id !== this._originalDomain.administered_by.id;
  }

  /**
   * Return if a form has any change
   * @returns {boolean}
   */
  private hasChangesInDomainForm(): boolean {
    return this.hasChangesLocationInDomainForm() || this.hasChangesDomainName() || this.hasChangeInAdministeredDomain();
  }

  /**
   * Detect if location has changes
   * @returns {boolean}
   */
  private hasChangesLocationInDomainForm(): boolean {
    for (const key in this.domainForm.value) {
      if (this.domainForm.value.hasOwnProperty(key) && key !== 'name'
        && this._originalDomain.main_location[key] !== this.domainForm.value[key]) {
        return true;
      }
    }
    return false;
  }

  /**
   * Check if form valid
   * @returns {boolean}
   */
  private hasErrorForm(): boolean {
    return this.domainForm.invalid;
  }

  /**
   * Return if an input has errors
   * @param input
   * @returns {boolean}
   */
  public hasErrorInput(input: any): boolean {
    return this._updateFirstTry && !_.isNull(input.errors) && (input.dirty || input.touched);
  }
}
