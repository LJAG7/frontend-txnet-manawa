import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {BaseComponentModel} from '../../../../../shared/models/base-component/base-component.class';
import {MasterClassModel} from '../../../../../shared/models/master/master-class.model';

import {AuthSrvService} from '../../../../../shared/services/auth.service';
import {HttpSrvService} from '../../../../../shared/services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-add-master-class-component',
  templateUrl: './add-master-class.component.html',
  styleUrls: ['./add-master-class.component.css']
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class AddMasterClassComponent extends BaseComponentModel implements OnInit, OnChanges, AfterViewInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('confirmDialogComponent') public confirmDialogComponent;
  @ViewChild('noCreateMasterClassDialogComponent') public noCreateMasterClassDialogComponent;

  /************************ COMPONENT VARS  ************************/

  public masterClass: Array<MasterClassModel> = [];

  private _validMasterClass: Array<boolean> = [];

  /************************ CONFIG VARS ************************/

  private _confirmDialogConfig = {
    header: 'Las clases de master que no tengan el nombre informado y validado no serán creadas.',
    message: '¿Desea crear el resto de clases de master?',
    accept: {
      action: 'createMasterClass',
      button: {
        text: 'Si'
      }
    }
  };

  private _noCreateMasterClassDialogConfig = {
    header: 'Ninguna clase de master de las indicadas tiene el campo nombre validado.',
    message: 'No se creará ninguna clase de master.',
    accept: {
      action: null,
      button: {
        text: 'Aceptar',
        classes: {
          main: 'btn-secondary',
          extra: 'btn-regular'
        }
      }
    },
    reject: {
      show: false
    }
  };

  private _nameInputValidatorConfig = {
    field: {
      placeholder: 'Nombre',
      css: {
        empty: true
      },
      validations: {
        required: {
          active: true,
          value: true,
          message: 'Campo obligatorio.'
        },
        maxlength: {
          active: true,
          value: 255,
          message: 'Máximo 255 caracteres.'
        },
        minlength: {
          active: true,
          value: 3,
          message: 'Mínimo 3 caracteres.'
        }
      },
      validationsApi: {
        url: 'teamtgroups/InputValidator/name',
        message: null
      }
    }
  };

  /************************ STATIC METHODS ************************/


  /************************ NATIVE METHODS ************************/

  constructor(private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _router: Router) {
    super();
    this._authSrv.verifyViewPermission('admin/master-class');
  }

  ngOnInit() {
    this.config.id = 'add_master_class_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    if (this.isAllowed()) {
      this.masterClass.push(new MasterClassModel());
      this._validMasterClass.push(true);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngAfterViewInit(): void {
  }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('admin/master-class', tag);
  }

  /************************ EVENT METHODS ************************/

  /**
   * Delete master class
   * @param {number} index
   */
  public deleteMasterClass(index: number): void {
    this.masterClass.splice(index, 1);
    this._validMasterClass.splice(index, 1);
  }

  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']]();
    }
  }

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getConfirmDialogSetup(): object {
    return this._confirmDialogConfig;
  }

  /**
   * Get name input validator
   * @returns {Object}
   */
  public getNameInputValidatorConfig(): object {
    return this._nameInputValidatorConfig;
  }

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getNoCreateMasterClassDialogSetup(): object {
    return this._noCreateMasterClassDialogConfig;
  }

  /**
   * Return to admin view
   */
  public goBackTo(): void {
    this._router.navigateByUrl('/admin');
  }

  /**
   * Check if master class is valid
   * @param {number} index
   * @returns {boolean}
   */
  public isValidMasterClass(index: number): boolean {
    return !_.isEmpty(this.masterClass[index].name);
  }

  /**
   * On invalid name setter
   * @param {Object | any} event
   * @param {number} index
   */
  public onInvalidMasterClass(event: object | any, index: number): void {
    this.masterClass[index].name = event['value'];
    this._validMasterClass[index] = false;
  }

  /**
   * On valid name setter
   * @param {Object | any} event
   * @param {number} index
   */
  public onValidMasterClass(event: object | any, index: number): void {
    this.masterClass[index].name = event['value'];
    this._validMasterClass[index] = true;
    this.checkAddMasterClass();
  }

  /**
   * Show confirm dialog for create master class
   */
  public showConfirmDialog(): void {
    if (this.checkMasterClass()) {
      this.confirmDialogComponent.showDialog(event);
    } else {
      this.noCreateMasterClassDialogComponent.showDialog(event);
    }
  }

  /************************ API METHODS ************************/

  /**
   * Create master class
   */
  private createMasterClass(): void {
    const createMasterClass = [];

    for (let i = 0; i < this._validMasterClass.length; i++) {
      const valid = this._validMasterClass[i];
      if (valid) {
        createMasterClass.push(this.masterClass[i]);
      }
    }

    if (!_.isNil(createMasterClass) && !_.isEmpty(createMasterClass)) {
      createMasterClass.forEach( masterClass => {
        this._httpSrv.post(
          'teamtgroups/',
          {
            name: masterClass.name
          }
        ).subscribe(
          (success) => this.onSuccessCreateMasterClass(success)
        );
      });
    } else {

    }
  }

  /**
   * On success create master class
   * @param {Object} response
   */
  private onSuccessCreateMasterClass(response: object): void {
    if (response['success']) {
      this.goBackTo();
    }
  }

  /************************ AUXILIAR METHODS ************************/

  /**
   * Check if must show new add master class row
   */
  private checkAddMasterClass(): void {
    let result = true;

    for (let i = 0; i < this._validMasterClass.length; i++) {
      if (result && (!this._validMasterClass[i] || _.isEmpty(this.masterClass[i].name))) {
        result = false;
      }
    }

    if (result === true) {
      this.masterClass.push(new MasterClassModel());
      this._validMasterClass.push(false);
    }
  }

  /**
   * Check if all master class are complete
   * @returns {boolean}
   */
  private checkMasterClass(): boolean {
    let validate = false;
    for (let i = 0; i < this._validMasterClass.length; i++) {
      if ( this._validMasterClass[i] === true) {
        validate = true;
        break;
      }
    }
    return validate;
  }
}
