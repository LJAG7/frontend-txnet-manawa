import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {LazyLoadEvent, DataTable} from 'primeng/primeng';

import {BaseComponentModel} from '../../../../shared/models/base-component/base-component.class';
import {MasterClassModel} from '../../../../shared/models/master/master-class.model';
import {HttpSrvService} from '../../../../shared/services/http.service';

const _ = require('lodash');

enum availableStatus {all, used, unused}

@Component({
  selector: 'app-edit-master-class-component',
  templateUrl: './edit-master-class.component.html',
  styleUrls: ['./edit-master-class.component.css']
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class EditMasterClassComponent extends BaseComponentModel implements OnInit, OnChanges, AfterViewInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('masterClassDatatable') public masterClassDatatable: DataTable;
  @ViewChild('confirmDialogComponent') public confirmDialogComponent;

  /************************ COMPONENT VARS  ************************/

  public masterClass: Array<MasterClassModel> = [];
  public filterMasterClassOptions: Array<object> = [
    {label: 'Todo', value: availableStatus.all},
    // {label: 'En uso', value: availableStatus.used},
    {label: 'Sin usar', value: availableStatus.unused}
  ];
  public selectedFilterMasterClassOptions = this.filterMasterClassOptions[0];

  private _originalMasterClass: Array<MasterClassModel> = [];
  private _editableFields: Array<boolean> = [];
  private _validFields: Array<string> = [];
  private _searchMasterClass = null;
  private _deleteMasterClassElement = null;

  /************************ CONFIG VARS ************************/

  public pagination = {
    page: 1,
    rows: 10,
    filters: {},
    sortField: 'name',
    sortOrder: 'asc',
    totalRecords: 0,
  };

  private _confirmDialogConfig = {
    header: 'Se eliminará la clase de master.',
    message: '¿Desea continuar?',
    accept: {
      action: 'deleteMasterClass',
      button: {
        text: 'Si'
      }
    },
    reject: {
      action: 'cancelDeleteMasterClass',
    }
  };

  /************************ NATIVE METHODS ************************/

  constructor(private _httpSrv: HttpSrvService,
              private _router: Router) {
    super();
  }

  ngOnInit() {
    this.config.id = 'edit_master_class_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    const activeDatatableFilter = Number(localStorage.getItem('editMasterClassSelectedFilterDatatable'));
    if (!_.isNil(activeDatatableFilter)) {
      this.selectedFilterMasterClassOptions = this.filterMasterClassOptions[activeDatatableFilter];
    }
    this.loadMasterClassLazy(this.masterClassDatatable.createLazyLoadMetadata());
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngAfterViewInit(): void {
    if (!_.isNil(this.masterClassDatatable)) {
      this.masterClassDatatable.resolveFieldData = function(data, field) {
        if (data && field && field === 'name') {
          return data[field];
        } else {
          return null;
        }
      };
    }
  }

  /************************ EVENT METHODS ************************/

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getConfirmDialogSetup(): object {
    return this._confirmDialogConfig;
  }

  /**
   * On delete master class
   * @param {number} index
   */
  public onDeleteMasterClass(index: number): void {
    if (!_.isNil(index)) {
      this._deleteMasterClassElement = this.masterClass[index];
      this.showConfirmDialog();
    }
  }
  /**
   * Get invalid field message
   * @param {number} index
   */
  public getInvalidFieldMessage(index: number): string {
    return this._validFields[index];
  }

  /**
   * Return if an input value has errors
   * @param name
   * @param {number} index
   */
  public hasErrorInput(name: any, index: number): void {
    if (!_.isNil(name) && !_.isEmpty(name)) {
      clearTimeout(this._searchMasterClass);
      this._searchMasterClass = setTimeout( () => {
        this._httpSrv
          .get('teamtgroups/InputValidator/name?value=' + name)
          .subscribe(
            success => this.checkNameValidatorResponse(success, index)
          );
      }, 500);
    } else {
      this.setInvalidField(index, 'Nombre requerido');
    }
  }

  /**
   * Check if field is editable
   * @param {number} index
   */
  public isEditableField(index: number): boolean {
    return this._editableFields[index];
  }

  /**
   * Check if field is valid
   * @param {number} index
   */
  public isValidField(index: number): boolean {
    return _.isEmpty(this._validFields[index]);
  }

  /**
   * Load master class lazy
   * @param {LazyLoadEvent} event
   */
  public loadMasterClassLazy(event: LazyLoadEvent) {
    this.pagination.page = (event.first / event.rows) + 1;
    this.pagination.sortField = !_.isNil(event.sortField) ? event.sortField : this.pagination.sortField;
    const order = !_.isNil(event.sortOrder) ? event.sortOrder : 1;
    this.pagination.sortOrder = order === 1 ? 'asc' : 'desc';
    this.pagination.filters = event.filters;

    setTimeout(() => {
      if (this.masterClass) {
        this.getAllMasterClass();
      }
    }, 250);
  }

  /**
   * On blur editable field
   * @param {number} index
   */
  public onBlurEditableField(index: number): void {
    if (this.isValidField(index)) {
      this.saveMasterClass(this.masterClass[index]);
    } else {
      this.masterClass[index] = _.cloneDeep(this._originalMasterClass[index]);
      this.setInvalidField(index, '');
      this.masterClassDatatable.reset();
    }
    this._editableFields[index] = !this._editableFields[index];
  }

  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']]();
    }
  }

  /**
   * On click create master class button
   */
  public onClickCreateMasterClass(): void {
    this._router.navigateByUrl('/admin/master-class');
  }

  /**
   * On click export datatable to csv
   */
  public onClickExportToCsv(): void {
    this.getAllMasterClassToExport();
  }

  /**
   * Set active datatable filter
   * @param {number} index
   */
  public setActiveDatatableFilter(index: number): void {
    this.selectedFilterMasterClassOptions = this.filterMasterClassOptions[index];
    localStorage.setItem('editMasterClassSelectedFilterDatatable', index.toString());
    this.getAllMasterClass();
  }

  /**
   * Set editable field
   * @param {number} index
   */
  public setEditableField(index: number): void {
    this._editableFields[index] = !this._editableFields[index];
  }

  /**
   * Show confirm dialog for delete master class
   */
  public showConfirmDialog(): void {
    this.confirmDialogComponent.showDialog(event);
  }

  /************************ API METHODS ************************/

  /**
   * Check name validation
   * @param response
   * @param {number} index
   */
  private checkNameValidatorResponse(response: any, index: number): void {
    if (response.hasOwnProperty('object')
      && response.object.hasOwnProperty('valid')) {

      if (response.object.valid) {
        this._originalMasterClass[index] = _.cloneDeep(this.masterClass[index]);
        this.setInvalidField(index, '');
      } else {
        this.setInvalidField(index, 'El nombre introducido ya existe en el sistema.');
      }
    }
  }

  /**
   * Get all master class
   */
  private getAllMasterClass(): void {
    this._httpSrv.get(
      this.getAllMasterClassEndpoint(),
      {
        page: this.pagination.page,
        elements: this.pagination.rows,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      }
    ).subscribe(
      (success) => this.onSuccessGetAllMasterClass(success)
    );
  }

  /**
   * On success get all master class
   * @param {Object | any} response
   * @param {boolean} exportToCsv
   */
  private onSuccessGetAllMasterClass(response: object | any, exportToCsv: boolean = false): void {
    this.masterClass = [];
    this._originalMasterClass = [];
    this._editableFields = [];
    this._validFields = [];
    if (response['success']) {

      response['object']['items'].forEach(masterClass => {
        this.masterClass.push(new MasterClassModel(masterClass));
        this._originalMasterClass.push(new MasterClassModel(masterClass));
        this._editableFields.push(false);
        this._validFields.push('');
      });
      this.pagination.totalRecords = response.object.total_count;

      if (exportToCsv) {
        setTimeout(() => {
          this.masterClassDatatable.exportCSV();
          this.getAllMasterClass();
        }, 300);
      }
    } else {
      this.pagination.totalRecords = 0;
    }
  }

  /**
   * Get all masters class to export
   */
  private getAllMasterClassToExport(): void {
    this._httpSrv.get(
      this.getAllMasterClassEndpoint(),
      {
        page: 1,
        elements: this.pagination.totalRecords,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      },
      'Listado de clases de master a exportar'
    ).subscribe(
      success => this.onSuccessGetAllMasterClass(success, true)
    );
  }

  /**
   * Get all master class endpoint depending active filters
   */
  private getAllMasterClassEndpoint(): string {
    let endpoint = 'teamtgroups/';

    if (!_.isNil(this.pagination.filters)
      && !_.isEmpty(this.pagination.filters)) {

      if (endpoint.charAt(endpoint.length - 1) !== '?') {
        endpoint += '?';
      }

      const filterKeys = Object.keys(this.pagination.filters);

      for (let i = 0; i < filterKeys.length ; i++) {
        if (endpoint.charAt(endpoint.length - 1) !== '?') {
          endpoint += '&';
        }
        const key = filterKeys[i];
        endpoint += key + '=%' + this.pagination.filters[key]['value'].toLowerCase() + '%';
      }
    }

    if (this.selectedFilterMasterClassOptions['value'] === availableStatus.unused) {
      if (endpoint.charAt(endpoint.length - 1) === '?') {
        endpoint += 'teamts=null';
      } else if (endpoint.charAt(endpoint.length - 1) === '/') {
        endpoint += '?teamts=null';
      } else {
        endpoint += '&teamts=null';
      }
    } else if (this.selectedFilterMasterClassOptions['value'] === availableStatus.used) {
      // @ToDo: Filtrado de clases de master usados
      // if (endpoint.charAt(endpoint.length - 1) === '?') {
      //   endpoint += 'teamts=null';
      // } else if (endpoint.charAt(endpoint.length - 1) === '/') {
      //   endpoint += '?teamts=null';
      // } else {
      //   endpoint += '&teamts=null';
      // }
    }

    return endpoint;
  }

  /**
   * Save master class
   * @param {MasterClassModel} masterClass
   */
  private saveMasterClass(masterClass: MasterClassModel): void {
    if (!_.isNil(masterClass)) {
      this._httpSrv.patch(
        'teamtgroups/' + masterClass.id,
        {
          name: masterClass.name
        }
      ).subscribe(
        (success) => this.onSuccessSaveMasterClass(success)
      );
    }
  }

  /**
   * On success save master class
   * @param {Object | any} response
   * @param {number} index
   */
  private onSuccessSaveMasterClass(response: object | any): void {
    this.masterClassDatatable.reset();
  }

  /**
   * Detele master class
   */
  private deleteMasterClass(): void {
    if (!_.isNil(this._deleteMasterClassElement)) {
      this._httpSrv.delete(
        'teamtgroups/' + this._deleteMasterClassElement.id
      ).subscribe(
        (success) => this.onSuccessDeleteMasterClass(success)
      );
    }
  }

  /**
   * On success delete master class
   * @param {Object | any} response
   */
  private onSuccessDeleteMasterClass(response: object | any): void {
    this._deleteMasterClassElement = null;
    this.masterClassDatatable.reset();
  }

  /************************ AUXILIAR METHODS ************************/

  /**
   * Cancel delete master class
   */
  private cancelDeleteMasterClass(): void {
    this._deleteMasterClassElement = null;
  }

  /**
   * Set invalid field
   * @param {number} index
   * @param {string} msg
   */
  private setInvalidField(index: number, msg: string): void {
    this._validFields[index] = msg;
  }

}
