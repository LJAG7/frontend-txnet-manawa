import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {BaseComponentModel} from '../../../../../shared/models/base-component/base-component.class';
import {RoleCategoryModel} from '../../../../../shared/models/role/role-category.model';

import {AuthSrvService} from '../../../../../shared/services/auth.service';
import {HttpSrvService} from '../../../../../shared/services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-add-role-category-component',
  templateUrl: './add-role-category.component.html',
  styleUrls: ['./add-role-category.component.css']
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class AddRoleCategoryComponent extends BaseComponentModel implements OnInit, OnChanges, AfterViewInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('confirmDialogComponent') public confirmDialogComponent;
  @ViewChild('noCreateRoleCategoryDialogComponent') public noCreateRoleCategoryDialogComponent;

  /************************ COMPONENT VARS  ************************/

  public roleCategory: Array<RoleCategoryModel> = [];

  private _validRoleCategory: Array<boolean> = [];

  /************************ CONFIG VARS ************************/

  private _confirmDialogConfig = {
    header: 'Las categorías de rol que no tengan el nombre informado y validado no serán creadas.',
    message: '¿Desea crear el resto de categorías de rol?',
    accept: {
      action: 'createRoleCategory',
      button: {
        text: 'Si'
      }
    }
  };

  private _noCreateRoleCategoryDialogSetupDialogConfig = {
    header: 'Ninguna categoria de rol de las indicadas tiene el campo nombre validado.',
    message: 'No se creará ninguna categoría de rol.',
    accept: {
      action: null,
      button: {
        text: 'Aceptar',
        classes: {
          main: 'btn-secondary',
          extra: 'btn-regular'
        }
      }
    },
    reject: {
      show: false
    }
  };

  private _nameInputValidatorConfig = {
    field: {
      placeholder: 'Nombre',
      css: {
        empty: true
      },
      validations: {
        required: {
          active: true,
          value: true,
          message: 'Campo obligatorio.'
        },
        maxlength: {
          active: true,
          value: 255,
          message: 'Máximo 255 caracteres.'
        },
        minlength: {
          active: true,
          value: 3,
          message: 'Mínimo 3 caracteres.'
        }
      },
      validationsApi: {
        url: 'teamtgroups/InputValidator/name',
        message: null
      }
    }
  };

  /************************ STATIC METHODS ************************/


  /************************ NATIVE METHODS ************************/

  constructor(private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _router: Router) {
    super();
    this._authSrv.verifyViewPermission('admin/role-category');
  }

  ngOnInit() {
    this.config.id = 'add_role_category_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    if (this.isAllowed()) {
      this.roleCategory.push(new RoleCategoryModel());
      this._validRoleCategory.push(true);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngAfterViewInit(): void {
  }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('admin/role-category', tag);
  }

  /************************ EVENT METHODS ************************/

  /**
   * Delete role category
   * @param {number} index
   */
  public deleteRoleCategory(index: number): void {
    this.roleCategory.splice(index, 1);
    this._validRoleCategory.splice(index, 1);
  }

  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']]();
    }
  }

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getConfirmDialogSetup(): object {
    return this._confirmDialogConfig;
  }

  /**
   * Get name input validator
   * @returns {Object}
   */
  public getNameInputValidatorConfig(): object {
    return this._nameInputValidatorConfig;
  }

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getNoCreateRoleCategoryDialogSetup(): object {
    return this._noCreateRoleCategoryDialogSetupDialogConfig;
  }

  /**
   * Return to admin view
   */
  public goBackTo(): void {
    this._router.navigateByUrl('/admin');
  }

  /**
   * Check if role category is valid
   * @param {number} index
   * @returns {boolean}
   */
  public isValidRoleCategory(index: number): boolean {
    return !_.isEmpty(this.roleCategory[index].name);
  }

  /**
   * On invalid name setter
   * @param {Object | any} event
   * @param {number} index
   */
  public onInvalidRoleCategory(event: object | any, index: number): void {
    this.roleCategory[index].name = event['value'];
    this._validRoleCategory[index] = false;
  }

  /**
   * On valid name setter
   * @param {Object | any} event
   * @param {number} index
   */
  public onValidRoleCategory(event: object | any, index: number): void {
    this.roleCategory[index].name = event['value'];
    this._validRoleCategory[index] = true;
    this.checkAddRoleCategory();
  }

  /**
   * Show confirm dialog for create team group
   */
  public showConfirmDialog(): void {
    if (this.checkRoleCategory()) {
      this.confirmDialogComponent.showDialog(event);
    } else {
      this.noCreateRoleCategoryDialogComponent.showDialog(event);
    }
  }

  /************************ API METHODS ************************/

  /**
   * Create role category
   */
  private createRoleCategory(): void {
    const createRoleCategories = [];

    for (let i = 0; i < this._validRoleCategory.length; i++) {
      const valid = this._validRoleCategory[i];
      if (valid) {
        createRoleCategories.push(this.roleCategory[i]);
      }
    }

    if (!_.isNil(createRoleCategories) && !_.isEmpty(createRoleCategories)) {
      // @ToDo: Modificar endpoint y atributos
      createRoleCategories.forEach( createRoleCategory => {
        this._httpSrv.post(
          'persons/batch/',
          {
            role_categories: createRoleCategory
          }
        ).subscribe(
          (success) => this.onSuccessCreateRoleCategory(success)
        );
      });
    }
  }

  /**
   * On success create role category
   * @param {Object} response
   */
  private onSuccessCreateRoleCategory(response: object): void {
    if (response['success']) {
      this.goBackTo();
    }
  }

  /************************ AUXILIAR METHODS ************************/

  /**
   * Check if must show new add role category row
   */
  private checkAddRoleCategory(): void {
    let result = true;

    for (let i = 0; i < this._validRoleCategory.length; i++) {
      if (result && (!this._validRoleCategory[i] || _.isEmpty(this.roleCategory[i].name))) {
        result = false;
      }
    }

    if (result === true) {
      this.roleCategory.push(new RoleCategoryModel());
      this._validRoleCategory.push(false);
    }
  }

  /**
   * Check if all role category are complete
   * @returns {boolean}
   */
  private checkRoleCategory(): boolean {
    let validate = false;
    for (let i = 0; i < this._validRoleCategory.length; i++) {
      if ( this._validRoleCategory[i] === true) {
        validate = true;
        break;
      }
    }
    return validate;
  }
}
