import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {LazyLoadEvent, DataTable} from 'primeng/primeng';

import {BaseComponentModel} from '../../../../shared/models/base-component/base-component.class';
import {RoleCategoryModel} from '../../../../shared/models/role/role-category.model';

import {HttpSrvService} from '../../../../shared/services/http.service';

const _ = require('lodash');

enum availableStatus {all, used, unused}

@Component({
  selector: 'app-edit-role-category-component',
  templateUrl: './edit-role-category.component.html',
  styleUrls: ['./edit-role-category.component.css']
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class EditRoleCategoryComponent extends BaseComponentModel implements OnInit, OnChanges, AfterViewInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('roleCategoryDatatable') public roleCategoryDatatable: DataTable;
  @ViewChild('confirmDialogComponent') public confirmDialogComponent;

  /************************ COMPONENT VARS  ************************/

  public roleCategory: Array<RoleCategoryModel> = [];
  public filterRoleCategoryOptions: Array<object> = [
    {label: 'Todo', value: availableStatus.all},
    // {label: 'En uso', value: availableStatus.used},
    {label: 'Sin usar', value: availableStatus.unused}
  ];
  public selectedFilterRoleCategoryOptions = this.filterRoleCategoryOptions[0];

  private _originalRoleCategory: Array<RoleCategoryModel> = [];
  private _editableFields: Array<boolean> = [];
  private _validFields: Array<string> = [];
  private _searchRoleCategory = null;
  private _deleteRoleCategoryElement = null;

  /************************ CONFIG VARS ************************/

  public pagination = {
    page: 1,
    rows: 10,
    filters: {},
    sortField: 'name',
    sortOrder: 'asc',
    totalRecords: 0,
  };

  private _confirmDialogConfig = {
    header: 'Se eliminará la categoría de rol.',
    message: '¿Desea continuar?',
    accept: {
      action: 'deleteRoleCategory',
      button: {
        text: 'Si'
      }
    },
    reject: {
      action: 'cancelDeleteRoleCategory',
    }
  };

  /************************ NATIVE METHODS ************************/

  constructor(private _httpSrv: HttpSrvService,
              private _router: Router) {
    super();
  }

  ngOnInit() {
    this.config.id = 'edit_role_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    const activeDatatableFilter = Number(localStorage.getItem('editRoleCategorySelectedFilterDatatable'));
    if (!_.isNil(activeDatatableFilter)) {
      this.selectedFilterRoleCategoryOptions = this.filterRoleCategoryOptions[activeDatatableFilter];
    }
    this.loadRoleCategoryLazy(this.roleCategoryDatatable.createLazyLoadMetadata());
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngAfterViewInit(): void {
    if (!_.isNil(this.roleCategoryDatatable)) {
      this.roleCategoryDatatable.resolveFieldData = function(data, field) {
        if (data && field && field === 'name') {
          return data[field];
        } else {
          return null;
        }
      };
    }
  }

  /************************ EVENT METHODS ************************/

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getConfirmDialogSetup(): object {
    return this._confirmDialogConfig;
  }

  /**
   * On delete role category
   * @param {number} index
   */
  public onDeleteRoleCategory(index: number): void {
    if (!_.isNil(index)) {
      this._deleteRoleCategoryElement = this.roleCategory[index];
      this.showConfirmDialog();
    }
  }
  /**
   * Get invalid field message
   * @param {number} index
   */
  public getInvalidFieldMessage(index: number): string {
    return this._validFields[index];
  }

  /**
   * Return if an input value has errors
   * @param name
   * @param {number} index
   */
  public hasErrorInput(name: any, index: number): void {
    if (!_.isNil(name) && !_.isEmpty(name)) {
      clearTimeout(this._searchRoleCategory);
      this._searchRoleCategory = setTimeout( () => {
        // @ToDo: Modificar endpoint
        this._httpSrv
          .get('persons/InputValidator/email?value=' + name)
          .subscribe(
            success => this.checkNameValidatorResponse(success, index)
          );
      }, 500);
    } else {
      this.setInvalidField(index, 'Nombre requerido');
    }
  }

  /**
   * Check if field is editable
   * @param {number} index
   */
  public isEditableField(index: number): boolean {
    return this._editableFields[index];
  }

  /**
   * Check if field is valid
   * @param {number} index
   */
  public isValidField(index: number): boolean {
    return _.isEmpty(this._validFields[index]);
  }

  /**
   * Load role category lazy
   * @param {LazyLoadEvent} event
   */
  public loadRoleCategoryLazy(event: LazyLoadEvent) {
    this.pagination.page = (event.first / event.rows) + 1;
    this.pagination.sortField = !_.isNil(event.sortField) ? event.sortField : this.pagination.sortField;
    const order = !_.isNil(event.sortOrder) ? event.sortOrder : 1;
    this.pagination.sortOrder = order === 1 ? 'asc' : 'desc';
    this.pagination.filters = event.filters;

    setTimeout(() => {
      if (this.roleCategory) {
        this.getAllRoleCategory();
      }
    }, 250);
  }

  /**
   * On blur editable field
   * @param {number} index
   */
  public onBlurEditableField(index: number): void {
    if (this.isValidField(index)) {
      this.saveRoleCategory(this.roleCategory[index]);
    } else {
      this.roleCategory[index] = _.cloneDeep(this._originalRoleCategory[index]);
      this.setInvalidField(index, '');
      this.roleCategoryDatatable.reset();
    }
    this._editableFields[index] = !this._editableFields[index];
  }

  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']]();
    }
  }

  /**
   * On click create role category button
   */
  public onClickCreateRoleCategory(): void {
    this._router.navigateByUrl('/admin/role-category');
  }

  /**
   * On click export datatable to csv
   */
  public onClickExportToCsv(): void {
    this.getAllRoleCategoryToExport();
  }

  /**
   * Set active datatable filter
   * @param {number} index
   */
  public setActiveDatatableFilter(index: number): void {
    this.selectedFilterRoleCategoryOptions = this.filterRoleCategoryOptions[index];
    localStorage.setItem('editRoleCategorySelectedFilterDatatable', index.toString());
    this.getAllRoleCategory();
  }

  /**
   * Set editable field
   * @param {number} index
   */
  public setEditableField(index: number): void {
    this._editableFields[index] = !this._editableFields[index];
  }

  /**
   * Show confirm dialog for delete role category
   */
  public showConfirmDialog(): void {
    this.confirmDialogComponent.showDialog(event);
  }

  /************************ API METHODS ************************/

  /**
   * Check name validation
   * @param response
   * @param {number} index
   */
  private checkNameValidatorResponse(response: any, index: number): void {
    if (response.hasOwnProperty('object')
      && response.object.hasOwnProperty('valid')) {

      if (response.object.valid) {
        this._originalRoleCategory[index] = _.cloneDeep(this.roleCategory[index]);
        this.setInvalidField(index, '');
      } else {
        this.setInvalidField(index, 'El nombre introducido ya existe en el sistema.');
      }
    }
  }

  /**
   * Get all role category
   */
  private getAllRoleCategory(): void {
    this._httpSrv.get(
      this.getAllRoleCategoryEndpoint(),
      {
        page: this.pagination.page,
        elements: this.pagination.rows,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      }
    ).subscribe(
      (success) => this.onSuccessGetAllRoleCategory(success)
    );
  }

  /**
   * On success get all role category
   * @param {Object | any} response
   * @param {boolean} exportToCsv
   */
  private onSuccessGetAllRoleCategory(response: object | any, exportToCsv: boolean = false): void {
    this.roleCategory = [];
    this._originalRoleCategory = [];
    this._editableFields = [];
    this._validFields = [];
    if (response['success']) {

      response['object']['items'].forEach(teamGroup => {
        this.roleCategory.push(new RoleCategoryModel(teamGroup));
        this._originalRoleCategory.push(new RoleCategoryModel(teamGroup));
        this._editableFields.push(false);
        this._validFields.push('');
      });
      this.pagination.totalRecords = response.object.total_count;

      if (exportToCsv) {
        setTimeout(() => {
          this.roleCategoryDatatable.exportCSV();
          this.getAllRoleCategory();
        }, 300);
      }
    } else {
      this.pagination.totalRecords = 0;
    }
  }

  /**
   * Get all role category to export
   */
  private getAllRoleCategoryToExport(): void {
    this._httpSrv.get(
      this.getAllRoleCategoryEndpoint(),
      {
        page: 1,
        elements: this.pagination.totalRecords,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      },
      'Listado de categorías de rol a exportar'
    ).subscribe(
      success => this.onSuccessGetAllRoleCategory(success, true)
    );
  }

  /**
   * Get all role category endpoint depending active filters
   */
  private getAllRoleCategoryEndpoint(): string {
    let endpoint = 'roleclasses/';

    if (!_.isNil(this.pagination.filters)
      && !_.isEmpty(this.pagination.filters)) {

      if (endpoint.charAt(endpoint.length - 1) !== '?') {
        endpoint += '?';
      }

      const filterKeys = Object.keys(this.pagination.filters);

      for (let i = 0; i < filterKeys.length ; i++) {
        if (endpoint.charAt(endpoint.length - 1) !== '?') {
          endpoint += '&';
        }
        const key = filterKeys[i];
        endpoint += key + '=%' + this.pagination.filters[key]['value'].toLowerCase() + '%';
      }
    }

    // @ToDo: Filtrado de categorias de rol usadas y no usadas
    // if (this.selectedFilterRoleCategoryOptions['value'] === availableStatus.unused) {
    //   if (endpoint.charAt(endpoint.length - 1) === '?') {
    //     endpoint += 'teamts=null';
    //   } else if (endpoint.charAt(endpoint.length - 1) === '/') {
    //     endpoint += '?teamts=null';
    //   } else {
    //     endpoint += '&teamts=null';
    //   }
    // }  else if (this.selectedFilterRoleCategoryOptions['value'] === availableStatus.used) {
    //
    // }

    return endpoint;
  }

  /**
   * Save role category
   * @param {RoleCategoryModel} roleCategory
   */
  private saveRoleCategory(roleCategory: RoleCategoryModel): void {
    // @ToDo: Cambiar endpoint de guardado
    if (!_.isNil(roleCategory)) {
      // this._httpSrv.patch(
      //   'teamtgroups/' + roleCategory.id,
      //   {
      //     role_category: roleCategory.name
      //   }
      // ).subscribe(
      //   (success) => this.onSuccessSaveRoleCategory(success)
      // );
    }
  }

  /**
   * On success save role category
   * @param {Object | any} response
   * @param {number} index
   */
  private onSuccessSaveRoleCategory(response: object | any): void {
    this.roleCategoryDatatable.reset();
  }

  /**
   * Detele role category
   */
  private deleteRoleCategory(): void {
    // @ToDo: Cambiar endpoint de eliminacion
    if (!_.isNil(this._deleteRoleCategoryElement)) {
      // this._httpSrv.delete(
      //   'teamtgroups/' + this._deleteRoleCategoryElement.id
      // ).subscribe(
      //   (success) => this.onSuccessDeleteRoleCategory(success)
      // );
    }
  }

  /**
   * On success delete role category
   * @param {Object | any} response
   */
  private onSuccessDeleteRoleCategory(response: object | any): void {
    this._deleteRoleCategoryElement = null;
    this.roleCategoryDatatable.reset();
  }

  /************************ AUXILIAR METHODS ************************/

  /**
   * Cancel delete role category
   */
  private cancelDeleteRoleCategory(): void {
    this._deleteRoleCategoryElement = null;
  }

  /**
   * Set invalid field
   * @param {number} index
   * @param {string} msg
   */
  private setInvalidField(index: number, msg: string): void {
    this._validFields[index] = msg;
  }

}
