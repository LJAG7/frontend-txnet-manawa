import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {SelectItem} from 'primeng/api';

import {BaseComponentModel} from '../../../../../shared/models/base-component/base-component.class';
import {RoleModel} from '../../../../../shared/models/role/role.model';
import {RoleCategoryModel} from '../../../../../shared/models/role/role-category.model';

import {AuthSrvService} from '../../../../../shared/services/auth.service';
import {HttpSrvService} from '../../../../../shared/services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-add-role-component',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.css']
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class AddRoleComponent extends BaseComponentModel implements OnInit, OnChanges, AfterViewInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('confirmDialogComponent') public confirmDialogComponent;
  @ViewChild('noCreateRoleDialogComponent') public noCreateRoleDialogComponent;

  /************************ COMPONENT VARS  ************************/

  // Static vars for static methods

  public getRoleCategory = AddRoleComponent.getRoleCategory;

  // Component vars

  public role: Array<RoleModel> = [];
  public roleCategory: Array<SelectItem> = [];

  private _validRole: Array<boolean> = [];

  /************************ CONFIG VARS ************************/

  private _confirmDialogConfig = {
    header: 'Los roles que no tengan los campos nombre y categoría informados y validados no serán creadas.',
    message: '¿Desea crear el resto de roles?',
    accept: {
      action: 'createRole',
      button: {
        text: 'Si'
      }
    }
  };

  private _noCreateRoleDialogSetupDialogConfig = {
    header: 'Ninguno de los roles indicados tienen los campos nombre y categoría validados.',
    message: 'No se creará ningun rol.',
    accept: {
      action: null,
      button: {
        text: 'Aceptar',
        classes: {
          main: 'btn-secondary',
          extra: 'btn-regular'
        }
      }
    },
    reject: {
      show: false
    }
  };

  private _nameInputValidatorConfig = {
    field: {
      placeholder: 'Nombre',
      css: {
        empty: true
      },
      validations: {
        required: {
          active: true,
          value: true,
          message: 'Campo obligatorio.'
        },
        maxlength: {
          active: true,
          value: 255,
          message: 'Máximo 255 caracteres.'
        },
        minlength: {
          active: true,
          value: 3,
          message: 'Mínimo 3 caracteres.'
        }
      },
      validationsApi: {
        url: 'teamtgroups/InputValidator/name',
        message: null
      }
    }
  };

  /************************ STATIC METHODS ************************/

  /**
   * Get person role tag
   */
  private static getRoleCategory(role: RoleModel): RoleCategoryModel {
    return role.getRoleCategory();
  }

  /************************ NATIVE METHODS ************************/

  constructor(private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _router: Router) {
    super();
    this._authSrv.verifyViewPermission('admin/role');
  }

  ngOnInit() {
    this.config.id = 'add_role_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    this.getAllRoleCategory();
    if (this.isAllowed()) {
      this.role.push(new RoleModel({role_class: this.roleCategory[0]}));
      this._validRole.push(true);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngAfterViewInit(): void {
  }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('admin/role', tag);
  }

  /************************ EVENT METHODS ************************/

  /**
   * Delete role
   * @param {number} index
   */
  public deleteRole(index: number): void {
    this.role.splice(index, 1);
    this._validRole.splice(index, 1);
  }

  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']]();
    }
  }

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getConfirmDialogSetup(): object {
    return this._confirmDialogConfig;
  }

  /**
   * Get name input validator
   * @returns {Object}
   */
  public getNameInputValidatorConfig(): object {
    return this._nameInputValidatorConfig;
  }

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getNoCreateRoleDialogSetup(): object {
    return this._noCreateRoleDialogSetupDialogConfig;
  }

  /**
   * Return to admin view
   */
  public goBackTo(): void {
    this._router.navigateByUrl('/admin');
  }

  /**
   * Check if role is valid
   * @param {number} index
   * @returns {boolean}
   */
  public isValidRole(index: number): boolean {
    return !_.isEmpty(this.role[index].name);
  }

  /**
   * On change role category
   * @returns {string}
   */
  public onChangeRoleCategory(event: RoleCategoryModel, index: number): void {
    this.role[index].role_class = event;
  }

  /**
   * On invalid name setter
   * @param {Object | any} event
   * @param {number} index
   */
  public onInvalidRole(event: object | any, index: number): void {
    this.role[index].name = event['value'];
    this._validRole[index] = false;
  }

  /**
   * On valid name setter
   * @param {Object | any} event
   * @param {number} index
   */
  public onValidRole(event: object | any, index: number): void {
    this.role[index].name = event['value'];
    this._validRole[index] = true;
    this.checkAddRole();
  }

  /**
   * Show confirm dialog for create team group
   */
  public showConfirmDialog(): void {
    if (this.checkRole()) {
      this.confirmDialogComponent.showDialog(event);
    } else {
      this.noCreateRoleDialogComponent.showDialog(event);
    }
  }

  /************************ API METHODS ************************/

  /**
   * Create role
   */
  private createRole(): void {
    const createRoles = [];

    for (let i = 0; i < this._validRole.length; i++) {
      const valid = this._validRole[i];
      if (valid) {
        createRoles.push(this.role[i]);
      }
    }

    if (!_.isNil(createRoles) && !_.isEmpty(createRoles)) {
      // @ToDo: Modificar endpoint y atributos
      createRoles.forEach( createRol => {
        this._httpSrv.post(
          'persons/batch/',
          {
            roles: createRol
          }
        ).subscribe(
          (success) => this.onSuccessCreateRole(success)
        );
      });
    }
  }

  /**
   * On success create role
   * @param {Object} response
   */
  private onSuccessCreateRole(response: object): void {
    if (response['success']) {
      this.goBackTo();
    }
  }

  /**
   * Get all role category
   */
  private getAllRoleCategory(): void {
    this._httpSrv.get(
      'roleclasses/',
      {
        page: 1,
        elements: 1000
      },
      'Listado de categorías de rol'
    ).subscribe(
      success => this.onSuccessGetAllRoleCategory(success)
    );
  }

  /**
   * On success get all role category
   * @param {Object | any} response
   */
  private onSuccessGetAllRoleCategory(response: object | any): void {
    this.roleCategory = [];
    if (response['success']) {
      response['object']['items'].forEach(category => {
        const auxRoleCategory = new RoleCategoryModel(category);
        this.roleCategory.push({label: auxRoleCategory.name, value: auxRoleCategory});
      });
    }
  }

  /************************ AUXILIAR METHODS ************************/

  /**
   * Check if must show new add role row
   */
  private checkAddRole(): void {
    let result = true;

    for (let i = 0; i < this._validRole.length; i++) {
      if (result && (!this._validRole[i] || _.isEmpty(this.role[i].name))) {
        result = false;
      }
    }

    if (result === true) {
      this.role.push(new RoleModel());
      this._validRole.push(false);
    }
  }

  /**
   * Check if all role are complete
   * @returns {boolean}
   */
  private checkRole(): boolean {
    let validate = false;
    for (let i = 0; i < this._validRole.length; i++) {
      if ( this._validRole[i] === true) {
        validate = true;
        break;
      }
    }
    return validate;
  }
}
