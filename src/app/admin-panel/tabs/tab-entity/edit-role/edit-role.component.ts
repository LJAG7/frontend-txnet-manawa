import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {SelectItem} from 'primeng/api';

import {LazyLoadEvent, DataTable} from 'primeng/primeng';

import {BaseComponentModel} from '../../../../shared/models/base-component/base-component.class';
import {RoleModel} from '../../../../shared/models/role/role.model';
import {RoleCategoryModel} from '../../../../shared/models/role/role-category.model';

import {HttpSrvService} from '../../../../shared/services/http.service';
import {AlertService} from '../../../../shared/services/alert.service';

const _ = require('lodash');

enum availableStatus {all, used, unused}

@Component({
  selector: 'app-edit-role-component',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.css']
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class EditRoleComponent extends BaseComponentModel implements OnInit, OnChanges, AfterViewInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('roleDatatable') public roleDatatable: DataTable;
  @ViewChild('confirmDialogComponent') public confirmDialogComponent;

  /************************ COMPONENT VARS  ************************/

  // Static vars for static methods

  public getRoleCategory = EditRoleComponent.getRoleCategory;

  // Component vars

  public role: Array<RoleModel> = [];
  public roleCategory: Array<SelectItem> = [];

  public filterRoleOptions: Array<object> = [
    {label: 'Todo', value: availableStatus.all},
    // {label: 'En uso', value: availableStatus.used},
    {label: 'Sin usar', value: availableStatus.unused}
  ];
  public selectedFilterRoleOptions = this.filterRoleOptions[0];

  public dropdownOptions: SelectItem[] = [];
  public selectedDropdownOption: RoleCategoryModel;

  private _originalRole: Array<RoleModel> = [];
  private _editableFields: Array<boolean> = [];
  private _validFields: Array<string> = [];
  private _searchRole = null;
  private _deleteRoleElement = null;

  /************************ CONFIG VARS ************************/

  public pagination = {
    page: 1,
    rows: 10,
    filters: {},
    sortField: 'name',
    sortOrder: 'asc',
    totalRecords: 0,
  };

  private _confirmDialogConfig = {
    header: 'Se eliminará el rol.',
    message: '¿Desea continuar?',
    accept: {
      action: 'deleteRole',
      button: {
        text: 'Si'
      }
    },
    reject: {
      action: 'cancelDeleteRole',
    }
  };

  /************************ STATIC METHODS ************************/

  /**
   * Get person role tag
   */
  private static getRoleCategory(role: RoleModel): RoleCategoryModel {
    return role.getRoleCategory();
  }

  /************************ NATIVE METHODS ************************/

  constructor(private _alertSrv: AlertService,
              private _httpSrv: HttpSrvService,
              private _router: Router) {
    super();
  }

  ngOnInit() {
    this.config.id = 'edit_role_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    const activeDatatableFilter = Number(localStorage.getItem('editRoleSelectedFilterDatatable'));
    if (!_.isNil(activeDatatableFilter)) {
      this.selectedFilterRoleOptions = this.filterRoleOptions[activeDatatableFilter];
    }
    this.loadRoleLazy(this.roleDatatable.createLazyLoadMetadata());
    this.getAllRoleCategory();
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngAfterViewInit(): void {
    if (!_.isNil(this.roleDatatable)) {
      this.roleDatatable.resolveFieldData = function(data, field) {
        if (data && field && field === 'name') {
          return data[field];
        } else if (data && field && field === 'roleCategory') {
          return EditRoleComponent.getRoleCategory(data).name;
        } else {
          return null;
        }
      };
    }
  }

  /************************ EVENT METHODS ************************/

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getConfirmDialogSetup(): object {
    return this._confirmDialogConfig;
  }

  /**
   * On delete role
   * @param {number} index
   */
  public onDeleteRole(index: number): void {
    if (!_.isNil(index)) {
      this._deleteRoleElement = this.role[index];
      this.showConfirmDialog();
    }
  }
  /**
   * Get invalid field message
   * @param {number} index
   */
  public getInvalidFieldMessage(index: number): string {
    return this._validFields[index];
  }

  /**
   * Return if an input value has errors
   * @param name
   * @param {number} index
   */
  public hasErrorInput(name: any, index: number): void {
    if (!_.isNil(name) && !_.isEmpty(name)) {
      clearTimeout(this._searchRole);
      this._searchRole = setTimeout( () => {
        // @ToDo: Modificar endpoint
        this._httpSrv
          .get('persons/InputValidator/email?value=' + name)
          .subscribe(
            success => this.checkNameValidatorResponse(success, index)
          );
      }, 500);
    } else {
      this.setInvalidField(index, 'Nombre requerido');
    }
  }

  /**
   * Check if field is editable
   * @param {number} index
   */
  public isEditableField(index: number): boolean {
    return this._editableFields[index];
  }

  /**
   * Check if field is valid
   * @param {number} index
   */
  public isValidField(index: number): boolean {
    return _.isEmpty(this._validFields[index]);
  }

  /**
   * Load role lazy
   * @param {LazyLoadEvent} event
   */
  public loadRoleLazy(event: LazyLoadEvent) {
    this.pagination.page = (event.first / event.rows) + 1;
    this.pagination.sortField = !_.isNil(event.sortField) ? event.sortField : this.pagination.sortField;
    const order = !_.isNil(event.sortOrder) ? event.sortOrder : 1;
    this.pagination.sortOrder = order === 1 ? 'asc' : 'desc';
    this.pagination.filters = event.filters;

    setTimeout(() => {
      if (this.role) {
        this.getAllRole();
      }
    }, 250);
  }

  /**
   * On blur editable field
   * @param {number} index
   */
  public onBlurEditableField(index: number): void {
    if (this.isValidField(index)) {
      this.saveRole(this.role[index]);
    } else {
      this.role[index] = _.cloneDeep(this._originalRole[index]);
      this.setInvalidField(index, '');
      this.roleDatatable.reset();
    }
    this._editableFields[index] = !this._editableFields[index];
  }

  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']]();
    }
  }

  /**
   * On change header dropdown option
   */
  public onChangeDropdownOption(): void {
    console.log(this.selectedDropdownOption);
    // TeamComponent.staticSelectedDropdownOption = this.selectedDropdownOption;
    // if (this.selectedDropdownOption === dropdownAvailableOptions.create) {
    //   this.teamDateType = 'Fecha de creación';
    // } else if (this.selectedDropdownOption === dropdownAvailableOptions.edit) {
    //   this.teamDateType = 'Fecha de modificación';
    // }
  }

  /**
   * On change role category
   * @returns {string}
   */
  public onChangeRoleCategory(event: RoleCategoryModel, index: number): void {
    this.role[index].role_class = event;

    // @ToDo: Llamada a modificar rol
    // this._httpSrv.put(
    //   'persons/' + user.id + '/roleSystems/',
    //   {role_systems: [{'tag': event}]},
    //   'Rol usuario'
    // ).subscribe(
    //   success => this.onSuccessChangeUserSystemRole(success),
    //   fail => this.onErrorApiResponse(fail)
    // );
  }

  /**
   * On click create role button
   */
  public onClickCreateRole(): void {
    if (!_.isEmpty(this.roleCategory)) {
      this._router.navigateByUrl('/admin/role');
    } else {
      this._alertSrv.prompt('No existen clases de rol. Debe crear alguna clase de rol antes de poder crear roles.');
    }
  }

  /**
   * On click export datatable to csv
   */
  public onClickExportToCsv(): void {
    this.getAllRoleToExport();
  }

  /**
   * Set active datatable filter
   * @param {number} index
   */
  public setActiveDatatableFilter(index: number): void {
    this.selectedFilterRoleOptions = this.filterRoleOptions[index];
    localStorage.setItem('editRoleSelectedFilterDatatable', index.toString());
    this.getAllRole();
  }

  /**
   * Set editable field
   * @param {number} index
   */
  public setEditableField(index: number): void {
    this._editableFields[index] = !this._editableFields[index];
  }

  /**
   * Show confirm dialog for delete role
   */
  public showConfirmDialog(): void {
    this.confirmDialogComponent.showDialog(event);
  }

  /************************ API METHODS ************************/

  /**
   * Check name validation
   * @param response
   * @param {number} index
   */
  private checkNameValidatorResponse(response: any, index: number): void {
    if (response.hasOwnProperty('object')
      && response.object.hasOwnProperty('valid')) {

      if (response.object.valid) {
        this._originalRole[index] = _.cloneDeep(this.role[index]);
        this.setInvalidField(index, '');
      } else {
        this.setInvalidField(index, 'El nombre introducido ya existe en el sistema.');
      }
    }
  }

  /**
   * Get all role
   */
  private getAllRole(): void {
    this._httpSrv.get(
      this.getAllRoleEndpoint(),
      {
        page: this.pagination.page,
        elements: this.pagination.rows,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      }
    ).subscribe(
      (success) => this.onSuccessGetAllRole(success)
    );
  }

  /**
   * On success get all role
   * @param {Object | any} response
   * @param {boolean} exportToCsv
   */
  private onSuccessGetAllRole(response: object | any, exportToCsv: boolean = false): void {
    this.role = [];
    this._originalRole = [];
    this._editableFields = [];
    this._validFields = [];
    if (response['success']) {

      response['object']['items'].forEach(teamGroup => {
        this.role.push(new RoleModel(teamGroup));
        this._originalRole.push(new RoleModel(teamGroup));
        this._editableFields.push(false);
        this._validFields.push('');
      });
      this.pagination.totalRecords = response.object.total_count;

      if (exportToCsv) {
        setTimeout(() => {
          this.roleDatatable.exportCSV();
          this.getAllRole();
        }, 300);
      }
    } else {
      this.pagination.totalRecords = 0;
    }
  }

  /**
   * Get all role to export
   */
  private getAllRoleToExport(): void {
    this._httpSrv.get(
      this.getAllRoleEndpoint(),
      {
        page: 1,
        elements: this.pagination.totalRecords,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      },
      'Listado de roles a exportar'
    ).subscribe(
      success => this.onSuccessGetAllRole(success, true)
    );
  }

  /**
   * Get all role endpoint depending active filters
   */
  private getAllRoleEndpoint(): string {
    let endpoint = 'roles/';

    if (!_.isNil(this.pagination.filters)
      && !_.isEmpty(this.pagination.filters)) {

      if (endpoint.charAt(endpoint.length - 1) !== '?') {
        endpoint += '?';
      }

      const filterKeys = Object.keys(this.pagination.filters);

      for (let i = 0; i < filterKeys.length ; i++) {
        if (endpoint.charAt(endpoint.length - 1) !== '?') {
          endpoint += '&';
        }
        const key = filterKeys[i];
        endpoint += key + '=%' + this.pagination.filters[key]['value'].toLowerCase() + '%';
      }
    }

    // @ToDo: Filtrado de roles usados y no usados
    // if (this.selectedFilterRoleOptions['value'] === availableStatus.unused) {
    //   if (endpoint.charAt(endpoint.length - 1) === '?') {
    //     endpoint += 'teamts=null';
    //   } else if (endpoint.charAt(endpoint.length - 1) === '/') {
    //     endpoint += '?teamts=null';
    //   } else {
    //     endpoint += '&teamts=null';
    //   }
    // }  else if (this.selectedFilterRoleOptions['value'] === availableStatus.used) {
    //
    // }

    return endpoint;
  }

  /**
   * Save role
   * @param {RoleModel} role
   */
  private saveRole(role: RoleModel): void {
    // @ToDo: Cambiar endpoint de guardado
    if (!_.isNil(role)) {
      // this._httpSrv.patch(
      //   'teamtgroups/' + role.id,
      //   {
      //     name: role.name
      //   }
      // ).subscribe(
      //   (success) => this.onSuccessSaveRole(success)
      // );
    }
  }

  /**
   * On success save role
   * @param {Object | any} response
   * @param {number} index
   */
  private onSuccessSaveRole(response: object | any): void {
    this.roleDatatable.reset();
  }

  /**
   * Detele role
   */
  private deleteRole(): void {
    // @ToDo: Cambiar endpoint de eliminacion
    if (!_.isNil(this._deleteRoleElement)) {
      // this._httpSrv.delete(
      //   'teamtgroups/' + this._deleteRoleElement.id
      // ).subscribe(
      //   (success) => this.onSuccessDeleteRole(success)
      // );
    }
  }

  /**
   * On success delete role
   * @param {Object | any} response
   */
  private onSuccessDeleteRole(response: object | any): void {
    this._deleteRoleElement = null;
    this.roleDatatable.reset();
  }

  /**
   * Get all role category
   */
  private getAllRoleCategory(): void {
    this._httpSrv.get(
      'roleclasses/',
      {
        page: 1,
        elements: 1000
      },
      'Listado de categorías de rol'
    ).subscribe(
      success => this.onSuccessGetAllRoleCategory(success)
    );
  }

  /**
   * On success get all role category
   * @param {Object | any} response
   */
  private onSuccessGetAllRoleCategory(response: object | any): void {
    this.roleCategory = [];
    if (response['success']) {
      response['object']['items'].forEach(category => {
        const auxRoleCategory = new RoleCategoryModel(category);
        this.roleCategory.push({label: auxRoleCategory.name, value: auxRoleCategory});
        this.dropdownOptions = _.cloneDeep(this.roleCategory);
        this.dropdownOptions.unshift({label: 'Todas', value: null});
      });
    }
  }

  /************************ AUXILIAR METHODS ************************/

  /**
   * Cancel delete role
   */
  private cancelDeleteRole(): void {
    this._deleteRoleElement = null;
  }

  /**
   * Set invalid field
   * @param {number} index
   * @param {string} msg
   */
  private setInvalidField(index: number, msg: string): void {
    this._validFields[index] = msg;
  }

}
