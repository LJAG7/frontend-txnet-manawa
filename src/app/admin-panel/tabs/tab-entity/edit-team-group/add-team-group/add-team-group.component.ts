import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {BaseComponentModel} from '../../../../../shared/models/base-component/base-component.class';
import {TeamGroupModel} from '../../../../../shared/models/team/team-group.model';

import {AuthSrvService} from '../../../../../shared/services/auth.service';
import {HttpSrvService} from '../../../../../shared/services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-add-team-group-component',
  templateUrl: './add-team-group.component.html',
  styleUrls: ['./add-team-group.component.css']
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class AddTeamGroupComponent extends BaseComponentModel implements OnInit, OnChanges, AfterViewInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('confirmDialogComponent') public confirmDialogComponent;
  @ViewChild('noCreateTeamGroupDialogComponent') public noCreateTeamGroupDialogComponent;

  /************************ COMPONENT VARS  ************************/

  public teamGroup: Array<TeamGroupModel> = [];

  private _validTeamGroup: Array<boolean> = [];

  /************************ CONFIG VARS ************************/

  private _confirmDialogConfig = {
    header: 'Los grupos de equipo que no tengan el nombre informado y validado no serán creados.',
    message: '¿Desea crear el resto de grupos de equipos?',
    accept: {
      action: 'createTeamGroup',
      button: {
        text: 'Si'
      }
    }
  };

  private _noCreateTeamGroupDialogSetupDialogConfig = {
    header: 'Ningun grupo de equipo de los indicados tiene el campo nombre validado.',
    message: 'No se creará ningun grupo de equipo.',
    accept: {
      action: null,
      button: {
        text: 'Aceptar',
        classes: {
          main: 'btn-secondary',
          extra: 'btn-regular'
        }
      }
    },
    reject: {
      show: false
    }
  };

  private _nameInputValidatorConfig = {
    field: {
      placeholder: 'Nombre',
      css: {
        empty: true
      },
      validations: {
        required: {
          active: true,
          value: true,
          message: 'Campo obligatorio.'
        },
        maxlength: {
          active: true,
          value: 255,
          message: 'Máximo 255 caracteres.'
        },
        minlength: {
          active: true,
          value: 3,
          message: 'Mínimo 3 caracteres.'
        },
      },
      validationsApi: {
        url: 'teamgroups/InputValidator/name',
        message: null
      }
    }
  };

  /************************ STATIC METHODS ************************/


  /************************ NATIVE METHODS ************************/

  constructor(private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _router: Router) {
    super();
    this._authSrv.verifyViewPermission('admin/team-group');
  }

  ngOnInit() {
    this.config.id = 'add_team_group_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    if (this.isAllowed()) {
      this.teamGroup.push(new TeamGroupModel());
      this._validTeamGroup.push(true);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngAfterViewInit(): void {
  }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('admin/team-group', tag);
  }

  /************************ EVENT METHODS ************************/

  /**
   * Delete team group
   * @param {number} index
   */
  public deleteTeamGroup(index: number): void {
    this.teamGroup.splice(index, 1);
    this._validTeamGroup.splice(index, 1);
  }

  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']]();
    }
  }

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getConfirmDialogSetup(): object {
    return this._confirmDialogConfig;
  }

  /**
   * Get name input validator
   * @returns {Object}
   */
  public getNameInputValidatorConfig(): object {
    return this._nameInputValidatorConfig;
  }

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getNoCreateTeamGroupDialogSetup(): object {
    return this._noCreateTeamGroupDialogSetupDialogConfig;
  }

  /**
   * Return to admin view
   */
  public goBackTo(): void {
    this._router.navigateByUrl('/admin');
  }

  /**
   * Check if team group is valid
   * @param {number} index
   * @returns {boolean}
   */
  public isValidTeamGroup(index: number): boolean {
    return !_.isEmpty(this.teamGroup[index].name);
  }

  /**
   * On invalid name setter
   * @param {Object | any} event
   * @param {number} index
   */
  public onInvalidTeamGroup(event: object | any, index: number): void {
    this.teamGroup[index].name = event['value'];
    this._validTeamGroup[index] = false;
  }

  /**
   * On valid name setter
   * @param {Object | any} event
   * @param {number} index
   */
  public onValidTeamGroup(event: object | any, index: number): void {
    this.teamGroup[index].name = event['value'];
    this._validTeamGroup[index] = true;
    this.checkAddTeamGroup();
  }

  /**
   * Show confirm dialog for create team group
   */
  public showConfirmDialog(): void {
    if (this.checkTeamGroup()) {
      this.confirmDialogComponent.showDialog(event);
    } else {
      this.noCreateTeamGroupDialogComponent.showDialog(event);
    }
  }

  /************************ API METHODS ************************/

  /**
   * Create team group
   */
  private createTeamGroup(): void {
    const createTeamGroups = [];

    for (let i = 0; i < this._validTeamGroup.length; i++) {
      const valid = this._validTeamGroup[i];
      if (valid) {
        createTeamGroups.push(this.teamGroup[i]);
      }
    }

    if (!_.isNil(createTeamGroups) && !_.isEmpty(createTeamGroups)) {
      createTeamGroups.forEach( createTeamGroup => {
        this._httpSrv.post(
          'teamgroups/',
          {
            name: createTeamGroup.name
          }
        ).subscribe(
          (success) => this.onSuccessCreateTeamGroup(success)
        );
      });
    } else {

    }
  }

  /**
   * On success create team group
   * @param {Object} response
   */
  private onSuccessCreateTeamGroup(response: object): void {
    if (response['success']) {
      this.goBackTo();
    }
  }

  /************************ AUXILIAR METHODS ************************/

  /**
   * Check if must show new add team group row
   */
  private checkAddTeamGroup(): void {
    let result = true;

    for (let i = 0; i < this._validTeamGroup.length; i++) {
      if (result && (!this._validTeamGroup[i] || _.isEmpty(this.teamGroup[i].name))) {
        result = false;
      }
    }

    if (result === true) {
      this.teamGroup.push(new TeamGroupModel());
      this._validTeamGroup.push(false);
    }
  }

  /**
   * Check if all team group are complete
   * @returns {boolean}
   */
  private checkTeamGroup(): boolean {
    let validate = false;
    for (let i = 0; i < this._validTeamGroup.length; i++) {
      if ( this._validTeamGroup[i] === true) {
        validate = true;
        break;
      }
    }
    return validate;
  }
}
