import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {LazyLoadEvent, DataTable} from 'primeng/primeng';

import {BaseComponentModel} from '../../../../shared/models/base-component/base-component.class';
import {TeamGroupModel} from '../../../../shared/models/team/team-group.model';

import {HttpSrvService} from '../../../../shared/services/http.service';

const _ = require('lodash');

enum availableStatus {all, used, unused}

@Component({
  selector: 'app-edit-team-group-component',
  templateUrl: './edit-team-group.component.html',
  styleUrls: ['./edit-team-group.component.css']
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class EditTeamGroupComponent extends BaseComponentModel implements OnInit, OnChanges, AfterViewInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('teamGroupDatatable') public teamGroupDatatable: DataTable;
  @ViewChild('confirmDialogComponent') public confirmDialogComponent;

  /************************ COMPONENT VARS  ************************/

  public teamGroup: Array<TeamGroupModel> = [];
  public filterTeamGroupOptions: Array<object> = [
    {label: 'Todo', value: availableStatus.all},
    // {label: 'En uso', value: availableStatus.used},
    {label: 'Sin usar', value: availableStatus.unused}
  ];
  public selectedFilterTeamGroupOptions = this.filterTeamGroupOptions[0];

  private _originalTeamGroup: Array<TeamGroupModel> = [];
  private _editableFields: Array<boolean> = [];
  private _validFields: Array<string> = [];
  private _searchTeamGroup = null;
  private _deleteTeamGroupElement = null;

  /************************ CONFIG VARS ************************/

  public pagination = {
    page: 1,
    rows: 10,
    filters: {},
    sortField: 'name',
    sortOrder: 'asc',
    totalRecords: 0,
  };

  private _confirmDialogConfig = {
    header: 'Se eliminará el grupo de equipo.',
    message: '¿Desea continuar?',
    accept: {
      action: 'deleteTeamGroup',
      button: {
        text: 'Si'
      }
    },
    reject: {
      action: 'cancelDeleteTeamGroup',
    }
  };

  /************************ NATIVE METHODS ************************/

  constructor(private _httpSrv: HttpSrvService,
              private _router: Router) {
    super();
  }

  ngOnInit() {
    this.config.id = 'edit_team_group_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    const activeDatatableFilter = Number(localStorage.getItem('editTeamGroupSelectedFilterDatatable'));
    if (!_.isNil(activeDatatableFilter)) {
      this.selectedFilterTeamGroupOptions = this.filterTeamGroupOptions[activeDatatableFilter];
    }
    this.loadTeamGroupLazy(this.teamGroupDatatable.createLazyLoadMetadata());
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngAfterViewInit(): void {
    if (!_.isNil(this.teamGroupDatatable)) {
      this.teamGroupDatatable.resolveFieldData = function(data, field) {
        if (data && field && field === 'name') {
          return data[field];
        } else {
          return null;
        }
      };
    }
  }

  /************************ EVENT METHODS ************************/

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getConfirmDialogSetup(): object {
    return this._confirmDialogConfig;
  }

  /**
   * On delete team group
   * @param {number} index
   */
  public onDeleteTeamGroup(index: number): void {
    if (!_.isNil(index)) {
      this._deleteTeamGroupElement = this.teamGroup[index];
      this.showConfirmDialog();
    }
  }
  /**
   * Get invalid field message
   * @param {number} index
   */
  public getInvalidFieldMessage(index: number): string {
    return this._validFields[index];
  }

  /**
   * Return if an input value has errors
   * @param name
   * @param {number} index
   */
  public hasErrorInput(name: any, index: number): void {
    if (!_.isNil(name) && !_.isEmpty(name)) {
      clearTimeout(this._searchTeamGroup);
      this._searchTeamGroup = setTimeout( () => {
        this._httpSrv
          .get('teamgroups/InputValidator/name?value=' + name)
          .subscribe(
            success => this.checkNameValidatorResponse(success, index)
          );
      }, 500);
    } else {
      this.setInvalidField(index, 'Nombre requerido');
    }
  }

  /**
   * Check if field is editable
   * @param {number} index
   */
  public isEditableField(index: number): boolean {
    return this._editableFields[index];
  }

  /**
   * Check if field is valid
   * @param {number} index
   */
  public isValidField(index: number): boolean {
    return _.isEmpty(this._validFields[index]);
  }

  /**
   * Load team group lazy
   * @param {LazyLoadEvent} event
   */
  public loadTeamGroupLazy(event: LazyLoadEvent) {
    this.pagination.page = (event.first / event.rows) + 1;
    this.pagination.sortField = !_.isNil(event.sortField) ? event.sortField : this.pagination.sortField;
    const order = !_.isNil(event.sortOrder) ? event.sortOrder : 1;
    this.pagination.sortOrder = order === 1 ? 'asc' : 'desc';
    this.pagination.filters = event.filters;

    setTimeout(() => {
      if (this.teamGroup) {
        this.getAllTeamGroup();
      }
    }, 250);
  }

  /**
   * On blur editable field
   * @param {number} index
   */
  public onBlurEditableField(index: number): void {
    if (this.isValidField(index)) {
      this.saveTeamGroup(this.teamGroup[index]);
    } else {
      this.teamGroup[index] = _.cloneDeep(this._originalTeamGroup[index]);
      this.setInvalidField(index, '');
      this.teamGroupDatatable.reset();
    }
    this._editableFields[index] = !this._editableFields[index];
  }

  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']]();
    }
  }

  /**
   * On click create team group button
   */
  public onClickCreateTeamGroup(): void {
    this._router.navigateByUrl('/admin/team-group');
  }

  /**
   * On click export datatable to csv
   */
  public onClickExportToCsv(): void {
    this.getAllTeamGroupToExport();
  }

  /**
   * Set active datatable filter
   * @param {number} index
   */
  public setActiveDatatableFilter(index: number): void {
    this.selectedFilterTeamGroupOptions = this.filterTeamGroupOptions[index];
    localStorage.setItem('editTeamGroupSelectedFilterDatatable', index.toString());
    this.getAllTeamGroup();
  }

  /**
   * Set editable field
   * @param {number} index
   */
  public setEditableField(index: number): void {
    this._editableFields[index] = !this._editableFields[index];
  }

  /**
   * Show confirm dialog for delete team group
   */
  public showConfirmDialog(): void {
    this.confirmDialogComponent.showDialog(event);
  }

  /************************ API METHODS ************************/

  /**
   * Check name validation
   * @param response
   * @param {number} index
   */
  private checkNameValidatorResponse(response: any, index: number): void {
    if (response.hasOwnProperty('object')
      && response.object.hasOwnProperty('valid')) {

      if (response.object.valid) {
        this._originalTeamGroup[index] = _.cloneDeep(this.teamGroup[index]);
        this.setInvalidField(index, '');
      } else {
        this.setInvalidField(index, 'El nombre introducido ya existe en el sistema.');
      }
    }
  }

  /**
   * Get all team group
   */
  private getAllTeamGroup(): void {
    this._httpSrv.get(
      this.getAllTeamGroupEndpoint(),
      {
        page: this.pagination.page,
        elements: this.pagination.rows,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      }
    ).subscribe(
      (success) => this.onSuccessGetAllTeamGroup(success)
    );
  }

  /**
   * On success get all team group
   * @param {Object | any} response
   * @param {boolean} exportToCsv
   */
  private onSuccessGetAllTeamGroup(response: object | any, exportToCsv: boolean = false): void {
    this.teamGroup = [];
    this._originalTeamGroup = [];
    this._editableFields = [];
    this._validFields = [];
    if (response['success']) {

      response['object']['items'].forEach(teamGroup => {
        this.teamGroup.push(new TeamGroupModel(teamGroup));
        this._originalTeamGroup.push(new TeamGroupModel(teamGroup));
        this._editableFields.push(false);
        this._validFields.push('');
      });
      this.pagination.totalRecords = response.object.total_count;

      if (exportToCsv) {
        setTimeout(() => {
          this.teamGroupDatatable.exportCSV();
          this.getAllTeamGroup();
        }, 300);
      }
    } else {
      this.pagination.totalRecords = 0;
    }
  }

  /**
   * Get all team group to export
   */
  private getAllTeamGroupToExport(): void {
    this._httpSrv.get(
      this.getAllTeamGroupEndpoint(),
      {
        page: 1,
        elements: this.pagination.totalRecords,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      },
      'Listado de grupos de equipo a exportar'
    ).subscribe(
      success => this.onSuccessGetAllTeamGroup(success, true)
    );
  }

  /**
   * Get all team group endpoint depending active filters
   */
  private getAllTeamGroupEndpoint(): string {
    let endpoint = 'teams/groups/';

    if (!_.isNil(this.pagination.filters)
      && !_.isEmpty(this.pagination.filters)) {

      if (endpoint.charAt(endpoint.length - 1) !== '?') {
        endpoint += '?';
      }

      const filterKeys = Object.keys(this.pagination.filters);

      for (let i = 0; i < filterKeys.length ; i++) {
        if (endpoint.charAt(endpoint.length - 1) !== '?') {
          endpoint += '&';
        }
        const key = filterKeys[i];
        endpoint += key + '=%' + this.pagination.filters[key]['value'].toLowerCase() + '%';
      }
    }

    if (this.selectedFilterTeamGroupOptions['value'] === availableStatus.unused) {
      if (endpoint.charAt(endpoint.length - 1) === '?') {
        endpoint += 'teams=null';
      } else if (endpoint.charAt(endpoint.length - 1) === '/') {
        endpoint += '?teams=null';
      } else {
        endpoint += '&teams=null';
      }
    }  else if (this.selectedFilterTeamGroupOptions['value'] === availableStatus.used) {
      // @ToDo: Filtrado de grupos de equipos usados
      // if (endpoint.charAt(endpoint.length - 1) === '?') {
      //   endpoint += 'teams=null';
      // } else if (endpoint.charAt(endpoint.length - 1) === '/') {
      //   endpoint += '?teams=null';
      // } else {
      //   endpoint += '&teams=null';
      // }
    }

    return endpoint;
  }

  /**
   * Save team group
   * @param {TeamGroupModel} teamGroup
   */
  private saveTeamGroup(teamGroup: TeamGroupModel): void {
    if (!_.isNil(teamGroup)) {
      this._httpSrv.patch(
        'teamgroups/' + teamGroup.id,
        {
          name: teamGroup.name
        }
      ).subscribe(
        (success) => this.onSuccessSaveTeamGroup(success)
      );
    }
  }

  /**
   * On success save team group
   * @param {Object | any} response
   * @param {number} index
   */
  private onSuccessSaveTeamGroup(response: object | any): void {
    this.teamGroupDatatable.reset();
  }

  /**
   * Detele team group
   */
  private deleteTeamGroup(): void {
    if (!_.isNil(this._deleteTeamGroupElement)) {
      this._httpSrv.delete(
        'teamgroups/' + this._deleteTeamGroupElement.id
      ).subscribe(
        (success) => this.onSuccessDeleteTeamGroup(success)
      );
    }
  }

  /**
   * On success delete team group
   * @param {Object | any} response
   */
  private onSuccessDeleteTeamGroup(response: object | any): void {
    this._deleteTeamGroupElement = null;
    this.teamGroupDatatable.reset();
  }

  /************************ AUXILIAR METHODS ************************/

  /**
   * Cancel delete team group
   */
  private cancelDeleteTeamGroup(): void {
    this._deleteTeamGroupElement = null;
  }

  /**
   * Set invalid field
   * @param {number} index
   * @param {string} msg
   */
  private setInvalidField(index: number, msg: string): void {
    this._validFields[index] = msg;
  }

}
