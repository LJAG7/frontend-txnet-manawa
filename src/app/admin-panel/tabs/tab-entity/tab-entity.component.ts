import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Router} from '@angular/router';

import {BaseComponentModel} from '../../../shared/models/base-component/base-component.class';

import {AlertService} from '../../../shared/services/alert.service';

const _ = require('lodash');

enum availableEntities {masterClass, teamGroup, role, roleCategory}

@Component({
  selector: 'app-tab-entity-component',
  templateUrl: './tab-entity.component.html',
  styleUrls: ['./tab-entity.component.css']
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TabEntityComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT VARS  ************************/

  public availableEntities = availableEntities;
  public activeTab = this.availableEntities.masterClass;

  /************************ CONFIG VARS ************************/

  /************************ NATIVE METHODS ************************/
  constructor(private _alertSrv: AlertService,
              private _router: Router) {
    super();
  }

  ngOnInit() {
    this.config.id = 'tab_entity_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    const activeTab = localStorage.getItem('adminEntitySelectedTab');
    if (!_.isNil(activeTab)) {
      this.activeTab = Number(activeTab);
    }
  }

  ngOnChanges(changes: SimpleChanges) { }

  /************************ EVENT METHODS ************************/

  /**
   * Set active menu option
   */
  public setActiveMenu(index: number): void {
    localStorage.setItem('adminEntitySelectedTab', index.toString());
    this.activeTab = index;
  }

  /**
   * Go to master view
   */
  public goToMaster(): void {
    this._router.navigateByUrl('/master');
  }

  /************************ AUXILIAR METHODS ************************/

  public functionalityNotImplemented(msg?: string): void {
    this._alertSrv.prompt(msg);
  }
}
