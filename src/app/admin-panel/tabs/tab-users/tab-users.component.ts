'use strict';

import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  OnChanges,
  SimpleChanges,
  AfterViewInit
} from '@angular/core';
import {Router} from '@angular/router';
import {CurrencyPipe} from '@angular/common';

import {LazyLoadEvent, SelectItem, DataTable} from 'primeng/primeng';

import {ROLESYSTEMS} from '../../../shared/const/rolesystems.const';
import {REGEXPATTERNS} from '../../../shared/const/system.const';
import {BaseComponentModel} from '../../../shared/models/base-component/base-component.class';

import {PersonModel} from '../../../shared/models/person.model';
import {RoleSystemModel} from '../../../shared/models/role-system.model';
import {CSVModel} from '../../../shared/models/csv/csv.model';
import {CSVRowModel} from '../../../shared/models/csv/row.model';

import {AlertService} from '../../../shared/services/alert.service';
import {AuthSrvService} from '../../../shared/services/auth.service';
import {HttpSrvService} from '../../../shared/services/http.service';
import {ParseCsvService} from '../../../shared/services/parseCsv.service';
import {LoaderService} from '../../../shared/services/loader.service';

const _ = require('lodash');

let tabUsersComponent: any;

@Component({
  selector: 'app-tab-users-component',
  templateUrl: './tab-users.component.html',
  styleUrls: ['./tab-users.component.css'],
  providers: [HttpSrvService, ParseCsvService, CurrencyPipe]
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TabUsersComponent extends BaseComponentModel implements OnInit, OnChanges, AfterViewInit {

  /************************ COMPONENT STATIC VARS  ************************/

  private static _activeDatatableFilter = -1;
  private static _activeDatatableFilterTag = 'ALL';

  /************************ COMPONENT INPUTS  ************************/

  @Input() public externalConfig: object;
  @Input() public visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() public outputEvent = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('usersDatatable') public usersDatatable: DataTable;
  @ViewChild('confirmDialogComponent') public confirmDialogComponent;
  @ViewChild('confirmDialogCsvComponent') public confirmDialogCsvComponent;
  @ViewChild('fileCsvInput') private fileCsvInput;

  /************************ COMPONENT VARS  ************************/

  // Static vars for static methods

  public getActiveDatatableFilter = TabUsersComponent.getActiveDatatableFilter;
  public getActiveDatatableFilterTag = TabUsersComponent.getActiveDatatableFilterTag;
  public getPersonRoleTag = TabUsersComponent.getPersonRoleTag;
  public getUserScope = TabUsersComponent.getUserScope;
  public hasTeam = TabUsersComponent.hasTeam;
  public isSuperAdmin = TabUsersComponent.isSuperAdmin;
  public isUserRemovable = TabUsersComponent.isUserRemovable;

  // Component vars

  public loadingDatatable = false;
  public personSearchDefault = '';
  public restartPersonSearch = 0;
  public roleSystems: Array<SelectItem> = [];
  public users: Array<PersonModel> = [];

  private _personFilter = null;
  private _filterRoleSystemsOptions: Array<RoleSystemModel> = [];
  private _windowInnerWidth; // Window size

  public csvErrorMessage: string = null;
  public csvSuccessMessage: string = null;

  private _csvData: Array<CSVRowModel> = [];
  private _csvMassiveUpdown = {
    pack: 25,
    counter: 0
  };
  private _subscribeCsv;

  /************************ CONFIG VARS  ************************/

  public pagination = {
    page: 1,
    rows: 10,
    filters: {},
    sortField: 'surname',
    sortOrder: 'asc',
    totalRecords: 0,
  };


  private _confirmDialogCsvConfig = {
    header: 'Va a importar el csv.',
    message: '',
    accept: {
      action: 'importUsers',
      button: {
        text: 'Si'
      }
    },
    reject: {
      action: 'resetCsv',
      button: {
        text: 'Cancelar'
      }
    }
  };

  private _personSearchConfig = {
    title: 'name',
    subtitle: 'email',
    keyToFilter: 'name',
    alternativeKeysToFilter: ['email', 'surname'],
    api: {
      endpoint: 'persons/',
      queryStringExtra: 'name',
    },
    flags: {
      dataOnLive: true,
      hasWhiteBackground: true,
      showResultList: true,
      showResultListUnderClick: true
    },
    error: {
      show: false
    },
    icon: {
      hasIconRight: true,
      hasSearchIcon: true,
      hideIconOnSearch: true,
      showUnselectIcon: true
    },
    input: {
      placeholder: 'Selecciona una persona...'
    },
    css: {
      extraClasses: 'form-style'
    }
  };

  /************************ STATIC METHODS  ************************/

  /**
   * Generate search keys for mate
   * @param {Object} person
   * @returns {Object}
   */
  private static createPersonsBySearchKeys (person: object): object {
    return {
      fullname: person['name'] + ' ' + person['surname'],
      fulldata: person['name'] + ' ' + person['surname'] + ' - ' + person['email']
    };
  }

  /**
   * Get active datatable filter
   * @returns {number}
   */
  private static getActiveDatatableFilter(): number {
    return TabUsersComponent._activeDatatableFilter;
  }

  /**
   * Get active datatable filter tag
   * @returns {number}
   */
  private static getActiveDatatableFilterTag(): string {
    return TabUsersComponent._activeDatatableFilterTag;
  }

  /**
   * Get person role tag
   */
  private static getPersonRoleTag(person: PersonModel): string {
    return person.getRoleTag();
  }

  /**
   * Get user scope
   * @returns {string}
   */
  private static getUserScope(user: PersonModel): string {
    let result = 'N/A';

    if (user.role_systems.length > 0) {
      switch (user.getRoleTag()) {
        case ROLESYSTEMS.ROLE_SUPER_ADMIN:
          result = 'Todo';
          break;
        case ROLESYSTEMS.ROLE_ADMIN:
          if (!_.isEmpty(user.domain)) {
            // @ToDo: Modificar cuando se pueda tener más de un dominio
            result = user.domain.name;
            // const auxResult = [];
            // user['domain'].forEach(domain => {
            //   auxResult.push(domain['name']);
            // });
            // result = auxResult.join(',');
          }
          break;
        case ROLESYSTEMS.ROLE_GESTOR:
          result = 'Equipos a definir';
          if (user.managed_teams.length > 0) {
            const auxResult = [];
            user.managed_teams.forEach(team => {
              auxResult.push(team.name);
            });
            result = auxResult.join(',');
          }
          break;
        case ROLESYSTEMS.ROLE_USER:
          result = 'N/A';
          break;
        default:
          result = 'N/A';
          break;
      }
    }

    return result;
  }

  /**
   * Check if user has team
   * @param {PersonModel} user
   * @returns {boolean}
   */
  private static hasTeam(user: PersonModel): boolean {
    return user.isGestor();
  }

  /**
   * Check if user is super admin
   */
  private static isSuperAdmin(user: PersonModel): boolean {
    return user.isSuperAdmin();
  }

  /**
   * Return if an user is removable
   * @param {PersonModel} user
   * @returns {boolean}
   */
  public static isUserRemovable(user: PersonModel): boolean {
    return false; // !_.isNil(user) && user.id !== 0;
  }

  /************************ NATIVE METHODS  ************************/

  constructor(private _alertSrv: AlertService,
              private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _router: Router,
              private _csvSrv: ParseCsvService,
              private _loaderSrv: LoaderService,
              private _currencyPipe: CurrencyPipe) {
    super();
    tabUsersComponent = this;
  }

  ngOnInit() {
    this.config.id = 'tab_admin_gestor_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    this._windowInnerWidth = window.innerWidth;
    window.scrollTo(0, 0);
    _.merge(this.config, this.externalConfig);

    const activeDatatableFilter = Number(localStorage.getItem('adminUserSelectedFilterDatatable'));
    if (!_.isNil(activeDatatableFilter)) {
      TabUsersComponent._activeDatatableFilter = activeDatatableFilter;
    }

    if (!_.isNaN(activeDatatableFilter) && !_.isNil(activeDatatableFilter) && activeDatatableFilter !== -1) {
      this.getAllAvailableSystemRoles(activeDatatableFilter);
    } else {
      this.getAllAvailableSystemRoles();
    }

    this.loadUsersLazy(this.usersDatatable.createLazyLoadMetadata());
  }

  ngAfterViewInit(): void {
    if (!_.isNil(this.usersDatatable)) {
      this.usersDatatable.resolveFieldData = function (data, field) {
        if (data && field) {
          if (field === 'userType') {
            return TabUsersComponent.getPersonRoleTag(data);
          } else if (field === 'scope') {
            return TabUsersComponent.getUserScope(data);
          } else if (field.indexOf('.') === -1) {
            return data[field];
          } else {
            const fields = field.split('.');
            let value = data;
            for (let i = 0, len = fields.length; i < len; ++i) {
              if (value == null) {
                return '';
              }
              value = value[fields[i]];
            }
            return value;
          }
        } else {
          return null;
        }
      };
    }
  }

  ngOnChanges(changes: SimpleChanges): void { }

  /************************ SETUP METHODS ************************/

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getConfirmDialogCsvSetup(): object {
    return this._confirmDialogCsvConfig;
  }

  /**
   * Get options to filter datatable
   * @returns {Array<Object>}
   */
  public getFilterRoleSystemsOptions(): Array<RoleSystemModel> {
    return this._filterRoleSystemsOptions;
  }

  /**
   * Get mate search widgetConfig
   * @returns {Object}
   */
  public getPersonSearchConfig(): object {
    return this._personSearchConfig;
  }


  public getCsvDemoFileLink(): string {

    return this._authSrv.isRoleSuperAdmin()
      ? '/api/download/csv/demouser_adm.csv'
      : this._authSrv.isRoleAdmin()
        ? '/api/download/csv/demouser_manager.csv'
        : '#';
  }


  /************************ EVENTS METHODS ************************/

  /**
   * Check if actual user can add users
   * @returns {boolean}
   */
  public canAddUser(): boolean {
    return this._authSrv.getPersonLogged().getRoleTag() !== ROLESYSTEMS.ROLE_USER;
  }

  /**
   * Go to edit team
   */
  public goToEditTeams(user: PersonModel): void {
    if (this.hasTeam(user)) {
      localStorage.setItem('selectedTab', '1');
      localStorage.setItem('gestor_email', user.email);
      this._router.navigateByUrl('/admin/scope/' + user.id);
    }
  }

  /**
   * Check if user has managed team
   */
  public hasManagedTeams(user: PersonModel): boolean {
    if (this.hasTeam(user)) {
      return !_.isNil(user.managed_teams) && !_.isEmpty(user.managed_teams);
    }
    return false;
  }


  /**
   * Load users lazy
   * @param {LazyLoadEvent} event
   */
  public loadUsersLazy(event: LazyLoadEvent) {
    this.pagination.page = (event.first / event.rows) + 1;
    this.pagination.sortField = !_.isNil(event.sortField) ? event.sortField : this.pagination.sortField;
    const order = !_.isNil(event.sortOrder) ? event.sortOrder : 1;
    this.pagination.sortOrder = order === 1 ? 'asc' : 'desc';
    this.pagination.filters = event.filters;

    setTimeout(() => {
      if (this.users) {
        this.getAllUsers();
      }
    }, 250);
  }

  /**
   * On change user system role
   * @returns {string}
   */
  public onChangeUserSystemRole(event: string, user: PersonModel): void {
    this._httpSrv.put(
      'persons/' + user.id + '/roleSystems/',
      {role_systems: [{'tag': event}]},
      'Rol usuario'
    ).subscribe(
      success => this.onSuccessChangeUserSystemRole(success),
      fail => this.onErrorApiResponse(fail)
    );
  }

  /**
   * On click add user button
   */
  public onClickAddUser(): void {
    localStorage.setItem('selectedTab', '1');
    this._router.navigateByUrl('/admin/user');
  }


  /**
   * On click export datatable to csv
   */
  public onClickExportToCsv(): void {
    this.getAllUsersToExport();
  }

  /**
   * On blur search component
   * @param {Object | any} event
   */
  public onClickPersonSearch(event: object | any): void {
    if (event['event'] === 'blur' && !event['itHasACorrectValue']) {
      this._personFilter = null;
    } else if (event['event'] === 'blur' && event['itHasACorrectValue']) {
      this.personSearchDefault = TabUsersComponent.createPersonsBySearchKeys(this._personFilter)['fulldata'];
    } else if (event['event'] === 'unselect') {
      this._personFilter = null;
      this.usersDatatable.reset();
    }
  }


  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']](event['params']);
    }
  }

  /**
   * On select mate from search component
   * @param {Object | any} event
   */
  public onSelectedPerson(event: object | any): void {
    this.setActiveDatatableFilter(-1);
    this._personFilter = event['element'];
  }


  /**
   * Set active datatable filter
   */
  public setActiveDatatableFilter(index: number): void {
    localStorage.setItem('adminUserSelectedFilterDatatable', index.toString());
    TabUsersComponent._activeDatatableFilter = index;

    if (index !== -1) {
      TabUsersComponent._activeDatatableFilterTag = this._filterRoleSystemsOptions[index].tag;
    } else {
      TabUsersComponent._activeDatatableFilterTag = 'ALL';
    }

    if (!_.isNil(this._personFilter)) {
      this._personFilter = null;
      this.restartPersonSearch++;
    }

    this.usersDatatable.reset();
  }



  /**
   * On tableMenu event success
   * @param event
   */
  public onTableMenuEvent(event): void {
    if (event['action'] === 'reset') {
      this.setActiveDatatableFilter(TabUsersComponent.getActiveDatatableFilter());

    } else if (event['action'] === 'error') {
    }
  }

  /************************ API METHODS  ************************/

  /**
   * Get all users
   */
  private getAllUsers(): void {
    this.loadingDatatable = true;

    this._httpSrv.get(
      this.getAllUsersEndpoint(),
      {
        page: parseInt(this.pagination.page.toString(), 0),
        elements: this.pagination.rows,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      },
      'Listado usuarios'
    ).subscribe(
      (success) => this.onSuccessGetAllUsers(success),
      (fail) => this.onErrorApiResponse(fail)
    );
  }

  /**
   * On success get all users by role
   * @param {Object | any} response
   * @param {boolean} exportToCsv
   */
  private onSuccessGetAllUsers(response: object | any, exportToCsv: boolean = false): void {
    if (response['success']) {
      this.users = [];
      if (!_.isNil(this._personFilter)) {
        this.users.push(new PersonModel(response['object']));
        this.pagination.totalRecords = 1;
        this.loadingDatatable = false;
      } else {
        const activePersonRole = this._authSrv.getPersonLogged().getRoleTag();
        response['object']['items'].forEach(person => {
          switch (activePersonRole) {
            case (ROLESYSTEMS.ROLE_SUPER_ADMIN):
              if (person.role_systems[0].tag === ROLESYSTEMS.ROLE_ADMIN
                || person.role_systems[0].tag === ROLESYSTEMS.ROLE_GESTOR
                || person.role_systems[0].tag === ROLESYSTEMS.ROLE_USER) {
                this.users.push(new PersonModel(person));
              }
              break;
            case (ROLESYSTEMS.ROLE_ADMIN):
              if (person.role_systems[0].tag === ROLESYSTEMS.ROLE_GESTOR
                || person.role_systems[0].tag === ROLESYSTEMS.ROLE_USER) {
                this.users.push(new PersonModel(person));
              }
              break;
            case (ROLESYSTEMS.ROLE_GESTOR):
              if (person.role_systems[0].tag === ROLESYSTEMS.ROLE_USER) {
                this.users.push(new PersonModel(person));
              }
              break;
          }
        });
        this.pagination.totalRecords = response['object']['total_count'];
        this.loadingDatatable = false;
      }

      if (exportToCsv) {
        setTimeout(() => {
          this.usersDatatable.exportCSV();
          this.getAllUsers();
        }, 300);
      }
    } else {
      this.onErrorApiResponse(response);
    }
  }

  /**
   * Get all users to export
   */
  private getAllUsersToExport(): void {
    this.loadingDatatable = true;

    this._httpSrv.get(
      this.getAllUsersEndpoint(),
      {
        page: 1,
        elements: this.pagination.totalRecords,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      },
      'Listado usuarios a exportar'
    ).subscribe(
      (success) => this.onSuccessGetAllUsers(success, true),
      (fail) => this.onErrorApiResponse(fail)
    );
  }

  /**
   * Get all users endpoint depending active filters
   */
  private getAllUsersEndpoint(): string {
    let endpoint = 'persons/';

    if (!_.isNil(this._personFilter)) {
      endpoint = 'persons/' + this._personFilter['id'];
    } else {

      if (TabUsersComponent.getActiveDatatableFilter() !== -1) {
        const role = this.getActiveDatatableFilterTag();
        if (!_.isNil(role) && !_.isEmpty(role)) {
          endpoint += 'rolesystemtype/' + role;
        }
      }
    }

    if (!_.isNil(this.pagination.filters)
      && !_.isEmpty(this.pagination.filters)) {

      if (endpoint.charAt(endpoint.length - 1) !== '?') {
        endpoint += '?';
      }

      const filterKeys = Object.keys(this.pagination.filters);

      for (let i = 0; i < filterKeys.length ; i++) {
        if (endpoint.charAt(endpoint.length - 1) !== '?') {
          endpoint += '&';
        }
        const key = filterKeys[i];
        endpoint += key + '=%' + this.pagination.filters[key]['value'].toLowerCase() + '%';
      }
    }

    return endpoint;
  }

  /**
   * Get all avalible system roles
   */
  private getAllAvailableSystemRoles(activeDatatableFilter: number = null): void {
    this._httpSrv.get(
      'domains/self/roleSystems/'
    ).subscribe((success) => this.onSuccessGetAllAvailableSystemRoles(success, activeDatatableFilter),
      (fail) => this.onErrorApiResponse(fail)
    );
  }

  /**
   * On success get all avalible system roles
   * @param {Object | any} response
   * @param {number} activeDatatableFilter
   */
  private onSuccessGetAllAvailableSystemRoles(response: object | any, activeDatatableFilter: number = null): void {

    if (response['success']
      && response['object']) {
      this.roleSystems = [];
      this._filterRoleSystemsOptions = [];

      const activePersonRole = this._authSrv.getPersonLogged().getRoleTag();

      response['object'].forEach(role => {
        switch (activePersonRole) {
          case (ROLESYSTEMS.ROLE_SUPER_ADMIN):
            if (role['tag'] === ROLESYSTEMS.ROLE_ADMIN
              || role['tag'] === ROLESYSTEMS.ROLE_GESTOR
              || role['tag'] === ROLESYSTEMS.ROLE_USER) {
              this._filterRoleSystemsOptions.push(new RoleSystemModel(role));
              this.roleSystems.push({label: role['name'], value: role['tag']});
            }
            break;
          case (ROLESYSTEMS.ROLE_ADMIN):
            if (role['tag'] === ROLESYSTEMS.ROLE_GESTOR
              || role['tag'] === ROLESYSTEMS.ROLE_USER) {
              this._filterRoleSystemsOptions.push(new RoleSystemModel(role));
              this.roleSystems.push({label: role['name'], value: role['tag']});
            }
            break;
          case (ROLESYSTEMS.ROLE_GESTOR):
            if (role['tag'] === ROLESYSTEMS.ROLE_USER) {
              this._filterRoleSystemsOptions.push(new RoleSystemModel(role));
            }
            break;
        }
      });

      if (!_.isNil(activeDatatableFilter)) {
        this.setActiveDatatableFilter(activeDatatableFilter);
      }
    } else {
      this.onErrorApiResponse(response);
    }
  }


  /**
   * On success change user system role
   * @param {Object | any} response
   */
  private onSuccessChangeUserSystemRole(response: object | any): void {
    if (response['success']) {
      this.setActiveDatatableFilter(TabUsersComponent.getActiveDatatableFilter());
    } else {
      this.onErrorApiResponse(response);
    }
  }

  /**
   * On error api response
   * @param response
   */
  private onErrorApiResponse(response: object | any): void {
    this.users = [];
    this.loadingDatatable = false;
  }

  /************************ AUXILIAR METHODS  ************************/

  public functionalityNotImplemented(msg?: string): void {
    this._alertSrv.prompt(msg);
  }



  /**************** CSV METHODS *****************************/

  private validateUserCostCsv(csv: CSVModel): void {
    _.forEach(csv.getValidRows(), (data: CSVRowModel) => {

      if (data.values.hasOwnProperty('cost_import') && data.values.hasOwnProperty('cost_type')) {
        if (REGEXPATTERNS.costType.test(data.values['cost_type'])) {
          data.values[data.values['cost_type']] = _.isNil(data.values['cost_import'])
            ? 0
            : data.values['cost_import'];

        } else if (_.isNil(data.values['cost_type']) && !_.isNil(data.values['cost_import'])) {
          csv.error = 'Error: cost_type no definido para cost_import dado. Revise fila ' + data.row;
        }
      }

      data.values = _.omit(data.values, ['cost_type', 'cost_import']);
    });
  }

  public onUploadCsv(event): any {
    this._subscribeCsv = this._csvSrv.getCsv()
      .subscribe(
      (csv: CSVModel) => {
            if (!_.isNil(csv)) {
              this.validateUserCostCsv(csv);

              if (csv.error) {
                this.resetCsv();
                this.csvErrorMessage = csv.error;

              } else {
                this.csvErrorMessage = null;
                this.confirmDialogCsvComponent.showDialog(csv.getValidRows(), this._csvSrv.standardImportMessage(csv));
              }
            }
          },
      (error) => {
        console.log(error);
      }
    );

    this._csvSrv.processFile(
      event,
      {
        email:        {required: true, pattern: REGEXPATTERNS.email, validation: 'persons/InputValidator/email'},
        name:         {required: true, pattern: REGEXPATTERNS.anyMin3},
        surname:      {required: true, pattern: REGEXPATTERNS.anyMin3},
        cost_import:  {required: false, pattern: REGEXPATTERNS.moneyCsv},
        cost_type:    {required: false, pattern: REGEXPATTERNS.costType},
        role_systems: {required: false, pattern: REGEXPATTERNS.role_systems}
      }
    );
  }


  private importUsers(data: Array<CSVRowModel>): void {
    if (_.isNil(data) || _.isEmpty(data)) {
      this._alertSrv.error('Debe importar un archivo CSV antes de enviarlo.');

    } else {
      this._csvData = data;
      this.importCsv();
    }
  }


  private importCsv(): void {
    this._loaderSrv.softImporting();
    const newPersons = [];

    _.slice(
      this._csvData,
      this._csvMassiveUpdown.counter,
      (this._csvMassiveUpdown.pack + this._csvMassiveUpdown.counter)
    ).forEach((item: CSVRowModel) => {
      if (item.hasOwnProperty('values') && item.values) {
        newPersons.push(item.values);
      }
    });

    this._httpSrv
      .post(
        'persons/batch/',
        {new_persons: newPersons},
        'Importación CSV',
        false,
        false)
      .subscribe(
        response => this.onImportCsv(response),
        fail => this.onFailImportCsv(fail)
      );
  }

  private onImportCsv(response): void {
    this._csvMassiveUpdown.counter += this._csvMassiveUpdown.pack;

    if (this._csvMassiveUpdown.counter >= this._csvData.length) {
      const totalUpdown = this._csvData.length.toString();
      this._loaderSrv.stopSoft();
      this.resetCsv();

      if (response.hasOwnProperty('success') && response['success']) {
        localStorage.setItem('GeneralDomainWidgetContainerComponent', 'person');
        this.csvSuccessMessage =  ':t usuarios importados correctamente.'.replace(':t', totalUpdown);
        this.usersDatatable.reset();

      } else {
        this.setCsvErrorMessage(response);
      }

    } else {
      this.importCsv();
    }
  }

  private setCsvErrorMessage(response): void {
    this.csvErrorMessage = response.hasOwnProperty('message')
      ? response['message']
      : 'se ha producido un error desconocido, prueba más tarde';
  }

  private onFailImportCsv(fail): void {
    this._loaderSrv.stopSoft();
    this.resetCsv();
    this.setCsvErrorMessage(fail);
    this.onErrorApiResponse(fail);
  }

  private resetCsv (): void {
    this.fileCsvInput.nativeElement.value = '';
    this.csvErrorMessage = null;
    this.csvSuccessMessage = null;
    this._csvMassiveUpdown.counter = 0;
    this._csvData = [];
    this._csvSrv.resetCsv();
    if (!_.isNil(this._subscribeCsv)) {
      this._subscribeCsv.unsubscribe();
    }
  }
}
