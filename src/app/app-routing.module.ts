import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// import {} from
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {LocationComponent} from './location/location.component';
import {IssuesDetailsComponent} from './issues/details/issues-details.component';
import {IssuesComponent} from './issues/issues.component';
import {TeamMateComponent} from './team-mate/team-mate.component';
import {MateProfileComponent} from './mate-profile/mate-profile.component';
import {MateDashboardConfigComponent} from './mate-profile/mate-profile-tabs/tab-mate-dashboard/mate-dashboard-config/mate-dashboard-config.component';
import {AdminPanelComponent} from './admin-panel/admin-panel.component';
import {TabAdminGestorScopeComponent} from './admin-panel/subviews/gestor-scope/gestor-scope.component';
import {TabAdminGestorAddUserComponent} from './admin-panel/subviews/add-user/add-user.component';
import {TabBasicInfoComponent} from './mate-profile/mate-profile-tabs/tab-mate-profile/tab-basic-info/tab-basic-info.component';
import {EditTeamComponent} from './team/edit/edit-team.component';
import {TeamComponent} from './team/team.component';
import {EditLocationComponent} from './location/edit/edit-location.component';
import {PersonComponent} from './person/person.component';
import {MasterComponent} from './master/master.component';
import {EditMasterComponent} from './master/edit/edit-master.component';
import {AddMasterClassComponent} from './admin-panel/tabs/tab-entity/edit-master-class/add-master-class/add-master-class.component';
import {AddTeamGroupComponent} from './admin-panel/tabs/tab-entity/edit-team-group/add-team-group/add-team-group.component';
import {AddRoleCategoryComponent} from './admin-panel/tabs/tab-entity/edit-role-category/add-role-category/add-role-category.component';
import {AddRoleComponent} from './admin-panel/tabs/tab-entity/edit-role/add-role/add-role.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'person', children:
    [
      {path: '', component: PersonComponent},
      {path: 'edit', children:
        [
          {path: '', redirectTo: '/person', pathMatch: 'full'},
          {path: ':id', component: TabBasicInfoComponent}
        ]
      }
    ]
  },
  {path: 'team', children:
    [
      {path: '', component: TeamComponent},
      {path: 'edit', children:
        [
          {path: '', component: EditTeamComponent},
          {path: ':id', component: EditTeamComponent}
        ]
      }
    ]
  },
  {path: 'team-mate', component: TeamMateComponent},
  {path: 'mate-profile', children:
    [
      {path: '', redirectTo: '/home', pathMatch: 'full'},
      {path: ':id', children:
        [
          {path: '', component: MateProfileComponent},
          {path: 'dashboard', component: MateDashboardConfigComponent}
        ]
      }
    ]
  },
  {path: 'master', children:
    [
      {path: '', component: MasterComponent},
      {path: 'edit', children:
        [
          {path: '', component: EditMasterComponent},
          {path: ':id', component: EditMasterComponent}
        ]
      }
    ]
  },
  {path: 'login', component: LoginComponent},
  {path: 'admin', children:
    [
      {path: '', component: AdminPanelComponent},
      {path: 'scope', children:
        [
          {path: '', redirectTo: '/admin', pathMatch: 'full'},
          {path: ':id', component: TabAdminGestorScopeComponent}
        ]
      },
      {path: 'user', component: TabAdminGestorAddUserComponent},
      {path: 'master-class', component: AddMasterClassComponent},
      {path: 'team-group', component: AddTeamGroupComponent},
      {path: 'role', component: AddRoleComponent},
      {path: 'role-category', component: AddRoleCategoryComponent}
    ]
  },
  {path: 'locations', children:
    [
      {path: '', component: LocationComponent},
      {path: 'edit', children:
        [
          {path: '', component: EditLocationComponent},
          {path: ':id', component: EditLocationComponent}
        ]
      }
    ]
  },
  {path: 'issues', children: [
      {path: '', component: IssuesComponent},
      {path: ':id', component: IssuesDetailsComponent}
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {
}
