import {Component, OnInit, DoCheck} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {APP_NAME} from './shared/const/system.const';

import {AuthSrvService} from './shared/services/auth.service';
import {AlertService} from './shared/services/alert.service';
import {PersonModel} from "./shared/models/person.model";

const _ = require('lodash');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AuthSrvService, AlertService],
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class AppComponent implements OnInit, DoCheck {

  public selectedMenuOption: object;
  private _homeLink = {link: 'mate-profile', enabled: true};
  public teamMateLink = {name: 'Team Mates', link: 'team-mate', enabled: true};
  public menu: Array<object> = [
    {name: 'Buscar', icon: 'search', link: 'search', enabled: false},
    // {name: 'Notificaciones', icon: 'bell', link: 'notes', enabled: false},
    {name: 'Mi perfil', icon: 'male', link: this._homeLink.link, enabled: this._homeLink.enabled},
    {name: 'FAQ', icon: 'question-circle', link: 'faq', enabled: false},
    {name: 'AdminPanel', icon: 'cog', link: 'admin', enabled: true},
    {name: 'Logout', icon: 'sign-out', link: 'logout', enabled: true}
  ];

  public showMenu = false;
  public appName = APP_NAME;
  public person: PersonModel;

  private _navBarList: Array<object> = [
    {name: 'Personas', link: 'person', enabled: true},
    {name: 'Equipos', link: 'team', enabled: true},
    {name: 'Masters', link: 'master', enabled: true},
    {name: 'Localizaciones', link: 'locations', enabled: true},
    {name: 'Issues', link: 'issues', enabled: true}
  ];

  constructor(private _router: Router,
              private _authSrv: AuthSrvService,
              private _alertSrv: AlertService) {
    this.person = this._authSrv.getPersonLogged();
  }


  ngOnInit() {
    this._authSrv.verifySession();
    this._router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.selectedMenuOption = null;
        const urlSplit = event.url.split('/');
        if (this._homeLink.link === urlSplit[1]) {
          this.selectedMenuOption = this._homeLink;
        }
        if (_.isNil(this.selectedMenuOption) && this.teamMateLink.link === urlSplit[1]) {
          this.selectedMenuOption = this.teamMateLink;
        }
        if (_.isNil(this.selectedMenuOption)) {
          this.selectedMenuOption = this._navBarList.find(element => {
            return element['link'] === urlSplit[1];
          });
        }
        if (_.isNil(this.selectedMenuOption)) {
          this.selectedMenuOption = this.menu.find( element => {
            return element['link'] === urlSplit[1];
          });
        }
      }
    });
  }

  ngDoCheck() { }

  public showHamburgerMenu(): void {
    this.showMenu = !this.showMenu;
  }

  public isLogged(): boolean {
    return this._authSrv.isLogged();
  }

  public getNavBarRoutes(): Array<object> {
    return this._navBarList.filter((route) => {
      return this.isAllowed(route)
        ? this._authSrv.hasViewPermission(route['link'])
        : false;
    });
  }


  public isAllowed(route: object): boolean {
    return route.hasOwnProperty('enabled') && route['enabled'];
  }

  public goToTeamMate(): void {
    localStorage.setItem('selectedNode', null);
    this.routeTo(this._authSrv.hasViewPermission(this.teamMateLink['link'])
      ? this.teamMateLink
      : this._homeLink
    );
  }

  // @TODO: refactorizar cuando esten disponibles mas opciones del menu
  public routeTo(route: object): void {
    this.showMenu = false;

    if (!this._authSrv.hasViewPermission(route['link'])) {
      this._alertSrv.prompt('Acceso denegado: no tienes permisos para acceder a la página.');
    } else if (route['enabled']) {

      if (route['name'] === 'Mi perfil') {
        localStorage.setItem('mateProfileSelectedTab', '0');
      }

      switch (route['link']) {
        case 'logout':
          localStorage.setItem('logout', '1');
          this._router.navigateByUrl('/');
          break;
        case 'mate-profile':
          this._authSrv.goToProfile();
          break;
        default:
          this._router.navigateByUrl(route['link']);
      }

    } else {
      this._alertSrv.prompt('Funcionalidad pendiente de ESPECIFICACIONES.');
    }
  }

  public getMenuFiltered(): Array<object> {
    return this.menu.filter((route) => {
      return _.isNull(route['link']) || this._authSrv.hasViewPermission(route['link']);
    });
  }

  public hasDomain(): boolean {
    return this._authSrv.hasDomain();
  }

  public getDomainName(): string {
    return this._authSrv.hasDomain()
      ? 'Mi dominio: ' + this._authSrv.getDomainName()
      : '';
  }

  public hasSubDomain(): boolean {
    return this._authSrv.hasSubDomain();
  }

  public getSubDomain(): string {
    return this.hasSubDomain()
      ? ' / Mi subdominio: ' + this._authSrv.getSubDomainName()
      : '';
  }

  public isSelectedMenuOption(route: object): boolean {
    return !_.isNil(this.selectedMenuOption) && this.selectedMenuOption['link'] === route['link'];
  }

  /**
   * Get if person logged is MATE
   * @returns {boolean}
   */
  public isMate(): boolean {
    return this._authSrv.getPersonLogged().isMate();
  }
}
