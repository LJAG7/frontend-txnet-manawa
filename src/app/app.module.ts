import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {HttpModule, Http} from '@angular/http';

import {registerLocaleData} from '@angular/common';

import {AppRoutingModule} from './app-routing.module';

import {HomeModule} from './home/home.module';
import {PersonModule} from './person/person.module';
import {ComponentsModule} from './shared/components/components.module';
import {WikiModule} from './wiki/wiki.module';
import {TeamModule} from './team/team.module';
import {MasterModule} from './master/master.module';
import {CoreModule} from './core/core.module';
import {LoginModule} from './login/login.module';
import {LocationModule} from './location/location.module';
import {IssuesModule} from './issues/issues.module';
import {TeamMateModule} from './team-mate/team-mate.module';

import {TranslateModule, TranslateStaticLoader} from 'ng2-translate/ng2-translate';
import {TranslateLoader} from 'ng2-translate';

import {AuthSrvService} from './shared/services/auth.service';

import {AppComponent} from './app.component';
import {AdminPanelModule} from './admin-panel/admin-panel.module';

import {AlertComponent} from './shared/components/alert/alert.component';
import {AlertService} from './shared/services/alert.service';
import {MateProfileModule} from './mate-profile/mate-profile.module';
import {LoaderService} from './shared/services/loader.service';
import {LoaderComponent} from './shared/components/loader/loader.component';
import {DragulaModule} from 'ng2-dragula';

// Push notifications
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {PushNotificationsModule} from 'ng-push';
import {PushNotificationsComponent} from './push-notifications/push-notifications.component';

export function translateLoaderFactory(http: any) {
  return new TranslateStaticLoader(http, self !== top ? parent.location.origin + '/angular2/assets/i18n' : '../assets/i18n', '.json');
}

import localeEs from '@angular/common/locales/es';
registerLocaleData(localeEs, 'es');

import {CurrencyMaskModule} from 'ng2-currency-mask';
import {environment} from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    LoaderComponent,
    PushNotificationsComponent
  ],
  imports: [
    PushNotificationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    CurrencyMaskModule,
    BrowserModule,
    DragulaModule,
    HttpModule,
    AppRoutingModule,
    HomeModule,
    PersonModule,
    ComponentsModule,
    WikiModule,
    TeamModule,
    MasterModule,
    CoreModule,
    LoginModule,
    LocationModule,
    IssuesModule,
    TeamMateModule,
    MateProfileModule,
    AdminPanelModule,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: translateLoaderFactory,
      deps: [Http]
    })
  ],
  exports: [
    ComponentsModule
  ],
  providers: [AuthSrvService, AlertService, LoaderService, { provide: LOCALE_ID, useValue: 'es' }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
