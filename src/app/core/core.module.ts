import {BrowserModule} from '@angular/platform-browser';
import {NgModule, Optional, SkipSelf} from '@angular/core';
import {HttpModule} from '@angular/http';
import {CommonModule} from '@angular/common';
// Separate third party components from core
import {throwIfAlreadyLoaded} from './module-import-guard';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BrowserModule,
    HttpModule
  ],
  exports: [],
  providers: [],
  bootstrap: []
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}

