'use strict';

import {Component, OnInit} from '@angular/core';

import {AuthSrvService} from '../shared/services/auth.service';

const _ = require('lodash');

@Component({
  selector: 'app-home-component',
  templateUrl: './home.component.html',
  providers: [],
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  /************************ NATIVE METHODS  ************************/

  constructor(private _authSrv: AuthSrvService) {
    if (!_.isNil(localStorage.getItem('logout'))) {
      this._authSrv.logout();

    } else {
      this._authSrv.goToProfile();
    }
  }

  ngOnInit() { }
}

