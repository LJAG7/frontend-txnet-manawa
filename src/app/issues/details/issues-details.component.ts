'use strict';

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router'

import {HttpSrvService} from '../../shared/services/http.service';
import {LoaderService} from '../../shared/services/loader.service';

@Component({
  selector: 'app-issues-details-component',
  templateUrl: './issues-details.component.html',
  providers: [HttpSrvService],
  styleUrls: ['./issues-details.component.css']
})

export class IssuesDetailsComponent implements OnInit {

  /************************ COMPONENT VARS  ************************/

  public issue = {};

  /************************ CONFIG VARS  ************************/

  public basicInfoConfig = [
    {name: 'Identificador', key: 'id'},
    {name: 'Tiempo estimado', key: 'timetracking', keyChild: 'originalEstimate'},
    {name: 'Tiempo invertido', key: 'timetracking', keyChild: 'timeSpent'},
    {name: 'Tiempo restante', key: 'timetracking', keyChild: 'remainingEstimate'},
  ];

  /************************ NATIVE METHODS  ************************/

  constructor(private _activatedRoute: ActivatedRoute,
              private _router: Router,
              private _httpSrv: HttpSrvService,
              private _loaderSrv: LoaderService) {
    this._loaderSrv.general();
    this.getIssue();
  }

  ngOnInit() {
  }

  /************************ EVENTS METHODS ************************/

  public getIssueInfo(config: object): string | any {
    return this.issue.hasOwnProperty('id')
      ? config.hasOwnProperty('keyChild')
        ? this.issue[config['key']][config['keyChild']]
        : this.issue[config['key']]
      : '';
  }

  /**
   * Return to admin view
   */
  public goBackTo(): void {
    this._router.navigateByUrl('issues');
  }

  /************************ API METHODS  ************************/

  /**
   * Get issue
   */
  private getIssue(): void {
    this._httpSrv
      .get(
        'issues/' + this._activatedRoute['params']['value']['id']
      )
      .subscribe(
      (response) => this.onSuccessGetIssue(response)
    );
  }

  /**
   * On succes get issue
   * @param {Object} response
   */
  private onSuccessGetIssue(response: object): void {
    this._loaderSrv.stopGeneral();
    if (response['success']) {
      this.issue = response['object'];
    }
  }
}

