'use strict';

import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {DataTable, LazyLoadEvent} from 'primeng/primeng';

import {AuthSrvService} from '../shared/services/auth.service';
import {HttpSrvService} from '../shared/services/http.service';
import {LoaderService} from '../shared/services/loader.service';

const _ = require('lodash');

@Component({
  selector: 'app-issues-component',
  templateUrl: './issues.component.html',
  providers: [HttpSrvService],
  styleUrls: ['./issues.component.css']
})

/**
 * @whatItDoes Contains the methods for Issues to list and create this entity via API-endpoint
 * @author Elias Romero <elias.romero@steelmood.com>
 * @author Miguel Rico <miguel.rico@steelmood.com>
 */
export class IssuesComponent implements OnInit {

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('issuesDatatable') private _issuesDatatable: DataTable;

  /************************ COMPONENT VARS  ************************/

  public issues = [];

  /************************ CONFIG VARS  ************************/

  public pagination = {
    page: 1,
    rows: 4,
    sortField: 'name',
    sortOrder: 'asc',
    totalRecords: 0,
  };

  /************************ NATIVE METHODS  ************************/

  constructor(private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _loaderSrv: LoaderService,
              private _router: Router) {

    this._authSrv.verifyViewPermission('issues');
    this._loaderSrv.general();
  }

  ngOnInit() {
  }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('issues', tag);
  }

  /************************ EVENTS METHODS ************************/

  /**
   * Load issues lazy
   * @param {LazyLoadEvent} event
   */
  public loadIssuesLazy(event: LazyLoadEvent) {
    this.pagination.page = (event.first / event.rows) + 1;
    this.pagination.sortField = !_.isNil(event.sortField) ? event.sortField : this.pagination.sortField;
    const order = !_.isNil(event.sortOrder) ? event.sortOrder : 1;
    this.pagination.sortOrder = order === 1 ? 'asc' : 'desc';

    setTimeout(() => {
      if (this.issues) {
        this.getAllIssues();
      }
    }, 250);
  }

  /**
   * On click export datatable to csv
   */
  public onClickExportToCsv(): void {
    this.getAllIssuesToExport();
  }

  /**
   * On click issue (Select datatable row)
   * @param event
   */
  public onClickIssue(event: any): void {
    if (event
      && event.hasOwnProperty('data')
      && !_.isNil(event['data'])
      && !_.isEmpty(event['data'])) {
      this._router.navigateByUrl('/issues/' + event['data']['id']);
    }
  }

  /************************ API METHODS  ************************/

  /**
   * Get all issues
   */
  private getAllIssues(): void {
    this._httpSrv.get(
      'issues/',
      {
        page: this.pagination.page,
        elements: this.pagination.rows,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      }
    ).subscribe(
      (response) => this.onSuccessGetAllIssues(response)
    );
  }

  /**
   * On success get all issues
   * @param response
   * @param {boolean} exportToCsv
   */
  private onSuccessGetAllIssues(response: any, exportToCsv: boolean = false): void {
    this._loaderSrv.stopGeneral();
    if (!_.isNil(response.object) && response.object.items.length > 0) {
      this.issues = response.object.items;
      this.pagination.totalRecords = response.object.total_count;
    }

    if (exportToCsv) {
      setTimeout(() => {
        this._issuesDatatable.exportCSV();
        this.getAllIssues();
      }, 300);
    }
  }

  /**
   * Get all issues to export
   */
  private getAllIssuesToExport(): void {
    this._httpSrv.get(
      'issues/',
      {
        page: 1,
        elements: this.pagination.totalRecords,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      },
      'Listado issues a exportar'
    ).subscribe(
      (response) => this.onSuccessGetAllIssues(response, true)
    );
  }
}

