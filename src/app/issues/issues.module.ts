/*GENERAL ASSETS*/
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';


import {IssuesComponent} from './issues.component';
import {IssuesDetailsComponent} from './details/issues-details.component';
import {HttpSrvService} from '../shared/services/http.service';
import {ComponentsModule} from '../shared/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    ComponentsModule
  ],
  exports: [
    IssuesComponent,
    IssuesDetailsComponent
  ],
  declarations: [
    IssuesComponent,
    IssuesDetailsComponent
  ],
  providers: [
    HttpSrvService
  ]
})

export class IssuesModule {
}
