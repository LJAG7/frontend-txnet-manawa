'use strict';

import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgModel} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';

import {TranslateService, TranslatePipe} from 'ng2-translate/ng2-translate';

import {ValidationObjectPipe} from '../../shared/pipes/validation.pipe';

import {REGEXPATTERNSHTML, REGEXPATTERNS} from '../../shared/const/system.const';
import {BaseComponentModel} from '../../shared/models/base-component/base-component.class';

import {LocationModel} from '../../shared/models/location.model';

import {AlertService} from '../../shared/services/alert.service';
import {AuthSrvService} from '../../shared/services/auth.service';
import {HttpSrvService} from '../../shared/services/http.service';
import {LoaderService} from '../../shared/services/loader.service';

const _ = require('lodash');


@Component({
  selector: 'app-edit-location-component',
  templateUrl: './edit-location.component.html',
  styleUrls: ['./edit-location.component.css'],
  providers: [
    HttpSrvService,
    TranslateService,
    TranslatePipe,
    ValidationObjectPipe
  ]
})

export class EditLocationComponent extends BaseComponentModel implements OnInit {

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('locationForm') private locationForm;

  /************************ COMPONENT VARS  ************************/

  public urlMap: any = null;
  public htmlPatterns;

  public location: LocationModel = new LocationModel();
  public disabledEdition = false;
  public buttonTitle = 'Actualizar';
  public googleMapUrl = 'https://www.google.es/maps/place/Steelmood+Huelva/@37.2540045,-6.9576686,17z/data=!3m1!4b1!4m5!3m4!1s0xd11d02fd2003ac3:0xa3177c0c101ad7e!8m2!3d37.2540045!4d-6.9554746';

  private _countryCode = 'ESP';
  private _updateFirstTry = false;
  private _viewMode: string;

  private _isLocationDomain = false;

  /************************ CONFIG VARS ************************/

  private _neededFromApi: Array<string> = ['location'];

  private _searchLocationConfigs = {
    postalCode: {
      title: 'postal_code',
      keyToFilter: 'postal_code',
      alternativeKeysToFilter: [],
      titleOption: 'text',
      flags: {
        dataOnLive: true,
        hasWhiteBackground: true,
        showResultList: true,
        showResultListUnderClick: true
      },
      error: {
        show: false
      },
      icon: {
        hasIconRight: false,
        hasSearchIcon: false,
        hideIconOnSearch: true
      },
      input: {
        placeholder: '',
        value: null
      },
      css: {
        extraClasses: 'form-style list-size-4x'
      }
    },
    city: {
      title: 'municipality',
      keyToFilter: 'municipality',
      alternativeKeysToFilter: [],
      titleOption: 'text',
      flags: {
        dataOnLive: true,
        hasWhiteBackground: true,
        showResultList: true,
        showResultListUnderClick: true
      },
      error: {
        show: false
      },
      icon: {
        hasIconRight: false,
        hasSearchIcon: false,
        hideIconOnSearch: true
      },
      input: {
        placeholder: '',
        value: null
      },
      css: {
        extraClasses: 'form-style list-size-2x'
      }
    }
  };

  public searchLocationList = {
    data: [],
    timer: null,
    restart: {
      postal_code: 0,
      city: 0,
      province: 0
    }
  };

  private _timeoutKeyup = {
    first: 250,
    last: 500
  };

  /************************ NATIVE METHODS ************************/

  constructor(private _activedRoute: ActivatedRoute,
              private _alertSrv: AlertService,
              private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _loaderSrv: LoaderService,
              private _translateSrv: TranslateService,
              private _router: Router,
              private _domSanitizer: DomSanitizer,
              private _validationObjectPipe: ValidationObjectPipe) {
    super();

    this.config.id = 'edit_location_component_n'.concat(Math.floor(Math.random() * 100000).toString());

    this._authSrv.verifyViewPermission('locations/edit');
    _translateSrv.setDefaultLang('es');
    _translateSrv.use('es');

    this._loaderSrv.general(this._neededFromApi);

    if (this.isAllowed()) {

      this.detectLocationType(localStorage.getItem('EditLocationComponent'));

      if (!_.isNil(this._activedRoute.params['value']['id'])
        && !_.isEmpty(this._activedRoute.params['value']['id'])) {
        this._viewMode = 'EDIT';
        this.buttonTitle = 'Actualizar';
        this.getLocation();

      } else {
        this._viewMode = 'CREATE';
        this.buttonTitle = 'Crear';
        this.location.country = 'España';
        this.updateMap();
        setTimeout( () => {
          this.checkNeededFromApi('location');
        }, 300);
      }
    }
  }

  ngOnInit() {
    this.htmlPatterns = Object.assign({}, REGEXPATTERNSHTML);
    this.htmlPatterns.postalCode =  this.htmlPatterns.postalCode[this._countryCode];
  }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('locations/edit', tag);
  }

  /************************ SETUP METHODS ************************/

  /**
   * Detect the endpoint to call for create or edit location
   * @param {string} keyStorage
   */
  private detectLocationType(keyStorage: string): void {
    if (keyStorage && keyStorage === 'isDomainLocation') {
      this._isLocationDomain = true;
      localStorage.removeItem('EditLocationComponent');
    }
  }



  /**
   * Get search city config
   * @returns {Object}
   */
  public getSearchCityConfig(): object {
    return this._searchLocationConfigs.city;
  }

  /**
   * Get search location
   * @returns {Object}
   */
  public getSearchLocationList(): Array<any> {
    return this.searchLocationList.data;
  }

  /**
   * Get search postal codes
   * @returns {Object}
   */
  public getSearchPostalCodesConfig(): object {
    return this._searchLocationConfigs.postalCode;
  }

  /************************ EVENTS METHODS ************************/

  /**
   * Return if has available map to render it
   * @returns {boolean}
   */
  public hasMap(): boolean {
    return !_.isNil(this.urlMap);
  }

  /**
   * Get css error class
   * @param input
   * @returns {string}
   */
  public getClassCssErrorInput(input: any): string {
    return this._updateFirstTry && this.hasErrorInput(input)
      ? 'invalid-field'
      : '';
  }

  /**
   * Get error message
   * @param {NgModel} input
   * @returns {string}
   */
  public getErrorInput(input: NgModel): string {
    if (this.hasErrorInput(input)) {
      if (input.getError('required')) {
        return 'Requerido';
      }

      if (input.getError('minlength')) {
        return 'Mínimo ' + input.errors.minlength.requiredLength + ' caracteres';
      }

      if (input.getError('maxlength')) {
        return 'Máximo ' + input.errors.maxlength.requiredLength + ' caracteres';
      }

      if (input.getError('pattern')) {
        return 'Formato inválido'.concat(
          input.hasOwnProperty('name') && (input.name === 'latitude' || input.name === 'longitude')
          ? ': sólo números separados por .'
          : ''
        );
      }
    }
    return '';
  }

  /**
   * Return to list of location
   */
  public goBackToList(): void {
    this._router.navigateByUrl('/locations');
  }

  /**
   * Translate according key passed as param
   * @param {string} tag
   * @param {string} page
   * @returns {string}
   */
  public getTagTranslated(tag: string, page: string = 'LOCATION'): string {
    return this._translateSrv.instant(page)[tag];
  }

  /**
   * Return if an input has errors
   * @param input
   * @returns {boolean}
   */
  public hasErrorInput(input: any): boolean {
    return this._updateFirstTry && !_.isNull(input.errors) && (input.dirty || input.touched);
  }

  /**
   * Check form to find errors
   * @returns {boolean}
   */
  private hasErrorForm(): boolean {
    return this.locationForm.invalid;
  }

  /**
   * Is valid location model
   * @returns {boolean}
   */
  public isValidLocationModel(): boolean {
    return this.location
      && this.location.hasOwnProperty('id')
      && Number(this.location.id) !== -1
      && this.location.hasOwnProperty('latitude')
      && REGEXPATTERNS.coordinates.test(
        !_.isNil(this.location.latitude) ? this.location.latitude.toString() : ''
        )
      && this.location.hasOwnProperty('longitude')
      && REGEXPATTERNS.coordinates.test(
        !_.isNil(this.location.latitude) ? this.location.latitude.toString() : ''
        );
  }

  /**
   * On click over search postal codes result list
   * @param {Object | any} event
   * @param {NgModel} input
   */
  public onClickSearchLocation(event: object | any,
                               input: NgModel): void {
    if (event['event'] === 'keyup' && !_.isEmpty(event['currentText'])) {
      clearTimeout(this.searchLocationList.timer);

      const searchTimer = _.isEmpty(this.searchLocationList.data)
        ? this._timeoutKeyup.first
        : this._timeoutKeyup.last;

      this.searchLocationList.timer = setTimeout(() => {
        this.getPostalCodes(event['currentText']);
      }, searchTimer);


    } else if (event['event'] === 'blur') {
      this.location[input.name] = event['currentText'];
    }
  }

  /**
   * On change coordinates
   * @param value
   */
  public onChangeCoordinates(value): void {
    if (REGEXPATTERNS.coordinates.test(value)) {
      this.updateMap();

    } else {
      this._alertSrv.error(this.getTagTranslated('errorCoord', 'FORM'));
      this.checkFormFields();
    }
  }

  /**
   * On click postal codes option. Double data bindin will update the searchs values default
   * @param {Object | any} event
   */
  public onSelectedSearchLocation(event: object | any): void {
    if (this._validationObjectPipe.isValidValueObjectProperty(event, 'element')) {
      this.location.postal_code = event['element']['postal_code'];
      this.location.province = event['element']['province'];
      this.location.city = event['element']['municipality'];
    }
  }

  /**
   * Update location
   */
  public updateLocation() {
    this._updateFirstTry = true;

    if (!this.hasErrorForm()) {
      if (this._viewMode === 'CREATE') {
        this._httpSrv.post(
          !this._isLocationDomain ? 'locations/' : 'domains/' + this._authSrv.getDomainId() + '/location/',
          this.location
        ).subscribe(
          success => this.onSuccessUpdateLocation(success)
        );

      } else if (this._viewMode === 'EDIT' && !this.disabledEdition) {
        this._httpSrv.patch(
          'locations/' + this._activedRoute.params['value']['id'],
          this.location
        ).subscribe(
          success => this.onSuccessUpdateLocation(success)
        );
      } else {
        this._alertSrv.info('No puede editar localizaciones que no pertenezcan a su dominio.');
      }
    } else {
      this.checkFormFields();
    }
  }

  /**
   * Show error from message
   * @returns {boolean}
   */
  public showErrorFromMessage(): boolean {
    return this._updateFirstTry && this.hasErrorForm();
  }


  /************************ API METHODS ************************/

  /**
   * Get the team type information of current team type
   */
  private getLocation(): void {
    this._httpSrv
      .get(
        'locations/' + this._activedRoute.params['value']['id'],
        undefined,
        'Localización'
      ).subscribe(
      success => this.onSuccessGetLocation(success)
    );
  }

  /**
   * On success get location
   *
   * @param response
   */
  private onSuccessGetLocation(response: Object): void {

    this.checkNeededFromApi('location');

    if (response['success']) {
      this.location = new LocationModel(response['object']);
      this.disabledEdition = this._authSrv.getPersonLogged().domain.id !== this.location.domain.id;
      this.updateMap();

    } else {
      setTimeout(() => {
        this.goBackToList();
      }, 3000);

    }
  }

  /**
   * Return a list of CP and cities
   * @param {string} text
   */
  private getPostalCodes(text: string): void {
    this._httpSrv.get(
      'postalcodes/find/',
      {
        page: 1,
        elements: 50,
        cp: text
      }
    ).subscribe(
      success => this.onSuccessGetPostalCodes(success)
    );
  }

  /**
   * On success get postal codes
   * @param response
   */
  private onSuccessGetPostalCodes(response): void {

    if (this._validationObjectPipe.isValidValueObjectProperty(response, 'success')
      && this._validationObjectPipe.isValidValueObjectProperty(response, 'object')
      && response['object'].hasOwnProperty('items')) {
      this.searchLocationList.data = _.cloneDeep(response['object']['items']);
    }
  }

  /**
   * On success update location
   * @param response
   */
  private onSuccessUpdateLocation(response: any): void {
    if (!_.isNil(response.object) && response['success']) {
      this.location = new LocationModel(
        this._viewMode === 'CREATE' && this._isLocationDomain
          ? response['object']['main_location']
          : response['object']
      );

      if (this._viewMode === 'CREATE' || this._isLocationDomain) {

        if (this._isLocationDomain) {
          this._loaderSrv.softRedirecting();
        }

        setTimeout(() => {
          this._router.navigateByUrl(
            this._isLocationDomain
              ? '/admin'
              : '/locations/edit/' + this.location.id);
        }, 1000);
      }
    }
  }

  /**
   * Check needed from api
   * @param {string} value
   */
  private checkNeededFromApi(value: string): void {
    if (this._neededFromApi.indexOf(value) !== -1) {
      this._neededFromApi.splice(this._neededFromApi.indexOf(value), 1);
      this._loaderSrv.setNeededFromApi(this._neededFromApi);
    }
  }

  /************************ VALIDATION METHODS  ************************/

  /**
   * Touch all field for launch validation process
   */
  private checkFormFields(): void {
    _.forIn(this.locationForm.control.controls, (input) => {
      if (input.invalid === true) {
        input.markAsTouched();
      }
    });
  }

  /************************ MAP FUNCTIONS ************************/

  /**
   * Updates the coordiantes for maps
   *
   */
  private updateMap(): void {
    const coord = {
      lat: _.isNil(this.location.latitude) || !this.isValidLocationModel()
        ? 0 // 40.415800
        : this.location.latitude,
      lon: _.isNil(this.location.longitude) || !this.isValidLocationModel()
        ? 0 // -3.710414
        : this.location.longitude,
      zoom: _.isNil(this.location.latitude) || _.isNil(this.location.longitude) || !this.isValidLocationModel()
        ? 1
        : 14
    };

    this.urlMap = this._domSanitizer.bypassSecurityTrustResourceUrl('https://maps.google.com/maps?q='
      + coord.lat + ',' + coord.lon + '&hl=es;z=14&output=embed');
  }
}
