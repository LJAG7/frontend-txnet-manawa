'use strict';

import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {DataTable, LazyLoadEvent} from 'primeng/primeng';

import {TranslateService, TranslatePipe} from 'ng2-translate/ng2-translate';

import {LocationModel} from '../shared/models/location.model';

import {AuthSrvService} from '../shared/services/auth.service';
import {HttpSrvService} from '../shared/services/http.service';
import {LoaderService} from '../shared/services/loader.service';

const _ = require('lodash');

@Component({
  selector: 'app-location-component',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css'],
  providers: [
    HttpSrvService,
    TranslateService,
    TranslatePipe
  ]
})

/**
 * @whatItDoes Contains the methods for Locations to list and create this entity via API-endpoint
 * @author Elias Romero <elias.romero@steelmood.com>
 * @author Miguel Rico <miguel.rico@steelmood.com>
 */
export class LocationComponent implements OnInit, AfterViewInit {

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('locationsDatatable') private _locationsDatatable: DataTable;

  /************************ COMPONENT VARS  ************************/

  // Static vars for static methods

  public getLocationCompleteAddress = LocationComponent.getLocationCompleteAddress;

  // Component vars

  public locations: Array<LocationModel> = [];

  /************************ CONFIG VARS  ************************/

  public pagination = {
    page: 1,
    rows: 10,
    sortField: 'name',
    sortOrder: 'asc',
    totalRecords: 0,
  };

  /************************ STATIC METHODS  ************************/

  /**
   *
   * @param {LocationModel} location
   * @returns {string}
   */
  private static getLocationCompleteAddress(location: LocationModel): string {
    return location.getCompleteAddress();
  }

  /************************ NATIVE METHODS  ************************/

  constructor(private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _loaderService: LoaderService,
              private _router: Router,
              private _translateSrv: TranslateService) {

    this._authSrv.verifyViewPermission('locations');
    this._translateSrv.setDefaultLang('es');
    this._translateSrv.use('es');
    this._loaderService.general();
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    if (!_.isNil(this._locationsDatatable)) {
      this._locationsDatatable.resolveFieldData = function(data, field) {
        if (data && field) {
          if (field === 'latitude' && _.isNil(data[field])) {
            return '';
          } else if (field === 'longitude' && _.isNil(data[field])) {
            return '';
          } else if (field === 'address') {
            return LocationComponent.getLocationCompleteAddress(data);
          } else {
            const fields = field.split('.');
            let value = data;
            for (let i = 0, len = fields.length; i < len; ++i) {
              if (value == null) {
                return '';
              }
              value = value[fields[i]];
            }
            return value;
          }
        } else {
          return null;
        }
      };
    }
  }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('locations', tag);
  }

  /************************ EVENTS METHODS ************************/

  /**
   * Create location
   */
  public createLocation(): void {
    this._router.navigateByUrl('/locations/edit/');
  }

  /**
   * Load locations lazy
   * @param {LazyLoadEvent} event
   */
  public loadLocationsLazy(event: LazyLoadEvent) {
    this.pagination.page = (event.first / event.rows) + 1;
    this.pagination.sortField = !_.isNil(event.sortField) ? event.sortField : this.pagination.sortField;
    const order = !_.isNil(event.sortOrder) ? event.sortOrder : 1;
    this.pagination.sortOrder = order === 1 ? 'asc' : 'desc';

    setTimeout(() => {
      if (this.locations) {
        this.getAllLocations();
      }
    }, 250);
  }

  /**
   * On click export datatable to csv
   */
  public onClickExportToCsv(): void {
    this.getAllLocationsToExport();
  }

  /**
   * On click location (Select datatable row)
   * @param event
   */
  public onClickLocation(event: object): void {
    if (!_.isNil(event) && event.hasOwnProperty('data')
      && event['data'].hasOwnProperty('id')) {
      this._router.navigateByUrl('/locations/edit/' + event['data']['id']);
    }
  }

  /************************ API METHODS  ************************/

  /**
   * Get all locations
   */
  private getAllLocations(): void {
    this._httpSrv.get(
      'locations/',
      {
        page: this.pagination.page,
        elements: this.pagination.rows,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      }
    ).subscribe(
      success => this.onSuccessGetAllLocations(success)
    );
  }

  /**
   * On success get all locations
   * @param response
   * @param {boolean} exportToCsv
   */
  private onSuccessGetAllLocations(response: any, exportToCsv: boolean = false): void {
    this._loaderService.stopGeneral();
    if (!_.isNil(response.object)
      && !_.isNil(response['object'].items)) {
      this.locations = [];
      response.object.items.forEach( item => {
        this.locations.push(new LocationModel(item));
      });
      this.pagination.totalRecords = response.object.total_count;
    }

    if (exportToCsv) {
      setTimeout(() => {
        this._locationsDatatable.exportCSV();
        this.getAllLocations();
      }, 300);
    }
  }

  /**
   * Get all locations to export
   */
  private getAllLocationsToExport(): void {
    this._httpSrv.get(
      'locations/',
      {
        page: 1,
        elements: this.pagination.totalRecords,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      },
      'Listado masters a exportar'
    ).subscribe(
      success => this.onSuccessGetAllLocations(success, true)
    );
  }
}
