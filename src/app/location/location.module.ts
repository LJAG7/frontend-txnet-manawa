/*GENERAL ASSETS*/
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule, Http} from '@angular/http';
/*MODULE ASSETS*/
import {LocationComponent} from './location.component';
import {EditLocationComponent} from './edit/edit-location.component';

/*SERVICES*/
// import {HttpSrvService} from 'app/services/httpSrv/http-srv.service';
// import {LogService} from 'app/services/log.service';

import {ComponentsModule} from '../shared/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    ComponentsModule
  ],
  exports: [
  ],
  declarations: [
    LocationComponent,
    EditLocationComponent
  ],
  providers: [
    // HttpSrvService,
  ]
})

export class LocationModule {
}

