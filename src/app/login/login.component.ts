'use strict';

import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';

import {AuthService} from 'angular2-social-login';

import {APP_NAME, REGEXPATTERNSHTML} from '../shared/const/system.const';

import {AuthSrvService} from '../shared/services/auth.service';
import {HttpNoJwtSrvService} from '../shared/services/noJwtHttp.service';
import {AlertService} from '../shared/services/alert.service';
import {NgModel} from "@angular/forms";
import {HttpSrvService} from "../shared/services/http.service";

const _ = require('lodash');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [HttpNoJwtSrvService],
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  public appName = APP_NAME;
  public socialLogins = [
    {id: 1, name: 'google', enable: false},
    {id: 2, name: 'facebook', enable: false},
    {id: 3, name: 'linkedin', enable: false},
  ];


  public formLoginData = {
    email: {
      value: '',
      backupValue: '',
      validations: [
        {type: 'required', message: 'Campo obligatorio.'},
        {type: 'maxlength', message: 'No puede tener más de 255'},
        {type: 'pattern', message: 'Nombre de usuario o email no válido'}
        ],
      checker: {
        timer: null,
        status: null
      },
      style: 'lowerCase',
      error: null,
      pattern: REGEXPATTERNSHTML.username
    },
    password: {
      value: '',
      backupValue: '',
      validations: [
        {type: 'required', message: 'Campo obligatorio.'},
        {type: 'minlength', message: 'Debe tener al menos 5 caracteres.'},
        {type: 'maxlength', message: 'No puede tener más de 255'},
        {type: 'pattern', message: 'Caracteres permitidos: alfanuméricos _ . # @ -'}
      ],
      checker: {
        timer: null,
        status: null
      },
      style: null,
      error: null,
      pattern: REGEXPATTERNSHTML.password
    }
  };

  public formSignUpData = {
    username: {
      value: '',
      backupValue: '',
      validations: [
        {type: 'required', message: 'Campo obligatorio.'},
        {type: 'maxlength', message: 'No puede tener más de 255'},
        {type: 'minlength', message: 'Debe tener al menos 3 caracteres.'},
        {type: 'pattern', message: 'Caracteres permitidos: alfanuméricos _ .'}
      ],
      validationsApi: {
        url: 'persons/InputValidator/username',
        message: 'El nombre de usuario ya existe.'
      },
      checker: {
        timer: null,
        status: null
      },
      style: 'lowerCase',
      error: null,
      pattern: REGEXPATTERNSHTML.alphanumSpecialPoint
    },
    email: {
      value: '',
      backupValue: '',
      validations: [
        {type: 'required', message: 'Campo obligatorio.'},
        {type: 'maxlength', message: 'No puede tener más de 255'},
        {type: 'pattern', message: 'Email no válido'}
      ],
      validationsApi: {
        url: 'persons/InputValidator/email',
        message: 'El email ya existe.'
      },
      checker: {
        timer: null,
        status: null
      },
      style: 'lowerCase',
      error: null,
      pattern: REGEXPATTERNSHTML.email
    },
    password: {
      value: '',
      backupValue: '',
      validations: [
        {type: 'required', message: 'Campo obligatorio.'},
        {type: 'maxlength', message: 'No puede tener más de 255'},
        {type: 'minlength', message: 'Debe tener al menos 6 caracteres.'},
        {type: 'pattern', message: 'Caracteres permitidos: alfanuméricos _ . # @ -'}
      ],
      checker: {
        timer: null,
        status: null
      },
      style: null,
      error: null,
      pattern: REGEXPATTERNSHTML.password
    },
    domain: {
      value: '',
      backupValue: '',
      validations: [
        {type: 'required', message: 'Campo obligatorio.'},
        {type: 'maxlength', message: 'No puede tener más de 255'},
        {type: 'minlength', message: 'Debe tener al menos 3 caracteres.'},
      ],
      checker: {
        timer: null,
        status: null
      },
      style: null,
      error: null,
      copy: {
        field: 'url',
        pattern: [/\W+/g, '']
      },
      pattern: REGEXPATTERNSHTML.range3_255
    },
    url: {
      value: '',
      backupValue: '',
      validations: [
        {type: 'required', message: 'Campo obligatorio.'},
        {type: 'maxlength', message: 'No puede tener más de 255'},
        {type: 'minlength', message: 'Debe tener al menos 3 caracteres.'},
        {type: 'pattern', message: 'Sólo debe contener caracteres alfanuméricos y _'},
      ],
      validationsApi: {
        url: 'domains/InputValidator/url',
        message: 'La url del dominio ya existe.'
      },
      checker: {
        timer: null,
        status: null
      },
      style: 'lowerCase',
      error: null,
      pattern: REGEXPATTERNSHTML.alphanumSpecialMin3
    }
  };

  private _isSign = {doIt: false, social: false};
  private _errorRequired = 'Campo obligatorio.';
  private _tabId = 'login';
  private _textbox;
  private _toggler;
  private _timeout = 500;

  @ViewChild('containerLogin') private _containerLogin;
  @ViewChild('containerRegister') private _containerRegister;

  @ViewChild('loginForm') private _loginForm;
  @ViewChild('registerForm') private _registerForm;


  /**
   * show the loading icon for a specific input
   * @param {Object} field
   */
  private static iniLoadingChecker(field: object): void {
    field['checker']['status'] = {checking: true};
  }

  /**
   * clear checker
   * @param {Object} field
   */
  private static clearCheckerStatus(field: object): void {
    field['checker']['status'] = null;
  }

  // ************ NATIVE METHODS ************ //
  constructor(private _httpNoJwt: HttpNoJwtSrvService,
              private _auth: AuthService,
              private _authSrv: AuthSrvService,
              private _alertSrv: AlertService,
              private _el: ElementRef) {
    this.onCheckLogin();
  }

  ngOnInit() {
    if (this._containerLogin) {
      setTimeout(() => {
        this._containerLogin.nativeElement.classList.add('fadeIn');
        this._containerRegister.nativeElement.classList.add('fadeIn');
      }, 500);
    }
  }


  // ************ API METHODS ************ //

  public loginWithSn(provider, $event): void {
    if (provider['enable']) {
      this._auth
        .login(provider['name'])
        .subscribe((response) => {
            response
            && response.hasOwnProperty('name')
            && response.hasOwnProperty('email')
            && response.hasOwnProperty('provider')
            && response.hasOwnProperty('uid')
              ? this._authSrv
                .loginWithSn(response)
                .subscribe((callback) => {
                  this._isSign = {doIt: true, social: true};
                  callback
                  && callback['success']
                  && callback['auth']
                    ? this.onCheckLogin()
                    : this.onFailLoginSocialNetwork(callback);
                })
              : this.onFailLoginSocialNetwork(response);
          }
        );
    } else {
      this.functionalityNotImplemented('Funcionalidad pendiente de integración');
    }
  }

  private onFailLoginSocialNetwork(response: object | any): void {
    this._isSign = {doIt: false, social: true};
    this._alertSrv.error(response.hasOwnProperty('message') && response['message']
      ? response['message']
      : 'Fallo en la autenticación con la red social. Inténtalo de nuevo.');
  }

  private onCheckLogin(): void {
    this._authSrv.goToProfile();
  }

  private onCheckRegister(): void {
    this._authSrv.goToAdminPanel();
  }


  public logUser(): void {
    this._isSign = {doIt: true, social: false};
    this._authSrv
      .login(this.formLoginData.email.value, this.formLoginData.password.value)
      .subscribe(
        success => this.onSuccessLogin(success),
        fail => this.onFailLogin(fail)
      );
  }

  private onSuccessLogin(response: object | any): void {
    this._isSign = {doIt: false, social: false};
    response['success']
      ? this.onCheckLogin()
      : this.onFailLogin(response);
  }

  private onFailLogin(response: object | any): void {
    this._isSign = {doIt: false, social: false};
    this._alertSrv.error(response.hasOwnProperty('message') && response['message']
      ? response['message']
      : 'Fallo en la autenticación. Inténtalo de nuevo.');
  }


  /**
   * Api call to register user and domain
   */
  private registerUser(): void {
    this._isSign = {doIt: true, social: false};
    this._httpNoJwt.call(
      'post',
      'tokens/signin/domain/',
      {
        person: {
          email: this.formSignUpData.email.value,
          username: this.formSignUpData.username.value.toLowerCase(),
          password: this.formSignUpData.password.value
        },
        domain: {
          name: this.formSignUpData.domain.value,
          url_domain: this.formSignUpData.url.value.toLowerCase()
        }
      }
    ).subscribe(
      success => this.onSuccessRegister(success),
      fail => this.onFailRegister(fail)
    );
  }

  private onSuccessRegister(response: object | any): void {
    this._isSign = {doIt: false, social: false};
    response['success']
      ? this.firstLogUser()
      : this.onFailRegister(response);
  }

  public firstLogUser(): void {
    this._isSign = {doIt: true, social: false};
    this._authSrv
      .login(this.formSignUpData.email.value, this.formSignUpData.password.value)
      .subscribe(
        success => this.onSuccessFirstLogin(success),
        fail => this.onFailLogin(fail)
      );
  }

  private onSuccessFirstLogin(response: object | any): void {
    this._isSign = {doIt: false, social: false};
    response['success']
      ? this.onCheckRegister()
      : this.onFailLogin(response);
  }

  private onFailRegister(response: object | any): void {
    this._isSign = {doIt: false, social: false};
    this._alertSrv.error(response.hasOwnProperty('message') && response['message']
      ? response['message']
      : 'Fallo en el registro. Inténtalo de nuevo.');
  }

  /**
   * Api call to validate user data has unique in system
   * @param {Object} field
   * @param {Object | any} input
   */
  private validateFromApi(field: object,
                          input: object | any): void {
    LoginComponent.iniLoadingChecker(field);
    this._httpNoJwt.call(
      'get',
      field['validationsApi']['url'] + '?value=' + encodeURIComponent(field['value'])
    ).subscribe(
      success => this.onSuccessValidateFromApi(success, field, input),
      fail => this.onFailValidateFromApi(fail, field, input)
    );
  }


  private onSuccessValidateFromApi(response: object,
                                      field: object,
                                      input: object | any): void {
    // console.log(response);
    if (response.hasOwnProperty('object')
    && response['object'].hasOwnProperty('valid')) {
      field['checker']['status'] = response['object'];
      field['backupValue'] = field['value'];

      if (!response['object']['valid']) {
        field['error'] = this.validateFieldMessage(field, input);
      }

    } else {
      LoginComponent.clearCheckerStatus(field);
    }

  }


  private onFailValidateFromApi(response: object | any,
                                field: object,
                                input: object | any): void {
    LoginComponent.clearCheckerStatus(field);
    field['error'] = this.validateFieldMessage(field, input);
  }



  // ************ EVENTS METHODS ************ //
  /**
   * Submit actions for login form
   */
  public onSubmitLoginForm(): void {
    if (this.isCompleteForm(this.formLoginData)) {
      this.logUser();

    } else {
      this.highlightRequiredFields(this.formLoginData);
    }
  }

  /**
   * Submit actions for register form
   */
  public onSubmitRegisterForm(): void {
    if (this.isCompleteForm(this.formSignUpData)) {
      this.registerUser();

    } else {
      this.highlightRequiredFields(this.formSignUpData);
    }
  }

  /**
   * Actions when keys press in login form inputs
   * @param {Object | any} event
   * @param {Object} field
   * @param {Object | any} input
   */
  public captureIntroEvent(event: object | any,
                           field: object,
                           input: object | any): void {
    event.stopPropagation();
    event.preventDefault();

    if (event && event['keyCode'] === 13) {
      setTimeout( () => {
        this.onSubmitLoginForm();
      }, 300);
    } else {
      this.onLiveValidate(event, field, input);
    }
  }

  /**
   * REset a indicated input
   * @param {Object} field
   * @param {Object | any} input
   */
  public resetInputAssociated(field: object,
                              input: any): void {
    field['value'] = '';
    field['checker']['status'] = null;
    field['blocked'] = null;
    field['error'] = null;
    input.reset();
  }


  /**
   * Copy a value input into another input, with regex replace parser
   * @param {Object | any} field
   * @param {Object | any} copyField
   * @param {Object | any} copyInput
   */
  private copyFieldInDestiny(field: object | any,
                             copyField: object | any = null,
                             copyInput: object | any = null): void {

    if (_.isNull(copyField)
        || _.isNull(copyInput)
        || !field.hasOwnProperty('copy')
        || _.isNull(field['copy'])
        || !this.formSignUpData[field['copy']['field']].hasOwnProperty('blocked')
        || this.formSignUpData[field['copy']['field']]['blocked']) {

      return;
    }

    this.formSignUpData[field['copy']['field']]['value'] = field['value']
      .replace(
        field['copy']['pattern'][0],
        field['copy']['pattern'][1]
      );

    this.formSignUpData[field['copy']['field']]['backupValue'] = this.formSignUpData[field['copy']['field']]['value'];
    this.inputLiveValidate(copyField, copyInput);
  }

  /**
   * Apply style defined in field widgetConfig
   * @param {Object} field
   */
  private applyFieldStyle(field: object): void {
    if (!_.isEmpty(field['value']) && field['style'] === 'lowerCase') {
      field['value'] = field['value'].toLowerCase();
    }
  }

  /**
   * Make the combo validation: native and apiCall
   * @param {Object} field
   * @param {Object | any} input
   */
  private inputLiveValidate(field: object,
                            input: object | any): void {

    this.applyFieldStyle(field);

    if (!this.hasErrorInput(input)) {
      field['error'] = null;
    }

    clearTimeout(field['checker']['timer']);
    LoginComponent.clearCheckerStatus(field);
    field['checker']['timer'] = setTimeout(() => {
      if (field.hasOwnProperty('validationsApi') && !this.hasErrorInput(input)) {
        this.validateFromApi(field, input);

      } else {
        field['backupValue'] = field['value'];
        field['error'] = this.validateFieldMessage(field, input);
      }
    }, this._timeout);
  }

  /**
   * Compare the last value with the old value recorded
   * @param {Object} field
   * @returns {boolean}
   */
  private isChangedValue(field: object): boolean {
    return field['value'] !== field['backupValue'];
  }

  /**
   * Validation for input. Generate the associated error if exist
   * @param {Object | any} event
   * @param {Object} field
   * @param {Object | any} input
   * @param {Object | any} copyField
   * @param {Object | any} copyInput
   */
  public onLiveValidate(event: object | any,
                        field: object,
                        input: object | any,
                        copyField: object | any = null,
                        copyInput: object | any = null): void {
    event.stopPropagation();
    event.preventDefault();

    if (_.isEmpty(_.trim(input.value))) {
      return;
    }

    if ((input.name === 'user_register' && (input.value.indexOf('..') !== -1) || input.value.indexOf('__') !== -1)
      || (input.name === 'email_register' && (input.value.indexOf('..') !== -1) || input.value.indexOf('__') !== -1)
      || (input.name === 'domain_register' && (input.value.indexOf('..') !== -1) || input.value.indexOf('__') !== -1)) {
      field['error'] = 'No se permiten más de dos caracteres especiales consecutivos';
    } else if ((event['type'] === 'change' && this.isChangedValue(field))
        || (!_.isUndefined(event['keyCode']) && Number(event['keyCode']) !== 9)) {
      field['blocked'] = true;
      this.inputLiveValidate(field, input);

      if (!this.hasErrorInput(input)) {
        this.copyFieldInDestiny(field, copyField, copyInput);
      }
    }
  }


  // ************ DOM METHODS ************ //
  /**
   * Reset all form inputs
   */
  private resetInputs(): void {
    this._loginForm.reset();
    this._registerForm.reset();

    for (const field in this.formLoginData) {
      if (this.formSignUpData.hasOwnProperty(field)) {
        this.formLoginData[field]['checker']['status'] = null;
        this.formLoginData[field]['blocked'] = null;
        this.formLoginData[field]['error'] = null;
        clearTimeout(this.formLoginData[field]['checker']['timer']);
      }
    }

    for (const field in this.formSignUpData) {
      if (this.formSignUpData.hasOwnProperty(field)) {
        this.formSignUpData[field]['checker']['status'] = null;
        this.formSignUpData[field]['blocked'] = null;
        this.formSignUpData[field]['error'] = null;
        clearTimeout(this.formSignUpData[field]['checker']['timer']);
      }
    }
  }

  /**
   * Return if a tab is selected. Add event listener to input password if no exist yet
   * @param {string} section
   * @returns {boolean}
   */
  public isTabSelected(section: string): boolean {
    return section === this._tabId;
  }

  /**
   * Update tab selected.
   * Add click event to input password (register) to toggle visibility string
   * @param {string} section
   */
  public selectTab(section: string): void {
    this.resetInputs();
    this._tabId = section;

    if (this._tabId === 'register' && _.isUndefined(this._textbox)) {
      this._textbox = this._el.nativeElement.querySelector('input[name="password_register"]');
      this._toggler = this._el.nativeElement.querySelector('.btn-toggle-password');

      this._toggler.addEventListener('click', (e) => {
        this._textbox.type = this._textbox.type === 'password' ? 'text' : 'password';
        this._toggler.classList.toggle('fa-eye');
        this._toggler.classList.toggle('fa-eye-slash');
      });
    }
  }

  /**
   * Returns the url domain
   * @returns {string}
   */
  public getUrlDomain(): string {
    return !_.isNull(this.formSignUpData.url.value) && this.formSignUpData.url.value.length > 0
      ? this.formSignUpData.url.value.replace(/\W+/g, '').concat('.txnet.es') + ' (definitivo)'
      : '';
  }

  /**
   * Get error message
   * @param {NgModel} input
   * @returns {string}
   */
  public getErrorInput(input: NgModel): string {
    if (this.hasErrorInput(input)) {
      if (input.getError('required')) {
        return 'Requerido';
      }

      if (input.getError('minlength')) {
        return 'Mínimo ' + input.errors.minlength.requiredLength + ' caracteres';
      }

      if (input.getError('maxlength')) {
        return 'Máximo ' + input.errors.maxlength.requiredLength + ' caracteres';
      }

      if (input.getError('pattern')) {
        return 'Formato inválido'.concat(
          input.hasOwnProperty('name') && (input.name === 'latitude' || input.name === 'longitude')
            ? ': sólo números separados por .'
            : ''
        );
      }
    }
    return '';
  }

  /**
   * Return if an input has errors
   * @param input
   * @returns {boolean}
   */
  public hasErrorInput(input: any): boolean {
    return !_.isNull(input.errors) && (input.dirty || input.touched);
  }


  /**
   * Return if an input has errors in object field
   * @param {Object} field
   * @returns {boolean}
   */
  public hasError(field: object): boolean {
    return !_.isNull(field['error']);
  }

  /**
   * Return if a form has any error
   * @param {Object} form
   * @returns {boolean}
   */
  private isCompleteForm(form: object): boolean {
    for (const field in form) {
      if (_.isNull(form[field]['value']) || form[field]['value'].length === 0 || this.hasError(form[field])) {
        return false;
      }
    }

    return true;
  }

  /**
   * Higlight the input with required condition invalid
   * @param {Object} form
   */
  private highlightRequiredFields(form: object): void {
    for (const field in form) {
      if (_.isNull(form[field]['value']) || form[field]['value'].length === 0 || this.hasError(form[field])) {
        form[field]['error'] = this._errorRequired;
      }
    }
  }


  /**
   * Return the message for input error
   * @param {Object} field
   * @param input
   * @returns {string}
   */
  public validateFieldMessage(field: object, input: object | any): string {
    if (!field.hasOwnProperty('validations')
      || !_.isArray(field['validations'])
      || field['validations'].length === 0) {
      return 'Revise este campo.';
    }

    if (this.hasApiValidationError(field)) {
      return field['validationsApi']['message'];
    }

    for (const validation of field['validations']) {
      if (input.hasError(validation['type'])) {
        return validation['message'];
      }
    }

    return null;
  }


  /**
   * Return if a input value is pending to API response to receive the validation
   * @param {Object} field
   * @returns {boolean}
   */
  private isValidatingInput(field: object): boolean {
    return !_.isNull(field['checker']['status'])
      && field['checker']['status'].hasOwnProperty('checking')
      && field['checker']['status']['checking'];
  }

  /**
   * Return if a input value is not allow by the API
   * @param {Object} field
   * @returns {boolean}
   */
  private hasApiValidationError(field: object): boolean {
    return this.isInitializeValidation(field)
      && !this.isValidatingInput(field)
      && !field['checker']['status']['valid'];
  }

  /**
   * Return if a validation status is enabled
   * @param {Object} field
   * @returns {boolean}
   */
  public isInitializeValidation(field: object): boolean {
    return field.hasOwnProperty('checker')
        && field['checker'].hasOwnProperty('status')
        && !_.isNull(field['checker']['status']);
  }

  /**
   * Return the validation status object
   * @param {Object} field
   * @returns {any}
   */
  public getValidationApiMessage(field: object): any {
    if (!this.isInitializeValidation(field)) {
      return '';
    }

    if (this.isValidatingInput(field)) {
      return '<i class="fa fa-spinner fa-pulse fa-fw"></i>';

    } else if (field['checker']['status']['valid']) {
      return '<i class="fa fa-check text-success"></i>';

    }

    return '<i class="fa fa-exclamation-triangle text-danger"></i>';
  }

  /**
   * Return a chrome extension by domain type
   * @returns {string}
   */
  public getChromeExtensionUrl(): string {
    return HttpSrvService.isProdMode()
      ? 'txnet-extension_v1.0.2.zip'
      : 'txnet-extension_PRE_v1.0.2.zip';
  }

  public functionalityNotImplemented(msg?: string): void {
    this._alertSrv.prompt(msg);
  }
}

