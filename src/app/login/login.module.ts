/*GENERAL ASSETS*/
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';
/*MODULE ASSETS*/
import {LoginComponent} from './login.component';
/*SERVICES*/
import {HttpSrvService} from '../shared/services/http.service';
import {HttpNoJwtSrvService} from '../shared/services/noJwtHttp.service'

import { Angular2SocialLoginModule } from 'angular2-social-login';
import {ComponentsModule} from '../shared/components/components.module';

let providers = {
  "google": {
    "clientId": "552278260194-39ut09di12kb26soq5fv92kfcl2vf4ne.apps.googleusercontent.com"
  },
  "linkedin": {
    "clientId": "783ke43j5bj2dk"
  },
  "facebook": {
    "clientId": "976538405780040",
    "apiVersion": "v2.8"
  }
};

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    ComponentsModule,
    Angular2SocialLoginModule
  ],
  exports: [LoginComponent],
  declarations: [LoginComponent],
  providers: [
    HttpSrvService,
    HttpNoJwtSrvService
  ]
})

export class LoginModule {}

Angular2SocialLoginModule.loadProvidersScripts(providers);
