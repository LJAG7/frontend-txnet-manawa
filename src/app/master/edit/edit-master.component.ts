import {Component, ElementRef, QueryList, HostListener, OnDestroy, OnInit, AfterViewInit, ViewChild, ViewChildren} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';

import {DragulaService} from 'ng2-dragula';

import {CapitalizePipe} from '../../shared/pipes/capitalize.pipe';
import {OrderByPipe} from '../../shared/pipes/orderBy.pipe';

import {SYSTEMLOCALPARAMS, REGEXPATTERNS} from '../../shared/const/system.const';

import {AlertService} from '../../shared/services/alert.service';
import {AuthSrvService} from '../../shared/services/auth.service';
import {HttpSrvService} from '../../shared/services/http.service';
import {LoaderService} from '../../shared/services/loader.service';
import {ValidationObjectPipe} from '../../shared/pipes/validation.pipe';

const _ = require('lodash');
const $ = require('jquery');

@Component({
  selector: 'app-wizard-master',
  templateUrl: './edit-master.component.html',
  styleUrls: ['./edit-master.component.css'],
  providers: [
    HttpSrvService,
    CapitalizePipe,
    DragulaService,
    OrderByPipe,
    ValidationObjectPipe
  ]
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class EditMasterComponent implements OnInit, OnDestroy, AfterViewInit {

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('confirmDialogComponent') public confirmDialogComponent;
  @ViewChildren('jokerNameInput') public jokerNameInput: QueryList<any>;

  @ViewChildren('persons') private persons;
  @ViewChild('searchComponent') private searchComponent;
  @ViewChild('searchTeamTypesTemplate') private searchTeamTypesTemplate;
  @ViewChild('searchGroupTemplate') private searchGroupTemplate;

  /************************ COMPONENT VARS  ************************/

  public teamTypeWizardForm: FormGroup;
  public isLoading = true;

  public teamTypeBusinessRuleSelected = 1;
  public optionTeamTypeQuantity = 0;
  public teamTypeAllArray: Array<Object> = [];
  public showDragOptionsOnMobile = false;

  public searchingDashboard = false;
  public restartSearchComponent = 0;

  public teamTypeOnWizard: Object = {
    id: 0,
    name: null,
    description: null,
    domain: 'Fixtures',
    groups: 0,
    rule_person: [],
    rule_teamt: [],
    status: 'INCOMPLETE'
  };

  public tabSelectedId = 1;

  public rolesListVisible = -1;
  public dragAndDropList: Array<object> | any = [];

  public roles: Array<Object> = [];
  public tabs = [
    {id: 1, name: 'roles', icon: 'user-o'},
    {id: 2, name: 'sub-equipos', icon: 'users'}
  ];

  public showSearchGroup = false;

  public showAddNewRoleArea = false;
  public errorCreatingRol = null;
  public newRol = {
    name: '',
    category: -1
  };

  public filterCategoryModel = 0;
  public thereAreResultsFiltered = true;
  public filterCategoriesByNameValue = '';

  public teamTypeTemplatesShown = false;
  public tooltipFullTeamTypeIndex = -1;
  public cardHoverLayerIndex = -1;
  public teamTypesFiltered = [];

  public teamTypeTooltipEntity: object | any = [];
  public teamTypeTooltipScrolling = false;

  public jokerRulePersonComboActived = false;
  public addingNewRoleActived = false;
  public showAlertUncompletedRuleWizard = false;

  public changedRuleComboOption = false;

  private _subscribeIdParam;

  private _masterInUse = false;

  private _teamTypeGroupFilterSelected: Array<object> = [];

  private _isDragging = false;
  private _dragContainerActive = null;
  private _dragPillType = null;
  private _dragAndDropListCopy: Array<object> | any = [];
  private _gotAllRoles = false;

  private _setLastRulePersonAsComboBox = false;
  private _noShowLastRulePersonAsComboBox = false;

  private _forcedCleanGroupSearch = false;

  private _newMultiRoleRuleChanged = false;

  public dropdownOptions: any[] = [];
  public selectedDropdownOption: any;
  private _defaultDropdownOption = {title: 'Selecciona una opción', value: 0};
  public roleClasses = [];
  private _rcOffset = 100;
  private _createNoBounds = false;

  private _ttOffsetDefault = 12;
  private _teamTypes = [];
  private _ttOffset = 1000;
  private _ttOffsetToShow = this._ttOffsetDefault;
  private _ttLinkableOffset = 1000;
  private _totalCountTeamTypes = 0;
  private _ruleTeamTypeIndex = -1;

  private _ttgOffset = 100;
  private _rolesOffset = 1000;

  private _ruleComboOptionSelected = -1;
  private _editableRulePersonIndex = -1;
  private _hasChangeRulePerson = {
    description: false,
    category: false,
    min: false,
    max: false,
    infinite: false
  };

  private _confirmType = 'rulePerson';


  /*********************** DEFAULT SYSTEM PARAMS *****************************/
  private _masterRuleParams = Object.assign(SYSTEMLOCALPARAMS.master);
  private _multiRoleLimit = SYSTEMLOCALPARAMS.master.multiRules.items;

  private _newMultiRoleRulePerson = {
    child_roles: Array(this._multiRoleLimit).fill({
      id: -1,
      name: ' '
    }),
    min: SYSTEMLOCALPARAMS.master.multiRules.person.min,
    max: SYSTEMLOCALPARAMS.master.multiRules.person.max
  };

  /************************ CONFIG VARS ************************/

  public teamTypeTooltipPosition: object = {
    top: 0,
    left: 0
  };

  public ruleComboOptions = {
    max: 0,
    minmax: 1,
    equal: 2
  };

  public dragOptions: any = {
    revertOnSpill: true,
    copy: true,
    copySortSource: false,
    ignoreInputTextSelection: true,
    accepts: (el, source, handle, sibling) => {

      return el.getAttribute('data-type-option') === this._categoriesDefinition.role_class.dragType
        ? source.getAttribute('data-accept-pills') === this._categoriesDefinition.role_class.dragType
        || source.getAttribute('data-accept-pills') === 'all'
        : el.getAttribute('data-type-option') === this._categoriesDefinition.groups.dragType
        && el.getAttribute('data-id-option') !== null
        && Number(el.getAttribute('data-id-option')) > 0
          ? source.getAttribute('data-accept-pills') === this._categoriesDefinition.groups.dragType
          || source.getAttribute('data-accept-pills') === 'all'
          : false;
    },
    invalid: (el, handle) => {
      return this.isMasterInUse();
    }
  };

  private _categoriesDefinition = {
    role_class: {
      dragType: 'role',
      title: 'Roles'
    },
    groups: {
      dragType: 'team_type',
      title: 'Tipos Equipos'
    }
  };


  private _teamTypeGroupFilterConfig = {
    endpoint: 'teamtgroups/',
    text: 'name',
    key: 'id',
    flags: {
      isMultiFilter: true,
      showLabel: false
    }
  };

  private _urlsRulePersonRoleEntity: object = {
    post: 'teamts/idtt/rulepersonrole/',
    put: 'teamts/rulepersonrole/idrule',
    delete: 'teamts/rulepersonrole/idrule'
  };
  private _urlsRuleTeamTypeEntity: object = {
    post: 'teamts/idtt/ruleteamtteamt/',
    put: 'teamts/ruleteamtteamt/idrule',
    delete: 'teamts/ruleteamtteamt/idrule'
  };

  private _teamTypeTooltipElement = 'tab';
  private _tooltipOffset = {
    tab: {top: 30, left: 10},
    card: {top: 30, left: 13}
  };

  private _jokerRuleRolePerson: object = {
    id: -1,
    min: null,
    max: null,
    description: '',
    category_name: '',
    child_roles: []
  };

  private _confirmDialogConfig = {
    rulePerson: {
      header: 'Se eliminará la regla de role.',
      message: '¿Desea continuar?',
      accept: {
        action: 'deleteRulePerson',
        button: {
          text: 'Si'
        }
      }
    },
    ruleTeam: {
      header: 'Se eliminará la regla de subteam.',
      message: '¿Desea continuar?',
      accept: {
        action: 'deleteRuleTeam',
        button: {
          text: 'Si'
        }
      }
    }
  };

  /************************ COMPONENT HOSTLISTENER  ************************/

  @HostListener('window:scroll', [])
  onWindowScroll() {
    this.updateTooltipCategoriesPosition();
  }

  /************************ NATIVE METHODS ************************/


  constructor(private _alertSrv: AlertService,
              private _authSrv: AuthSrvService,
              private _capitalizePipe: CapitalizePipe,
              private _dragulaSrv: DragulaService,
              private _elementRef: ElementRef,
              private _formBuilder: FormBuilder,
              private _httpSrv: HttpSrvService,
              private _loaderSrv: LoaderService,
              private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _validationObjectPipe: ValidationObjectPipe) {

    this._authSrv.verifyViewPermission('master/edit');
    this._loaderSrv.general();

    if (this.isAllowed()) {
      localStorage.removeItem('teamtReturn');
      this.loadDataModel(this._activatedRoute.params['value'].hasOwnProperty('id')
        ? this._activatedRoute.params['value']['id']
        : -1);

      this.getAllNeededInfoFromApi();
      this.getAllRolesClasses();
      this.enableDragServiceListeners();

      setTimeout( () => {
        this._loaderSrv.stopGeneral();
      }, 500);
    }
  }

  ngOnInit(): void {
    if (this.isAllowed()) {
      this._subscribeIdParam = this._activatedRoute.params.subscribe(params => {
        if (this.teamTypeOnWizard['id'] !== params['id']) {
          this.loadDataModel(params['id']);
        }
      });

      this.teamTypeWizardForm = this._formBuilder.group({
        teamTypeName: ['', Validators.required],
        teamTypeDescription: ['', Validators.required],
        justRequired: ['', Validators.required]
      });
    }
  }

  ngAfterViewInit() {
    if (localStorage.getItem('newMaster')) {
      localStorage.removeItem('newMaster');
      this._setLastRulePersonAsComboBox = false;

      setTimeout(() => {
        if (this.getTeamTypeRulesPersonSimple().length > 0) {
          this.editRuleRolePerson(0);
        }
      }, 2000);
    }
  }

  ngOnDestroy() {
    this._subscribeIdParam.unsubscribe();
  }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('master/edit', tag);
  }

  /************************ SETUP METHODS ************************/

  /**
   * Get confirm dialog setup for Rule Persons
   * @returns {Object}
   */
  public getConfirmDialogSetup(): object {
    return this._confirmDialogConfig[this._confirmType];
  }

  /**
   * Setting widgetConfig
   *
   * @returns {Object}
   */
  public setDropdownConfig(): object {
    return {
      showListOptions: true,
      hideOptionSelected: false,
      options: {
        defaultFirstOption: true,
        defaultFirstOptionTitle: 'Selecciona una opción'
      }
    };
  }

  /**
   * Get filter group widgetConfig
   * @returns {Object}
   */
  public getTeamTypeGroupFilterConfig(): object {
    return this._teamTypeGroupFilterConfig;
  }

  public setSearchRolesDashboardConfig(): object {
    return {
      flags: {
        showResultList: false,
        showResultListUnderClick: false,
        showBackgroundCover: false,
        forcedUnselectIconShow: true
      },
      error: {
        show: false
      },
      icon: {
        hasIconRight: true,
        hasSearchIcon: true,
        hideIconOnSearch: true,
        showUnselectIcon: true
      },
      input: {
        placeholder: 'busca un role o un equipo'
      }
    };
  }

  public setSearchAddNewRolConfig(): object {
    return {
      flags: {
        showResultList: true,
        showResultListUnderClick: true
      },
      error: {
        show: true,
        message: 'No hay categorias con el nombre <b>{{wordToFilter}}</b>'
      },
      input: {
        placeholder: 'busca una categoría para este role',
        value: this.filterCategoriesByNameValue !== '' ? this.filterCategoriesByNameValue : '',
        showAsSelectable: true
      },
      icon: {
        hasSelectableIcon: true
      }
    };
  }

  public createNewRolConfig(): object | any {
    return {
      button: {
        text: 'Crear',
        classes: {
          main: 'secondary',
          extra: 'col-12 p-3 mt-2',
          hasOutlineBorder: true
        }
      }
    };
  }

  public loadMoreTemplatesConfig(): object | any {
    return {
      button: {
        text: 'cargar más',
        classes: {
          main: 'secondary',
          extra: 'mt-5 mb-1 bg-white',
          hasOutlineBorder: true
        }
      }
    };
  }

  public useAndCopyTeamTypeTemplateButtonConfig(): object | any {
    return {
      button: {
        hasText: false,
        classes: {
          main: 'secondary',
          extra: 'btn-icon',
          hasOutlineBorder: true
        }
      },
      icon: {
        has: true,
        classes: {
          main: 'files-o'
        }
      }
    };
  }

  public editTeamTypeTemplateButtonConfig(): object | any {
    return {
      button: {
        hasText: false,
        classes: {
          main: 'secondary',
          extra: 'btn-icon',
          hasOutlineBorder: true
        }
      },
      icon: {
        has: true,
        classes: {
          main: 'pencil'
        }
      }
    };
  }

  public searchTeamTypeTemplatesConfig(): object {
    return {
      flags: {
        showResultList: false,
      },
      error: {
        show: false
      },
      input: {
        placeholder: 'busca un master por nombre o clase'
      },
      icon: {
        hasIconRight: true,
        hasSearchIcon: true,
        hideIconOnSearch: true
      },
    };
  }

  public setSearchGroupClassConfig(): object {
    return {
      flags: {
        showResultListUnderClick: true,
        forcedUnselectIconShow: true,
        showResultListUnderUnselect: true,
        isMiniSearch: true
      },
      error: {
        show: false
      },
      input: {
        placeholder: this.getMasterGroupPlaceHolder(),
        value: this.getMasterGroupClassName()
      },
      icon: {
        hasIconRight: false,
        showUnselectIcon: true
      },
      api: {
        endpoint: 'teamtgroups/',
        queryString: {
          elements: this._ttgOffset
        },
        showError: false
      }
    };
  }

  /************************ EVENTS METHODS ************************/

  public showMultiRoleSection(): boolean {
    return this.getTeamTypeRulesPersonMultiple().length > 0
      || this.getTeamTypeRulesPersonSimple().length >= this._multiRoleLimit;
  }

  public getEditMasterTooltip(teamt: object): string {
    return this.isMasterEntityInUse(teamt)
      ? 'Editar (Master ya en uso)'
      : 'Editar';
  }

  /**
   * Return to list of team type
   */
  public goBackToList(): void {
    localStorage.removeItem('newMaster');
    this._router.navigateByUrl('/master');
  }

  /**
   * Returns if a master is already in use
   * @param {Object} teamt
   * @returns {boolean}
   */
  public isMasterEntityInUse(teamt: object): boolean {
    return teamt.hasOwnProperty('already_in_use') && teamt['already_in_use'];
  }

  /**
   * Detect if the role box active is complete
   * @return {boolean}
   */
  public isMasterInUse(): boolean {
    return this._masterInUse;
  }

  /**
   * On change group filter
   * @param {Object} event
   */
  public onChangeTeamTypeGroupFilter(event: object): void {
    if (this._validationObjectPipe.isValidValueObjectProperty(event, 'elements')
      && !_.isNil(event['elements'])
      && !_.isEmpty(event['elements'])) {

      this.restartSearchTTTemplatesComponent();
      this._teamTypeGroupFilterSelected = event['elements'];
      this.filterTeamTypesTemplates();
      this.teamTypesFiltered = this.getAllSearchableTeamTypes();
    }
  }

  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']](event['params']);
    }
  }

  public onDeleteRulePerson(rule: object): void {
    this._confirmType = 'rulePerson';
    this.confirmDialogComponent.showDialog(rule);
  }

  public onDeleteRuleTeam(rule: object): void {
    this._confirmType = 'ruleTeam';
    this.confirmDialogComponent.showDialog(rule);
  }

  /**
   * Save any changes on basic information input detected
   *
   * @param key
   */
  public saveBasicInformation(key: string): void {
    const options = {};

    options[key] = this.teamTypeOnWizard[key];

    this._httpSrv.patch(
      'teamts/' + this.teamTypeOnWizard['id'],
      options
    ).subscribe(
      success => this.onSuccessSavedInfo(success, key),
      fail => this.onFailResponse()
    );
  }

  /**
   * Set the role-team options by search action.
   * Only updates the options list in keyup event (search bad working on focusout)
   * @param {Object} event
   */
  public getResponseSearchRolesDashboardComponent(event: object): void {
    if (_.isEmpty(event['currentText']) && event['event'] !== 'keyup') {
      this.searchingDashboard = false;
      this.onClickTab(this.tabSelectedId);

    } else if (event.hasOwnProperty('resultsFound') && event['event'] === 'keyup') {
      this.searchingDashboard = true;
      this.formatDragPillsList('all', event['data']);
    }
  }

  /**
   * Interact with mate rule entity adding, modifying or deleting itself
   *
   * @param action
   * @param index
   * @param idRole
   */
  public doSomethingWithRulePersonRoleEntity(action: string = 'post',
                                             index: number = -1,
                                             idRole: number = -1,
                                             newRule: object = null): void {
    const endpoint = this._urlsRulePersonRoleEntity[action].replace(/idtt|idrule/gi, (matched) => {
      return {
        idtt: this.teamTypeOnWizard['id'],
        idrule: index > -1
          ? this.getTeamTypeRulesPerson()[index]['id']
          : index
      }[matched];
    });

    const options = index > -1
      ? this.getTeamTypeRulesPerson()[index]
      : _.isNull(newRule)
        ? {
          category_name: '',
          min: null,
          max: null,
          child_roles: [{id: idRole}]
        }
        : newRule;

    const successCallBack = action !== 'delete'
      ? action === 'post'
        ? 'onNewRulePersonRoleEntity'
        : 'onChangeRulePersonRoleEntity'
      : 'onSuccessDeletedRulePerson';

    this._httpSrv.call(
      action,
      endpoint,
      options
    ).subscribe(
      success => this[successCallBack](success),
      fail => this.onFailResponse()
    );
  }


  /**
   * Interact with team type rule entity adding, modifying or deleting itself
   *
   * @param action
   * @param index
   * @param idRole
   */
  public doSomethingWithRuleTeamTypeRoleEntity(action: string = 'post',
                                               index: number = -1,
                                               idSubTeam: number = -1): void {
    if (action === 'put' &&
      !_.isNil(this.getTeamTypeSubTeams()[index]['child_teamts'])
      && this.getTeamTypeSubTeams()[index]['child_teamts'].length === 0) {
      this.getTeamTypeSubTeams()[index]['child_teamts'].push({
        id: _.size(_.filter(this.teamTypeAllArray, (subteam) => {
          return Number(subteam['groups']) === 0;
        })) > 0 ? _.filter(this.teamTypeAllArray, (subteam) => {
          return Number(subteam['groups']) === 0;
        })[0]['id'] : 1
      });
    }

    const endpoint = this._urlsRuleTeamTypeEntity[action].replace(/idtt|idrule/gi, (matched) => {
      return {
        idtt: this.teamTypeOnWizard['id'],
        idrule: index > -1
          ? this.getTeamTypeSubTeams()[index]['id']
          : index
      }[matched];
    });

    const options = index > -1
      ? this.getTeamTypeSubTeams()[index]
      : {
        num_components: 1,
        child_teamts: [{id: idSubTeam}]
      };


    this._httpSrv.call(
      action,
      endpoint,
      options
    ).subscribe(
      success => this.onChangeRuleTeamTypeEntity(success),
      fail => this.onFailResponse()
    );
  }

  public getChildTeamName(teamType: object): string {
    return teamType
    && teamType.hasOwnProperty('child_teamts')
    && teamType['child_teamts'].length > 0
    && teamType['child_teamts'][0].hasOwnProperty('name')
      ? teamType['child_teamts'][0]['name']
      : 'Tipo de equipo hijo';
  }

  public getTeamTypeRulesPerson(): Array<object> {
    return this.hasTeamTypePersonRules()
      ? this.teamTypeOnWizard['rule_person']
      : [];
  }

  /**
   * Returh the Rule Role Person with one role or joker (0 roles)
   * @returns {Object[]}
   */
  public getTeamTypeRulesPersonSimple(): Array<object> {
    return this.getTeamTypeRulesPerson().filter((rule) => {
      return rule.hasOwnProperty('child_roles')
        && rule['child_roles'].length <= 1
        || this.isRuleRolePersonJoker(rule);
    });
  }


  public getNewRolesPersonToCreateRule(): Array<object> {
    let index = 0;

    if (!this.hasTeamTypeRulesPersonMultiple()
      && this._newMultiRoleRuleChanged === false) {
      for (const rule of this.getTeamTypeRulesPerson()) {
        if (rule.hasOwnProperty('child_roles')) {
          for (const rol of rule['child_roles']) {
            if (index >= this._multiRoleLimit) {
              return this._newMultiRoleRulePerson.child_roles;
            }

            this._newMultiRoleRulePerson.child_roles[index] = {...rol};
            index++;
          }
        }
      }
    }
    return this._newMultiRoleRulePerson.child_roles;
  }

  public newMultiRoleRulePersonLength(): number {
    return this._newMultiRoleRulePerson.child_roles.filter((item) => {
      return item['id'] !== -1;
    }).length;
  }

  public isNewRulePersonRoleMultiConformable(): boolean {
    return this._newMultiRoleRuleChanged
      && this.newMultiRoleRulePersonLength() >= this._multiRoleLimit;
  }

  public hasTeamTypeRulesPersonMultiple(): boolean {
    return this.getTeamTypeRulesPersonMultiple().length > 0;
  }

  public getTeamTypeRulesPersonMultiple(): Array<object> {
    return this.getTeamTypeRulesPerson().filter((rule) => {
      return rule.hasOwnProperty('child_roles')
        && rule['child_roles'].length > 1;
    });
  }

  /**
   * Returns the data
   */
  public serializeDragAndDropList(): Array<object> {
    return _.concat(this.roles, this.teamTypeAllArray);
  }

  /**
   * Return true if the co-role rule is valid (no duplicity)
   * @returns {boolean}
   */
  public checkMultiRulePersonDuplicity(newRol: number, index: number): boolean {

    if (this.newMultiRoleRulePersonLength() < this._multiRoleLimit - 1) {
      return true;
    }

    const temporalRule = {...this._newMultiRoleRulePerson};
    temporalRule.child_roles[Number(index)]['id'] = Number(newRol);

    return this.getTeamTypeRulesPersonMultiple().filter((rule) => {
      return rule['child_roles'].filter((role) => {
        for (const item of temporalRule.child_roles) {
          if (item['id'] === role['id']) {
            return true;
          }
        }
        return false;
      }).length === this._multiRoleLimit;
    }).length === 0;
  }


  public getTeamTypeSubTeams(): Array<object> {
    return this.hasTeamTypeSubTeamsRules()
      ? this.teamTypeOnWizard['rule_teamt']
      : [];
  }


  public hasRulePersonCategoryName(rulePerson: object): boolean {
    return rulePerson.hasOwnProperty('category_name') && rulePerson['category_name'].length > 0;
  }


  /**
   * Returns the role name to simple rule mate. If the rule is a joker then returns the description
   * @param rulePerson
   * @returns {string}
   */
  public getUniqueRolIntoRulePerson(rulePerson: object): string {
    return rulePerson.hasOwnProperty('child_roles')
    && rulePerson['child_roles'].length === 1
    && rulePerson['child_roles'][0].hasOwnProperty('name')
      ? rulePerson['child_roles'][0]['name']
      : this.isRuleRolePersonJoker(rulePerson)
        ? '<i class="fa fa-user-secret"></i> ' + rulePerson['description']
        : '';
  }


  public getUniqueRolIdIntoRulePerson(rulePerson: object): number {
    return rulePerson.hasOwnProperty('child_roles')
    && rulePerson['child_roles'].length === 1
    && rulePerson['child_roles'][0].hasOwnProperty('id')
      ? rulePerson['child_roles'][0]['id']
      : -1;
  }

  public getMinMaxRulePersonToRender(rulePerson: object): string {
    return rulePerson.hasOwnProperty('min') && rulePerson.hasOwnProperty('max')
      ? _.isNull(rulePerson['min']) || _.isNull(rulePerson['max'])
        ? '# ?'
        : rulePerson['min'] === rulePerson['max']
          ? rulePerson['max']
          : rulePerson['min'] + '-' + rulePerson['max']
      : '';
  }

  public toggleRulePersonCombo(): void {
    if (!this.isTeamTypeCreated()) {
      this._alertSrv.warn('Debe crear el master antes de añadir un role comodín.');
    } else {
      this.closeDragOptionsOnMobile();
      if (!this.jokerRulePersonComboActived) {
        this.teamTypeOnWizard['rule_person'].push(Object.assign({}, this._jokerRuleRolePerson));
        this._editableRulePersonIndex = this.getTeamTypeRulesPersonSimple().length - 1;
        this.jokerRulePersonComboActived = true;
        this.addingNewRoleActived = false;
        this.changedRuleComboOption = false;
        this._ruleComboOptionSelected = -1;

        setTimeout(() => {
          this._elementRef.nativeElement.querySelector('#rule_person_joker_name_' + this._editableRulePersonIndex).focus();
        }, 20);
      }
    }
  }

  public showRuleCombo(index: number = -1): boolean {
    return this._editableRulePersonIndex === index;
  }

  public showCloseAreaCombo(index: number = -1): boolean {
    return this.jokerRulePersonComboActived
      ? this.showRuleCombo(index) && !this.showAlertUncompletedRuleWizard
      : this.showRuleCombo(index);
  }

  public showAlertAreaCombo(index: number = -1): boolean {
    return this.jokerRulePersonComboActived
      && this.showRuleCombo(index)
      && this.showAlertUncompletedRuleWizard;
  }

  public thereIsComboSelected(): boolean {
    return this._editableRulePersonIndex > -1;
  }

  public resetComboBoxSelection(index: number = -1): void {
    if (this.jokerRulePersonComboActived) {
      this.isRulePersonJokerCompleted(index)
        ? this.updateRulePersonRole(index)
        : this.showAlertUncompletedRuleWizard = true;

    } else {
      if (this._hasChangeRulePerson.min) {
        this.changeMinRulePersonRole(index, false);
      }

      if (this._hasChangeRulePerson.max) {
        this.changeMaxRulePersonRole(index, false);
      }


      if (!_.isEmpty(_.pickBy(this._hasChangeRulePerson, (flag) => {return flag === true; }))) {
        this.updateRulePersonRole(index);
      }

      this._ruleComboOptionSelected = -1;
      this._editableRulePersonIndex = -1;
    }
  }

  public editRuleRolePerson(index: number): void {

    if (this.isMasterInUse()) {
      return;
    }

    if (!_.isUndefined(this.getTeamTypeRulesPersonSimple()[index]) && index !== this._editableRulePersonIndex) {
      if (this.jokerRulePersonComboActived) {
        this.deleteUnsaveJoker();
      }
      this._editableRulePersonIndex = index;
      this.jokerRulePersonComboActived = false;
      this.changedRuleComboOption = false;
      this._ruleComboOptionSelected = this.getRuleRolePersonComboOption(true);
    }
  }

  /**
   * Return the option to show.
   * @returns {number}
   */
  public getRuleRolePersonComboOption(reset: boolean = false): number {
    if (reset) {
      this._ruleComboOptionSelected = -1;
    }

    if (this.jokerRulePersonComboActived || this.changedRuleComboOption
      || this._editableRulePersonIndex === -1 || this._ruleComboOptionSelected === -2) {
      return this._ruleComboOptionSelected;
    }

    if (!this.isRulePersonMinMaxDefined(this._editableRulePersonIndex)) {
      return -1;
    }

    if (!this.isMaxRulePersonValues()) {
      const max = this.getTeamTypeRulesPersonSimple()[this._editableRulePersonIndex]['max'];
      const min = this.getTeamTypeRulesPersonSimple()[this._editableRulePersonIndex]['min'];

      if (max !== min) {
        return 1;
      }

      return 2;
    }

    return 0;
  }

  public showFirstStepRuleCombo(): boolean {
    const comboOption = this.getRuleRolePersonComboOption();
    return comboOption < 0 || comboOption === this.ruleComboOptions.max;
  }

  public isComboOptionActive(option: number): boolean {
    return !_.isUndefined(this.getRuleRolePersonComboOption())
      && this._ruleComboOptionSelected === option;
  }

  public saveJoker(index: number = -1): void {
    if (this.getTeamTypeRulesPersonSimple()[index]['description'].length === 0) {
      this.getTeamTypeRulesPersonSimple()[index]['description'] = 'Comodín#' + Math.floor(Math.random() * 1234);
    }
    this.showAlertUncompletedRuleWizard = !this.isRulePersonJokerCompleted(index);
    this._noShowLastRulePersonAsComboBox = !this.isRulePersonJokerCompleted(index);
    this.doSomethingWithRulePersonRoleEntity('post', this.getIndexRulePersonRuleSimpleOnModelByIndex(index));
  }

  public isRulePersonMinMaxDefined(index: number = -1): boolean {
    return this.getTeamTypeRulesPersonSimple()[index].hasOwnProperty('min')
      && this.getTeamTypeRulesPersonSimple()[index].hasOwnProperty('max')
      && !_.isNull(this.getTeamTypeRulesPersonSimple()[index]['min'])
      && !_.isNull(this.getTeamTypeRulesPersonSimple()[index]['max']);
  }


  public updateRulePersonRole(index: number = -1): void {
    if (this.getTeamTypeRulesPersonSimple()[index]['id'] !== -1) {
      this.doSomethingWithRulePersonRoleEntity('put', this.getIndexRulePersonRuleSimpleOnModelByIndex(index));

    } else if (this.isRulePersonJokerCompleted(index)) {
      this.saveJoker(index);
    }

    this._hasChangeRulePerson = {
      description: false,
      category: false,
      min: false,
      max: false,
      infinite: false
    };
  }

  public onKeyUpRulePersonCategoryName(): void {
    this._hasChangeRulePerson.category = true;
  }

  public onKeyUpRulePersonDescription(): void {
    this._hasChangeRulePerson.description = true;
  }


  /**
   * Detect if a rule is incompleted
   * @param index
   */
  public isRulePersonCompleted(index: number = -1): boolean {
    return this.isRuleRolePersonJoker(this.getTeamTypeRulesPersonSimple()[index])
      ? this.isRulePersonJokerCompleted(index)
      : this.isRulePersonSimpleCompleted(index);
  }

  public selectComboOption(option: number, index: number = -1): void {
    if (option === this.ruleComboOptions.max) {
      this.getTeamTypeRulesPersonSimple()[index]['max'] = this._masterRuleParams.rules.person.max;
      this.getTeamTypeRulesPersonSimple()[index]['min'] = this._masterRuleParams.rules.person.min;

      this._hasChangeRulePerson.infinite = true;
      this.resetComboBoxSelection(index);

    } else {
      this._ruleComboOptionSelected = option;
      this.changedRuleComboOption = true;
      $('input').blur();

      if (option === this.ruleComboOptions.minmax) {

      } else if (option === this.ruleComboOptions.equal) {
        if (this.getTeamTypeRulesPersonSimple()[index]['min'] !== this.getTeamTypeRulesPersonSimple()[index]['max']) {
          this.getTeamTypeRulesPersonSimple()[index]['min'] = this.getTeamTypeRulesPersonSimple()[index]['max'];
          this.updateRulePersonRole(index);
        }
      }
    }
  }

  public isRuleRolePersonJoker(rulePerson: object): boolean {
    return !_.isUndefined(rulePerson)
      && rulePerson.hasOwnProperty('child_roles')
      && rulePerson['child_roles'].length === 0
      || !rulePerson.hasOwnProperty('child_roles');
  }

  public isRulePersonWithRolAdded(rulePerson: object): boolean {
    return !_.isUndefined(rulePerson)
      && rulePerson.hasOwnProperty('child_roles_pending')
      && rulePerson['child_roles_pending'].length === 1;
  }

  public isRulePersonJokerOrRulePersonWithRolAdded(rulePerson: object): boolean {
    return this.isRuleRolePersonJoker(rulePerson)
      || this.isRulePersonWithRolAdded(rulePerson);
  }

  public updateRuleRolPersonDescription(index: number): void {
    this.updateRulePersonRole(index);
  }

  public changeMinRulePersonRole(index: number, update: boolean = true): void {
    if (REGEXPATTERNS.numbers.test(this.getTeamTypeRulesPersonSimple()[index]['min'])) {
      this.limitRangeMinOrMax(index, 'min');

      if (_.isNull(this.getTeamTypeRulesPersonSimple()[index]['max'])
        || this.getTeamTypeRulesPersonSimple()[index]['min'] > this.getTeamTypeRulesPersonSimple()[index]['max']) {
        this.getTeamTypeRulesPersonSimple()[index]['max'] = this.getTeamTypeRulesPersonSimple()[index]['min'] === this._masterRuleParams.rules.person.min
          ? this._masterRuleParams.rules.person.default
          : this.getTeamTypeRulesPersonSimple()[index]['min'];
      }

      if (update) {
        this.updateRulePersonRole(index);
      }
    }
  }



  /**
   * Change rule mate for max value
   * @param {number} index
   */
  public changeMaxRulePersonRole(index: number, update: boolean = true): void {
    if (REGEXPATTERNS.numbers.test(this.getTeamTypeRulesPersonSimple()[index]['max'])) {
      this.limitRangeMinOrMax(index, 'max');

      if (_.isNull(this.getTeamTypeRulesPersonSimple()[index]['min'])) {
        this.getTeamTypeRulesPersonSimple()[index]['min'] = this._masterRuleParams.rules.person.min;

      } else if (this.getTeamTypeRulesPersonSimple()[index]['max'] < this.getTeamTypeRulesPersonSimple()[index]['min']
        || this._ruleComboOptionSelected === this.ruleComboOptions.equal) {
        this.getTeamTypeRulesPersonSimple()[index]['min'] = this.getTeamTypeRulesPersonSimple()[index]['max'];
      }

      if (update) {
        this.updateRulePersonRole(index);
      }
    }
  }


  public setMax(index: number): void {
    this.getTeamTypeRulesPersonSimple()[index]['max'] = this._masterRuleParams.rules.person.max;
    if (_.isNil(this.getTeamTypeRulesPersonSimple()[index]['min'])) {
      this.getTeamTypeRulesPersonSimple()[index]['min'] = this._masterRuleParams.rules.person.default;
    }
    this.updateRulePersonRole(index);
  }


  public onKeyUpMinRulePersonRole(index: number, event): void {
    if (event.keyCode === 13) {
      this.resetComboBoxSelection(index);

    } else  if (REGEXPATTERNS.number.test(event.key)) {
      this.limitRangeMinOrMax(index, 'min');
    }

    this._hasChangeRulePerson.min = true;
  }

  public onKeyUpMaxRulePersonRole(index: number, event): void {
    if (event.keyCode === 13) {
      this.resetComboBoxSelection(index);

    } else {
      if (REGEXPATTERNS.number.test(event.key)) {
        this.limitRangeMinOrMax(index, 'max');
      }

      if (this.getTeamTypeRulesPersonSimple()[index]['max'] === this._masterRuleParams.rules.person.min) {
        this.getTeamTypeRulesPersonSimple()[index]['max'] = this._masterRuleParams.rules.person.default;
      }

      if (this.isComboOptionActive(this.ruleComboOptions.equal)) {
        this.getTeamTypeRulesPersonSimple()[index]['min'] = this.getTeamTypeRulesPersonSimple()[index]['max'];
      }
    }

    this._hasChangeRulePerson.max = true;
  }


  public deleteUnsaveJoker(): void {
    this.showAlertUncompletedRuleWizard = false;
    this.jokerRulePersonComboActived = false;
    this._editableRulePersonIndex = -1;
    this.teamTypeOnWizard['rule_person'] = this.teamTypeOnWizard['rule_person'].filter((item) => {
      return item['id'] !== -1;
    });
  }


  public deleteRulePerson(rule: object): void {
    if (!_.isUndefined(rule) && rule.hasOwnProperty('id')) {

      if (this.jokerRulePersonComboActived) {
        this.deleteUnsaveJoker();
      } else {
        this._editableRulePersonIndex = -1;
        this.doSomethingWithRulePersonRoleEntity('delete', this.getIndexRulePersonRuleOnModel(rule['id']));
      }

    } else {
      this._alertSrv.error('UNDEFINED RULE: No se puede eliminar esta regla');
    }
  }

  public createMultiRolRule(): void {
    if (this.newMultiRoleRulePersonLength() === this._multiRoleLimit
      && (this._newMultiRoleRuleChanged || !this.hasTeamTypeRulesPersonMultiple())) {
      this._newMultiRoleRuleChanged = true;
      this.doSomethingWithRulePersonRoleEntity('post', -1, -1, this._newMultiRoleRulePerson);

    } else {
      this._alertSrv.error('No es posible guardar esta regla, deben asignarse 2 roles.');
    }

  }

  /**
   * Updates the Min Max values for Draggable and Source Person Rule
   * @param draggableIndex
   * @param sourceIndex
   */
  public updateRulesPersonsGrouped(draggableIndex: number, sourceIndex: number): void {
    this.getTeamTypeRulesPerson()[draggableIndex]['max'] -= this.getTeamTypeRulesPerson()[sourceIndex]['max'];

    if (this.getTeamTypeRulesPerson()[draggableIndex]['max'] < this.getTeamTypeRulesPerson()[draggableIndex]['min']) {
      this.getTeamTypeRulesPerson()[draggableIndex]['min'] = this.getTeamTypeRulesPerson()[draggableIndex]['max'];
    }
    this.doSomethingWithRulePersonRoleEntity('put', draggableIndex);
  }

  public onSelectCategory(event: object): void {
    if (event
      && event.hasOwnProperty('element')
      && event['element'].hasOwnProperty('id')
      && event['element']['id'] > 0) {
      this.newRol['category'] = event['element']['id'];
    }
  }

  public categoryWithOptions(category: object | any): boolean {
    return category
      && category.hasOwnProperty('options')
      && category['options'].length > 0;
  }

  public isOptionAvailable(option: object): boolean {
    return !option.hasOwnProperty('visible') || !option['visible'];
  }


  /**
   * Previus call to API for create master from drag&d
   * @param {HTMLElement} dragElement
   */
  private createTeamTypeFromDrag(dragEl: HTMLElement): void {
    const newRule = {
      type: null,
      data: {}
    };

    if (dragEl.getAttribute('data-type-option') === 'role') {
      newRule.type = 'rulePerson';
      newRule.data = {
        child_roles: [{id: dragEl.getAttribute('data-id-option')}],
        min: null,
        max: null
      };

    } else {
      newRule.type = 'ruleTeam';
      newRule.data = {
        child_teamts: [{id: dragEl.getAttribute('data-id-option')}],
        num_components: this._masterRuleParams.rules.team.max
      };
    }

    this.createTeamType(newRule);
  }

  /**
   * For a simple first new rule in master, return if is a valid rule
   * @param {Object} newRule
   * @returns {boolean}
   */
  private isValidFirstNewRule(newRule: object): boolean {
    return !(_.isEmpty(newRule)
      || !newRule.hasOwnProperty('type') || _.isEmpty(newRule['type'])
      || !newRule.hasOwnProperty('data') || _.isEmpty(newRule['data']));
  }
  /**
   * Call to API: create a teamType (POST)
   * @param {Object} newRule
   */
  public createTeamType(newRule: object): void {
    if (!this.isValidFirstNewRule(newRule)) {
      this._alertSrv.error('Error: no hay datos válidos para crear el Master');
      return;
    }

    if (!this._createNoBounds) {
      this._createNoBounds = true;
      this._httpSrv.post(
        'teamts/',
        {name: 'Nuevo tipo de equipo'}
      ).subscribe(
        success => this.onSuccessCreateTeamType(success, newRule),
        fail => this.onFailResponse()
      );
    }
  }

  /**
   * Actions on success API: all teamTypes (POST)
   * @param {Object} response
   * @param {Object} newRule
   */
  private onSuccessCreateTeamType(response: object,
                                  newRule: object): void {
    if (!this.isValidFirstNewRule(newRule)) {
      this._alertSrv.error('Error: no hay datos válidos para crear la primera regla del Master');
      return;
    }

    if (response['success']) {
      this.teamTypeOnWizard = response['object'];
      const endpoint = newRule['type'] === 'rulePerson'
        ? 'teamts/:id/rulepersonrole/'
        : 'teamts/:id/ruleteamtteamt/';

      this._httpSrv.post(
        endpoint.replace(':id', this.teamTypeOnWizard['id']),
        newRule['data']
      ).subscribe(
        success => this.onSuccessFirstRuleTeamType(success),
        fail => this.onFailResponse()
      );

    } else {
      this.onFailResponse();
    }
  }

  /**
   * Actions on success API:
   * If success is ok, redirect to wizard form
   * @param response
   */
  private onSuccessFirstRuleTeamType(response: object): void {
    response['success']
      ? this.getAllInfoOfCurrentTeamTypeId()
      : this.onFailResponse();
  }

  public showWhenTeamTypeEditionOrDisplayed(): boolean {
    return this.isTeamTypeCreated()
      ? true
      : this.teamTypeTemplatesShown ? true : false;
  }

  public showOnNoTypeCreated(): boolean {
    return !this.isTeamTypeCreated()
      ? true
      : this.teamTypeTemplatesShown ? true : false;
  }

  public showButtonNoTypeCreated(): boolean {
    return this.isTeamTypeCreated()
      ? false
      : !this.teamTypeTemplatesShown
      && !this.isTeamTypeCreated()
        ? true
        : this.teamTypeTemplatesShown ? false : true;
  }

  public showMoreTemplatesOnCreationButton(): boolean {
    return this._teamTypes.length > 4 && this.showButtonNoTypeCreated();
  }

  public toggleDragOptionsOnMobile(): void {
    this.showDragOptionsOnMobile = !this.showDragOptionsOnMobile;
    // @TODO: agregar a body fixed o initial segun esta opcion, para no mostrar tantos scroll en vista mobile
  }

  public closeDragOptionsOnMobile(): void {
    this.showDragOptionsOnMobile = false;
  }

  // @TODO: eliminar en caso de estar aburrido y modelar Master
  public isMasterCompleted(): boolean {
    return this._validationObjectPipe.isValidObjectProperty(this.teamTypeOnWizard, 'status')
      && this.teamTypeOnWizard['status'] === 'COMPLETE';
  }

  public finalizeMaster(): void {
    this._httpSrv.patch(
      'teamts/' + this.teamTypeOnWizard['id'] + '/status/',
      {status: 'COMPLETE'}
    ).subscribe(
      (responseOk) => this.onSuccessFinalizeMaster(responseOk),
      (fail) =>  this.onFailResponse()
    );
  }

  public elementsOnThisCategoryFiltered(event: object): void {
    // @TODO: Evitar el refresco existente al eliminar o agregar roles cuando se tiene algún filtro en el dropdwon
    // @TODO: (Se muestra la lista completa en lugar de mantener el filtrado y posteriormente se actualiza a los elementos filtrados)
    this.filterCategoryModel = Number(event['index']) === 0 ? 0 : Number(event['id']);
    this.thereAreResultsFiltered =
      Number(this.filterCategoryModel) === 0
        ? true
        : this.roleClasses[this.filterCategoryModel - 1] !== undefined
        && this.roles.filter((rol) => {
          return (rol['role_class']['id']) === Number(this.filterCategoryModel);
        }).length > 0;
    if (!_.isNaN(this.filterCategoryModel) && Number(this.filterCategoryModel) !== 0) {
      this.filterCategoriesByNameValue = event['name'];
    } else {
      this.filterCategoriesByNameValue = '';
    }
    this.filterCategoriesByName();
  }

  public onSuccessCreateTeamTypeToNewRol(response: object): void {
    this.teamTypeOnWizard['id'] = response['object']['id'];
    setTimeout( () => {
      this._httpSrv.post('roles/',
        {
          name: this.newRol['name'],
          role_class: {
            id: this.newRol['category']
          }
        }
      ).subscribe(
        (success) => this.onSuccessCreatedNewRol(success),
        (fail) => this.onFailResponse()
      );
    }, 500);
  }

  public setActiveToDrag(element: object): void {
    if (this.showDragOptionsOnMobile) {
      this.closeDragOptionsOnMobile();

      if (!this.isTeamTypeCreated()) {
        const newRule = {
          type: null,
          data: {}
        };

        if (element['type'] === 'role') {
          newRule.type = 'rulePerson';
          newRule.data = {
            child_roles: [{id: element['id']}],
            min: null,
            max: null
          };

        } else {
          newRule.type = 'ruleTeam';
          newRule.data = {
            child_teamts: [{id: element['id']}],
            num_components: this._masterRuleParams.rules.team.max
          };
        }

        this.createTeamType(newRule);

      } else {
        element['type'] === 'team_type'
          ? this.doSomethingWithRuleTeamTypeRoleEntity('post', -1, element['id'])
          : this.doSomethingWithRulePersonRoleEntity('post', -1, element['id']);
      }

    } else {
      !this.elementIsReadyToBeDrag(element)
        ? element['drag_ready'] = true
        : delete element['drag_ready'];
    }
  }

  public elementIsReadyToBeDrag(element: object): boolean {
    return element
      && element.hasOwnProperty('drag_ready')
      && element['drag_ready'];
  }

  public getNumberActiveDrags(): number {
    return this.getAllActiveDragsElements().length;
  }

  public getAllActiveDragsElements(): Array<object | any> {
    let result = [];
    for (const options of this.dragAndDropList['categories']) {
      result = result.concat(options['options'].filter(option => this.elementIsReadyToBeDrag(option)));
    }
    return result;
  }

  public isMasterAlreadyInEdition(teamt): boolean {
    return Number(teamt['id']) !== Number(this._activatedRoute.params['value']['id']);
  }

  public filterTeamTypeTemplates(event: object | any): void {
    if (event.hasOwnProperty('data')) {
      this.teamTypesFiltered = event['currentText'] === ''
        ? this.getAllSearchableTeamTypes()
        : event['data'];
    }
  }

  public noResultsAccordingFilter(): boolean {
    return this.teamTypesFiltered.length === 0;
  }

  public toggleTeamTypeTemplates(): void {
    this.tooltipFullTeamTypeIndex = -1;
    this.teamTypeTemplatesShown = !this.teamTypeTemplatesShown;
  }

  public loadMoreTemplates(): void {
    this._ttOffsetToShow += this._ttOffsetDefault;
    this.teamTypesFiltered = this.getTeamTypesFiltered();
  }

  public copyAndUseOnNewTeamType(teamType: object | any): void {
    this._httpSrv
      .post('teamts/' + teamType['id'] + '/clone/')
      .subscribe(
        (responseOk) => this.onSuccessCreateNewBasicSchemaTeamTypeFromTemplate(responseOk),
        (fail) =>  this.onFailResponse()
      );
  }

  public ruleTeamTypeIsToggled(index: number = -1): boolean {
    return this._ruleTeamTypeIndex === index;
  }

  public setTeamTypeIsToggledIndex(index: number = -1): void {
    if (this.isMasterInUse()) {
      this._ruleTeamTypeIndex = -1;
      return;
    }

    this._ruleTeamTypeIndex = this._ruleTeamTypeIndex === index ? -1 : index;
  }


  public hasErrorNewRolForm(): boolean {
    return !_.isNil(this.errorCreatingRol) || !_.isEmpty(this.errorCreatingRol);
  }

  public isValidNewRolForm(): boolean {
    return !this.hasErrorNewRolForm() && this.newRol['name'].length !== 0 && this.newRol['category'] !== -1;
  }

  public onCreateNewRol(): void {
    if (this.newRol['name'].length === 0) {
      this.errorCreatingRol = 'Nombre de Rol requerido';

    } else if (this.newRol['category'] < 1) {
      this.errorCreatingRol = 'Debes seleccionar una categoría';

    } else {
      this.errorCreatingRol = null;
      this.createApiNewRolCall();
    }
  }

  public checkDuplicityNewRolName(): void {
    // @TODO: realizar verificacion UNIQUE desde endpoint cuando BACK unifique los verify a queryAll
    this.errorCreatingRol = !_.isEmpty(this.newRol['name'])
      && this.roles.filter((rol) => {
        return rol.hasOwnProperty('name') && _.lowerCase(rol['name']) === _.lowerCase(this.newRol['name']);
      }).length === 1
      ? 'Nombre de Rol duplicado'
      : null;
  }

  public addNewRole(): void {
    this.clearNewRolEntity();
    this.addingNewRoleActived = !this.addingNewRoleActived;
    this.jokerRulePersonComboActived = false;
    this.showAddNewRoleArea = !this.showAddNewRoleArea;
  }

  /**
   * Recover all the team types from the API
   */
  public getTeamTypesFromApi(): void {
    this._httpSrv.get(
      'teamts/',
      {page: 1, elements: this._ttOffset}
    ).subscribe(
      success => this.OnSuccessGetAllTeamTypes(success),
      fail => this.onFailResponse()
    );
  }

  /**
   * Recover all the team types from the API
   */
  public getTeamTypesLinkableFromApi(): void {
    this._httpSrv.get(
      this.isTeamTypeCreated()
        ? 'teamts/' + this.teamTypeOnWizard['id'] + '/linkable/'
        : 'teamts/',
      {page: 1, elements: this._ttLinkableOffset}
    ).subscribe(
      success => this.OnSuccessGetAllTeamTypesLinkable(success),
      fail => this.onFailResponse()
    );
  }

  /**
   * Recover all the team types from the API
   */
  public getAllRolesClasses(): void {
    this._httpSrv.get(
      'roleclasses/',
      {page: 1, elements: this._rcOffset}
    ).subscribe(
      success => this.OnSuccessGetAllRolesClasses(success),
      fail => this.onFailResponse()
    );
  }

  public onScrollTabCategories(event) {
    this.teamTypeTooltipScrolling = true;
    this.updateTooltipCategoriesPosition();
  }

  public getDragAndDropListCategories(): Array<any> {
    return !_.isNil(this.dragAndDropList) && this.dragAndDropList.hasOwnProperty('categories')
      ? this.dragAndDropList['categories']
      : [];
  }

  public getTeamTypeName(tt: object): string {
    return tt['name'].length > 0 ? tt['name'] : 'Sin Nombre';
  }

  public showTooltipFullTeamMembers(teamt: object = {id: -1}, elementType: string = 'tab'): void {
    this.tooltipFullTeamTypeIndex = teamt['id'] === this.tooltipFullTeamTypeIndex
      ? -1
      : teamt['id'];

    this._teamTypeTooltipElement = elementType;
    this.teamTypeTooltipEntity = teamt;
    this.teamTypeTooltipScrolling = false;
    this.updateTooltipCategoriesPosition();
  }

  public getIconForTeamType(teamt: object): string {
    return 'fa-' + (teamt.hasOwnProperty('icon') ? teamt['icon'] : 'building');
  }

  public isTeamTypeCreated(): boolean {
    return this._activatedRoute.params['value'].hasOwnProperty('id')
      && Number(this._activatedRoute.params['value']['id']) > 0;
  }

  /**
   * Check if the team type on wizard has mate business rule active
   *
   * @returns {boolean}
   */
  public hasTeamTypePersonRules(): boolean {
    return this.teamTypeOnWizard.hasOwnProperty('rule_person')
      && this.teamTypeOnWizard['rule_person'].length > 0;
  }

  /**
   * Determine if has team type on business rule
   *
   * @returns {boolean}
   */
  public hasTeamTypeSubTeamsRules(): boolean {
    return this.teamTypeOnWizard.hasOwnProperty('rule_teamt')
      && this.teamTypeOnWizard['rule_teamt'].length > 0;
  }

  public getTeamTypeSubTeamsRules(): Array<object> {
    return this.hasTeamTypeSubTeamsRules()
      ? this.teamTypeOnWizard['rule_teamt']
      : [];
  }

  /**
   * Control the response from API called asking for all team types
   *
   * @param response
   * @returns {Array}
   */
  public OnSuccessGetAllTeamTypes(response: object): void {
    if (response['success']) {
      this.getTeamTypesLinkableFromApi();
      this._teamTypes = response['object']['items'];
      this.teamTypesFiltered = this.getTeamTypesFiltered();
      this._totalCountTeamTypes = response['object']['total_count'];
      this.changeTeamTypeBusinessRule();
    } else {
      this.goBackToList();
    }
  }

  /**
   * Control the response from API called asking for all team types
   *
   * @param response
   * @returns {Array}
   */
  public OnSuccessGetAllTeamTypesLinkable(response: object): void {
    if (response['success']) {
      this.teamTypeAllArray = _.filter(response['object']['items'], (team) => {
        return team['id'] !== this.teamTypeOnWizard['id'];
      });
    } else {
      this.goBackToList();
    }
  }

  /**
   * Updates the Min Max values for Draggable and Source Team Rule
   * @param draggableIndex
   * @param sourceIndex
   */
  public updateRulesTeamsGrouped(draggableIndex: number, sourceIndex: number): void {
    if (this.getTeamTypeSubTeams()[draggableIndex]['max'] === this.getTeamTypeSubTeams()[sourceIndex]['max']) {
      this.doSomethingWithRuleTeamTypeRoleEntity('delete', draggableIndex);

    } else {
      this.getTeamTypeSubTeams()[draggableIndex]['max'] -= this.getTeamTypeSubTeams()[sourceIndex]['max'];

      if (this.getTeamTypeSubTeams()[draggableIndex]['max'] < this.getTeamTypeSubTeams()[draggableIndex]['min']) {
        this.getTeamTypeSubTeams()[draggableIndex]['min'] = this.getTeamTypeSubTeams()[draggableIndex]['max'];
      }
      this.doSomethingWithRuleTeamTypeRoleEntity('put', draggableIndex);
    }
  }

  public isDraggingEventEnabled(container: string = null): boolean {
    return this._isDragging && (_.isNil(container)
      || container === this._dragContainerActive);
  }

  public isPillAccepted(type: string = null): boolean {
    return !_.isNil(type) && (this._dragPillType === type || type === 'all');
  }

  public tabIdSelected(id: number): boolean {
    return this.tabSelectedId === id;
  }

  public selectNewGroupClass(): void {
    this.showSearchGroup = !this.showSearchGroup;
  }

  public onMouseOutSearchGroups(): void {
    if (this.showSearchGroup) {
      this.showSearchGroup = false;
    }
  }

  public getMasterGroupClassName(): string {
    return this._forcedCleanGroupSearch
      ? ''
      : this.teamTypeHasGroup()
        ? this.teamTypeOnWizard['groups'][0]['name']
        : 'Clase por definir';
  }


  public getMasterGroupPlaceHolder(): string {
    return this.teamTypeHasGroup()
        ? ''
        : 'busca un master por nombre o clase';
  }

  public getMasterGroupClassNameTag(): string {
    return this.teamTypeHasGroup()
      ? this.teamTypeOnWizard['groups'][0]['name']
      : 'Clase por definir';
  }


  /**
   * Determines if hide or show the first item in list
   */
  public hideFirstItemDropInList(option: Object = null): boolean {
    const dropTypeCaseToHideOption = {
      role: this.searchingDashboard ? -1 : 1,
      team_type: -1
    };

    if (_.isNil(option)) {
      return !this.searchingDashboard && this.tabSelectedId === 1;
    }

    return option['id'] === dropTypeCaseToHideOption[option['type']];
  }

  public getTypeAccordingTabSelected(): string {
    return {
      1: this._categoriesDefinition.role_class.dragType,
      2: this._categoriesDefinition.groups.dragType
    }[this.tabSelectedId];
  }

  public onClickTab(id: number): void {
    this.tabSelectedId = id;
    this.searchingDashboard = false;
    this.formatDragPillsList(this.getTypeAccordingTabSelected(), this.dragAndDropList = {
      1: this.roles,
      2: this.teamTypeAllArray
    }[id]);
  }

  public formatDragPillsList(type: string = this._categoriesDefinition.role_class.dragType,
                             options: Array<Object>): void {
    this.dragAndDropList = {type: type, categories: []};
    for (const option of options) {

      const optionClass = option.hasOwnProperty('groups') ? 'groups' : 'role_class';

      if (optionClass === 'groups' && option['id'] === this.teamTypeOnWizard['id']) {
        continue;
      }

      const indexObjectOnArray = _.findIndex(this.dragAndDropList['categories'], (element) => {
        if (type === 'all') {
          return element.name === this._categoriesDefinition[optionClass]['title'];
        }
        return element.name === option[optionClass]['name'];
      });

      if (indexObjectOnArray === -1) {
        this.dragAndDropList['categories'].push({
          name: (type === 'all' || _.isNil(option[optionClass]))
            ? this._categoriesDefinition[optionClass]['title']
            : option[optionClass]['name'],
          options: [{
            name: option['name'],
            id: option['id'],
            type: this._categoriesDefinition[optionClass]['dragType'],
            icon: option.hasOwnProperty('groups') && option['groups'].length > 0
              ? option['groups'][0]['iconcss']
              : ''
          }]
        });
      } else {
        this.dragAndDropList['categories'][indexObjectOnArray]['options'].push({
          name: option['name'],
          id: option['id'],
          type: this._categoriesDefinition[optionClass]['dragType'],
          icon: option.hasOwnProperty('groups') && option['groups'].length > 0
            ? option['groups'][0]['iconcss'] :
            ''
        });
      }
    }
    if (this._gotAllRoles) {
      this._dragAndDropListCopy = {...this.dragAndDropList};
      this._gotAllRoles = false;
    }

    if (!this.searchingDashboard) {
      this.cleanUsedRolesOnPersonRules();
    }

  }

  public teamTypeCardIsHovered(index: number = -1): boolean {
    return this.cardHoverLayerIndex === index;
  }

  public teamTypePopoverEyeIsSelected(index: number = -1): boolean {
    return index !== -1
      ? this.tooltipFullTeamTypeIndex === index
      : this.tooltipFullTeamTypeIndex !== -1;
  }


  public showHideOptions(index: number = -1): void {
    this.cardHoverLayerIndex = this.cardHoverLayerIndex === index
      ? -1
      : index;
  }

  public isIncompleteTeam(tt: object): boolean {
    return tt
      && tt.hasOwnProperty('status')
      && tt['status'].toLowerCase() === 'incomplete';
  }
  public showLoadMoreTeamTypesTemplatesButton(): boolean {
    return this.teamTypesFiltered.length > 0
      && this._ttOffsetToShow < this._totalCountTeamTypes
      && this.isAllFilterEnabled();
  }

  public teamTypeOnTemplatesHasRulePerson(teamType: object | any): boolean {
    return teamType
      && teamType.hasOwnProperty('rule_person')
      && teamType['rule_person'].length > 0;
  }

  public teamTypeHasRuleP(teamType: object | any): boolean {
    return teamType
      && teamType.hasOwnProperty('child_teamts')
      && teamType['child_teamts'].length > 0
      && teamType['child_teamts'][0].hasOwnProperty('id')
      && this.findInfoTeamTypeChildren(teamType['child_teamts'][0]['id']) !== undefined
      && this.findInfoTeamTypeChildren(teamType['child_teamts'][0]['id']).hasOwnProperty('rule_person')
      && this.findInfoTeamTypeChildren(teamType['child_teamts'][0]['id'])['rule_person'].length > 0;
  }

  public teamTypeHasRuleTeamT(teamType: object | any): boolean {
    return teamType
      && teamType.hasOwnProperty('child_teamts')
      && teamType['child_teamts'].length === 1
      && teamType['child_teamts'][0].hasOwnProperty('id')
      && this.findInfoTeamTypeChildren(teamType['child_teamts'][0]['id']) !== undefined
      && this.findInfoTeamTypeChildren(teamType['child_teamts'][0]['id'])['rule_teamt'].length > 0;
  }

  public getRuleTeamtOnTeamTChild(ruleT: object): Array<object | any> {
    return this.teamTypeHasRuleTeamT(ruleT)
      ? this.findInfoTeamTypeChildren(ruleT['child_teamts'][0]['id'])['rule_teamt']
      : [];
  }

  public getRulePersonOnTeamTChild(ruleT: object): Array<object | any> {
    return this.teamTypeHasRuleP(ruleT)
      ? this.findInfoTeamTypeChildren(ruleT['child_teamts'][0]['id'])['rule_person']
      : [];
  }

  public teamTypeByIdHasRuleP(teamtId: number | any): boolean {
    return this.teamTypeOnTemplatesHasRulePerson(this.getTeamTypeById(teamtId));
  }

  public rulePHasRoles(ruleP: object | any): boolean {
    return ruleP
      && ruleP.hasOwnProperty('child_roles')
      && ruleP['child_roles'].length > 0;
  }

  public rulePHasMinMaxQuantities(ruleP: object | any): boolean {
    return ruleP
      && ruleP.hasOwnProperty('min')
      && ruleP.hasOwnProperty('max')
      && !_.isNull(ruleP['min'])
      && !_.isNull(ruleP['max'])
      && ruleP['min'] > -2
      && ruleP['max'] > -2;
  }

  public getAllRolesFromRulePerson(ruleP: object | any): string {
    if (this.rulePHasRoles(ruleP)) {
      let concatenated = '';
      for (const rule of ruleP['child_roles']) {
        concatenated += rule['name'].length > 0 ? rule['name'] + ', ' : '';
      }
      return concatenated.substring(0, concatenated.length - 2);

    } else if (ruleP.hasOwnProperty('description')) {
      return '<i class="fa fa-user-secret"></i> ' + ruleP['description'];

    }
    return '';
  }

  public getQuantityFromRulePerson(ruleP: object | any): string {
    return this.rulePHasMinMaxQuantities(ruleP)
      ? (ruleP['min'] === ruleP['max']
        ? ruleP['min']
        : ruleP['min'] + '/' + ruleP['max']).toString().replace('-1', '∞')
      : _.isNull(ruleP['min']) || _.isNull(ruleP['max'])
        ? '# ?'
        : '';
  }

  public teamTypeHasMoreThan3RuleP(teamType: object | any): boolean {
    return teamType['rule_person'].length > 3;
  }

  public teamTypeRulePNotShown(teamType: object | any): string {
    return this.teamTypeHasMoreThan3RuleP(teamType)
      ? '+' + (teamType['rule_person'].length - 3)
      : '0';
  }

  public teamTypeSubTeamsNotShown(teamType: object | any): string {
    return this.teamTypeHasOneSubTeamAtLeast(teamType)
      ? '+' + (teamType['rule_teamt'].length - 1)
      : '0';
  }

  public teamTypeHasOneSubTeamAtLeast(teamType: object | any): boolean {
    return teamType.hasOwnProperty('rule_teamt') && teamType['rule_teamt'].length > 0;
  }

  public teamTypeByHasOneSubTeamAtLeast(teamtId: number | any): boolean {
    return this.teamTypeHasOneSubTeamAtLeast(this.getTeamTypeById(teamtId));
  }

  public teamTypeHasMoreThanOneSubTeams(teamType: object | any): boolean {
    return teamType.hasOwnProperty('rule_teamt') && teamType['rule_teamt'].length > 1;
  }

  public getRuleTeamTName(ruleTeamt: object | any): string {
    return ruleTeamt.hasOwnProperty('category_name')
    && ruleTeamt['category_name'].length > 0
      ? ruleTeamt['category_name']
      : ruleTeamt.hasOwnProperty('child_teamts')
      && ruleTeamt['child_teamts'].length > 0
      && ruleTeamt['child_teamts'][0].hasOwnProperty('name')
        ? ruleTeamt['child_teamts'][0]['name']
        : 'Sin nombre';
  }

  public getMainTitle(): string {
    return !this.isTeamTypeCreated() && !this.teamTypeTemplatesShown
      ? 'Master - plantillas'
      : 'Master - plantillas';
  }


  public selectNewGroupForTeamType(event: object): void {
    if (event['event'] === 'blur' && event['resultsFound'] === false) {
      this.restartSearchGroupComponent();
    } else if (event['event'] === 'blur' && event['resultsFound'] === true) {
      this.restartSearchGroupComponent();
      // if (event['data'].length === 1) {
      //   this.teamTypeOnWizard['groups'] = [{id: event['data'][0]['id']}];
      // } else {
      //   this.restartSearchGroupComponent();
      // }
      this.saveBasicInformation('groups');
    } else if (event
      && event.hasOwnProperty('element')
      && event['element'].hasOwnProperty('id')
      && event['element']['id'] > -1) {
      this.teamTypeOnWizard['groups'] = [{id: event['element']['id']}];
      this.saveBasicInformation('groups');

    } else if (event['event'] === 'unselect') {
      this._forcedCleanGroupSearch = true;
    }
  }

  public editTeamTypeOnWorkTable(teamType: object | any): void {
    this.teamTypeOnWizard = teamType;
    if (teamType && teamType.hasOwnProperty('id')) {
      localStorage.removeItem('newMaster');
      this._router.navigateByUrl('/master/edit/' + teamType['id']);
      this.getAllNeededInfoFromApi();
      this.toggleTeamTypeTemplates();
    }
  }

  /************************ API METHODS ************************/

  /**
   * Update all the need information from api in front end side
   */
  private getAllNeededInfoFromApi(): void {
    this.getSystemParams();
    this.getRolesFromApi();
    this.getTeamTypesFromApi();
  }

  private onSuccessSavedInfo(response: object, key: string): void {
    if (response['success']) {
      if (key === 'groups'
        && response.hasOwnProperty('object')
        && response['object'].hasOwnProperty('groups')) {
        this.teamTypeOnWizard['groups'] = response['object']['groups'];
        this.showSearchGroup = false;
        this._forcedCleanGroupSearch = false;
        // this.teamTypeOnWizard['groups'][0]
      }

    } else {
      this.onFailResponse();
    }
  }

  /**
   * Get the team type information of current team type
   */
  private getTeamTypeOnWizard(): void {
    this._httpSrv.get(
      'teamts/' + this.teamTypeOnWizard['id']
    ).subscribe(
      success => this.onSuccessGetTeamTs(success),
      fail => this.onFailResponse()
    );
  }

  /**
   * CallBack function called on success get Team type id
   *
   * @param response
   */
  private onSuccessGetTeamTs(response: Object): void {
    this.isLoading = false;
    if (response['success']) {

      this.resetNewMultiRoleRulePerson();
      this.refreshInterfaceAccordingToNewTeamTypeSelected(response['object']);
      this.cleanAllActiveRolesDragPills();

      if (this.tabSelectedId === 1) {
        this.cleanUsedRolesOnPersonRules();
      }


      if (response.hasOwnProperty('object') && this.isMasterEntityInUse(response['object'])) {
        this._masterInUse = true;

      } else {
        this._masterInUse = false;

        if (this._setLastRulePersonAsComboBox) {
          this._editableRulePersonIndex = this.getTeamTypeRulesPersonSimple().length - 1;
          this._setLastRulePersonAsComboBox = false;
        }
      }
    } else {
      this.goBackToList();
    }
  }

  private onNewRulePersonRoleEntity(response: object): void {
    if (response['success']) {
      this.jokerRulePersonComboActived = false;
      this._setLastRulePersonAsComboBox = this.showLastRulePersonAfterOnChange(response);
      this.getTeamTypeOnWizard();

    } else {
      this.onFailResponse();
    }
  }

  private onChangeRulePersonRoleEntity(response: object): void {
    if (response['success']) {
      this.jokerRulePersonComboActived = false;
      this._setLastRulePersonAsComboBox = this.showLastRulePersonAfterOnChange(response);

    } else {
      this.onFailResponse();
    }
  }


  private onSuccessDeletedRulePerson(response: object): void {
    if (response['success']) {
      this.jokerRulePersonComboActived = false;
      this._setLastRulePersonAsComboBox = this.showLastRulePersonAfterOnChange(response);
      this.getTeamTypeOnWizard();
      this.getRolesFromApi();

    } else {
      this.onFailResponse();

    }
  }


  private onChangeRuleTeamTypeEntity(response: object): void {
    if (response['success']) {
      this.getTeamTypeOnWizard();

    } else {
      this.onFailResponse();
    }
  }

  private createNewTeamWithMultipleDrags(pillType: HTMLElement): void {
    this._httpSrv.post(
      'teamts/',
      {name: 'Nuevo equipo'}
    ).subscribe(
      (response) => this.onSuccessCreateNewTeamWithMultipleDrags(response, pillType),
      (response) => this.onFailResponse()
    );
  }

  private onSuccessCreateNewTeamWithMultipleDrags(response: object | any,
                                                  pillType: HTMLElement): void {
    if (response['success']) {
      this.teamTypeOnWizard = response['object'];
      pillType.hasAttribute('data-type-option')
      && pillType.getAttribute('data-type-option') === 'role'
        ? this.createMultipleRolesDragsOnNewTeam()
        : this.createMultipleRuleTTDragsOnNewTeam();
    } else {
      this.onFailResponse();
    }
  }

  private createMultipleRolesDragsOnNewTeam(): void {
    this._httpSrv.post(
      'teamts/' + this.teamTypeOnWizard['id'] + '/multi_rulepersonrole/',
      {rulepersonsByDefault: this.convertAllActiveDragsElementsToIds()}
    ).subscribe(
      (response) => this.onSuccessCreateMultipleRolesOnNewTeam(response),
      (response) => this.onFailResponse()
    );
  }

  private onSuccessCreateMultipleRolesOnNewTeam(response: object | any): void {
    response['success']
      ? this.getAllInfoOfCurrentTeamTypeId()
      : this.onFailResponse();
  }

  private createMultipleRuleTTDragsOnNewTeam(): void {
    this._httpSrv.post(
      'teamts/' + this.teamTypeOnWizard['id'] + '/multi_ruleteamtteamt/',
      {ruleteamtsByDefault: this.convertAllActiveDragsElementsToIds()}
    ).subscribe(
      (response) => this.onSuccessCreateMultipleRuleTeamTOnNewTeam(response),
      (response) => this.onFailResponse()
    );
  }

  private onSuccessCreateMultipleRuleTeamTOnNewTeam(response: object | any): void {
    response['success']
      ? this.getAllInfoOfCurrentTeamTypeId()
      : this.onFailResponse();
  }

  private createMultipleRolesDrags(): void {
    this._httpSrv.post(
      'teamts/' + this.teamTypeOnWizard['id'] + '/multi_rulepersonrole/',
      {rulepersonsByDefault: this.convertAllActiveDragsElementsToIds()}
    ).subscribe(
      (response) => this.onSuccessCreateMultipleRoles(response),
      (response) => this.onFailResponse()
    );
  }

  private onSuccessCreateMultipleRoles(response: object | any): void {
    response['success']
      ? this.onChangeRulePersonRoleEntity(response)
      : this.onFailResponse();
  }

  private createMultipleRuleTTDrags(): void {
    this._httpSrv.post(
      'teamts/' + this.teamTypeOnWizard['id'] + '/multi_ruleteamtteamt/',
      {ruleteamtsByDefault: this.convertAllActiveDragsElementsToIds()}
    ).subscribe(
      (response) => this.onSuccessCreateMultipleRuleTeamT(response),
      (response) => this.onFailResponse()
    );
  }

  private onSuccessCreateMultipleRuleTeamT(response: object | any): void {
    response['success']
      ? this.onChangeRuleTeamTypeEntity(response)
      : this.onFailResponse();
  }

  /**
   * Get all the roles info from api
   */
  private getRolesFromApi(): void {
    this._httpSrv.get(
      'roles/',
      {elements: this._rolesOffset}
    ).subscribe(
      success => this.OnSuccessGetAllRoles(success),
      fail => this.onFailResponse()
    );
  }

  /**
   * Control the callback on roles api call
   *
   * @param response
   * @constructor
   */
  private OnSuccessGetAllRoles(response: object): void {
    if (response['success']) {
      this._gotAllRoles = true;
      this.formatDragPillsList(this._categoriesDefinition.role_class.dragType, this.roles = response['object']['items']);
    } else {
      this.goBackToList();
    }

    this.filterCategoriesByName();
  }

  private OnSuccessGetAllRolesClasses(response: object): void {
    response['success'] && response['object']['items'].length > 0
      ? this.roleClasses = response['object']['items'].map((item) => {
        item['title'] = this._capitalizePipe.transform(item['name']);
        return item;
      })
      : this.goBackToList();

    this.dropdownOptions = [...this.roleClasses];
    this.dropdownOptions.unshift(this._defaultDropdownOption);
    this.selectedDropdownOption = this.dropdownOptions[0];
  }

  private onSuccessCreateNewBasicSchemaTeamTypeFromTemplate(response: object | any): void {
    response['success']
      ? this.editTeamTypeOnWorkTable(response['object'])
      :  this.onFailResponse();
  }


  private onSuccessFinalizeMaster(response: object | any): void {
    if (response['success']) {
      this.teamTypeOnWizard = response['object'];
      setTimeout(() => {
        this.goBackToList();
      }, 2000);
    } else {
      this.onFailResponse();
    }
  }

  private onSuccessCreatedNewRol(response: object | any): void {
    if (response['success']) {
      this.addNewRole();
      if (_.isNil(this._activatedRoute.params['id'])) {
        localStorage.setItem('newMaster', '1');
      }

      if (Number(response['object']['id']) > 0) {
        this.doSomethingWithRulePersonRoleEntity(
          'post',
          -1,
          response['object']['id']
        );
      }
      this._router.navigateByUrl('/master/edit/' + this.teamTypeOnWizard['id']);
      this.getAllNeededInfoFromApi();
    } else {
      this.onFailResponse();
    }
  }

  private deleteRuleTeam(rule: object): void {
    this._httpSrv
      .delete('teamts/ruleteamtteamt/' + rule['id'])
      .subscribe(
        (success) => this.onSuccessDeletedRuleTeamT(success),
        (fail) => this.onFailResponse(false)
      );
  }

  private onSuccessDeletedRuleTeamT(response: object): void {
    if (response['success']) {
      this.setTeamTypeIsToggledIndex(-1);
      this.getTeamTypeOnWizard();

    } else {
      this.onFailResponse(false);
    }
  }

  private createApiNewRolCall(): void {
    if (this.teamTypeOnWizard['id'] === 0) {
      this._httpSrv.post(
        'teamts/',
        {name: 'Nuevo tipo de equipo'}
      ).subscribe(
        success => this.onSuccessCreateTeamTypeToNewRol(success),
        fail => this.onFailResponse()
      );
    } else {
      this._httpSrv.post(
        'roles/',
        {
          name: this.newRol['name'],
          role_class: {
            id: this.newRol['category']
          }
        }
      ).subscribe(
        (success) => this.onSuccessCreatedNewRol(success),
        (fail) => this.onFailResponse()
      );
    }
  }

  /**
   * Catch error on API Communication
   * @param {boolean} reset
   */
  private onFailResponse(reset: boolean = true): void {
    this._forcedCleanGroupSearch = false;
    if (reset) {
      this.getTeamTypeOnWizard();
    }
  }

  /************************ DRAGULA ************************/

  private enableDragServiceListeners(): void {

    this._dragulaSrv.drag.subscribe((value) => {
      this.showTooltipFullTeamMembers();
      this.thisRolesListIsSelectedToBeVisible(-1);
      this.setDraggingFlag(value, true);
    });
    this._dragulaSrv.over.subscribe((value) => {
      this.overDragging(value, true);
    });
    this._dragulaSrv.out.subscribe((value) => {
      this.outDragging(value, false);
    });
    this._dragulaSrv.dragend.subscribe((value) => {
      this.endDragging(value, false);
    });
    this._dragulaSrv.cancel.subscribe((value) => {
      this.setDraggingFlag(value, false);
    });
    this._dragulaSrv.remove.subscribe((value) => {
      this.setDraggingFlag(value, false);
    });
    this._dragulaSrv.drop.subscribe((value) => {
      this.onDrop(value.slice(1), value);
    });

  }

  public thisRolesListIsSelectedToBeVisible(index: number): boolean {
    return this.rolesListVisible === index;
  }

  private resetDraggingFlag(): void {
    this._isDragging = false;
    this._dragContainerActive = null;
    this._dragPillType = null;
  }

  private setDraggingFlag(el, value: boolean): void {
    const event = el.slice(1);
    this._dragContainerActive = el[0];
    this._isDragging = value;
    this._dragPillType = event[0].getAttribute('data-type-option');
  }

  private overDragging(el, value: boolean): void {
    this.setDraggingFlag(el, value);
    const asideContainer = el.slice(1)[1];
    if (!_.isNil(asideContainer)) {
      asideContainer.className += ' lighting-active';
    }

    if (el[2].className.indexOf('drop-area') !== -1) {
      const names = this.getAllActiveDragsElements().filter(team => team['name'] !== el[1].innerText.trim());
      for (let i = 0; i < names.length; i++) {
        const clone = el[1].cloneNode(true);
        el[2].appendChild(clone);
        el[2].lastChild.innerText = names[i]['name'];
      }
    }
  }

  private outDragging(el, value: boolean): void {
    const asideContainer = el.slice(1)[1];
    if (!_.isNil(asideContainer)) {
      asideContainer.className = asideContainer.className.replace('lighting-active', '');
    }
    if (el[2].className.indexOf('drop-area') !== -1) {
      while (el[2].firstChild) {
        el[2].removeChild(el[2].firstChild);
      }
    }
  }

  private endDragging(el, value: boolean): void {
    this.setDraggingFlag(el, value);
    const asideContainer = el.slice(1)[1];
    if (!_.isNil(asideContainer)) {
      asideContainer.className = asideContainer.className.replace('lighting-active', '');
    }
  }

  /**
   * Control the drop event in draggable elements
   *
   * @param event
   * @param el
   */
  private onDrop(event: object, el: object): void {
    if (el[2].hasAttribute('data-create-teamt')) {
      this.getNumberActiveDrags() > 1
        ? this.createNewTeamWithMultipleDrags(el[1])
        : this.createTeamTypeFromDrag(el[1]);

    } else if (el[2].hasAttribute('add-new-corole-rule-mate')
      && el[2].hasAttribute('data-drag-zone-index')
      && Number(event[0].getAttribute('data-id-option')) !== 1) {
      this.addNewCoRolToPersonRuleMultiRole(event, Number(el[2].getAttribute('data-drag-zone-index')));

    } else if (event[0].getAttribute('data-type-option') === this._categoriesDefinition.groups.dragType) {
      this.getNumberActiveDrags() > 1
        ? this.createMultipleRuleTTDrags()
        : this.doSomethingWithRuleTeamTypeRoleEntity('post', -1, event[0].getAttribute('data-id-option'));

    } else if (el[2].hasAttribute('add-new-subteam-rule-team') || el[2].hasAttribute('add-new-role-rule-mate')
      && el[2].hasAttribute('data-option-rule-id')
      && Number(el[2].getAttribute('data-option-rule-id')) > 0) {
      this.determineIfAddRoleOrTeam(event);

    } else {

      if (event[0].hasAttribute('data-draggable-element')) {
        if (event[0].getAttribute('data-draggable-element') === this._categoriesDefinition.role_class.dragType) {
          this.addingNewRoleToRulePerson(event);

        } else {
          this.addNewTeamTypeToSubTeamRule(event);
        }

      } else {
        if (Number(event[0].getAttribute('data-id-option')) !== 1) {
          if (!this.checkRoleDuplicity(event[0].getAttribute('data-id-option'))) {
            this.getNumberActiveDrags() > 1
              ? this.createMultipleRolesDrags()
              : this.doSomethingWithRulePersonRoleEntity('post', -1, event[0].getAttribute('data-id-option'));

          } else {
            this._alertSrv.error('Este role no se puede añadir porque está duplicado.');

          }
        } else {
          this.doSomethingWithRulePersonRoleEntity('post', -1, 1);

        }
      }

    }

    event[0].remove();
    this.resetDraggingFlag();
  }

  /************************ AUXILIAR METHODS ************************/

  /**
   * Load the data model for TeamT and forces to refresh the component model
   * @param teamtId
   */
  private loadDataModel(teamtId: number): void {
    if (teamtId > 0) {
      this.teamTypeOnWizard['id'] = teamtId;
      this.getTeamTypeOnWizard();
      this.onClickTab(1);
    } else {
      this.isLoading = false;
    }
  }

  /**
   * Refresh the interface according new team type passed as parameter
   *
   * @param teamType
   */
  private refreshInterfaceAccordingToNewTeamTypeSelected(teamType: Object): void {
    if (!teamType.hasOwnProperty('rule_person')
      || !this.teamTypeOnWizard.hasOwnProperty('rule_person')
      || teamType['rule_person'] < this.teamTypeOnWizard['rule_person']
      || this._editableRulePersonIndex < 0) {

      this._editableRulePersonIndex = -1;
      this.showSearchGroup = false;
    }

    this.teamTypeOnWizard = teamType;
    this.changeTeamTypeBusinessRule();
  }

  /**
   * Set the team type business rule as unlimited allowed
   */
  private setTeamTypeBusinessRuleAsUnlimited(): void {
    this.teamTypeBusinessRuleSelected = 1;
  }

  /**
   * Set the team type business rule as has to be like %%
   */
  private setTeamTypeBusinessRuleHasToBe(): void {
    this.teamTypeBusinessRuleSelected = 2;
    this.optionTeamTypeQuantity = this.teamTypeOnWizard['rule_teamt'][0]['value'];
  }

  /**
   * Filter for one single rule
   */
  private allowOnlyOneTeamTypeBusinessRule(): void {
    if (this.teamTypeOnWizard['rule_teamt'].length > 1) {
      this.teamTypeOnWizard['rule_teamt'] = [this.teamTypeOnWizard['rule_teamt'][this.teamTypeOnWizard['rule_teamt'].length - 1]];
    }
  }

  /**
   * Change the business applied by the team type for its team type children
   */
  private changeTeamTypeBusinessRule(): void {
    if (this.hasTeamTypeSubTeamsRules()
      && this.teamTypeOnWizard['rule_teamt'][0]['rule_id'] < 4) {
      this.allowOnlyOneTeamTypeBusinessRule();

      return {
        1: this.setTeamTypeBusinessRuleAsUnlimited,
        2: this.setTeamTypeBusinessRuleHasToBe
      }[this.teamTypeOnWizard['rule_teamt'][0]['rule_id']].apply(this);
    } else {
      // By default
      this.setTeamTypeBusinessRuleAsUnlimited();
    }
  }

  /**
   * Check if the role dragged exists as a rule mate on this team type
   *
   * @param id
   * @returns {boolean}
   */
  private checkRoleDuplicity(id: number): boolean {
    return _.size(_.filter(this.teamTypeOnWizard['rule_person'], (rule) => {
        return _.size(_.filter(rule['child_roles'], (rol) => {
            return Number(rol.id) === Number(id)
              && rol.id !== 1
              && rule['child_roles'].length === 1;
          })) > 0;
      })) > 0;
  }

  private showLastRulePersonAfterOnChange(response: object): boolean {
    if (this._noShowLastRulePersonAsComboBox) {
      this.showAlertUncompletedRuleWizard = false;
      this._noShowLastRulePersonAsComboBox = false;
      this._editableRulePersonIndex = -1;
      return false;
    }

    return response.hasOwnProperty('code_action')
      && response['code_action'] === 1
      && response.hasOwnProperty('object')
      && response['object'].hasOwnProperty('child_roles')
      && response['object']['child_roles'].length < 2;
  }

  private resetNewMultiRoleRulePerson(): void {
    if (this.isNewRulePersonRoleMultiConformable()) {
      this._newMultiRoleRuleChanged = false;
      this._newMultiRoleRulePerson.child_roles = Array(this._multiRoleLimit).fill({
        id: -1,
        name: ' '
      });
    }
  }

  private multiRulePersonRolHasThisRol(rol: number): boolean {
    if (this.getTeamTypeRulesPersonMultiple().length === 0) {
      return true;
    }

    return _.size(_.filter(this._newMultiRoleRulePerson.child_roles, (rolInRule) => {
        return Number(rolInRule['id']) === Number(rol)
          && Number(rol) !== -1;
      })) <= 0;
  }

  private isMaxRulePersonValues(): boolean {
    return this.getTeamTypeRulesPersonSimple()[this._editableRulePersonIndex]['max'] === this._masterRuleParams.rules.person.max
      && this.getTeamTypeRulesPersonSimple()[this._editableRulePersonIndex]['min'] === this._masterRuleParams.rules.person.min;
  }

  private isRulePersonJokerCompleted(index: number = -1): boolean {
    return this.isRuleRolePersonJoker(this.getTeamTypeRulesPersonSimple()[index])
      && this.isRulePersonMinMaxDefined(index)
      && this.getTeamTypeRulesPersonSimple()[index].hasOwnProperty('description')
      && this.getTeamTypeRulesPersonSimple()[index]['description'].length > 0;
  }

  private isRulePersonSimpleCompleted(index: number = -1): boolean {
    return this.isRulePersonMinMaxDefined(index)
      && this.getTeamTypeRulesPersonSimple()[index].hasOwnProperty('child_roles')
      && this.getTeamTypeRulesPersonSimple()[index]['child_roles'].length === 1;
  }

  private limitRangeMinOrMax(index: number, value: string) {
    if (!_.isNull(this.getTeamTypeRulesPersonSimple()[index][value])
        && this.getTeamTypeRulesPersonSimple()[index][value] > this._masterRuleParams.rules.person.max) {
      this.getTeamTypeRulesPersonSimple()[index][value] = this._masterRuleParams.rules.person.max;

    } else if (!_.isNull(this.getTeamTypeRulesPersonSimple()[index][value])
        && this.getTeamTypeRulesPersonSimple()[index][value] < this._masterRuleParams.rules.person.min) {
      this.getTeamTypeRulesPersonSimple()[index][value] = this._masterRuleParams.rules.person.min;
    }
  }

  private convertAllActiveDragsElementsToIds(): Array<object | any> {
    return this.getAllActiveDragsElements().map((dragE) => {
      return {id: dragE['id']};
    });
  }

  private determineIfAddRoleOrTeam(event): void {
    event[1].getAttribute('data-accept-pills') === this._categoriesDefinition.role_class.dragType
      ? this.addNewRolToPersonRuleRole(event)
      : this.addNewSubTeamToTeamRole(event);
  }


  private addNewCoRolToPersonRuleMultiRole(event: object, index: number): void {
    if (event[1].getAttribute('data-accept-pills') === this._categoriesDefinition.role_class.dragType) {

      const ruleId = event[0].getAttribute('data-id-option'),
        rule = this.getTeamTypeRulesPerson()[this.getIndexRulePersonRuleOnModel(ruleId)],
        rolId = this.getUniqueRolIdIntoRulePerson(rule);

      if (!this.multiRulePersonRolHasThisRol(rolId)) {
        this._alertSrv.error('El role que intentas añadir ya está asignado en la regla.');

      } else if (!this.checkMultiRulePersonDuplicity(rolId, index)) {
        this._alertSrv.error('La regla que intenta conformar ya existe.');

      } else if (!_.isUndefined(this._newMultiRoleRulePerson.child_roles[index])) {
        this._newMultiRoleRulePerson.child_roles[index] = {
          id: this.getUniqueRolIdIntoRulePerson(rule),
          name: this.getUniqueRolIntoRulePerson(rule)
        };

        this._newMultiRoleRuleChanged = this.newMultiRoleRulePersonLength() === this._multiRoleLimit;
      }
    }
  }

  private getTypeTeamRuleFilterById(id: number): object {
    return _.filter(this.getTeamTypeRulesPerson(), (rule) => {
      return Number(rule['id']) === Number(id);
    }).slice(0, 1);
  }

  /**
   * Add a new role for an rule mate
   *
   * @param draggable
   */

  private addingNewRoleToRulePerson(draggable: object): void {
    // @TODO REFACTORING
    const draggableRule = this.getTypeTeamRuleFilterById(draggable[0].getAttribute('data-option-rule-id')),
      sourceRule = this.getTypeTeamRuleFilterById(draggable[1].getAttribute('data-option-rule-id'));

    if (_.size(_.differenceBy(draggableRule['child_roles'], sourceRule['child_roles'], 'id')) > 0) {

      // if draggableRule.Max < source.Max => ERROR
      if (draggableRule['max'] >= sourceRule['max']) {
        const draggableIndex = _.findIndex(this.getTeamTypeRulesPerson(), (rule) => {
          return Number(rule['id']) === Number(draggable[0].getAttribute('data-option-rule-id'));
        }), sourceIndex = _.findIndex(this.getTeamTypeRulesPerson(), (rule) => {
          return Number(rule['id']) === Number(draggable[1].getAttribute('data-option-rule-id'));
        });

        this.getTeamTypeRulesPerson()[sourceIndex]['child_roles'] = _.concat(
          this.getTeamTypeRulesPerson()[sourceIndex]['child_roles'],
          draggableRule['child_roles']
        );

        this.updateRulesPersonsGrouped(draggableIndex, sourceIndex);
        this.doSomethingWithRulePersonRoleEntity('put', sourceIndex);
      } else {
        this._alertSrv.error('Combinación de reglas no permitida, revise Min y Max de ambas reglas.');
      }
    } else {
      this._alertSrv.error('Combinación de reglas no permitida, contienen los mismos roles.');
    }
  }

  private rulePersonRolHasThisRol(rol: number,
                                  indexRulePerson: number): boolean {
    return _.size(_.filter(this.getTeamTypeRulesPerson()[indexRulePerson]['child_roles'], (rolInRule) => {
        return Number(rolInRule['id']) === Number(rol)
          && Number(rol) !== 1;
      })) <= 0;
  }

  private getIndexRulePersonRuleOnModel(ruleId: number): number {
    return _.findIndex(this.getTeamTypeRulesPerson(), (rule) => {
      return Number(rule['id']) === Number(ruleId);
    });
  }

  private getIndexRulePersonRuleSimpleOnModelByIndex(index: number): number {
    return this.getIndexRulePersonRuleOnModel(this.getTeamTypeRulesPersonSimple()[index]['id']);
  }

  private addNewRolToPersonRuleRole(draggable: object): void {
    const rol = draggable[0].getAttribute('data-id-option'),
      index = this.getIndexRulePersonRuleOnModel(draggable[1].getAttribute('data-option-rule-id'));

    if (this.rulePersonRolHasThisRol(rol, index)) {
      if (Number(rol) === 1) {
        this.getTeamTypeRulesPerson()[index]['child_roles'].push({id: 1, name: 'Rol editable xxx'});
      } else {
        this.getTeamTypeRulesPerson()[index]['child_roles'].push({id: rol});
      }
    } else {
      this._alertSrv.error('El role que intentas añadir ya lo tiene asignado esta persona.');
    }
    this.doSomethingWithRulePersonRoleEntity('put', index);
  }

  private addNewSubTeamToTeamRole(draggable: object): void {

    const subteam = draggable[0].getAttribute('data-id-option'),
      index = _.findIndex(this.getTeamTypeSubTeams(), (rule) => {
        return Number(rule['id']) === Number(draggable[1].getAttribute('data-option-rule-id'));
      });

    if (_.size(_.filter(this.getTeamTypeSubTeams()[index]['child_teamts'], (rolInRule) => {
        return Number(rolInRule['id']) === Number(subteam);
      })) <= 0) {

      const condAgregate = _.filter(this.teamTypeAllArray, (item) => {
        return Number(item['groups']) === 0;
      });

      if (!_.isNil(condAgregate[0]) && Number(subteam) === condAgregate[0]['id']) {
        this.getTeamTypeSubTeams()[index]['child_teamts'] = [];
        this.getTeamTypeSubTeams()[index]['child_teamts'].push({id: condAgregate[0]['id']});

      } else {
        this.getTeamTypeSubTeams()[index]['child_teamts'].push({id: subteam});
        _.remove(this.getTeamTypeSubTeams()[index]['child_teamts'], (rolInArray) => {
          return Number(rolInArray['id']) === Number(condAgregate[0]['id']);
        });
      }
    } else {
      this._alertSrv.error('El tipo de equipo que intentas añadir ya está configurado dentro de esta regla.');
    }

    this.doSomethingWithRuleTeamTypeRoleEntity('put', index);
  }

  /**
   * Logic join for teams rules
   * @param draggable
   */
  private addNewTeamTypeToSubTeamRule(draggable: object): void {
    const draggableRule = _.filter(this.getTeamTypeSubTeams(), (rule) => {
      return Number(rule['id']) === Number(draggable[0].getAttribute('data-option-rule-id'));
    })[0], sourceRule = _.filter(this.getTeamTypeSubTeams(), (rule) => {
      return Number(rule['id']) === Number(draggable[1].getAttribute('data-option-rule-id'));
    })[0];


    if (_.size(_.differenceBy(draggableRule['child_teamts'], sourceRule['child_teamts'], 'id')) > 0
      && _.isNil(_.find(draggableRule['child_teamts'], function (rol) {
        return rol.id === 1;
      }))) {

      // if draggableRule.Max < source.Max => ERROR
      if (draggableRule['max'] >= sourceRule['max']) {
        const draggableIndex = _.findIndex(this.getTeamTypeSubTeams(), (rule) => {
          return Number(rule['id']) === Number(draggable[0].getAttribute('data-option-rule-id'));
        }), sourceIndex = _.findIndex(this.getTeamTypeSubTeams(), (rule) => {
          return Number(rule['id']) === Number(draggable[1].getAttribute('data-option-rule-id'));
        });

        this.getTeamTypeSubTeams()[sourceIndex]['child_teamts'] =
          _.concat(this.getTeamTypeSubTeams()[sourceIndex]['child_teamts'], draggableRule['child_teamts']);

        this.updateRulesTeamsGrouped(draggableIndex, sourceIndex);
        this.doSomethingWithRuleTeamTypeRoleEntity('put', sourceIndex);
      }
    }
  }

  private teamTypeHasGroup(): boolean {
    return this.teamTypeOnWizard
      && this.teamTypeOnWizard.hasOwnProperty('groups')
      && this.teamTypeOnWizard['groups'].length > 0
      && this.teamTypeOnWizard['groups'][0].hasOwnProperty('name');
  }

  private teamTypeHasGroupDefined(): boolean {
    return this.teamTypeHasGroup();
  }

  private getAllRolesIdUsed(): Array<number | any> {

    const rolesUsed: Array<object> = [];

    if (this.hasTeamTypePersonRules()) {
      for (const ruleP of this.getTeamTypeRulesPersonSimple()) {
        if (ruleP.hasOwnProperty('child_roles')
          && ruleP['child_roles'].length > 0) {
          for (const rol of ruleP['child_roles']) {
            if (rol.hasOwnProperty('id')) {
              rolesUsed.push(rol['id']);
            }
          }
        }
      }
    }

    return rolesUsed;
  }

  private cleanUsedRolesOnPersonRules(): void {
    if (this.dragAndDropList.hasOwnProperty('categories')
      && this.tabSelectedId === 1) {

      this.dragAndDropList = {...this._dragAndDropListCopy};
      if (this.getAllRolesIdUsed().length > 0) {
        for (const options of this.dragAndDropList['categories']) {
          options['options'] = options['options'].map((rol: object) => {
            rol['visible'] = this.getAllRolesIdUsed().includes(rol['id']);
            return rol;
          });
        }
      }
    }
  }

  private findInfoTeamTypeChildren(id): object {
    return this._teamTypes.find((tt) => {
      return tt['id'] === id;
    });
  }

  private getTeamTypeById(id: number): object {
    const teamt = this._teamTypes.filter((item) => {
      return item['id'] === id;
    });

    return teamt.length > 0 ? teamt[0] : [];
  }


  private teamTypehasValidName(): boolean {
    return this.teamTypeOnWizard.hasOwnProperty('name')
      && this.teamTypeOnWizard['name'] !== null
      && this.teamTypeOnWizard['name'] !== undefined
      && this.teamTypeOnWizard['name'].length > 0;
  }

  private teamTypehasValidDescription(): boolean {
    return this.teamTypeOnWizard.hasOwnProperty('description')
      && this.teamTypeOnWizard['description'] !== null
      && this.teamTypeOnWizard['description'] !== undefined
      && this.teamTypeOnWizard['description'].length > 0;
  }

  private teamTypeHasRulePOrRuleTeamT(): boolean {
    return this.hasTeamTypeSubTeamsRules()
      || this.hasTeamTypePersonRules();
  }

  private teamTypeHasValidGroupDefined(): boolean {
    return this.teamTypeHasGroup()
      && this.teamTypeHasGroupDefined();
  }

  public finishMasterProcess(): void {
    if (this.validateMaster()) {
      this.finalizeMaster();
    }
  }

  private validateMaster(): boolean {
    if (!this.teamTypehasValidName()) {
      this._alertSrv.error('Nombre de Master vacío');
      return false;
    } else if (!this.teamTypehasValidDescription()) {
      this._alertSrv.error('Descripción de Master vacía');
      return false;
    } else if (!this.teamTypeHasValidGroupDefined()) {
      this._alertSrv.error('Grupo de Master vacío');
      return false;
    } else if (!this.teamTypeHasRulePOrRuleTeamT()) {
      this._alertSrv.error('Reglas de roles o subteams vacias');
      return false;
    } else {
      return true;
    }
  }

  private restartSearchTTTemplatesComponent(): void {
    this.searchTeamTypesTemplate.restartComponent();
  }

  private restartSearchGroupComponent(): void {
    this._forcedCleanGroupSearch = false;
    this.searchGroupTemplate.restartComponent();
  }

  private clearSearchableFilter(): void {
    this._teamTypes = this._teamTypes.map((tt) => {
      delete tt['searchable'];
      return tt;
    });
  }

  private filterTeamTypesTemplates(): void {
    for (const group of this._teamTypeGroupFilterSelected) {
      if (group.hasOwnProperty('selected')
        && group['selected']) {
        this._teamTypes = this._teamTypes.map((tt) => {
          tt['searchable'] = this.getKeysSelectedGroups().includes(tt['groups'][0]['id']);
          return tt;
        });
      }
    }
  }

  private isAllFilterEnabled(): boolean {
    return !_.isEmpty(this._teamTypeGroupFilterSelected)
      && this._teamTypeGroupFilterSelected[0]['id'] === -1;
  }

  private resetTeamTypes(): void {
    this.clearSearchableFilter();
    this.teamTypesFiltered = this.getTeamTypesFiltered();
  }

  private getAllSearchableTeamTypes(): Array<object> {
    if (this.isAllFilterEnabled()) {
      this.resetTeamTypes();
      return this.teamTypesFiltered;
    } else {
      return this._teamTypes.slice(0, this._ttOffsetToShow).filter((tt) => {
        return tt['searchable'];
      });
    }
  }

  private getKeysSelectedGroups(): Array<number> {
    return this._teamTypeGroupFilterSelected.filter((tt) => {
      return tt['selected'];
    }).map((tt) => {
      return tt['id'];
    });
  }

  private getAllInfoOfCurrentTeamTypeId(): void {
    if (this.teamTypeOnWizard && this.teamTypeOnWizard.hasOwnProperty('id')) {
      localStorage.setItem('newMaster', '1');
      this._router.navigateByUrl('/master/edit/' + this.teamTypeOnWizard['id']);
      this.getTeamTypeOnWizard();
      this.getAllNeededInfoFromApi();
      this.toggleTeamTypeTemplates();
    }
  }

  private updateTooltipCategoriesPosition(): void {
    if (this.tooltipFullTeamTypeIndex !== -1) {
      const tooltipble = this._elementRef.nativeElement.querySelector(
        (this._teamTypeTooltipElement === 'tab'
          ? '#tab_tooltip_teamt_n_'
          : '#card_tooltip_teamt_n_')
        + this.tooltipFullTeamTypeIndex);

      this.teamTypeTooltipPosition = {
        top: Math.round(
          tooltipble.getBoundingClientRect().top
          + this._tooltipOffset[this._teamTypeTooltipElement]['top']
        ),
        left: Math.round(
          tooltipble.getBoundingClientRect().left
          + tooltipble.getBoundingClientRect().width
          + this._tooltipOffset[this._teamTypeTooltipElement]['left']
          - this._elementRef.nativeElement.querySelector('#tooltipTeamType').getBoundingClientRect().width)
      };
    }
  }

  private getTeamTypesFiltered(): any {
    return this._teamTypes.slice(0, this._ttOffsetToShow);
  }

  private clearNewRolEntity(): void {
    this.newRol = {
      name: '',
      category: -1
    };
  }

  private cleanAllActiveRolesDragPills(): void {
    if (this.dragAndDropList.hasOwnProperty('categories')) {
      for (const options of this.dragAndDropList['categories']) {
        options['options'] = options['options']
          .map(option => {
            delete option['drag_ready'];
            return option;
          });
      }
    }
  }

  private filterCategoriesByName(): void {
    if (this.filterCategoriesByNameValue !== '') {
      this.dragAndDropList['categories'] = this._dragAndDropListCopy['categories'].filter(category => {
        return (category['name']) === (this.filterCategoriesByNameValue);
      });
    } else {
      this.dragAndDropList['categories'] = this._dragAndDropListCopy['categories'];
      this.thereAreResultsFiltered = true;
    }
  }

  public functionalityNotImplemented(msg?: string): void {
    this._alertSrv.prompt(msg);
  }


  /**************************************************************************************************/
  /**
   * Get all the roles info from api
   */
  private getSystemParams(): void {
    this._httpSrv.get(
      'domains/:d/parameters/'.replace(':d', this._authSrv.getDomainId().toString()),
    ).subscribe(
      (response) => {
        if (response.hasOwnProperty('success') && response['success']
          && response.hasOwnProperty('object') && response['object']) {

          for (const param of response['object']) {
            if (!param.hasOwnProperty('value') || !param.hasOwnProperty('parameter_type')) {
              continue;
            }

            if (param.parameter_type.code === 'MAX_PERSONS_TEAMT') {
              this._masterRuleParams.rules.person.max = parseInt(param.value);
              this._masterRuleParams.rules.team.max = parseInt(param.value);
              this._newMultiRoleRulePerson.max = parseInt(param.value);
            }
          }
        }
      }
    );
  }
}
