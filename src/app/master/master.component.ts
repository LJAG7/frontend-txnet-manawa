'use strict';

import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {DataTable, LazyLoadEvent} from 'primeng/primeng';

import {AuthSrvService} from '../shared/services/auth.service';
import {HttpSrvService} from '../shared/services/http.service';
import {LoaderService} from '../shared/services/loader.service';

const _ = require('lodash');

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.css'],
  providers: [HttpSrvService]
})

/**
 * @whatItDoes Contains the methods for TeamType to list and create this entity via API-endpoint
 * @author Elias Romero <elias.romero@steelmood.com>
 * @author Miguel Rico <miguel.rico@steelmood.com>
 */
export class MasterComponent implements OnInit {

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('mastersDatatable') private _mastersDatatable: DataTable;

  /************************ COMPONENT VARS  ************************/

  public masters = [];
  public isMasterRemovable = MasterComponent.isMasterRemovable;

  /************************ CONFIG VARS  ************************/

  public pagination = {
    page: 1,
    rows: 10,
    sortField: 'name',
    sortOrder: 'asc',
    totalRecords: 0,
  };


  /************************ STATIC METHODS  ************************/
  /**
   * Return if a master is removable
   * @param {Object} master
   * @returns {boolean}
   */
  public static isMasterRemovable(master: object): boolean {
    return !_.isNil(master) && master.hasOwnProperty('already_in_use') && !master['already_in_use'];
  }

  /************************ NATIVE METHODS  ************************/

  constructor(private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _loaderSrv: LoaderService,
              private _router: Router) {
    this._authSrv.verifyViewPermission('master');
    this._loaderSrv.general();
  }

  ngOnInit() { }


  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('master', tag);
  }


  /************************ SETUP METHODS ************************/
  /**
   * Load masters lazy
   * @param {LazyLoadEvent} event
   */
  public loadMastersLazy(event: LazyLoadEvent) {
    this.pagination.page = (event.first / event.rows) + 1;
    this.pagination.sortField = !_.isNil(event.sortField) ? event.sortField : this.pagination.sortField;
    const order = !_.isNil(event.sortOrder) ? event.sortOrder : 1;
    this.pagination.sortOrder = order === 1 ? 'asc' : 'desc';

    setTimeout(() => {
      if (this.masters) {
        this.getAllMasters();
      }
    }, 250);
  }


  /************************ EVENTS METHODS ************************/

  /**
   * Go to create team type view
   */
  public goToCreateTeamType(): void {
    this._router.navigateByUrl('/master/edit');
  }


  /**
   * On click export datatable to csv
   */
  public onClickExportToCsv(): void {
    this.getAllMastersToExport();
  }


  /**
   * On tableMenu event success
   * @param event
   */
  public onTableMenuEvent(event): void {
    if (event['action'] === 'reset') {
      this._mastersDatatable.reset();

    } else if (event['action'] === 'error') {
    }
  }

  /************************ API METHODS  ************************/

  /**
   * Get all team types
   */
  private getAllMasters(): void {
    this._httpSrv.get(
      'teamts/',
      {
        page: this.pagination.page,
        elements: this.pagination.rows,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      },
      'Listado masters'
    ).subscribe(
      success => this.onSuccessGetAllMasters(success)
    );
  }

  /**
   * On succes get all masters
   * @param response
   * @param {boolean} exportToCsv
   */
  private onSuccessGetAllMasters(response: any, exportToCsv: boolean = false): void {
    this._loaderSrv.stop();

    if (!_.isNil(response.object)) {
      this.masters = response.object.items;
      this.pagination.totalRecords = response.object.total_count;
    } else {
      this.masters = [];
      this.pagination.totalRecords = 0;
    }

    if (exportToCsv) {
      setTimeout(() => {
        this._mastersDatatable.exportCSV();
        this.getAllMasters();
      }, 300);
    }
  }

  /**
   * Get all masters to export
   */
  private getAllMastersToExport(): void {
    this._httpSrv.get(
      'teamts/',
      {
        page: 1,
        elements: this.pagination.totalRecords,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      },
      'Listado masters a exportar'
    ).subscribe(
      success => this.onSuccessGetAllMasters(success, true)
    );
  }

}

