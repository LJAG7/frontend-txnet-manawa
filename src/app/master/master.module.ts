/*GENERAL ASSETS*/
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule, Http} from '@angular/http';
/*MODULE ASSETS*/
import {MasterComponent} from './master.component';
import {EditMasterComponent} from './edit/edit-master.component';

/*SERVICES*/

import {ComponentsModule} from '../shared/components/components.module';
import {PipesModule} from '../shared/pipes/pipes.module';
import {TreeModule} from 'angular-tree-component';
import {DragulaModule} from 'ng2-dragula';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    ComponentsModule,
    TreeModule,
    DragulaModule,
    PipesModule
  ],
  exports: [
  ],
  declarations: [
    MasterComponent,
    EditMasterComponent
  ],
  providers: [
    // HttpSrvService,
    // LogService
  ]
})

export class MasterModule {
}

