import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {PersonModel} from '../../shared/models/person.model';

import {AuthSrvService} from '../../shared/services/auth.service';
import {NodeModel} from "../../shared/models/tree-team-group/node.model";

@Component({
  selector: 'app-mate-profile-left-panel-component',
  templateUrl: './mate-profile-left-panel.component.html',
  styleUrls: ['./mate-profile-left-panel.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class MateProfileLeftPanelComponent implements OnInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() gestorViewMode = true;
  @Input() person: PersonModel;
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/



  /************************ CONFIG VARS  ************************/

  private _teamTreeConfig = {
    flags: {
      hasGeneralSearch: false,
      isTeamType: true,
      showDropdown: true
    },
    css: 'px-sidebar'
  };

  private _groupTreeConfig = {
    flags: {
      hasGeneralSearch: false,
      isGroupType: true,
      showDropdown: true
    },
    css: 'px-sidebar not-allowed not-allowed-cover'
  };

  /************************ NATIVE METHODS ************************/

  constructor(private _authSrv: AuthSrvService,
              private _router: Router) { }

  ngOnInit() {
  }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('mate-profile', tag);
  }

  /************************ CONFIG METHODS *************************/

  /**
   * Get team tree-team-group
   * @returns {Object}
   */
  public getTeamTreeComponentConfig(): object {
    return this._teamTreeConfig;
  }

  /**
   * Get group tree-team-group
   * @returns {Object}
   */
  public getGroupTreeComponentConfig(): object {
    return this._groupTreeConfig;
  }

  /**
   * CHeck if gestor mode
   * @returns {boolean}
   */
  public isGestorMode(): boolean {
    return this.gestorViewMode;
  }

  /**
   * On change tree-team-group components selected node
   * @param {Object} event
   */
  public onChangeTreeComponentSelectedNode(event: object) {
    // @ToDo: Implementar la preselección de un nodo mediante localStorage
    localStorage.setItem('selectedNode', JSON.stringify(event));
    this._router.navigateByUrl('/team-mate');
  }
}
