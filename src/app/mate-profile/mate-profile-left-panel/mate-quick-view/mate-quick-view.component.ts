import {Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';

import {ColorGeneratorPipe} from '../../../shared/pipes/colorGenerator.pipe';
import {DateLocalPipe} from '../../../shared/pipes/dateLocal.pipe';
import {ValidationObjectPipe} from '../../../shared/pipes/validation.pipe';

import {BaseComponentModel} from '../../../shared/models/base-component/base-component.class';
import {PersonModel} from '../../../shared/models/person.model';
import {TeamPersonModel} from '../../../shared/models/team/teamPerson.model';
import {RulePersonRoleModel} from '../../../shared/models/team/rulePersonRole.model';

const _ = require('lodash');

@Component({
  selector: 'app-mate-quick-view-component',
  templateUrl: './mate-quick-view.component.html',
  styleUrls: ['./mate-quick-view.component.css'],
  providers: [ColorGeneratorPipe, DateLocalPipe, ValidationObjectPipe]
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class MateQuickViewComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() person: PersonModel;
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() outputEvent = new EventEmitter();

  /************************ COMPONENT VARS  ************************/

  public expandedStatus = true;

  /************************ NATIVE METHODS ************************/

  constructor(private _colorGeneratorPipe: ColorGeneratorPipe,
              private _dateLocalPipe: DateLocalPipe) {
    super();
  }

  ngOnInit() {
    this.config.id = 'mate_quick_view_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void { }

  /************************ EVENTS METHODS ************************/

  /**
   * Get expansion button text
   * @returns {string}
   */
  public getExpansionButtonText(): string {
    if (!_.isNil(this.person) && !_.isEmpty(this.person)) {
      return this.expandedStatus ? 'Ocultar quick view sobre ' + this.person.getNameFirstSurname() :
        'Mostrar quick view sobre ' + this.person.getNameFirstSurname();
    } else {
      return '';
    }
  }

  /**
   * Get team dates
   * @param {TeamPersonModel} teamPerson
   * @returns {string}
   */
  public getTeamPersonDates(teamPerson: TeamPersonModel): string {
    let teamPersonDates = '';
    if (!_.isNil(teamPerson.team) && !_.isEmpty(teamPerson.team)) {
      if (!_.isNil(teamPerson.team.start_date) && !_.isEmpty(teamPerson.team.start_date)) {
        teamPersonDates += this._dateLocalPipe.transform(teamPerson.team.start_date, 'month-year') + ' - ';
      } else {
        teamPersonDates += 'Indefinido - ';
      }
      if (!_.isNil(teamPerson.team.end_date) && !_.isEmpty(teamPerson.team.end_date)) {
        teamPersonDates += this._dateLocalPipe.transform(teamPerson.team.end_date, 'month-year');
      } else {
        teamPersonDates += 'Indefinido';
      }
    }
    return teamPersonDates;
  }

  /**
   * Get person location
   * @returns {string}
   */
  public getMateLocation(): string {
    let location = '';
    if (!_.isNil(this.person.location) && !_.isEmpty(this.person.location)) {
      if (!_.isNil(this.person.location.province) && !_.isEmpty(this.person.location.province)) {
        location += this.person.location.province;
      }
      if (!_.isNil(this.person.location.country) && !_.isEmpty(this.person.location.country)) {
        if (location !== '') {
          location += ', ' + this.person.location.country;
        } else {
          location += this.person.location.country;
        }
      }
    }
    if (_.isEmpty(location)) {
      location = 'Sin localización asignada';
    }
    return location;
  }

  /**
   * Get team color
   * @param {string} teamName
   * @returns {string}
   */
  public getTeamColor(teamName: string): string {
    return this._colorGeneratorPipe.transform(teamName);
  }

  /**
   * Get rule person role description
   * @param {RulePersonRoleModel} rulePersonRole
   * @returns {string}
   */
  public getRulePersonRoleHeader(rulePersonRole: RulePersonRoleModel): string {
    const header = [];

    if (!_.isNil(rulePersonRole.alias) && rulePersonRole.alias !== '') {
      header.push(rulePersonRole.alias);

    } else if (!_.isNil(rulePersonRole.category_name) && rulePersonRole.category_name !== '') {
      header.push(rulePersonRole.category_name);
    }

    if (!_.isNil(rulePersonRole.description) && rulePersonRole.description !== '') {
      header.push(rulePersonRole.description);
    }


    return _.isEmpty(header)
      ? ''
      : header.join(', ');
  }



  /**
   * Check if person has team
   * @returns {boolean}
   */
  public hasTeams(): boolean {
    return (!_.isNil(this.person) && !_.isNil(this.person.team_persons));
  }

  /**
   * Check if expanded
   * @returns {boolean}
   */
  public isExpanded(): boolean {
    return this.expandedStatus;
  }

  /**
   * Toggle expanded status
   */
  public toggleExpandedStatus(): void {
    this.expandedStatus = !this.expandedStatus;
  }
}

