import {Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {ValidationObjectPipe} from '../../../../shared/pipes/validation.pipe';

import {WIDGETTYPES} from '../../../../shared/const/widgettypes.const';
import {BaseComponentModel} from '../../../../shared/models/base-component/base-component.class';
import {DashboardEntryModel} from '../../../../shared/models/widgets/dashboard-entry.model';
import {PersonModel} from '../../../../shared/models/person.model';

import {AuthSrvService} from '../../../../shared/services/auth.service';
import {HttpSrvService} from '../../../../shared/services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-mate-dashboard-config-component',
  templateUrl: './mate-dashboard-config.component.html',
  styleUrls: ['./mate-dashboard-config.component.css'],
  providers: [HttpSrvService, ValidationObjectPipe]
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class MateDashboardConfigComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() outputEvent = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  /************************ COMPONENT VARS  ************************/

  public person: PersonModel = new PersonModel();
  public widgetType = null;

  private _myDashboardEntriesSelected: Array<DashboardEntryModel> = [];
  private _subscribeIdParam;

  /************************ CONFIG VARS  ************************/

  /************************ NATIVE METHODS ************************/

  constructor(
              private _activatedRoute: ActivatedRoute,
              private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _router: Router,
              private _validationObjectPipe: ValidationObjectPipe) {
    super();
  }

  ngOnInit() {
    this._subscribeIdParam = this._activatedRoute.params.subscribe(params => {
      if (this.isValidObjectProperty(params, 'id')) {
        this.person.id = parseInt(params['id'], 0);
        if (this.person.id !== 0) {
          this.getPerson(this.person.id);
          if (this.person.id === this._authSrv.getPersonId()) {
            this.widgetType = WIDGETTYPES.MATE_MATE;
          } else if (this._authSrv.getPersonLogged().isGestor()) {
            this.widgetType = WIDGETTYPES.GESTOR_MATE;
          }

        }
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  /************************ CONFIG METHODS *************************/

  /************************ EVENTS METHODS ************************/

  /**
   * Return to main section
   */
  public goBackToProfile(): void {
    this._router.navigateByUrl('/mate-profile/' + this.person.id);
  }

  /**
   * On select widget-container
   * @param {Object} event
   */
  public onSelectWidget(event: object): void {
    if (this.isValidObjectProperty(event, 'dashboardEntries')) {
      this._myDashboardEntriesSelected = event['dashboardEntries'];
    }
  }

  /**
   * Restore default widgets
   */
  public restoreWidgets(): void {
    if (!_.isNil(this._myDashboardEntriesSelected) && !_.isEmpty(this._myDashboardEntriesSelected)) {
      this._httpSrv
        .get('persons/my/dashboards/' + this.widgetType + '/reset')
        .subscribe(
        (responseOk) => this.onSuccessRestoreWidgets(responseOk)
      );
    } else {
      this.goBackToProfile();
    }
  }

  /**
   * Save actual widget config
   */
  public saveConfig(): void {
    if ( !_.isNil(this._myDashboardEntriesSelected)) {
      this.sendData();
    } else {
      this.goBackToProfile();
    }
  }

  /**
   * Send data to API to save actual widgets selected
   */
  public sendData(): void {
    if (!_.isNil(this._myDashboardEntriesSelected)) {
      const saveWidgets = [];

      for (let i = 0; i < this._myDashboardEntriesSelected.length; i++) {
        const dashboardEntrySelected = this._myDashboardEntriesSelected[i];

        if (dashboardEntrySelected.position !== 0) {
          saveWidgets.push({
            position: dashboardEntrySelected.position,
            widget: {
              tag: dashboardEntrySelected.widget.tag
            }
          });
        }
      }

      this._httpSrv.put(
        'dashboards/' + this.widgetType + '/dashboardentries/',
        {
          array: saveWidgets
        }
      ).subscribe(
        (responseOk) => this.onSuccessSendData(responseOk)
      );
    }
  }

  /********************* API METHODS *********************/

  /**
   * Get person
   * @param {number} id
   */
  private getPerson(id: number) {
    this._httpSrv
      .get('persons/' + id)
      .subscribe(
        (responseOk) => this.onSuccessGetPerson(responseOk)
      );
  }


  /**
   * On success get person
   * @param {Object | any} response
   */
  private onSuccessGetPerson(response: object | any): void {
    if (this._validationObjectPipe.isSuccessResponse(response)
      && this._validationObjectPipe.isValidObjectProperty(response, 'object')) {
      this.person = Object.assign(new PersonModel(), response['object']);
    }
  }

  /**
   * On success restore default widgets
   * @param {Object | any} response
   */
  private onSuccessRestoreWidgets(response: object | any): void {
    if (this.isValidObjectProperty(response, 'success')) {
      this.goBackToProfile();
    }
  }

  /**
   * On success save widgetConfig
   * @param {Object} response
   */
  private onSuccessSendData(response: object): void {
    if (this.isValidObjectProperty(response, 'success')) {
      this.goBackToProfile();
    }
  }
}
