import {
  Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ViewChild, AfterViewInit
} from '@angular/core';
import {DataTable} from 'primeng/primeng';

import {BaseComponentModel} from '../../../../../shared/models/base-component/base-component.class';
import {DatatableDataModel} from '../../../../../shared/models/scope-analytics/datatable/datatable-data.model';
import {DatatableLabelModel} from '../../../../../shared/models/scope-analytics/datatable/datatable-label.model';
import {DatatableElementModel} from '../../../../../shared/models/scope-analytics/datatable/datatable-element.model';
import {BarChartDataModel} from '../../../../../shared/models/scope-analytics/charts/barchart/bar-chart-data.model';
import {LineChartDataModel} from '../../../../../shared/models/scope-analytics/charts/linechart/line-chart-data.model';
import {StatusPeriodsFilterModel} from '../../../../../shared/models/status-periods/status-periods-filter.model';
import {StatusPeriodsModel} from '../../../../../shared/models/status-periods/status-periods.model';
import {StatusPeriodsDateModel} from '../../../../../shared/models/status-periods/status-periods-date.model';


const moment = require('moment');
const _ = require('lodash');

@Component({
  selector: 'app-mate-scope-analytics-chart-component',
  templateUrl: './mate-scope-analytics-chart.component.html',
  styleUrls: ['./mate-scope-analytics-chart.component.css']
})

/**
 * @author Elias Romero <elias.romero@steelmood.com>
 * @author Miguel Rico <miguel.rico@steelmood.com>
 */
export class MateScopeAnalyticsChartComponent extends BaseComponentModel implements OnInit, OnChanges, AfterViewInit {

  public static staticStatusPeriodsDatatableData;

  /************************ COMPONENT INPUTS  ************************/

  @Input() statusPeriods: StatusPeriodsFilterModel;
  @Input() expanded: false;
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() onChangeShowByTeams = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('datatableChart') private _datatableChart: DataTable;
  @ViewChild('barChart') barChart;
  @ViewChild('lineChart') lineChart;

  /************************ COMPONENT VARS  ************************/

  public barChartData = new BarChartDataModel();
  public lineChartData = new LineChartDataModel();

  public statusPeriodsDatatableData = new DatatableDataModel();

  public getData = MateScopeAnalyticsChartComponent.getData;

  public showByTeams = true;

  private _activeChart = 0;

  private _initialStatusPeriods: StatusPeriodsModel;
  private _comparativeStatusPeriods: StatusPeriodsModel;

  private _initialStatusPeriodsDatatableData = new DatatableDataModel();
  private _comparativeStatusPeriodsDatatableData = new DatatableDataModel();

  /************************ CONFIG VARS  ************************/

  public barChartOptions = {
    responsive: true,
    maintainAspectRatio: true,
    scales: {
      yAxes: [{
        ticks: {
          min: 0
        },
        scaleLabel: {
          display: true,
          labelString: 'Hrs'
        }
      }],
      xAxes: [{
        scaleLabel: {
          display: false
        }
      }]
    }
  };

  public lineChartOptions = {
    responsive: true,
    maintainAspectRatio: true,
    scales: {
      yAxes: [{
        ticks: {
          min: 0
        },
        scaleLabel: {
          display: true,
          labelString: 'Hrs'
        }
      }]
    }
  };

  /************************ STATIC METHODS  ************************/

  /**
   * CHeck if date is before leap date
   * @param {string} date
   * @returns {boolean}
   */
  private static checkDateBeforeLeapDate(date: string): boolean {
    return moment(date, 'DD/MM/YYYY').get('date') === 28
      && moment(date, 'DD/MM/YYYY').get('month') === 1;
  }

  /**
   * Check if date is leap date
   * @param {string} date
   * @returns {boolean}
   */
  private static isLeapDate(date: string): boolean {
    return moment(date, 'DD/MM/YYYY').get('date') === 29
      && moment(date, 'DD/MM/YYYY').get('month') === 1;
  }

  /**
   * Get data
   * @param {DatatableElementModel} dataset
   * @param {number} i
   * @returns {number}
   */
  public static getData(dataset: DatatableElementModel, i: number): string {
    return i <= dataset.data.length ?
      dataset.data[i - 1]
      : '0.00';
  }

  /**
   * Get datatable value
   * @param {string | number} value
   * @returns {string}
   */
  private static getDatatableValue(value: string | number): string {
    if (_.isString(value)) {
      return (Number(value) > 0) ? (Number(value) / 60).toFixed(2) + 'hrs' : '0.00 hrs';
    } else if (_.isNumber(value)) {
      return value > 0 ? (Number(value) / 60).toFixed(2) + 'hrs' : '0.00 hrs';
    }
  }

  /**
   * CHeck if status periods has leap date
   * @param {StatusPeriodsModel} statusPeriods
   * @returns {boolean}
   */
  private static hasLeapDate(statusPeriods: StatusPeriodsModel): boolean {
    for (let i = 0; i < statusPeriods.dates.length; i++) {
      if (MateScopeAnalyticsChartComponent.isLeapDate(statusPeriods.dates[i].date)) {
        return true;
      }
    }
    return false;
  }

  /************************ NATIVE METHODS  ************************/

  constructor() {
    super();
  }

  ngOnInit() {
    this.config.id = 'mate_scope_analytics_chart_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ( (!_.isNil(changes['statusPeriods']) && !_.isNil(changes['statusPeriods']['currentValue']))) {
      this._initialStatusPeriods = changes['statusPeriods']['currentValue'].initialStatusPeriods;
      this._comparativeStatusPeriods = changes['statusPeriods']['currentValue'].comparativeStatusPeriods;
      this.buildDatatable();
      this.buildChart();
    }
  }

  ngAfterViewInit(): void {
    if (!_.isNil(this._datatableChart)) {
      this._datatableChart.resolveFieldData = function (data, field) {
        if (data && field) {
          if (field === 'Equipos' || field === 'Total') {
            return data.label;
          } else {
            const dateIndex = MateScopeAnalyticsChartComponent.staticStatusPeriodsDatatableData.labels.findIndex( label => {
              return label.value === field;
            });
            if (dateIndex <= data.data.length) {
              return data.data[dateIndex].replace('hrs', '').replace('.', ',').trim();
            }
          }
        } else {
          return '0.00';
        }
      };
    }
  }

  /************************ EVENTS METHODS ************************/

  /**
   * Get active chart type
   * @returns {number}
   */
  public getActiveChart(): number {
    return this._activeChart;
  }

  /**
   * Get datatable field name
   * @param {number} index
   * @returns {string}
   */
  public getDatatableFieldName(index: number): string {
    return this.statusPeriodsDatatableData.labels[index].value;
  }

  /**
   * Check if has status periods
   * @returns {boolean}
   */
  public hasStatusPeriods(): boolean {
    return (!_.isNil(this._initialStatusPeriods) && !_.isEmpty(this._initialStatusPeriods.dates))
      || (!_.isNil(this._comparativeStatusPeriods) && !_.isEmpty(this._comparativeStatusPeriods.dates));
  }

  /**
   * Check if has data
   */
  public hasData(): boolean {
    return (!_.isNil(this._initialStatusPeriods) && !_.isEmpty(this._initialStatusPeriods.teams))
      || (!_.isNil(this._comparativeStatusPeriods) && !_.isEmpty(this._comparativeStatusPeriods.teams));
  }

  /**
   * On select chart type
   * @param {number} type
   */
  public onChangeActiveChart(type: number): void {
    this.setActiveChart(type);
    // this.buildChart();
  }

  /**
   * On change show by teams
   */
  public onChangeShowByTeamsValue(): void {
    this.showByTeams = !this.showByTeams;
    this.onChangeShowByTeams.emit({
      idComponent: this.config.id,
      value: this.showByTeams
    });
    this.buildDatatable();
    this.buildChart();
  }

  /**
   * On click export datatable to csv
   */
  public onClickExportToCsv(): void {
    this._datatableChart.exportCSV();
  }

  /**
   * Set active chart type
   * @returns {number}
   */
  public setActiveChart(type: number): void {
    this._activeChart = type;
  }

  /************************ AUXILIAR METHODS ************************/

  /**
   * Build chart dataset according type
   */
  private buildChart(): void {
    if (!_.isNil(this.statusPeriods)) {
      this.barChartData = new BarChartDataModel();
      this.buildCharDataset(this.barChartData);
      this.lineChartData = new LineChartDataModel();
      this.buildCharDataset(this.lineChartData);
    }
  }

  /**
   * Build dataset
   * @param data
   */
  private buildCharDataset(data: BarChartDataModel | LineChartDataModel): void {
    data = data instanceof BarChartDataModel ? new BarChartDataModel()
        : data instanceof LineChartDataModel ? new LineChartDataModel()
          : null;

    if (!_.isNil(data)) {

      // Build labels
      if (!_.isNil(this._initialStatusPeriods) && !_.isEmpty(this._initialStatusPeriods.teams)
        && !_.isNil(this._comparativeStatusPeriods) && !_.isEmpty(this._comparativeStatusPeriods.teams)) {

        const initialLabels = this.getChartLabels(data, this._initialStatusPeriods.dates);
        const comparativeLabels = this.getChartLabels(data, this._comparativeStatusPeriods.dates);
        const maxLength = initialLabels.length === comparativeLabels.length || initialLabels.length > comparativeLabels.length ?
          initialLabels.length : comparativeLabels.length;

        for (let i = 0; i < maxLength; i++) {
          let finalLabel = '';
          if (i < initialLabels.length) {
            finalLabel += initialLabels[i];
          } else {
            finalLabel += 'N/A';
          }
          if (i < comparativeLabels.length) {
            finalLabel += ' - ' + comparativeLabels[i];
          } else {
            finalLabel += ' - N/A';
          }
          data.addLabel(finalLabel);
        }

        // if (this._initialStatusPeriods.dates.length === this._comparativeStatusPeriods.dates.length
        //   || this._initialStatusPeriods.dates.length > this._comparativeStatusPeriods.dates.length) {
        //   this.getChartLabels(data, this._initialStatusPeriods.dates);
        // } else {
        //   this.getChartLabels(data, this._initialStatusPeriods.dates, this._comparativeStatusPeriods.dates.length);
        // }

        // if (this._initialStatusPeriods.dates.length >= this._comparativeStatusPeriods.dates.length) {
        //   this.getChartLabels(data, this._initialStatusPeriods.dates);
        // } else if (this._initialStatusPeriods.dates.length < this._comparativeStatusPeriods.dates.length) {
        //   this.getChartLabels(data, this._comparativeStatusPeriods.dates);
        // }

      } else if (!_.isNil(this._initialStatusPeriods) && !_.isEmpty(this._initialStatusPeriods.teams)) {
        const labels = this.getChartLabels(data, this._initialStatusPeriods.dates);
        labels.forEach( label => {
          data.addLabel(label);
        });

      } else if (!_.isNil(this._comparativeStatusPeriods) && !_.isEmpty(this._comparativeStatusPeriods.teams)) {
        const labels = this.getChartLabels(data, this._comparativeStatusPeriods.dates);
        labels.forEach( label => {
          data.addLabel(label);
        });
        // this.getChartLabels(data, this._comparativeStatusPeriods.dates);
      }

      // Build datasets
      if (!_.isNil(this._initialStatusPeriods) && !_.isEmpty(this._initialStatusPeriods.teams)) {
        if (this.showByTeams) {
          this._initialStatusPeriods.teams.forEach(team => {
            data.addDataset(team.name);
          });
        } else {
          data.addDataset('Total');
        }
      }
      if (!_.isNil(this._comparativeStatusPeriods) && !_.isEmpty(this._comparativeStatusPeriods.teams)) {
        if (this.showByTeams) {
          this._comparativeStatusPeriods.teams.forEach(team => {
            data.addDataset(team.name + ' Comparativo');
          });
        } else {
          data.addDataset('Total comparativo');
        }
      }

      // Build data for each dataset
      if (!_.isNil(this._initialStatusPeriods) && !_.isEmpty(this._initialStatusPeriods.teams)) {
        if (this.showByTeams) {
          this._initialStatusPeriods.dates.forEach( date => {
            date.teams.forEach( team => {
              data.addDataToDataset(team.name, Number(team.value));
            });
          });
        } else {
          this._initialStatusPeriods.dates.forEach( date => {
            let total = 0;
            date.teams.forEach( team => {
              total += Number(team.value);
            });
            data.addDataToDataset('Total', total);
          });
        }
      }
      if (!_.isNil(this._comparativeStatusPeriods) && !_.isEmpty(this._comparativeStatusPeriods.teams)) {
        if (this.showByTeams) {
          this._comparativeStatusPeriods.dates.forEach( date => {
            date.teams.forEach( team => {
              data.addDataToDataset(team.name + ' Comparativo', Number(team.value));
            });
          });
        } else {
          this._comparativeStatusPeriods.dates.forEach( date => {
            let total = 0;
            date.teams.forEach( team => {
              total += Number(team.value);
            });
            data.addDataToDataset('Total comparativo', total);

          });
        }
      }
    }

    if (data instanceof BarChartDataModel) {
      this.barChartData = data;
    } else if (data instanceof LineChartDataModel) {
      this.lineChartData = data;
    }
  }

  private getChartLabels(chart: BarChartDataModel | LineChartDataModel, dates: Array<StatusPeriodsDateModel>): Array<string> {
    const labels = [];
    let year = moment(dates[0].date, 'DD/MM/YYYY').get('year');
    dates.forEach( date => {
      if (date.date === 'N/A') {
        // chart.addLabel('29/02/' + year);
        labels.push('29/02/' + year);
      } else {
        // chart.addLabel(date.date);
        labels.push(date.date);
      }
      year = moment(date).get('year');
    });

    return labels;
  }

  /**
   * Build complete status periods dataset
   */
  private buildDatatable(): void {
    this._initialStatusPeriodsDatatableData = null;
    this._comparativeStatusPeriodsDatatableData = null;
    this.statusPeriodsDatatableData = new DatatableDataModel();

    if (!_.isNil(this._initialStatusPeriods)) {
      this._initialStatusPeriodsDatatableData = this.buildStatusPeriodsDatatable(this._initialStatusPeriods, false);
    }

    if (!_.isNil(this._comparativeStatusPeriods)) {
      this._comparativeStatusPeriodsDatatableData = this.buildStatusPeriodsDatatable(this._comparativeStatusPeriods, true);
    }

    this.buildComparativeStatusPeriods();
    MateScopeAnalyticsChartComponent.staticStatusPeriodsDatatableData = this.statusPeriodsDatatableData;
  }

  /**
   * Build comparative datatable between two status periods
   */
  private buildComparativeStatusPeriods(): void {
    if (_.isNil(this._initialStatusPeriodsDatatableData)
      && !_.isNil(this._comparativeStatusPeriodsDatatableData)) {
      this.statusPeriodsDatatableData = this._comparativeStatusPeriodsDatatableData;
    } else if (_.isNil(this._comparativeStatusPeriodsDatatableData)
      && !_.isNil(this._initialStatusPeriodsDatatableData)) {
      this.statusPeriodsDatatableData = this._initialStatusPeriodsDatatableData;
    } else if (!_.isNil(this._initialStatusPeriodsDatatableData)
      && !_.isNil(this._comparativeStatusPeriodsDatatableData)) {

      // Build datatable labels
      this.statusPeriodsDatatableData = new DatatableDataModel();

      // Team or total label
      if (_.isEmpty(this.statusPeriodsDatatableData.labels) && this.showByTeams) {
        this.statusPeriodsDatatableData.addLabel(new DatatableLabelModel({value: 'Equipos'}));
      } else if (_.isEmpty(this.statusPeriodsDatatableData.labels) && !this.showByTeams) {
        this.statusPeriodsDatatableData.addLabel(new DatatableLabelModel({value: 'Total'}));
        this.statusPeriodsDatatableData.addDataset('Total');
      }

      // Find leap date
      const initialWithLeapDate = MateScopeAnalyticsChartComponent.hasLeapDate(this._initialStatusPeriods);
      const comparativeWithLeapDate = MateScopeAnalyticsChartComponent.hasLeapDate(this._comparativeStatusPeriods);

      // Check leap date
      if (initialWithLeapDate) {
        for (let i = 0; i < this._initialStatusPeriods.dates.length; i++) {
          if (MateScopeAnalyticsChartComponent.checkDateBeforeLeapDate(this._initialStatusPeriods.dates[i].date)) {
            this._comparativeStatusPeriods.addNullDate('N/A', i + 1);
            break;
          }
        }
      } else if (comparativeWithLeapDate) {
        for (let i = 0; i < this._comparativeStatusPeriods.dates.length; i++) {
          if (MateScopeAnalyticsChartComponent.checkDateBeforeLeapDate(this._comparativeStatusPeriods.dates[i].date)) {
            this._initialStatusPeriods.addNullDate('N/A', i + 1);
            break;
          }
        }
      }

      // Get max length of status periods
      const maxLength = this._initialStatusPeriods.dates.length === this._comparativeStatusPeriods.dates.length ?
        this._initialStatusPeriods.dates.length :
        this._initialStatusPeriods.dates.length > this._comparativeStatusPeriods.dates.length ?
          this._initialStatusPeriods.dates.length : this._comparativeStatusPeriods.dates.length;

      // Build status periods
      for (let i = 0; i < maxLength ; i++) {

        if (!_.isNil(this._initialStatusPeriods.dates[i])) {
          const initialStatusPeriods = this._initialStatusPeriods.dates[i];
          this.statusPeriodsDatatableData.addLabel(new DatatableLabelModel({value: initialStatusPeriods.date, comparative: false}));

          let total = 0;
          initialStatusPeriods.teams.forEach( team => {
            if (this.showByTeams) {
              this.statusPeriodsDatatableData.addDataset(team.name);
              this.statusPeriodsDatatableData.addDataToDataset(team.name, MateScopeAnalyticsChartComponent.getDatatableValue(team.value));
            } else {
              total += Number(team.value);
            }
          });
          if (!this.showByTeams) {
            this.statusPeriodsDatatableData.addDataToDataset('Total', MateScopeAnalyticsChartComponent.getDatatableValue(total));
          }
        }

        if (!_.isNil(this._comparativeStatusPeriods.dates[i])) {
          const comparativeStatusPeriods = this._comparativeStatusPeriods.dates[i];
          this.statusPeriodsDatatableData.addLabel(new DatatableLabelModel({value: comparativeStatusPeriods.date, comparative: true}));

          let total = 0;
          comparativeStatusPeriods.teams.forEach( team => {
            if (this.showByTeams) {
              this.statusPeriodsDatatableData.addDataset(team.name);
              this.statusPeriodsDatatableData.addDataToDataset(team.name, MateScopeAnalyticsChartComponent.getDatatableValue(team.value));
            } else {
              total += Number(team.value);
            }
          });
          if (!this.showByTeams) {
            this.statusPeriodsDatatableData.addDataToDataset('Total', MateScopeAnalyticsChartComponent.getDatatableValue(total));
          }
        }
      }
    }
  }

  /**
   * BUild a single status periods datatable
   * @param {StatusPeriodsModel} statusPeriods
   * @param {boolean} comparative
   * @returns {DatatableDataModel}
   */
  private buildStatusPeriodsDatatable(statusPeriods: StatusPeriodsModel, comparative: boolean = false): DatatableDataModel {
    const data = new DatatableDataModel();

    if (_.isEmpty(data.labels) && this.showByTeams) {
      data.addLabel(new DatatableLabelModel({value: 'Equipos'}));
    } else if (_.isEmpty(data.labels) && !this.showByTeams) {
      data.addLabel(new DatatableLabelModel({value: 'Total'}));
      data.addDataset('Total');
    }

    statusPeriods.dates.forEach( date => {
      // Add label to collection
      data.addLabel(new DatatableLabelModel({value: date.date, comparative: comparative}));

      // Add team
      if (this.showByTeams) {
        if ( !_.isNil(date)) {
          date.teams.forEach(team => {
            data.addDataset(team.name);
          });
        }
      }
    });

    // Add values in each date
    statusPeriods.dates.forEach( date => {
      // Add team
      if (this.showByTeams) {
        if ( !_.isNil(date)) {
          date.teams.forEach(team => {
            data.addDataToDataset(team.name, MateScopeAnalyticsChartComponent.getDatatableValue(team.value));
          });
        }
      } else {
        data.addDataToDataset('Total', MateScopeAnalyticsChartComponent.getDatatableValue(date.status.working));
      }
    });

    return data;
  }
}
