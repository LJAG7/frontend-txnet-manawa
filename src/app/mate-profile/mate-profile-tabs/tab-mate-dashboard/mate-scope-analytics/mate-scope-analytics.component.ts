import {
  Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, AfterViewInit
} from '@angular/core';

import {PersonModel} from '../../../../shared/models/person.model';
import {BaseComponentModel} from '../../../../shared/models/base-component/base-component.class';
import {StatusPeriodsFilterModel} from '../../../../shared/models/status-periods/status-periods-filter.model';

const _ = require('lodash');

@Component({
  selector: 'app-mate-scope-analytics-component',
  templateUrl: './mate-scope-analytics.component.html',
  styleUrls: ['./mate-scope-analytics.component.css']
})

/**
 * @author Elias Romero <elias.romero@steelmood.com>
 * @author Miguel Rico <miguel.rico@steelmood.com>
 */
export class MateScopeAnalyticsComponent extends BaseComponentModel implements OnInit, AfterViewInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() expanded: false;
  @Input() person: PersonModel;
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() onChangeShowDetails = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  /************************ COMPONENT VARS  ************************/

  public statusPeriods: StatusPeriodsFilterModel = new StatusPeriodsFilterModel();

  /************************ CONFIG VARS  ************************/

  /************************ NATIVE METHODS ************************/

  constructor() {
    super();
  }

  ngOnInit() {
    this.config.id = 'mate_scope_analytics_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngAfterViewInit(): void { }

  /************************ EVENTS METHODS ************************/

  /**
   * On change status periods
   * @param {StatusPeriodsFilterModel} event
   */
  public onChangeStatusPeriods(event: StatusPeriodsFilterModel): void {
    this.statusPeriods = event;
  }
}
