import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {PersonModel} from '../../../shared/models/person.model';

import {BaseComponentModel} from '../../../shared/models/base-component/base-component.class';

const _ = require('lodash');

@Component({
  selector: 'app-tab-mate-dashboard-component',
  templateUrl: './tab-mate-dashboard.component.html',
  styleUrls: ['./tab-mate-dashboard.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TabMateDashboardComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUT  ************************/

  @Input() private person: PersonModel;
  @Input() private widgetType = null;

  /************************ COMPONENT VIEW CHILD  ************************/

  @ViewChild('noteFormComponent') noteFormComponent;
  @ViewChild('noteListComponent') noteListComponent;
  @ViewChild('noteBoardComponent') noteBoardComponent;

  /************************ COMPONENT VARS  ************************/

  public gestorViewMode = true;
  public apiLoader = {
    charts: true,
    notes: false
  };

  private _mainFullPage = false;
  private _mainViewMode = 'mate-dashboard-config';
  private _mainViewModeList = [
    'mate-dashboard-config',
    'notes'
  ];

  /************************ NATIVE METHODS ************************/

  constructor(private _router: Router) {
    super();
  }

  ngOnInit() {
    this.config.id = 'tab_mate_dashboard_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void { }

  /************************ EVENT METHODS ************************/

  /**
   * Event call for all view's appComponents
   * @param {Event} event
   */
  public appScopeEvents(event: Event): void {
    if (event['event'] === 'stopAllLoaders') {
      this.stopAllLoaders();

    } else if (event['event'] === 'newNoteCreated') {
      this.noteListComponent.resetComponent(true);

    } else if (event['event'] === 'noteListMode') {
      this.setMainViewMode('notes');

    } else if (event['event'] === 'dashboardMode') {
      this.setMainViewMode('mate-dashboard-config');
    }
  }

  /**
   * Go to dashboard config
   */
  public goToDashboardConfig(): void {
    if (this.person.id !== 0) {
      this._router.navigateByUrl('/mate-profile/' + this.person.id + '/dashboard');
    }
  }

  /**
   * Check if view mode is dashboard main
   * @returns {boolean}
   */
  public isDashboardMain(): boolean {
    return this._mainViewMode === this._mainViewModeList[0];
  }

  /**
   * Return if the view is full page mode
   * @returns {boolean}
   */
  public isMainFullPage(): boolean {
    return this._mainFullPage;
  }

  /**
   * Check if view mode is notes main
   * @returns {boolean}
   */
  public isNotesMain(): boolean {
    return this._mainViewMode === this._mainViewModeList[1];
  }

  /************************ AUXILIAR METHODS  ************************/

  /**
   * Change the main view mode to show
   * @param {string} mode
   */
  private setMainViewMode(mode: string): void {
    this._mainViewMode = this._mainViewModeList.indexOf(mode) !== -1
      ? mode
      : this._mainViewModeList[0];
  }

  /**
   * Stop all loaders
   */
  private stopAllLoaders(): void {
    this.apiLoader = {
      charts: false,
      notes: false
    };
  }
}
