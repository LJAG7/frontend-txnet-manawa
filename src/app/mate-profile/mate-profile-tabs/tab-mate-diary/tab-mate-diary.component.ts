import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';

import {BaseComponentModel} from '../../../shared/models/base-component/base-component.class';
import {PersonModel} from '../../../shared/models/person.model';
import {CalendarComponent} from '../../../shared/components/calendar/calendar.component';

import {AuthSrvService} from '../../../shared/services/auth.service';

const _ = require('lodash');

@Component({
  selector: 'app-tab-mate-diary-component',
  templateUrl: './tab-mate-diary.component.html',
  styleUrls: ['./tab-mate-diary.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TabMateDiaryComponent extends BaseComponentModel implements OnInit, OnChanges {

  @ViewChild('calendar') private _calendar: CalendarComponent;

  /************************ COMPONENT INPUTS  ************************/

  @Input() public person: PersonModel;
  @Input() public hidden = false;

  /************************ COMPONENT VARS  ************************/

  public isEnabledEdition = true;

  /************************ NATIVE METHODS  ************************/

  constructor(private _authSrv: AuthSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'tab_mate_diary_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!_.isNil(changes['person']) && !_.isUndefined(changes['person']['currentValue']) && changes['person']['currentValue']['id'] > 0) {
      this.isEnabledEdition = this.person.id === this._authSrv.getPersonLogged().id;
    }
  }

  /************************ DOM METHODS  ************************/
  /**
   * Return if new entry mode is active
   * @returns {boolean}
   */
  public isNewEntryMode(): boolean {
    return this._calendar.isNewEntryMode();
  }

  /************************ EVENT METHODS  ************************/

  /**
   * Handler on click in new entry button
   * @param event
   */
  public onNewEntryClick(): void {
    if (this._calendar.isAvailableNewEntryMode()) {
      this._calendar.startNewEntryMode();
    }
  }

}
