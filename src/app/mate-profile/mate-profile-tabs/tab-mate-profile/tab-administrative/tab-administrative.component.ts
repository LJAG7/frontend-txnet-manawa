import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {CurrencyPipe} from '@angular/common';
import {InputMaskModule} from 'primeng/primeng';
import {NumberPipe} from '../../../../shared/pipes/number.pipe';

import {REGEXPATTERNSHTML} from '../../../../shared/const/system.const';
import {REGEXPATTERNS} from '../../../../shared/const/system.const';
import {BaseComponentModel} from '../../../../shared/models/base-component/base-component.class';
import {PersonModel} from '../../../../shared/models/person.model';
import {CostModel} from '../../../../shared/models/administration/cost.model';

import {AuthSrvService} from '../../../../shared/services/auth.service';
import {HttpSrvService} from '../../../../shared/services/http.service';
import {AlertService} from '../../../../shared/services/alert.service';

const _ = require('lodash');

@Component({
  selector: 'app-tab-administrative-component',
  templateUrl: './tab-administrative.component.html',
  styleUrls: ['./tab-administrative.component.css'],
  providers: [NumberPipe, CurrencyPipe]
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TabAdministrativeComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() public person: PersonModel;
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() public onUpdatePersonEvent = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('billingInput') private billingInput;
  @ViewChild('salaryInput') private salaryInput;

  /************************ COMPONENT VARS  ************************/

  public costPattern;

  private inputFocus = '';
  private inputHover = {
    billing: false,
    salary: false
  };

  public cost = {
    id: 0,
    billing: 0,
    salary: 0
  };

  public costText = {
    billing: '',
    salary: ''
  };

  /************************ NATIVE METHODS  ************************/

  constructor(private _alertSrv: AlertService,
              private _authSrv: AuthSrvService,
              private _currencyPipe: CurrencyPipe,
              private _httpSrv: HttpSrvService,
              private _numberPipe: NumberPipe) {
    super();
  }

  ngOnInit() {
    this.config.id = 'tab_administrative_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    this.costPattern =  REGEXPATTERNSHTML.cost;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!_.isNil(this.person)
      && this.person.id !== 0) {
      this.cost = !_.isNil(this.person.cost) ? this.person.cost : this.person.cost = new CostModel();
      this.costText.billing = this.cost.billing.toString().replace('.', ',');
      this.costText.salary = this.cost.salary.toString().replace('.', ',');
    }
  }

  /************************ EVENT METHODS  ************************/

  /**
   * Change input opacity when focus
   * @param {string} input
   */
  public changeInputOpacity(id: string = '') {
    this.inputFocus = id;
  }

  /**
   * Get cost value
   * @param {string} id
   */
  public getCostLabel(id: string): string {
    if (!_.isNil(this.person) && !_.isNil(this.person.cost)) {
      if (id === 'billing') {
        return this.cost.billing === 0 ?
          'Sin tarifa asignada.' :
          this._currencyPipe.transform(this.costText.billing.toString().replace(',', '.'), '€');
      } else if (id === 'salary') {
        return this.cost.salary === 0 ?
          'Sin coste asignado.' :
          this._currencyPipe.transform(this.costText.salary.toString().replace(',', '.'), '€');
      }
    }
  }

  /**
   * Check if person was authorized to change cost
   * @returns {boolean}
   */
  public isAuthorizedPerson(): boolean {
    return !_.isNil(this.person) && this.person.id !== this._authSrv.getPersonLogged().id;
  }

  /**
   * Check if cost type is billing
   * @returns {boolean}
   */
  public isBillingCost(): boolean {
    return !_.isNil(this.person) && !_.isNil(this.person.cost) && !_.isNil(this.person.cost.billing) && !this.isBillingHover();
  }

  /**
   * Check if billing input is focus
   * @returns {boolean}
   */
  public isBillingFocus(): boolean {
    return !_.isEmpty(this.inputFocus) && this.isBillingHover();
  }

  /**
   * Check if billing input is hover
   * @returns {boolean}
   */
  public isBillingHover(): boolean {
    return this.inputHover.billing;
  }

  /**
   * Check if cost type is salary
   * @returns {boolean}
   */
  public isSalaryCost(): boolean {
    return !_.isNil(this.person) && !_.isNil(this.person.cost) && !_.isNil(this.person.cost.salary) && !this.inputHover.salary;
  }

  /**
   * Check if salary input is focus
   * @returns {boolean}
   */
  public isSalaryFocus(): boolean {
    return !_.isEmpty(this.inputFocus) && this.inputFocus === 'salary';
  }

  /**
   * On change cost value
   * @param {string} id
   */
  public onChangeCostValue(id: string): void {
    this.changeInputOpacity();
    this.onChangeInputHover(id, false);
    if (id === 'billing' && !_.isEmpty(this.billingInput.nativeElement.value.trim())) {
      this.saveCost(id);
    } else if (id === 'salary' && !_.isEmpty(this.salaryInput.nativeElement.value.trim())) {
      this.saveCost(id);
    }
  }

  /**
   * On change input hover
   * @param {string} id
   * @param {boolean} value
   */
  public onChangeInputHover(id: string, value: boolean): void {
    if (this.inputFocus !== id) {
      if (id === 'billing' && !this.isBillingFocus()
        || id === 'salary' && !this.isSalaryFocus()) {
        this.inputHover[id] = value;
      }
    }
  }

  /**
   * On format cost value
   * @param {string} id
   */
  public onFormatCostValue(event: any, id: string): void {
    let value;
    let text;
    let nativeElement;
    if (id === 'billing') {
      value = this.cost.billing;
      text = this.costText.billing;
      nativeElement = this.billingInput.nativeElement;
    } else if (id === 'salary') {
      value = this.cost.salary;
      text = this.costText.salary;
      nativeElement = this.salaryInput.nativeElement;
    }

    if (event['keyCode'] === 188) {
      const textSplit = nativeElement.value.split('');
      const totalComma = textSplit.filter( char => {
        return char === ',';
      });
      if (totalComma.length === 1) {
        value = Number(text.replace(',', '.'));
      } else if (totalComma.length > 1) {
        const commaSplit = nativeElement.value.split(',');
        nativeElement.value = commaSplit[0] + ',' + commaSplit[1];
      }
    } else if (/[0-9]/gi.test(event['key'])) {
      value = Number(text.replace(',', '.'));
    } else if (/[a-zA-Z]/gi.test(event['key'])) {
      nativeElement.value = nativeElement.value.replace(event['key'], '');
    }

    if (id === 'billing') {
      this.cost.billing = value;
      this.billingInput.nativeElement.value = nativeElement.value.replace('.', '');
    } else if (id === 'salary') {
      this.cost.salary = value;
      this.salaryInput.nativeElement.value = nativeElement.value.replace('.', '');
    }
  }

  /************************ API METHODS ************************/

  /**
   * Save cost
   * @param {string} id
   */
  private saveCost(id: string): void {
    if (id === 'billing') {
      this._httpSrv.post(
        'persons/' + this.person.id + '/administration/',
        {
          billing: this._numberPipe.unformatMoney(this.billingInput.nativeElement.value)
        }
      ).subscribe(
        success => this.onSuccessSaveCost(success)
      );
    } else if (id === 'salary') {
      this._httpSrv.post(
        'persons/' + this.person.id + '/administration/',
        {
          salary: this._numberPipe.unformatMoney(this.salaryInput.nativeElement.value)
        }
      ).subscribe(
        success => this.onSuccessSaveCost(success)
      );
    }
  }

  /**
   * On succes save cost
   */
  private onSuccessSaveCost(response: any): void {
    if (response.success && !_.isNil(response.object)) {
      this.person.cost = new CostModel(response.object);
      this.cost = this.person.cost;
      this.costText.billing = this.cost.billing.toString().replace('.', ',');
      this.costText.salary = this.cost.salary.toString().replace('.', ',');
      this.onUpdatePersonEvent.emit({
        person: this.person
      });
    }
  }

  /************************ AUXILIAR METHODS ************************/

}
