'use strict';

import {Component, OnInit, ViewChild, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

import {DateLocalPipe} from '../../../../shared/pipes/dateLocal.pipe';

import {BaseComponentModel} from '../../../../shared/models/base-component/base-component.class';
import {REGEXPATTERNSHTML, DATEPICKER, EMPTYPROFILEPICTURE, AGES} from '../../../../shared/const/system.const';
import {PersonModel} from '../../../../shared/models/person.model';

import {HttpSrvService} from '../../../../shared/services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-tab-basic-info-component',
  templateUrl: './tab-basic-info.component.html',
  styleUrls: ['./tab-basic-info.component.css'],
  providers: [HttpSrvService, DateLocalPipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TabBasicInfoComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() public person: PersonModel;

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('personForm') private personForm;

  /************************ COMPONENT VARS  ************************/

  public isLoading = true;
  public emailPattern = REGEXPATTERNSHTML.email;
  public validEmail: boolean;
  public validEmailMsg = 'Introduce un email válido';
  public personBirthdate: Date = null;

  public datePicker = {
    calendar: DATEPICKER.ESP,
    range: null,
    defaultDate: new Date(),
    minDate: new Date(1920, 0, 1),
    maxDate: new Date()
  };

  private _firstEmail = '';
  private _updateFirstTry = false;

  /************************ NATIVE METHODS ************************/

  constructor(private _dateLocalPipe: DateLocalPipe,
              private _httpSrv: HttpSrvService,
              private _domSanitizer: DomSanitizer) {
    super();
    this.setDatePickerSettings();
  }

  ngOnInit() {
    this.config.id = 'tab_basic_info_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('person')) {
      this.personBirthdate = !_.isNil(this.person.birthdate) ? new Date(this.person.birthdate) : null;
      this._firstEmail = this.person.email;
    }
  }

  /**
   * Set datePicker configuration
   */
  private setDatePickerSettings(): void {
    const d = new Date();

    this.datePicker.minDate = this._dateLocalPipe.yearCalc(-AGES.max, d);
    this.datePicker.maxDate = this._dateLocalPipe.yearCalc(-AGES.min, d);
    this.datePicker.defaultDate = this._dateLocalPipe.yearCalc(-AGES.default, d);
    this.datePicker.range = this.datePicker.minDate.getFullYear() + ':' + this.datePicker.maxDate.getFullYear();
  }

  /************************ EVENTS METHODS ************************/

  /**
   * Get default date
   * @returns {Date}
   */
  public getDefaultDate(): Date {
    return _.isNil(this.person.birthdate) ? this.datePicker.defaultDate : null;
  }


  /**
   * Get person name or email
   * @returns {string}
   */
  public getNameOrEmail(): string {
    return !_.isEmpty(this.person.name) && !_.isEmpty(this.person.surname)
      ? this.person.name + ' ' + this.person.surname : !_.isEmpty(this.person.email)
        ? this.person.email : '';
  }


  public getPersonPicture(): any {
    return this._domSanitizer.bypassSecurityTrustResourceUrl(
      _.isNil(this.person.profile_picture) || this.person.profile_picture === EMPTYPROFILEPICTURE
        ? EMPTYPROFILEPICTURE
        : this._httpSrv.getUrlApi().concat(this.person.profile_picture)
    );
  }

  /**
   * Check valid email
   * @returns {boolean}
   */
  public getValidEmail(): boolean {
    return _.isNil(this.validEmail) || this.validEmail === true;
  }

  /**
   * Return if an input has errors
   * @param input
   * @param event
   * @returns {boolean}
   */
  public hasErrorInput(input: any, event = null): boolean {
    if (input.hasOwnProperty('name') && input['name'] === 'email') {
      if (_.isNull(input.errors) && (input.dirty || input.touched)) {
        if (this.person.email !== this._firstEmail) {
          this._httpSrv
            .get('persons/InputValidator/email?value=' + input['value'])
            .subscribe(
              success => this.checkEmailValidatorResponse(success)
            );
        } else {
          this.validEmail = true;
        }
      } else if (!_.isNull(input.errors)) {
        this.validEmailMsg = 'Introduce un email válido';
        this.validEmail = false;
      }
    } else {
      return this._updateFirstTry && !_.isNull(input.errors) && (input.dirty || input.touched);
    }
  }

  public hasPerson(): boolean {
    return !_.isNil(this.person) && !_.isEmpty(this.person) && this.person.id !== 0;
  }

  public onChangeBirthdate(date: string): void {
    this.person.birthdate = !_.isNil(date) ? new Date(date) : null;
  }

  public onChangeNameSurname(field: string): void {
    if (field === 'name') {
      this.person.name = this.person.name.toLowerCase().replace(/\b\w/g, function(l) { return l.toUpperCase(); } );
    } else if (field === 'surname') {
      this.person.surname = this.person.surname.toLowerCase().replace(/\b\w/g, function(l) { return l.toUpperCase(); } );
    }
  }

  /**
   * Show error message
   * @returns {boolean}
   */
  public showErrorFromMessage(): boolean {
    return this._updateFirstTry && this.hasErrorForm();
  }

  /**
   * Update person
   */
  public updatePerson() {
    this._updateFirstTry = true;

    if (!this.hasErrorForm()) {
      this.isLoading = true;

      let isoDate = null;

      if (!_.isNil(this.person.birthdate)) {
        isoDate = this._dateLocalPipe.datetimeToISO(this.person.birthdate.toString());
      }

      this._httpSrv.patch(
        'persons/' + this.person.id,
        {
          name: this.person.name,
          surname: this.person.surname,
          email: this.person.email,
          phone: this.person.phone,
          birthdate: isoDate,
        }
      ).subscribe(
        success => this.onSuccessGetPerson(success)
      );
    } else {
      this.checkFormFields();
    }
  }

  /**
   * On success get person
   * @param response
   */
  private onSuccessGetPerson(response: any): void {
    this.isLoading = false;
    if (!_.isNil(response.object) && response.success) {
      this.person = new PersonModel(response['object']);
      this._firstEmail = this.person.email;
    }
  }

  /********************* VALIDATION METHODS *********************/

  /**
   * Touch all field for launch validation process
   */
  private checkFormFields(): void {
    _.forIn(this.personForm.control.controls, (input) => {
      if (input.invalid === true) {
        input.markAsTouched();
      }
    });
  }

  /**
   * Check email validation message
   * @param {Object} response
   * @returns {boolean}
   */
  private checkEmailValidatorResponse(response: object): boolean {
    if (response.hasOwnProperty('object') && response['object'].hasOwnProperty('valid')) {
      this.validEmail = response['object']['valid'];
      if (!this.validEmail) {
        this.validEmailMsg = 'El email introducido ya está en uso.';
      }
      return response['object']['valid'];
    }
    this.validEmailMsg = 'Introduce un email válido.';
    return false;
  }

  /**
   * Check if form has error
   * @returns {boolean}
   */
  private hasErrorForm(): boolean {
    return this.personForm.invalid || (!_.isNil(this.validEmail) && !this.validEmail);
  }
}
