import {Component, OnInit, Input} from '@angular/core';

import {BaseComponentModel} from '../../../../../shared/models/base-component/base-component.class';

@Component({
  selector: 'app-tab-academic-component',
  templateUrl: './tab-academic.component.html',
  styleUrls: ['./tab-academic.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TabAcademicComponent extends BaseComponentModel implements OnInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT VARS  ************************/


  /************************ NATIVE METHODS  ************************/

  constructor() {
    super();
  }

  ngOnInit() {
    this.config.id = 'tab_academic_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  /************************ EVENT METHODS  ************************/

}
