import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

import {BaseComponentModel} from '../../../../../../shared/models/base-component/base-component.class';
import {PersonModel} from '../../../../../../shared/models/person.model';

import {AlertService} from '../../../../../../shared/services/alert.service';
import {HttpSrvService} from '../../../../../../shared/services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-contact-form-component',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class ContactFormComponent extends BaseComponentModel implements OnInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() person: PersonModel;
  @Input() visible = true;

  /************************ COMPONENT VARS  ************************/

  public editControl = {
    editPhone: false,
    editMobile: false,
    editEmail: false,
    editSkype: false
  };

  public validEmail: boolean;
  public validEmailMsg = 'Introduce un email válido';

  private _firstEmail = '';
  private _updateFirstTry = false;



  /************************ NATIVE METHODS  ************************/

  constructor(private _alertSrv: AlertService,
              private _httpSrv: HttpSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'contact_form_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  /************************ EVENT METHODS  ************************/

  /**
   * Return if an input has errors
   * @param input
   * @param event
   * @returns {boolean}
   */
  public hasErrorInput(input: any, event = null): boolean {
    if (input.hasOwnProperty('name') && input['name'] === 'email') {
      if (_.isNull(input.errors) && (input.dirty || input.touched)) {
        if (this.person.email !== this._firstEmail) {
          this._httpSrv
            .get('persons/InputValidator/email?value=' + input['value'])
            .subscribe(
              success => this.checkEmailValidatorResponse(success)
            );
        } else {
          this.validEmail = true;
        }
      } else if (!_.isNil(event) && Number(event['keyCode']) !== 9) {
        this.validEmailMsg = 'Introduce un email válido';
        this.validEmail = false;
      }
    } else {
      return this._updateFirstTry && !_.isNull(input.errors) && (input.dirty || input.touched);
    }
  }

  /**
   * Change edit status
   * @param {string} field
   */
  public setEditField(field: string): void {
    if (this.editControl.hasOwnProperty(field)) {
      this.editControl[field] = !this.editControl[field];
    }
  }

  /************************ API METHODS  ************************/


  /**
   * Check email validation message
   * @param {Object} response
   * @returns {boolean}
   */
  private checkEmailValidatorResponse(response: object): boolean {
    if (response.hasOwnProperty('object') && response['object'].hasOwnProperty('valid')) {
      this.validEmail = response['object']['valid'];
      if (!this.validEmail) {
        this.validEmailMsg = 'El email introducido ya está en uso.';
      }
      return response['object']['valid'];
    }
    this.validEmailMsg = 'Introduce un email válido.';
    return false;
  }

  /************************ AUXILIAR METHODS  ************************/

  public functionalityNotImplemented(): void {
    this._alertSrv.prompt('Contact form: funcionalidad pendiente de REQUISITOS');
  }
}
