import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

import {BaseComponentModel} from '../../../../../../shared/models/base-component/base-component.class';
import {PersonModel} from '../../../../../../shared/models/person.model';

import {AlertService} from '../../../../../../shared/services/alert.service';

const _ = require('lodash');

@Component({
  selector: 'app-knowledge-form-component',
  templateUrl: './knowledge-form.component.html',
  styleUrls: ['./knowledge-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class KnowledgeFormComponent extends BaseComponentModel implements OnInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() person: PersonModel;
  @Input() visible = true;

  /************************ COMPONENT VARS  ************************/


  /************************ NATIVE METHODS  ************************/

  constructor(private _alertSrv: AlertService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'knowledge_form_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  /************************ EVENT METHODS  ************************/



  /************************ API METHODS  ************************/



  /************************ AUXILIAR METHODS  ************************/

  public functionalityNotImplemented(): void {
    this._alertSrv.prompt('Contact form: funcionalidad pendiente de REQUISITOS');
  }
}
