import {ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

import {BaseComponentModel} from '../../../../../../shared/models/base-component/base-component.class';
import {PersonModel} from '../../../../../../shared/models/person.model';

import {AlertService} from '../../../../../../shared/services/alert.service';

const _ = require('lodash');

@Component({
  selector: 'app-roles-form-component',
  templateUrl: './roles-form.component.html',
  styleUrls: ['./roles-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class RolesFormComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() person: PersonModel;
  @Input() visible = true;

  /************************ COMPONENT VARS  ************************/


  /************************ NATIVE METHODS  ************************/

  constructor(private _alertSrv: AlertService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'roles_form_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void { }

  /************************ EVENT METHODS  ************************/


  /************************ API METHODS  ************************/


  /************************ AUXILIAR METHODS  ************************/

  public functionalityNotImplemented(): void {
    this._alertSrv.prompt('Contact form: funcionalidad pendiente de REQUISITOS');
  }
}
