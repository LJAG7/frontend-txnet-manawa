import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

import {BaseComponentModel} from '../../../../../shared/models/base-component/base-component.class';
import {PersonModel} from '../../../../../shared/models/person.model';

import {AlertService} from '../../../../../shared/services/alert.service';

@Component({
  selector: 'app-tab-laboral-academic-rigth-panel-component',
  templateUrl: './tab-laboral-academic-rigth-panel.component.html',
  styleUrls: ['./tab-laboral-academic-rigth-panel.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabLaboralAcademicRigthPanelComponent extends BaseComponentModel implements OnInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() person: PersonModel;
  @Input() visible = true;

  /************************ COMPONENT VARS  ************************/

  /************************ NATIVE METHODS  ************************/

  constructor(private _alertSrv: AlertService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'tab_laboral_academic_rigth_panel_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  /************************ EVENT METHODS  ************************/



  /************************ API METHODS  ************************/



  /************************ AUXILIAR METHODS  ************************/

  public functionalityNotImplemented(): void {
    this._alertSrv.prompt('Laboral y academico: Panel derecho: funcionalidad pendiente de REQUISITOS');
  }
}
