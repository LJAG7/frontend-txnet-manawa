import {
  Component, OnInit, Input, SimpleChanges, OnChanges
} from '@angular/core';
import {SelectItem} from 'primeng/primeng';

import {ValidationObjectPipe} from '../../../../shared/pipes/validation.pipe';

import {BaseComponentModel} from '../../../../shared/models/base-component/base-component.class';
import {LaboralExperienceModel} from '../../../../shared/models/laboral-experience/laboral-experience.model';
import {LinkedinEndpointModel} from '../../../../shared/models/laboral-experience/linkedin-endpoint.model';
import {PersonModel} from '../../../../shared/models/person.model';

import {AlertService} from '../../../../shared/services/alert.service';
import {AuthSrvService} from '../../../../shared/services/auth.service';
import {HttpSrvService} from '../../../../shared/services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-tab-laboral-academic-component',
  templateUrl: './tab-laboral-academic.component.html',
  styleUrls: ['./tab-laboral-academic.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TabLaboralAcademicComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() public person: PersonModel;
  @Input() visible = true;

  /************************ COMPONENT VARS  ************************/

  public laboralExperiences: Array<LaboralExperienceModel>;
  public selectedTab: number;
  public showLinkedIn = false;
  public tabs: SelectItem[];

  /************************ NATIVE METHODS  ************************/

  constructor(private _alertSrv: AlertService,
              private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _validationObjectPipe: ValidationObjectPipe) {
    super();
    this._authSrv.verifyViewPermission('tab-mate-profile-tab-laboral-academic');
  }

  ngOnInit() {
    this.config.id = 'tab_laboral_academic_component_n'.concat(Math.floor(Math.random() * 100000).toString());

    if (this.isAllowed()) {
      const permissionConfig = this._authSrv.getViewConfig('tab-mate-profile-tab-laboral-academic');
      this.tabs = permissionConfig['tabs'];
      this.selectedTab = permissionConfig['selectedTab'];
    }

    const selectedTab = localStorage.getItem('laboralAcademicSelectedTab');
    if (!_.isNil(selectedTab)) {
      this.selectedTab = Number(selectedTab);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('person') && !_.isNil(changes.person.currentValue)) {
      this.showLinkedIn = this.person.id === this._authSrv.getPersonLogged().id;
    }
  }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('tab-mate-profile-tab-laboral-academic', tag);
  }

  /************************ EVENT METHODS  ************************/

  /**
   * Check if linkedin button is disabled
   * @returns {boolean}
   */
  public isDisabledLinkedIn(): boolean {
    return !this.showLinkedIn;
  }
  /**
   * On click synch linkedin
   */
  public onClickSynchLinkedin(): void {
    if (this.showLinkedIn) {
      this._httpSrv
        .get('linkedin/basicprofile/parameters/')
        .subscribe(
          (responseOk) => this.onSuccessSynchLinkedin(responseOk)
        );
    } else {
      this._alertSrv.info('Solo se pueden sincronizar los datos de tu propia cuenta de LinkedIn');
    }
  }

  /**
   * On tab selected
   * @param event
   */
  public onTabSelected(event): void {
    if (event['index'] === 1) {
      this.functionalityNotImplemented(event['index']);
    }
    localStorage.setItem('laboralAcademicSelectedTab', event['option']['value'].toString());
  }

  /********************* API METHODS *********************/

  /**
   * On success get linkedin token
   * @param {Object | any} response
   */
  private onSuccessSynchLinkedin(response: object | any): void {

    if (this._validationObjectPipe.isSuccessResponse(response)
      && this._validationObjectPipe.isValidObjectProperty(response, 'object')) {

      if (response['object'].hasOwnProperty('linkedin_endpoint')
        && response['object'].hasOwnProperty('backend_endpoint')) {

        const linkedinEndpoint = new LinkedinEndpointModel(response['object']);
        const authorizationEndpoint = linkedinEndpoint.getAuthorizationEndpoint();

        window.location.href = authorizationEndpoint;

      } else if (response['object'].hasOwnProperty('json_content')
        && response['object']['json_content'].hasOwnProperty('positions')
        && response['object']['json_content']['positions'].hasOwnProperty('values')
        && !_.isEmpty(response['object']['json_content']['positions']['values'])) {

        const auxLaboralExperiences: Array<LaboralExperienceModel> = [];
        response['object']['json_content']['positions']['values'].forEach( position => {
          auxLaboralExperiences.push(new LaboralExperienceModel(position));
        });

        this.laboralExperiences = Object.assign([], auxLaboralExperiences);
      }
    }
  }

  /************************ AUXILIAR METHODS  ************************/

  public functionalityNotImplemented(index): void {
    if (index === 1) {
      this._alertSrv.prompt('Perfil laboral y académico: Educación: funcionalidad pendiente de REQUISITOS');
    } else if (index === 3) {
      this._alertSrv.prompt('Perfil laboral y académico: Panel izquierdo: funcionalidad pendiente de DESARROLLO');
    }
  }

}
