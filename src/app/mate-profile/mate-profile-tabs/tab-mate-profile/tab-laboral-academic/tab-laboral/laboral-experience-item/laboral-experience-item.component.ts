import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

import {BaseComponentModel} from '../../../../../../shared/models/base-component/base-component.class';
import {LaboralExperienceModel} from '../../../../../../shared/models/laboral-experience/laboral-experience.model';
import {DATEPICKER} from '../../../../../../shared/const/system.const';

import {AlertService} from '../../../../../../shared/services/alert.service';

const _ = require('lodash');

@Component({
  selector: 'app-laboral-experience-item-component',
  templateUrl: './laboral-experience-item.component.html',
  styleUrls: ['./laboral-experience-item.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class LaboralExperienceItemComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() laboralExperience: LaboralExperienceModel;
  @Input() visible = true;

  /************************ COMPONENT VARS  ************************/

  public editMode = false;
  public spanishCalendar = DATEPICKER.ESP;
  public startDate: Date = null;
  public endDate: Date = null;

  private _updateFirstTry = false;

  /************************ NATIVE METHODS  ************************/

  constructor(private _alertSrv: AlertService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'tab_laboral_experience_item_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    if (this.laboralExperience.id === 0) {
      this.editMode = true;
    }
  }

  ngOnChanges(changes: SimpleChanges): void { }

  /************************ EVENT METHODS  ************************/

  /**
   * Check if edit mode is active
   * @returns {boolean}
   */
  public isEditMode(): boolean {
    return this.editMode;
  }
  /**
   * Return if an input has errors
   * @param input
   * @param event
   * @returns {boolean}
   */
  public hasErrorInput(input: any, event = null): boolean {
    return this._updateFirstTry && !_.isNull(input.errors) && (input.dirty || input.touched);
  }

  /**
   * Toggle edit experience
   */
  public toggleEditExperience(): void {
    // this.editMode = !this.editMode;
    // if (this.laboralExperience.id === 0) {
    //   this.visible = false;
    // }
  }

  /************************ AUXILIAR METHODS  ************************/

  public functionalityNotImplemented(): void {
    this._alertSrv.prompt('Perfil laboral: funcionalidad pendiente de REQUISITOS');
  }
}
