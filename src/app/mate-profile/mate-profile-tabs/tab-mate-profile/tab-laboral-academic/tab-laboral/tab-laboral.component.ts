import {Component, OnInit, Input, SimpleChanges, OnChanges} from '@angular/core';

import {BaseComponentModel} from '../../../../../shared/models/base-component/base-component.class';
import {PersonModel} from '../../../../../shared/models/person.model';
import {LaboralExperienceModel} from '../../../../../shared/models/laboral-experience/laboral-experience.model';

import {AlertService} from '../../../../../shared/services/alert.service';
import {HttpSrvService} from '../../../../../shared/services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-tab-laboral-component',
  templateUrl: './tab-laboral.component.html',
  styleUrls: ['./tab-laboral.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TabLaboralComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() laboralExperiences: Array<LaboralExperienceModel>;
  @Input() person: PersonModel;
  @Input() visible = true;

  /************************ COMPONENT VARS  ************************/



  /************************ NATIVE METHODS  ************************/

  constructor(private _alertSrv: AlertService,
              private _httpSrv: HttpSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'tab_laboral_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('person')
      && changes['person'].hasOwnProperty('currentValue')
      && changes['person']['currentValue']['id'] !== 0) {
      this.getPersonLinkedinData();
    }
  }

  /************************ EVENT METHODS  ************************/

  public getLaboralExperiences(): Array<LaboralExperienceModel> {
    return _.isNil(this.laboralExperiences) ? [] : this.laboralExperiences;
  }

  /********************* API METHODS *********************/

  /**
   * Get person linkedin data
   */
  private getPersonLinkedinData(): void {
    if (this.person.id !== 0) {
      this._httpSrv
        .get('persons/' + this.person.id + '/linkedinentry/')
        .subscribe(
          (responseOk) => this.onSuccessGetPersonLinkedinData(responseOk)
        );
    }
  }

  /**
   * On success get linkedin token
   * @param {Object | any} response
   */
  private onSuccessGetPersonLinkedinData(response: object | any): void {
    if (response.hasOwnProperty('success')
      && response['success'] === true) {

      if (response['object'].hasOwnProperty('json_content')
        && response['object']['json_content'].hasOwnProperty('positions')
        && response['object']['json_content']['positions'].hasOwnProperty('values')
        && !_.isEmpty(response['object']['json_content']['positions']['values'])) {

        const auxLaboralExperiences: Array<LaboralExperienceModel> = [];
        response['object']['json_content']['positions']['values'].forEach( position => {
          auxLaboralExperiences.push(new LaboralExperienceModel(position));
        });

        this.laboralExperiences = Object.assign([], auxLaboralExperiences);
      }
    }
  }

  /************************ AUXILIAR METHODS  ************************/

  public functionalityNotImplemented(): void {
    this._alertSrv.prompt('Perfil laboral: funcionalidad pendiente de REQUISITOS');
  }
}
