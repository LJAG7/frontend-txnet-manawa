import {
  Component, OnInit, Input, ChangeDetectionStrategy, SimpleChanges, OnChanges, Output,
  EventEmitter
} from '@angular/core';

import {BaseComponentModel} from '../../../shared/models/base-component/base-component.class';

import {AlertService} from '../../../shared/services/alert.service';
import {PersonModel} from '../../../shared/models/person.model';

const _ = require('lodash');

@Component({
  selector: 'app-tab-mate-profile-component',
  templateUrl: './tab-mate-profile.component.html',
  styleUrls: ['./tab-mate-profile.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TabMateProfileComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() person: PersonModel;
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() public onUpdatePersonEvent = new EventEmitter();

  /************************ COMPONENT VARS  ************************/

  public selectedTab = 0;

  /************************ NATIVE METHODS  ************************/

  constructor(private _alertSrv: AlertService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'tab_mate_profile_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    const selectedTab = localStorage.getItem('tabMateProfileSelectedTab');
    if (!_.isNil(selectedTab)) {
      this.selectedTab = Number(selectedTab);
    }
  }

  ngOnChanges(changes: SimpleChanges): void { }

  /************************ AUXILIAR METHODS  ************************/

  public onChangeTab(event): void {
    localStorage.setItem('tabMateProfileSelectedTab', event['index'].toString());
  }

  public onUpdatePerson(event: object): void {
    this.onUpdatePersonEvent.emit({
      person: event['person']
    });
  }

}
