import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';

import {CapitalizePipe} from '../shared/pipes/capitalize.pipe';
import {OrderByPipe} from '../shared/pipes/orderBy.pipe';
import {ValidationObjectPipe} from '../shared/pipes/validation.pipe';

import {WIDGETTYPES} from '../shared/const/widgettypes.const';
import {PersonModel} from '../shared/models/person.model';

import {AlertService} from '../shared/services/alert.service';
import {AuthSrvService} from '../shared/services/auth.service';
import {HttpSrvService} from '../shared/services/http.service';
import {LoaderService} from '../shared/services/loader.service';

const _ = require('lodash');

@Component({
  selector: 'app-mate-profile-component',
  templateUrl: './mate-profile.component.html',
  styleUrls: ['./mate-profile.component.css'],
  providers: [
    // LogService,
    HttpSrvService,
    CapitalizePipe,
    OrderByPipe,
    ValidationObjectPipe
  ]
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class MateProfileComponent implements OnInit, OnDestroy {

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('groupsTreeComponent') groupsTreeComponent;
  @ViewChild('teamsTreeComponent') teamsTreeComponent;
  @ViewChild('noteFormComponent') noteFormComponent;
  @ViewChild('noteListComponent') noteListComponent;
  @ViewChild('noteBoardComponent') noteBoardComponent;
  @ViewChild('scopeAnalyticsComponent') scopeAnalyticsComponent;

  /************************ COMPONENT VARS  ************************/

  public person: PersonModel = new PersonModel();
  public tabs: SelectItem[];
  public selectedTab: number;

  public widgetType = null;

  public activeGeneralTab = -1;

  public gestorViewMode = true;

  private _initializedView = false;

  private _showDetails = false;
  private _subscribeIdParam;
  private _mainFullPage = false;

  /************************ CONFIG VARS  ************************/

  private _neededFromApi: Array<string> = ['getPerson'];

  /************************ NATIVE METHODS ************************/

  constructor(private _activatedRoute: ActivatedRoute,
              private _alertSrv: AlertService,
              private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _loaderSrv: LoaderService,
              private _router: Router,
              private _validationObjectPipe: ValidationObjectPipe) {
    this._authSrv.verifyViewPermission('mate-profile');
    this._loaderSrv.general(this._neededFromApi);
  }

  ngOnInit(): void {
    if (this.isAllowed()) {
      const permissionConfig = this._authSrv.getViewConfig('mate-profile');
      this.tabs = permissionConfig['tabs'];
      this.selectedTab = permissionConfig['selectedTab'];

      this._subscribeIdParam = this._activatedRoute.params.subscribe(params => {
        if (this._validationObjectPipe.isValidObjectProperty(this.person, 'id') && this.person.id !== params['id']) {
          this.loadDataModel(parseInt(params['id'], 0));
        }
      });

      const selectedTab = localStorage.getItem('mateProfileSelectedTab');
      if (!_.isNil(selectedTab)) {
        this.selectedTab = Number(selectedTab);
      }

      setTimeout( () => {
        this._loaderSrv.stopGeneral();
      }, 500);


    } else {
      this._loaderSrv.denegation();

      setTimeout( () => {
        this._authSrv.logout();
      }, 5000);
    }
  }

  ngOnDestroy() {
    if (!_.isNil(this._subscribeIdParam)) {
      this._subscribeIdParam.unsubscribe();
    }
  }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('mate-profile', tag);
  }

  /************************ CONFIG METHODS *************************/

  /************************ EVENTS METHODS ************************/

  public getMateNameFirstSurname(): string {
    return _.isNil(this.person) ? '' : this.person.getNameFirstSurname();
  }

  /**
   * Return to previous view
   */
  public goBackTo(): void {
    this._router.navigateByUrl('/team-mate');
  }

  public hasActionHeader(): boolean {
    return this.isGestorMode();
  }

  public isGestorMode(): boolean {
    return this.gestorViewMode;
  }

  /**
   * Return if the view is full page mode
   * @returns {boolean}
   */
  public isMainFullPage(): boolean {
    return this._mainFullPage;
  }

  /**
   * Toggle full page status
   * @param {Object} event
   */
  public onToggleFullMain(event: object): void {
    this._mainFullPage = !this._mainFullPage;
  }

  /**
   * On tab selected
   * @param event
   */
  public onTabSelected(event): void {
    localStorage.setItem('mateProfileSelectedTab', event['option']['value'].toString());
  }

  /**
   * title for back button
   * @returns {string}
   */
  public titleGoBackTo(): string {
    return this.isGestorMode()
      ? this.getMateNameFirstSurname()
      : 'Mi Scope';
  }

  public onUpdatePerson(event: object): void {
    this.person = new PersonModel(event['person']);
  }

  /********************* API METHODS *********************/

  /**
   * Load the data model for mate and forces to refresh the component model
   * @param {number} mateId
   */
  private loadDataModel(id: number): void {
    if (id > 0) {
      this.getPerson(id);
      this.setViewMode();

      if (id === this._authSrv.getPersonId()) {
        this.widgetType = WIDGETTYPES.MATE_MATE;
      } else {
        this.widgetType = WIDGETTYPES.GESTOR_MATE;
      }
    }
  }

  /**
   * Get person
   * @param {number} id
   */
  private getPerson(id: number) {
    this._httpSrv
      .get('persons/' + id)
      .subscribe(
        (responseOk) => this.onSuccessGetPerson(responseOk)
      );
  }


  /**
   * On success get person
   * @param {Object | any} response
   */
  private onSuccessGetPerson(response: object | any): void {

    if (this._validationObjectPipe.isSuccessResponse(response)
      && this._validationObjectPipe.isValidObjectProperty(response, 'object')) {
      this.person = new PersonModel(response['object']);
      this.resetAllComponents();
      this.checkNeededFromApi('getPerson');
    }
  }

  /**
   * Check needed from api
   * @param {string} value
   */
  private checkNeededFromApi(value: string): void {
    if (this._neededFromApi.indexOf(value) !== -1) {
      this._neededFromApi.splice(this._neededFromApi.indexOf(value), 1);
      this._loaderSrv.setNeededFromApi(this._neededFromApi);
    }
  }

  /********************* AUXILIAR METHODS *********************/

  /**
   * Determine if the logged user is the mate in page
   * @returns {boolean}
   */
  private isOwnScope(): boolean {
    return this._authSrv.getPersonId() === Number(this._activatedRoute.params['value']['id']);
  }

  private setViewMode(): void {
    this.gestorViewMode = this._authSrv.isRoleAdminsGestor() && !this.isOwnScope();
  }

  private resetAllComponents(): void {
    if (!this._initializedView) {
      this._initializedView = true;
      return;
    }

    if (!_.isNil(this.scopeAnalyticsComponent)) {
      this.scopeAnalyticsComponent.resetComponent();
    }

    if (!_.isNil(this.noteFormComponent)) {
      this.noteFormComponent.resetComponent();
    }

    if (!_.isNil(this.noteListComponent)) {
      this.noteListComponent.resetComponent();
    }

    if (!_.isNil(this.noteBoardComponent)) {
      this.noteBoardComponent.resetComponent();
    }
  }

  public functionalityNotImplemented(index): void {
    if (index === 1) {
      this._alertSrv.prompt('Agenda: funcionalidad pendiente de REQUISITOS');
    }
  }
}
