/*GENERAL ASSETS*/
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';

/*MODULE ASSETS*/

import {ComponentsModule} from '../shared/components/components.module';
import {PipesModule} from '../shared/pipes/pipes.module';
import {MateProfileComponent} from './mate-profile.component';
import {MateQuickViewComponent} from './mate-profile-left-panel/mate-quick-view/mate-quick-view.component';
import {MateDashboardConfigComponent} from './mate-profile-tabs/tab-mate-dashboard/mate-dashboard-config/mate-dashboard-config.component';

/************************ TAB PROFILE  ************************/

import {TabMateDashboardComponent} from './mate-profile-tabs/tab-mate-dashboard/tab-mate-dashboard.component';
import {TabMateProfileComponent} from './mate-profile-tabs/tab-mate-profile/tab-mate-profile.component';

/************************ TAB MATE PROFILE  ************************/

import {TabBasicInfoComponent} from './mate-profile-tabs/tab-mate-profile/tab-basic-info/tab-basic-info.component';
import {TabLaboralAcademicComponent} from './mate-profile-tabs/tab-mate-profile/tab-laboral-academic/tab-laboral-academic.component';
import {TabLaboralComponent} from './mate-profile-tabs/tab-mate-profile/tab-laboral-academic/tab-laboral/tab-laboral.component';
import {TabAcademicComponent} from './mate-profile-tabs/tab-mate-profile/tab-laboral-academic/tab-academic/tab-academic.component';
import {TabAdministrativeComponent} from './mate-profile-tabs/tab-mate-profile/tab-administrative/tab-administrative.component';
import {MateProfileLeftPanelComponent} from './mate-profile-left-panel/mate-profile-left-panel.component';
import {LaboralExperienceItemComponent} from './mate-profile-tabs/tab-mate-profile/tab-laboral-academic/tab-laboral/laboral-experience-item/laboral-experience-item.component';
import {TabLaboralAcademicRigthPanelComponent} from './mate-profile-tabs/tab-mate-profile/tab-laboral-academic/tab-laboral-academic-rigth-panel/tab-laboral-academic-rigth-panel.component';
import {ContactFormComponent} from './mate-profile-tabs/tab-mate-profile/tab-laboral-academic/tab-laboral-academic-rigth-panel/contact-form/contact-form.component';
import {KnowledgeFormComponent} from './mate-profile-tabs/tab-mate-profile/tab-laboral-academic/tab-laboral-academic-rigth-panel/knowledge-form/knowledge-form.component';
import {RolesFormComponent} from './mate-profile-tabs/tab-mate-profile/tab-laboral-academic/tab-laboral-academic-rigth-panel/roles-form/roles-form.component';
import {TabMateDiaryComponent} from './mate-profile-tabs/tab-mate-diary/tab-mate-diary.component';
import {MateScopeAnalyticsComponent} from './mate-profile-tabs/tab-mate-dashboard/mate-scope-analytics/mate-scope-analytics.component';
import {MateScopeAnalyticsChartComponent} from './mate-profile-tabs/tab-mate-dashboard/mate-scope-analytics/mate-scope-analytics-chart/mate-scope-analytics-chart.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    ComponentsModule,
    PipesModule
  ],
  exports: [
  ],
  declarations: [
    MateProfileComponent,
    MateQuickViewComponent,
    MateDashboardConfigComponent,
    MateScopeAnalyticsComponent,
    MateScopeAnalyticsChartComponent,
    TabMateProfileComponent,
    TabBasicInfoComponent,
    TabLaboralAcademicComponent,
    TabLaboralComponent,
    TabAcademicComponent,
    TabAdministrativeComponent,
    TabMateDashboardComponent,
    MateProfileLeftPanelComponent,
    LaboralExperienceItemComponent,
    TabLaboralAcademicRigthPanelComponent,
    ContactFormComponent,
    KnowledgeFormComponent,
    RolesFormComponent,
    TabMateDiaryComponent,
  ],
  providers: [
    // HttpSrvService,
    // LogService
  ]
})

export class MateProfileModule {
}

