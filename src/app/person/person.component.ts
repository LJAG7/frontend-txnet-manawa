'use strict';

import {AfterViewInit, Component, OnInit, ViewChild, ViewChildren, QueryList} from '@angular/core';
import {Router} from '@angular/router';

import {DataTable, LazyLoadEvent} from 'primeng/primeng';

import {DateLocalPipe} from '../shared/pipes/dateLocal.pipe';

import {ROLESYSTEMS} from '../shared/const/rolesystems.const';
import {REGEXPATTERNSHTML, REGEXPATTERNS} from '../shared/const/system.const';

import {PersonModel} from '../shared/models/person.model';
import {CSVModel} from '../shared/models/csv/csv.model';
import {CSVRowModel} from '../shared/models/csv/row.model';

import {AlertService} from '../shared/services/alert.service';
import {AuthSrvService} from '../shared/services/auth.service';
import {HttpSrvService} from '../shared/services/http.service';
import {LoaderService} from '../shared/services/loader.service';
import {ParseCsvService} from '../shared/services/parseCsv.service';
import {InputValidatorComponent} from '../shared/components/input-validator/input-validator.component';

const _ = require('lodash');
const moment = require('moment');

const $ = require('jquery');

@Component({
  selector: 'app-person-component',
  templateUrl: './person.component.html',
  providers: [DateLocalPipe, HttpSrvService, ParseCsvService],
  styleUrls: ['./person.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class PersonComponent implements OnInit, AfterViewInit {

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('personsDatatable') private _personsDatatable: DataTable;
  @ViewChild('fileCsvInput') private fileCsvInput;
  @ViewChildren('inputsValidator') private inputsValidator: QueryList<InputValidatorComponent>;

  /************************ COMPONENT VARS  ************************/

  // Static vars for static methods

  public getPersonAge = PersonComponent.getPersonAge;
  public cancelModalAddPerson = PersonComponent.cancelModalAddPerson;

  // Component vars

  public persons: Array<PersonModel> = [];

  /************************ CONFIG VARS  ************************/

  public pagination = {
    page: 1,
    rows: 10,
    sortField: 'surname',
    sortOrder: 'asc',
    totalRecords: 0,
  };

  /************************ CSV VARs  ************************/
  private _csvData: Array<CSVRowModel> = [];
  private _subscribeCsv;
  private _csvMassiveUpdown = {
    pack: 25,
    counter: 0
  };

  public csvImportMessage = '';
  public csvSuccessMessage = '';
  public csvErrorMessage = '';

  public invitePerson = {
    name: null,
    surname: null,
    email: null,
    is_ghost: false
  };

  private _nameInputValidatorConfig = {
    field: {
      placeholder: 'Nombre',
      css: {
        empty: true
      },
      validations: {
        required: {
          active: true,
          value: true,
          message: 'Campo obligatorio.'
        },
        maxlength: {
          active: true,
          value: 255,
          message: 'Máximo 255 caracteres.'
        },
        minlength: {
          active: true,
          value: 3,
          message: 'Mínimo 3 caracteres.'
        }
      }
    }
  };

  private _surnameInputValidatorConfig = {
    field: {
      placeholder: 'Apellidos',
      css: {
        empty: true
      },
      validations: {
        required: {
          active: true,
          value: true,
          message: 'Campo obligatorio.'
        },
        maxlength: {
          active: true,
          value: 255,
          message: 'Máximo 255 caracteres.'
        },
        minlength: {
          active: true,
          value: 3,
          message: 'Mínimo 3 caracteres.'
        }
      }
    }
  };

  private _emailInputValidatorConfig = {
    field: {
      placeholder: 'Email',
      css: {
        empty: true
      },
      validations: {
        required: {
          active: true,
          value: true,
          message: 'Campo obligatorio.'
        },
        maxlength: {
          active: true,
          value: 255,
          message: 'No puede tener más de 255 caracteres.'
        },
        pattern: {
          active: true,
          value: REGEXPATTERNSHTML.email,
          message: 'Email no válido.'
        }
      },
      validationsApi: {
        url: 'persons/InputValidator/email',
        message: 'El email ya existe.'
      },
      style: 'lowerCase'
    }
  };

  /************************ STATIC METHODS  ************************/

  /**
   * Create age format
   * @param {Object} person
   * @returns {string}
   */
  private static createAgeFormat(person: object): string {
    if (!_.isNil(person) && person['birthdate']) {
      const ageDifMs = Date.now() - new Date(person['birthdate']).getTime(),
        ageDate = new Date(ageDifMs); // miliseconds from epoch
      return Math.abs(ageDate.getUTCFullYear() - 1970).toString();
    }
  }

  /**
   * Get person age
   * @param {object} person
   * @returns {string}
   */
  private static getPersonAge(person: object): string {
    return person && !_.isNil(person)
    && person.hasOwnProperty('birthdate')
    && !_.isNil(person['birthdate'])
      ? PersonComponent.createAgeFormat(person)
      : '-';
  }

  private static cancelModalAddPerson(): void {
    $('#emailInvitationModal .modal-header button').click();
  }


  /************************ NATIVE METHODS  ************************/

  constructor(private _alertSrv: AlertService,
              private _authSrv: AuthSrvService,
              private _dateLocalPipe: DateLocalPipe,
              private _httpSrv: HttpSrvService,
              private _loaderSrv: LoaderService,
              private _router: Router,
              private _csvSrv: ParseCsvService) {


    this._authSrv.verifyViewPermission('person');
    this._loaderSrv.general();
  }

  ngOnInit() { }

  ngAfterViewInit(): void {
    if (!_.isNil(this._personsDatatable)) {
      this._personsDatatable.resolveFieldData = function(data, field) {
        if (data && field) {
          if (field === 'age') {
            return PersonComponent.getPersonAge(data);
          } else if (field === 'birthdate') {
            // Added by me to export dates in excel compatible format
            if (moment(data[field], moment.ISO_8601, true).isValid()) {
              return (moment(data[field]).format('DD-MM-YYYY'));
            }
            // End of changes
            return '-';
          } else {
            const fields = field.split('.');
            let value = data;
            for (let i = 0, len = fields.length; i < len; ++i) {
              if (value == null) {
                return '';
              }
              value = value[fields[i]];
            }
            return value;
          }
        } else {
          return null;
        }
      };
    }
  }

  isAllowed(tag: string): boolean {
    return this._authSrv.hasComponentPermission('person', tag);
  }

  /************************ CONFIG METHODS ************************/

  /**
   * Get name input validator widgetConfig
   * @returns {Object}
   */
  public getNameInputValidatorConfig(): object {
    return this._nameInputValidatorConfig;
  }

  /**
   * Get surname input validator widgetConfig
   * @returns {Object}
   */
  public getSurnameInputValidatorConfig(): object {
    return this._surnameInputValidatorConfig;
  }

  /**
   * Get email input validator widgetConfig
   * @returns {Object}
   */
  public getEmailInputValidatorConfig(): object {
    return this._emailInputValidatorConfig;
  }

  /************************ EVENTS METHODS ************************/

  /**
   * On invalid input setter
   * @param {Object | any} event
   * @param param
   */
  public onInvalidInput(event: object | any,  param: string): void {
    if (event.hasOwnProperty('valid') && !event['valid'] && this.invitePerson.hasOwnProperty(param)) {
      this.invitePerson[param] = null;
    }
  }

  /**
   * On valid email setter
   * @param {Object | any} event
   * @param param
   */
  public onValidInput(event: object | any,  param: string): void {
    if (event.hasOwnProperty('valid') && event['valid'] && event.hasOwnProperty('value') && event['value']
      && this.invitePerson.hasOwnProperty(param)) {
      this.invitePerson[param] = event.value;
    }
  }

  /**
   * Check if mate has complete all fields
   */
  public isValidPerson(): boolean {
    return !_.isNil(this.invitePerson.name)
      && !_.isNil(this.invitePerson.surname)
      && !_.isNil(this.invitePerson.email);
  }

  /**
   * Get mate birthdate
   * @param {object} person
   * @returns {string}
   */
  public getPersonBirthdate(person: object): string {
    return person && !_.isNil(person)
      && person.hasOwnProperty('birthdate')
      && !_.isNil(person['birthdate'])
      ? this._dateLocalPipe.transform(person['birthdate'], 'es-ES')
      : '-';
  }

  /**
   * Load person lazy
   * @param {LazyLoadEvent} event
   */
  public loadMembersLazy(event: LazyLoadEvent) {
    this.pagination.page = (event.first / event.rows) + 1;
    this.pagination.sortField = !_.isNil(event.sortField) ? event.sortField : this.pagination.sortField;
    const order = !_.isNil(event.sortOrder) ? event.sortOrder : 1;
    this.pagination.sortOrder = order === 1 ? 'asc' : 'desc';

    setTimeout(() => {
      if (this.persons) {
        this.getAllPersons();
      }
    }, 250);
  }

  /**
   * On click export datatable to csv
   */
  public onClickExportToCsv(): void {
    this.getAllPersonsToExport();
  }

  /**
   * On click mate (Select datatable row)
   * @param event
   */
  public onClickPerson(event: any): void {
    if (event
      && event.hasOwnProperty('data')
      && !_.isNil(event['data'])
      && !_.isEmpty(event['data'])) {
      this._router.navigateByUrl('/mate-profile/' + event['data']['id']);
    }
  }

  /************************ API METHODS  ************************/

  /**
   * Get all persons
   */
  private getAllPersons(): void {
    this._httpSrv.get(
      'persons/',
      {
        page: this.pagination.page,
        elements: this.pagination.rows,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder
      },
      'Listado personas'
    ).subscribe(
      success => this.onSuccessGetAllPersons(success)
    );
  }

  /**
   * On success get all persons
   * @param response
   * @param {boolean} exportToCsv
   */
  private onSuccessGetAllPersons(response: any, exportToCsv: boolean = false): void {
    this._loaderSrv.stop();

    if (!_.isNil(response.object)) {
      this.persons = [];

      const activePersonRole = this._authSrv.getPersonLogged().getRoleTag();
      response['object']['items'].forEach(person => {
        switch (activePersonRole) {
          case (ROLESYSTEMS.ROLE_SUPER_ADMIN):
            if (person.role_systems[0].tag === ROLESYSTEMS.ROLE_ADMIN
              || person.role_systems[0].tag === ROLESYSTEMS.ROLE_GESTOR
              || person.role_systems[0].tag === ROLESYSTEMS.ROLE_USER) {
              this.persons.push(new PersonModel(person));
            }
            break;
          case (ROLESYSTEMS.ROLE_ADMIN):
            if (person.role_systems[0].tag === ROLESYSTEMS.ROLE_GESTOR
              || person.role_systems[0].tag === ROLESYSTEMS.ROLE_USER) {
              this.persons.push(new PersonModel(person));
            }
            break;
          case (ROLESYSTEMS.ROLE_GESTOR):
            if (person.role_systems[0].tag === ROLESYSTEMS.ROLE_USER) {
              this.persons.push(new PersonModel(person));
            }
            break;
        }
      });

      this.persons = response.object.items;
      this.pagination.totalRecords = response.object.total_count;

    } else {
      this.persons = [];
      this.pagination.totalRecords = 0;
    }

    if (exportToCsv) {
      setTimeout(() => {
        this._personsDatatable.exportCSV();
        this.getAllPersons();
      }, 300);
    }
  }

  /**
   * Get all persons to export
   */
  private getAllPersonsToExport(): void {
    this._httpSrv.get(
      'persons/',
      {
        page: 1,
        elements: this.pagination.totalRecords,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder,
      },
      'Listado personas a exportar'
    ).subscribe(
      success => this.onSuccessGetAllPersons(success, true)
    );
  }

  /************************ AUXILIAR METHODS  ************************/

  public functionalityNotImplemented(msg?: string): void {
    this._alertSrv.prompt(msg);
  }

  /************************ CSV METHODS ************************/

  public inviteByEmail(): void {
    this._httpSrv.post(
      'persons/',
      this.invitePerson,
      'Invitar persona'
    ).subscribe(
      () => {
          localStorage.setItem('GeneralDomainWidgetContainerComponent', 'person');
          this.cancelModalAddPerson();
        },
      () => {this.cancelModalAddPerson(); }
    );
  }

  public restartModalPerson(): void {
    this.inputsValidator.forEach((input: InputValidatorComponent) => {
      input.restart();
    });
    this.invitePerson = {
      name: null,
      surname: null,
      email: null,
      is_ghost: false
    };
  }


  /** CSV METHODS **/

  public getCvsResponse(event): any {
    this._subscribeCsv = this._csvSrv.getCsv().subscribe((csv: CSVModel) => {
      if (!_.isNil(csv)) {

        if (csv.error) {
          this.resetCsv();
          this.csvErrorMessage = csv.error;

        } else {
          this.reserCsvMessages();
          this._csvData = csv.getValidRows();
          this.csvImportMessage = this._csvSrv.standardImportMessage(csv);
        }
      }
    });

    this._csvSrv.processFile(
      event,
      {
        email:      {required: true, pattern: REGEXPATTERNS.email, validation: 'persons/InputValidator/email'},
        name:       {required: true, pattern: REGEXPATTERNS.anyMin3},
        surname:    {required: true, pattern: REGEXPATTERNS.anyMin3},
        phone:      {required: false, pattern: REGEXPATTERNS.anyMin6},
        birthdate:  {required: false, pattern: REGEXPATTERNS.birthdateCsv}
      }, false
    );
  }


  private reserCsvMessages(): void {
    this.csvImportMessage = '';
    this.csvSuccessMessage = '';
    this.csvErrorMessage = '';
  }

  public resetCsv (): void {
    this.reserCsvMessages();
    this._csvData = [];
    this._csvMassiveUpdown.counter = 0;
    this.fileCsvInput.nativeElement.value = '';
    this._csvSrv.resetCsv();

    if (!_.isNil(this._subscribeCsv)) {
      this._subscribeCsv.unsubscribe();
    }
  }

  public existsCsv(): boolean {
    return !_.isNil(this._csvData) && !_.isEmpty(this._csvData);
  }

  public importPersons(): void {
    if (!this.existsCsv()) {
      this._alertSrv.error('Debe importar un archivo CSV antes de enviarlo.');

    } else {
      this.importCsv();
    }
  }

  private importCsv(): void {
    this._loaderSrv.softImporting();
    const newPersons = [];

    _.slice(
      this._csvData,
      this._csvMassiveUpdown.counter,
      (this._csvMassiveUpdown.pack + this._csvMassiveUpdown.counter)
    ).forEach((item: CSVRowModel) => {
      if (item.hasOwnProperty('values') && item.values) {
        newPersons.push(item.values);
      }
    });

    this._httpSrv.post(
      'persons/batch/',
      {new_persons: newPersons},
      'Importación CSV',
      false,
      false
    ).subscribe(
      response => this.onImportCsv(response),
      fail => this.onFailImportCsv(fail)
    );
  }

  private onImportCsv(response): void {
    this._csvMassiveUpdown.counter += this._csvMassiveUpdown.pack;

    if (this._csvMassiveUpdown.counter >= this._csvData.length) {
      const totalUpdown = this._csvData.length.toString();
      this._loaderSrv.stopSoft();
      this.resetCsv();

      if (response.hasOwnProperty('success') && response['success']) {
        localStorage.setItem('GeneralDomainWidgetContainerComponent', 'person');
        this.csvSuccessMessage = ':t usuarios importados correctamente.'.replace(':t', totalUpdown);
        this.getAllPersons();

      } else {
        this.onFailImportCsv(response);
      }

    } else {
      this.importCsv();
    }
  }

  private onFailImportCsv(fail): void {
    this._loaderSrv.stopSoft();
    this.resetCsv();
    this.csvErrorMessage = fail.hasOwnProperty('message')
      ? fail['message']
      : 'se ha producido un error desconocido, prueba más tarde';
  }
}

