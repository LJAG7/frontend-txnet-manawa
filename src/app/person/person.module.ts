/*GENERAL ASSETS*/
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';

/*MODULE ASSETS*/
import {PersonComponent} from './person.component';

/*PIPES*/
import {DateLocalPipe} from '../shared/pipes/dateLocal.pipe';

/*SERVICES*/
import {HttpSrvService} from '../shared/services/http.service';

import {ComponentsModule} from '../shared/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    ComponentsModule
  ],
  exports: [
    PersonComponent
  ],
  declarations: [
    PersonComponent
  ],
  providers: [
    HttpSrvService,
    DateLocalPipe
  ]
})

export class PersonModule {}
