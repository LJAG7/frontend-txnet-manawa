import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {Observable} from 'rxjs/Observable';
import {PushNotification, PushNotificationsService} from 'ng-push';
import 'rxjs/add/operator/takeWhile';

import {PersonModel} from '../shared/models/person.model';
import {NotificationPushModel} from '../shared/models/push/notification-push.model';
import {PUSHNOTIFICATIONSMAXHOURS} from '../shared/const/system.const';

const moment = require('moment');
const _ = require('lodash');

@Component({
  selector: 'app-push-notifications-component',
  templateUrl: './push-notifications.component.html',
  styleUrls: ['./push-notifications.component.css']
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class PushNotificationsComponent implements OnInit, OnDestroy, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() person: PersonModel;

  /************************ COMPONENT VARS  ************************/

  public notificationObservable: Observable<any[]>;
  public showNotificationsList: Array<NotificationPushModel> = [];
  public first = true;

  private _actualDate = moment(new Date());
  private _active = true;
  private _notePushEnpoint: string;

  /************************ CONFIG VARS ************************/

  private _options: PushNotification = {
    dir: 'rtl',
    icon: 'assets/img/icon48.png'
  };

  private _notificationsShown: Array<string> = [];

  // body: string,
  // icon: string,
  // tag: string,
  // renotify: boolean,
  // silent: boolean,
  // sound: string,
  // noscreen: boolean,
  // sticky: boolean,
  // dir: 'auto' | 'ltr' | 'rtl',
  // lang: string,
  // vibrate: number[]

  constructor(private db: AngularFireDatabase,
              private _pushNotificationsSrv: PushNotificationsService) {
    // Request for permission as soon as component loads
    this._pushNotificationsSrv.requestPermission();
  }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges) {
    if (!_.isNil(changes['person'])
      && !_.isNil(changes['person']['currentValue'])
      && changes.person.currentValue.id !== 0) {
      this.initializePushNotifications();
    }
  }

  ngOnDestroy() {
    this._active = false;
  }

  getNotifications(listPath): Observable<any[]> {
    return this.db.list(listPath).valueChanges();
  }

  // Function to be called on click
  notify(notifications: Array<object> = []) {
    if (!_.isEmpty(notifications)) {
      notifications.forEach( notification => {

        const pushNotification = new NotificationPushModel(notification);

        // Find notification
        const notificationIndex = this.showNotificationsList.findIndex( note => {
          return note.getNotificationKey() === pushNotification.getNotificationKey();
        });

        if (notificationIndex === -1) {
          this.showNotificationsList.push(pushNotification);

          // Check notification date
          if (this._actualDate.diff(pushNotification.datetime, 'hours') > PUSHNOTIFICATIONSMAXHOURS) {
            this.deleteNotification(pushNotification.getNotificationKey());
          } else {

            this._options.body = pushNotification.description;

            // Create notification if it is not showing

            if (this._notificationsShown.indexOf(pushNotification.getNotificationKey()) === -1) {
              this._pushNotificationsSrv.create(pushNotification.titulo, this._options).subscribe(
                res => {
                  if (res.event.type === 'click') {
                    // You can do anything else here
                    res.notification.close();
                    this.deleteNotification(pushNotification.getNotificationKey());
                  }
                },
                err => console.log('Error de notificacion push ' + err)
              );
            }
          }
        }
      });
    }
  }

  /**
   * Delete notification
   * @param {string} notificationKey
   */
  private deleteNotification(notificationKey: string): void {
    this.db.object(this._notePushEnpoint + '/' + notificationKey).remove();
  }

  /**
   * Initialize push notifications
   */
  private initializePushNotifications(): void {
    this._notePushEnpoint = '/txnet/Domains/domain_' + this.person.domain.id + '/Persons/person_' + this.person.id + '/notification';
    this.notificationObservable = this.getNotifications(this._notePushEnpoint);
    this.notificationObservable.takeWhile(() => this._active).subscribe( notifications => {
      this.notify(notifications);
    });
  }
}
