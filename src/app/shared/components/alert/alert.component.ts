import { Component, OnInit, DoCheck} from '@angular/core';

import { Alert, AlertType } from '../../models/alert';
import { AlertService } from '../../services/alert.service';

const _ = require('lodash');

@Component({
  moduleId: module.id,
  selector: 'app-alert-component',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})

export class AlertComponent implements OnInit, DoCheck {
  alerts: Alert[] = [];
  private _limit = 5;
  private _timer = null;
  private _interval = 3000;
  private _stop = false;

  constructor(private _alertSrv: AlertService) { }

  ngOnInit() {
    this._alertSrv.getAlert().subscribe((alert: Alert) => {
      if (!alert) {
        // clear alert when an empty alert is received
        this.alerts = [];
        return;
      }

      if (this.alerts.length > this._limit) {
        this.alerts.splice(0, 1);
      }
      this.alerts.push(alert);
    });
  }

  ngDoCheck() {
    if (this.alerts.length > 0 && _.isNil(this._timer) && !this._stop) {
      this._timer = setInterval(() => {
        this.removeAlert(_.head(this.alerts));
      }, this._interval);
    }
  }

  removeAlert(alert: Alert) {
    this.alerts = this.alerts.filter(x => x !== alert);
    this.stopIntervalRemove();
  }

  cssClass(alert: Alert) {
    if (!alert) {
      return;
    }

    // return css class based on alert type
    switch (alert.type) {
      case AlertType.Success:
        return 'alert alert-success';
      case AlertType.Error:
        return 'alert alert-danger';
      case AlertType.Info:
        return 'alert alert-info';
      case AlertType.Warning:
        return 'alert alert-warning';
      case AlertType.Prompt:
        return 'alert alert-prompt';
    }
  }

  private stopIntervalRemove() {
    clearInterval(this._timer);
    this._timer = null;
  }

  onMouse(event: string) {
    if (this._stop = event === 'over') {
      this.stopIntervalRemove();
    }
  }
}
