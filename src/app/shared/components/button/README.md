## CONFIGURATION FOR COMPONENT

{
      button: {
        type: 'button', //submit, reset, button, link...
        hasText: true,
        text: 'Check this',
        isDisabled: false,
        classes: {
          main: 'primary',
          extra: '',
          isBlockButton: false,
          isLargeButton: false,
          isSmallButton: false,
          hasOutlineBorder: false
        }
      },
      icon: {
        has: true,
        classes: {
          main: 'search'
          isLg: false,
          isAlignLeft: false,
          faSize: 0,
          extra: ''
        }
      },
      popover: {
        has: false,
        options: [
          {
            id: 1,
            name: 'Acción 1',
            active: true,
            disabled: true,
            action: function () {
              alert('action 1');
            }
          }, {
            id: 2,
            name: 'Acción 2',
            disabled: false,
            link: {
              has: true,
              href: '/members'
            }
          }, {
            id: 3,
            isDivider: true
          }, {
            id: 4,
            name: 'Acción 3',
            disabled: false,
            hasDivider: false,
            callBackFunctionName: 'consoleLog'
             {
              id: 4,
              name: 'Acción 3',
              disabled: false,
              isModal: true,
              modalConfig: {}
              }
          }
        ]
      },
      clickEvent: function () {
        alert('basic configuration for button event click!');
      }
    }
