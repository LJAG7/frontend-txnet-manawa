import {Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';

const _ = require('lodash');

@Component({
  selector: 'app-button-component',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})

/**
 * @author Elias Romero <elias.romero@steelmood.com>
 * @author Miguel Rico <miguel.rico@steelmood.com>
 */
export class ButtonComponent implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() public disabled = false;
  @Input() public externalConfig: Object;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() public outputEvent = new EventEmitter();

  /************************ CONFIG VARS  ************************/

  private _config: any = {
    button: {
      type: 'button',
      hasText: true,
      text: 'Botón',
      isDisabled: false,
      classes: {
        isBlockButton: false,
        isLargeButton: false,
        isSmallButton: false,
        hasOutlineBorder: false,
        isDropdownButton: false,
        isInputGroupButton: false,
        extra: '',
        main: 'primary'
      }
    },
    icon: {
      has: false,
      classes: {
        main: 'search',
        extra: ''
      }
    },
    clickEvent: {
      callBackFunction: null
    }
  };

  private _classesConfiguration: Object = {
    button: {
      'btn-{}$main$': true,
      'btn': true,
      'btn-block': 'isBlockButton',
      'btn-lg': 'isLargeButton',
      'btn-sm': 'isSmallButton',
      'is-input-group-button': 'isInputGroupButton',
      'dropdown-toggle': 'isDropdownButton'
    },
    icon: {
      'fa': true,
      'fa-{}$main$': true,
      'flex-first': 'isAlignLeft',
      'fa-lg': 'isLg',
      'fa-{}$faSize$x': true,
    }
  };

  /************************ STATIC METHODS ************************/

  /**
   * Check if empty string
   * @param string
   * @returns {boolean}
   */
  public static isNotEmpty(string): boolean {
    return _.size(string) > 0;
  }

  /**
   * Check if null or undefined string
   * @param value
   * @returns {boolean}
   */
  public static isNullOrUndefined(value): boolean {
    return _.isNil(value);
  }

  /**
   * Generate key to search and replace on string
   * @param {string} text
   * @returns {string}
   */
  public static sliceVariableNameOnString(text: string): string {
    return text.slice(text.indexOf('$') + 1, text.lastIndexOf('$'));
  }

  /************************ NATIVE METHODS ************************/

  constructor() {}

  ngOnInit(): void {
    this.mergeConfigurations();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.mergeConfigurations();

    if (!_.isNil(changes['externalConfig']['currentValue']['button'])) {
      this._config.button.text = !_.isNil(changes['externalConfig']['currentValue']['button']['text'])
        ? changes['externalConfig']['currentValue']['button']['text']
        : this._config.button.text;
      this._config.button.classes = !_.isNil(changes['externalConfig']['currentValue']['button']['classes'])
        ? changes['externalConfig']['currentValue']['button']['classes']
        : this._config.button.classes;
      this._config.button.isDisabled = !_.isNil(changes['externalConfig']['currentValue']['button']['isDisabled'])
        ? changes['externalConfig']['currentValue']['button']['isDisabled']
        : this._config.button.isDisabled;
    }
  }

  /************************ SETUP METHODS ************************/

  /**
   * Get the schema of all notes for DOM
   * @param {string} target
   * @returns {Object}
   */
  public getClasses(target: string): Object {
    return this.generateExtraClass(target, this.generateBasicClasses(target));
  }

  /**
   * Get text of button
   * @returns {string}
   */
  public getTextButton(): string {
    return this._config.button.text;
  }

  /**
   * Check if show icon of button
   * @returns {boolean}
   */
  public showIconButton(): boolean {
    return this._config.icon.has;
  }

  /**
   * Check if show text of button
   * @returns {boolean}
   */
  public showTextButton(): boolean {
    return this._config.button.hasText;
  }

  /************************ EVENTS METHODS ************************/

  /**
   * Check if disabled button
   * @returns {boolean}
   */
  public isDisabled(): boolean {
    return this.disabled;
  }

  /**
   * On click event, if it exists callback function executes it else return true
   * @returns {any}
   */
  public onClickEvent(): any {
    return this.existsCallBackFunction()
      ? this.outputEvent.emit({callBack: this._config.clickEvent.callBackFunction})
      : this.outputEvent.emit({clicked: true});
  }

  /************************ AUXILIAR METHODS ************************/

  /**
   * Create a custom css attribute from composition
   * @param {string} attribute
   * @param {string} target
   * @returns {string}
   */
  private createCustomCssAttribute(attribute: string, target: string): string {
    const isAttributeToCreate = _.includes(attribute, '{}'),
      varToChange = ButtonComponent.sliceVariableNameOnString(attribute);

    return isAttributeToCreate ? attribute.replace('{}$'.concat(varToChange + '$'), this._config[target].classes[varToChange]) : attribute;
  }

  /**
   * Check if exists callBack function
   * @returns {boolean}
   */
  private existsCallBackFunction(): boolean {
    return !_.isNil(this._config.clickEvent.callBackFunction) && _.size(this._config.clickEvent.callBackFunction) > 0;
  }

  /**
   * Generate basic notes for each element in button
   * @param {string} target
   * @returns {Object}
   */
  private generateBasicClasses(target: string): Object {
    const schema = {},
      scope = this;

      _.each(this._classesConfiguration[target], function (config, prefix) {
      schema[scope.createCustomCssAttribute(prefix, target)] = !_.isBoolean(config) ? scope._config[target].classes[config] : config;
    });

    return schema;
  }

  /**
   * Generate extra notes for each element in button
   * @param {string} target
   * @param schema
   * @returns {Object}
   */
  private generateExtraClass(target: string, schema: any): Object {
    if (!ButtonComponent.isNullOrUndefined(this._config[target].classes)
      && ButtonComponent.isNotEmpty(this._config[target].classes.extra)) {
      schema[this._config[target].classes.extra] = true;
    }
    return schema;
  }

  /**
   * Merge config with external CONFIG
   */
  private mergeConfigurations(): void {
    _.merge(this._config, this.externalConfig);
  }
}
