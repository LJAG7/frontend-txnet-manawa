import {Component, OnInit, Input, ViewChild, OnChanges, SimpleChanges} from '@angular/core';

import {EntryFormComponent} from './entry-form/entry-form.component';
import {EntryDetailComponent} from './entry-detail/entry-detail.component';
import {ConfirmDialogComponent} from '../confirm-dialog/confirm-dialog.component';

import {AlertService} from '../../services/alert.service';
import {HttpSrvService} from '../../services/http.service';
import {AuthSrvService} from '../../services/auth.service';

import {CalendarModel} from '../../models/calendar/calendar.model';
import {CalendarEntryModel} from '../../models/calendar/entry.model';
import {PersonModel} from '../../models/person.model';
import {DateLocalPipe} from '../../pipes/dateLocal.pipe';

const _ = require('lodash');
const moment = require('moment');

enum modesAvailable {normal, new, edit, view}

@Component({
  selector: 'app-calendar-component',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class CalendarComponent implements OnInit, OnChanges {

  @ViewChild('calendarComponent') calendarComponent;
  @ViewChild('entryForm') entryForm: EntryFormComponent;
  @ViewChild('entryDetail') entryDetail: EntryDetailComponent;
  @ViewChild('confirmDeleteDialog') confirmDeleteDialog: ConfirmDialogComponent;
  @ViewChild('confirmChangeDialog') confirmChangeDialog: ConfirmDialogComponent;

  @Input() public visible = true;
  @Input() public person: PersonModel;

  public fnEvRender;

  public schedule: CalendarModel = new CalendarModel;
  public entriesList: Array<object> = [];
  public entrySelected: object;
  public isEnabledEdition = true;

  private  _entriesRange = {
    start: null,
    end: null
  };

  private _viewMode = modesAvailable.normal;

  /** COMPONENT CONFIG **/

  public confirmDeleteDialogConfig = {
    header: 'La nota va a ser eliminada',
    message: '¿Desea continuar?',
    accept: {
      action: 'onDeleteEntry',
      button: {
        text: 'Si'
      }
    }
  };

  public confirmChangeDialogConfig = {
    header: 'Se perderán todos los cambios',
    message: '¿Desea continuar?',
    accept: {
      action: 'onChangeEntry',
      button: {
        text: 'Si'
      }
    }
  };

  constructor(private _httpSrv: HttpSrvService,
              private _authSrv: AuthSrvService,
              private _alertSrv: AlertService,
              private _datePipe: DateLocalPipe) {

    this.entrySelected = this.newScheduleEntryObject();
    this._entriesRange.start = moment().startOf('month').format();
    this._entriesRange.end = moment().endOf('month').format();
  }

  /************************ NATIVE METHODS ************************/
  ngOnInit() {
    this.fnEvRender = _.bind(this.evRender, this); // here I use lodash; use whatever you like...

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!_.isNil(changes['person']) && !_.isUndefined(changes['person']['currentValue']) && changes['person']['currentValue']['id'] > 0) {
      this.isEnabledEdition = this.person.id === this._authSrv.getPersonLogged().id;
      this.getCalendarEntries();
    }
  }

  /************************ MODEL METHODS ************************/

  /**
   * Return a new object for p-schedule component
   * @param {Object | CalendarEntryModel} data
   * @returns {Object}
   */
  private newScheduleEntryObject(data?: object | CalendarEntryModel): object {
    const newScheduleEntry = {
      title: '',
      start: null,
      end: null,
      hasConference: false,
      model: new CalendarEntryModel(),
    };

    this.updateScheduleEntryObject(newScheduleEntry, data);
    return newScheduleEntry;
  }

  /**
   * Update the scheduleObject
   * @param {Object | CalendarEntryModel} data
   * @returns {Object}
   */
  private updateScheduleEntryObject(scheduleEntry: object, data?: object | CalendarEntryModel): void {
    scheduleEntry['model'] = _.isNil(data) || !(data instanceof CalendarEntryModel)
      ? new CalendarEntryModel(data)
      : data;

    const startDate = moment(scheduleEntry['model'].start_date),
      endDate = moment(scheduleEntry['model'].end_date),
      allDay = startDate.format('HH:mm') === '00:00' && endDate.format('HH:mm') === '23:59';

    scheduleEntry['title'] = scheduleEntry['model'].title;
    scheduleEntry['start'] = allDay ? startDate.format('YYYY-MM-DD') : scheduleEntry['model'].start_date;
    scheduleEntry['end'] = allDay ? endDate.format('YYYY-MM-DD') : scheduleEntry['model'].end_date;
    scheduleEntry['hasConference'] = scheduleEntry['model'].conference_allocated;
  }

  /**
   * Add entries to calendar component.
   * @param {Array<Object>} entries
   */
  private insertCalendarEntries(entries: Array<object>) {
    _.forEach(entries, (data: object) => {
      this.entriesList.push(this.newScheduleEntryObject(data));
    });
  }

  /************************ DOM METHODS ************************/
  /**
   * Permite la renderizacion del evento. Se asigna clases para establecer el color
   * @param ev
   * @param el
   * @param view
   * @returns {boolean}
   */
  evRender(ev, el, view) {
    if (ev.hasOwnProperty('eventType')) {
      el.addClass('event-' + ev.eventType);
    }

    if (ev.hasOwnProperty('meetUrl')) {
      el.addClass('event-has-meet');
    }

    return true; // or false if you don't want the element to be rendered
  }

  /**
   * Return a calendar DOM id
   * @returns {string}
   */
  getComponentId(): string {
    return 'calendar_component_n'.concat(this.schedule.id.toString());
  }

  /**
   * Return if the view is full page mode
   * @returns {boolean}
   */
  public isCalendarFullPage(): boolean {
    return this._viewMode === modesAvailable.normal;
  }

  /**
   * Return if view mode is active
   * @returns {boolean}
   */
  public isViewEntryMode(): boolean {
    return this._viewMode === modesAvailable.view;
  }

  /**
   * Return if edition mode is active
   * @returns {boolean}
   */
  public isEditEntryMode(): boolean {
    return this._viewMode === modesAvailable.edit;
  }

  /**
   * Return if new mode is active
   * @returns {boolean}
   */
  public isNewEntryMode(): boolean {
    return this._viewMode === modesAvailable.new;
  }


  /************************ DOM EVENT METHODS ************************/

  /**
   * Handle on click in day
   * @param event
   */
  onCalendarDayClick(event): void {
    if (this.isEditEntryMode() && !_.isNil(this.entryForm) && this.entryForm.hasChangesInEntry()) {
      this.confirmChangeDialog.showDialog(
        {
          mode: modesAvailable.new,
          entry: this.newScheduleEntryObject({start_date: event.date.format()})
        },
        'Se va a crear una nueva entrada, se perderán todos los cambios no guardados'
      );

    } else {
      if (!this.isNewEntryMode()) {
        if (this.isEnabledEdition) {
          this.entrySelected = this.newScheduleEntryObject({start_date: event.date.format()});
          this._viewMode = modesAvailable.new;
        }

      } else {
        this.entrySelected['model'].start_date = event.date.format();
        this.entryForm.updateForm();
      }
    }
  }

  /**
   * Handle on click in event
   * @param event
   */
  onCalendarEventClick(event): void {
    if ((this.isNewEntryMode() || this.isEditEntryMode()) && !_.isNil(this.entryForm) && this.entryForm.hasChangesInEntry()) {
      this.confirmChangeDialog.showDialog(
        {
          mode: modesAvailable.view,
          entry: event.calEvent
        },
        this.isNewEntryMode()
          ? 'La nueva entrada no ha sido guardada, se perderán todos los cambios'
          : 'Hay cambios sin guardar, se perderán todos los cambios'
      );

    } else {
      this._viewMode = modesAvailable.view;
      this.entrySelected = event.calEvent;
    }
  }

  /**
   * Handle on start change in event (resize or drop event)
   * @param event
   */
  onCalendarEventChangeStart(event): void {

  }

  /**
   * Handle on change in event (resize or drop event)
   * @param event
   */
  onCalendarEventChange(event): void {

  }


  /**
   * Entry component events handler
   * @param event
   */
  onEntryFormEvent(event): void {
    if (event.hasOwnProperty('event')) {

      if (event['event'] === 'close') {
        this._viewMode = modesAvailable.normal;
        this.entrySelected = this.newScheduleEntryObject();

      } else if (event['event'] === 'edit') {
        this._viewMode = modesAvailable.edit;

      } else if (event['event'] === 'backToView') { // entrySelected must be previously instantiated (return from edit mode
        this._viewMode = modesAvailable.view;

      } else if (event['event'] === 'delete') {
        this.showDeleteEntryConfirm();


      } else if (event['event'] === 'updated') {
        this.updateScheduleEntryObject(this.entrySelected, event['entry']);
        this.calendarComponent.updateEvent(this.entrySelected);
        this._viewMode = modesAvailable.view;

      } else if (event['event'] === 'created') {
        this.entrySelected = this.newScheduleEntryObject(event['entry']);
        this._viewMode = modesAvailable.view;
        this.entriesList.push(Object.assign({}, this.entrySelected));

      } else if (event['event'] === 'deleted') {
        const entryId = this.entrySelected['model'].id;
        this._viewMode = modesAvailable.normal;
        this.entrySelected = this.newScheduleEntryObject();

        _.remove(this.entriesList, (entryObj: object) => {
          return entryObj['model'].id === entryId;
        });
      }
    }
  }

  // DIALOG CONFIRM METHODS
  private showDeleteEntryConfirm(): void {

    if (!_.isNil(this.entrySelected) && !_.isNil(this.entrySelected['model']) && this.entrySelected['model'].id !== 0) {
      this.confirmDeleteDialog.showDialog(
        null,
        'La entrada "<i>' + this.entrySelected['title'] + '</i>" va a ser eliminada'
      );

    } else {
      this._alertSrv.error('La entrada no puede ser eliminada');
    }

  }


  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      _.isNil(event['params'])
        ? this[event['callBack']]()
        : this[event['callBack']](event['params']);
    }
  }

  /************************ EXTERNAL METHODS ************************/
  /**
   * Set view to new entry mode
   */
  public startNewEntryMode(): void {
    this.entrySelected = this.newScheduleEntryObject({start_date: moment().format()});
    this._viewMode = modesAvailable.new;
  }

  /**
   * REturn is new entry mode is available in view. Only if mode is normal o view
   * @returns {boolean}
   */
  public isAvailableNewEntryMode(): boolean {
    return !this.isNewEntryMode() && !this.isEditEntryMode();
  }

  /**
   * Command to delete an entry
   */
  public onDeleteEntry(): void {
    this.isViewEntryMode()
      ? this.entryDetail.deleteEntry()
      : this.entryForm.deleteEntry();
  }

  /**
   * Command to change an entry
   * @param {Object} params
   */
  public onChangeEntry(params: object): void {
    this.entrySelected = params['entry'];
    this._viewMode = params['mode'];
  }

  /************************ API METHODS ************************/
  private getCalendarEntries(): void {
    const endpoint = this.person['id'] === this._authSrv.getPersonId()
      ? 'persons/my/calendarentries/'
      : 'persons/' + this.person['id'] + '/calendarentries/';

    const query = {
      page: 1,
      elements: 2000,
      orderBy: 'startDate',
      orderType: 'asc',
      startDate: this._datePipe.rangeDateForApi('1990-01-01T00:00:00', '2020-11-31T23:59:59')
    };


    this._httpSrv
      .get(
        endpoint,
        query
      )
      .subscribe(
        (response) => {
          if (response['success']) {
            this.insertCalendarEntries(response['object']['items']);
          }
        }
      );
  }



}
