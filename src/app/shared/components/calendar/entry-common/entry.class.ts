import {Input, Output, EventEmitter, Injector} from '@angular/core';

import {PersonModel} from '../../../models/person.model';
import {CalendarEntryModel} from '../../../models/calendar/entry.model';

import {HttpSrvService} from '../../../services/http.service';
import {AlertService} from '../../../services/alert.service';
import {AuthSrvService} from '../../../services/auth.service';

const _ = require('lodash');

export abstract class EntryClass {

  @Input() public entry: CalendarEntryModel;
  @Input() public isEnabledEdition = true;
  @Output() public outputEvent = new EventEmitter();

  public isAvailableActions = false;

  protected _httpSrv: HttpSrvService;
  protected _authSrv: AuthSrvService;
  protected _alertSrv: AlertService;

  constructor(private _typeComponent: string, injector: Injector) {
    this._httpSrv = injector.get(HttpSrvService);
    this._authSrv = injector.get(AuthSrvService);
    this._alertSrv = injector.get(AlertService);
  }

  /************************ INTERNAL METHODS ************************/
  /**
   * Return the index for a determited person in an array
   * @param {PersonModel} person
   * @param {Array} list
   * @returns {number}
   */
  protected findPersonIndex(person: PersonModel, list: Array<PersonModel>): number {
    if (_.isNil(person) || _.isNil(list) || _.isEmpty(list)) {
      return -1;
    }

    return _.findIndex(list, (p: PersonModel) => {
      return p.id === person.id;
    });
  }


  /**
   * Return a guest list without its owner
   * @param {CalendarEntryModel} entry
   * @returns {Array<PersonModel>}
   */
  public getFilteredGuest(entry: CalendarEntryModel): Array<PersonModel> {
    if (_.isNil(entry) || _.isNil(entry.guests) || _.isEmpty(entry.guests)) {
      return [];
    }

    return _.filter(entry.guests, (person: PersonModel) => {
      return _.isNil(entry.created_by) || person.id !== entry.created_by.id;
    });
  }


  /**
   * Return if the logged user is entry owner
   * @returns {boolean}
   */
  protected isEntryOwner(): boolean {
    return _.isNil(this.entry.created_by) || this.entry.created_by.id === this._authSrv.getPersonId();
  }


  /************************ DOM EVENT METHODS ************************/

  /**
   * Emit output event method
   * @param {string} event
   * @param {Object} extra
   */
  protected emitOutputEvent(event: string, extra?: object): void {
    this.outputEvent.emit(
      Object.assign(
          {},
          {id: this._typeComponent, event: event},
          _.isNil(extra) ? {} : extra
        )
    );
  }

  /**
   * Cancel de form edition
   */
  public closeFormEmit() {
    this.emitOutputEvent('close');
  }


  /**
   * Delete entry
   */
  public deleteEntryEmit() {
    this.emitOutputEvent('delete');
  }

  /**
   * Editing entry
   */
  public editEntryEmit() {
    this.emitOutputEvent('edit');
  }


  /************************ API METHODS ************************/

  /**
   * Delete an entry
   */
  private deleteEntryFromApi(): void {
    if (!_.isNil(this.entry) && this.entry.id !== 0) {
      this._httpSrv
        .delete(
          'calendarentries/' + this.entry.id
        )
        .subscribe(
          (response) => {
            if (response.hasOwnProperty('success') && response['success']) {
              this.emitOutputEvent('deleted');
            }
          }
        );
    } else {
      this._alertSrv.error('Calendario: la entrada no puede ser eliminada');
    }
  }


  /************************ EXTERNAL METHODS ************************/

  /**
   * Delete an entry
   */
  public deleteEntry(): void {
    this.deleteEntryFromApi();
  }
}
