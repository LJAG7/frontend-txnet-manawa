import {Component, OnInit, OnChanges, SimpleChanges, Injector} from '@angular/core';

import {EntryClass} from '../entry-common/entry.class';

const _ = require('lodash');
const moment = require('moment');

@Component({
  selector: 'app-entry-detail',
  templateUrl: './entry-detail.component.html',
  styleUrls: ['./entry-detail.component.css']
})
export class EntryDetailComponent extends EntryClass implements OnInit, OnChanges {

  public entryHtml = {
    badge: {class: '', text: ''},
    date: '',
    location: null,
    conference: null,
    description: '',
    guests: []
  };

  constructor(injector: Injector) {
    super('entryDetail', injector);
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!_.isNil(changes['entry']) && !_.isUndefined(changes['entry']['currentValue'])) {
      this.setEntryDomInfo();
      this.isAvailableActions = this.isEntryOwner();
    }
  }

  /************************ MODEL EVENTS *******************************/


  /************************ DOM EVENTS *******************************/

  /**
   * Get badget for entry date
   */
  private getDateBadge(): void {
    if (moment().isBetween(this.entry.start_date, this.entry.end_date, 'day', '[]')) {
      this.entryHtml.badge = {class: 'badge-primary', 'text': 'Hoy'};

    } else if (moment().isSame(moment(this.entry.start_date).subtract(1, 'days'), 'day')) {
      this.entryHtml.badge = {class: 'badge-success', 'text': 'Mañana'};

    } else {
      this.entryHtml.badge = {class: '', 'text': ''};
    }
  }

  /**
   * Return a string parsed for entry date event
   */
  private getDateDetail(): void {
    const start = moment(this.entry.start_date);
    const end = moment(this.entry.end_date);

    this.entryHtml.date  = ':iniDateh - :endDateh'.replace(':iniDate', start.format('DD/MM/YYYY HH:mm'));

    if (start.isSame(end, 'day')) {
      this.entryHtml.date = this.entryHtml.date.replace(':endDate', end.format('HH:mm'));

    } else {
      this.entryHtml.date = this.entryHtml.date.replace(':endDate', end.format('DD/MM/YYYY HH:mm'));
    }
  }


  /**
   * Get the location name to render
   */
  private getLocation(): void {
    this.entryHtml.location = this.entry.getLocationName();
  }

  /**
   * Get the description to render
   */
  private getDescription(): void {
    this.entryHtml.description = _.isNil(this.entry.description) || this.entry.description === ''
      ? ''
      : this.entry.description;
  }

  /**
   * Get the conference link to render
   */
  private getConference(): void {
    this.entryHtml.conference = !this.entry.conference_allocated
      ? null
      : !this.hasLinkConference()
        ? 'El acceso no está disponible aún'
        : this.entry.url_conference;
  }

  /**
   * Return the guest list filtered
   */
  private getGuest(): void {
    this.entryHtml.guests = this.getFilteredGuest(this.entry);
  }


  /**
   * Set entry info in dom params to render
   */
  private setEntryDomInfo(): void {
    this.getDateBadge();
    this.getDateDetail();
    this.getLocation();
    this.getDescription();
    this.getConference();
    this.getGuest();
  }

  /**
   * Return if exist a link conference
   * @returns {boolean}
   */
  public hasLinkConference(): boolean {
    return !_.isNil(this.entry.url_conference) && this.entry.url_conference !== '';
  }

  /************************ DOM EVENT METHODS ************************/

}
