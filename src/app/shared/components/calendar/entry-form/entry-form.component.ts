import {Component, OnChanges, OnInit, Input, SimpleChanges, ViewChild, Injector} from '@angular/core';
import {NgModel, ValidationErrors} from '@angular/forms';

import {DateLocalPipe} from '../../../pipes/dateLocal.pipe';
import {DATEPICKER} from '../../../const/system.const';

import {EntryClass} from '../entry-common/entry.class';
import {PersonModel} from '../../../models/person.model';
import {CalendarEntryModel} from '../../../models/calendar/entry.model';
import {LoaderService} from '../../../services/loader.service';



const _ = require('lodash');
const moment = require('moment');

@Component({
  selector: 'app-entry-form',
  templateUrl: './entry-form.component.html',
  styleUrls: ['./entry-form.component.css']
})
export class EntryFormComponent extends EntryClass implements OnInit, OnChanges {


  @Input() public updateMode: boolean;

  @ViewChild('entryTitle') entryTitleInput;
  @ViewChild('entryDescription') entryDescriptionInput;
  @ViewChild('entryUrlConference') entryUrlConference;
  @ViewChild('startDateInput') startDateInput;
  @ViewChild('endDateInput') endDateInput;
  @ViewChild('searchLocationInput') searchLocationInput;
  @ViewChild('searchGuestInput') searchGuestInput;


  public spanishCalendar = DATEPICKER.ESP;

  public entryEditable: CalendarEntryModel;
  public startDate: Date;
  public endDate: Date;

  private _updateFirstTry = false;
  private _guestQuery = {
    add: [],
    del: []
  };

  /*********************** COMPONENT SETTING ***************************/
  public searchLocationList = {
    data: [],
    restart: 0
  };

  public searchGuestList = {
    data: [],
    dataFiltered: [],
    restart: 0
  };

  public searchLocationSetting = {
    title: 'name',
    keyToFilter: 'name',
    flags: {
      dataOnLive: true,
      showResultList: true,
      showResultListUnderClick: true,
      showResultListUnderUnselect: true,
      forcedUnselectIconShow: true
    },
    error: {
      show: false
    },
    icon: {
      hasIconRight: true,
      hideIconOnSearch: true,
      showUnselectIcon: true
    },
    input: {
      placeholder: 'ej. Steelmood Huelva'
    },
    css: {
      extraClasses: 'form-style'
    }
  };


  public searchGuestSetting = {
    title: 'fulldata',
    keyToFilter: 'name',
    alternativeKeysToFilter: ['email', 'surname'],
    flags: {
      showResultList: true,
      showResultListUnderClick: true,
      showResultListUnderUnselect: false,
      forcedUnselectIconShow: true
    },
    error: {
      show: false
    },
    icon: {
      hasIconRight: true,
      hasSearchIcon: false,
      hideIconOnSearch: true,
      showUnselectIcon: true
    },
    input: {
      placeholder: 'busca por nombre, apellido, email'
    },
    css: {
      extraClasses: 'form-style'
    }
  };


  /**
   * Generate search keys for guest
   * @param {Object} guest
   * @returns {Object}
   */
  private static createGuestsBySearchKeys (guest: PersonModel | object): object {
    const gPerson = guest instanceof PersonModel ? guest : new PersonModel(guest);

    return {
      fulldata: gPerson.getNameFirstSurname() + ', ' + guest['email']
    };
  }

  constructor(private _datePipe: DateLocalPipe,
              private _loaderSrv: LoaderService,
              injector: Injector) {
    super('entryForm', injector);
    this.getLocationsFromApi();
    this.getGuestsFromApi();
  }

  /************************ NATIVE METHODS ************************/
  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!_.isNil(changes['entry']) && !_.isUndefined(changes['entry']['currentValue'])) {
      this.entryEditable = new CalendarEntryModel(this.entry);
      this.entryEditable.location_name = this.entry.getLocationName();
      this.initializeDatesInputs();

      this.isAvailableActions = this.isEntryOwner();
    }

    if (!_.isNil(changes['updateMode']) && !_.isUndefined(changes['updateMode']['currentValue'])) {
      this._updateFirstTry = false;

      if (!_.isNil(this.entryTitleInput)) {
        this.entryTitleInput.reset();
      }

      if (!_.isNil(this.entryDescriptionInput)) {
        this.entryDescriptionInput.reset();
      }

      if (!_.isNil(this.entryUrlConference)) {
        this.entryUrlConference.reset();
      }
    }
  }

  /************************ MODEL METHODS ************************/

  /**
   * Set dates for entry form. Require the Input entry
   */
  private initializeDatesInputs(): void {

    if (!_.isNil(this.entry.start_date)) {
      this.startDate = new Date(this.entry.start_date);

      if (this.updateMode) {
        this.endDate = new Date(this.entry.end_date);

      } else {
        this.endDate = new Date(this.entry.start_date);

        if (moment(this.entry.start_date).creationData().format.includes('HH:mm')) {
          this.endDate.setHours(this.endDate.getHours() + 1);

        } else {
          this.startDate.setHours(0);
          this.endDate.setHours(23);
          this.endDate.setMinutes(59);
        }
      }
    }
  }


  /**
   * Update the add and delete guest into internal array for next relationship updating
   * @param {PersonModel} person
   * @param {boolean} added
   */
  private updateGuestQuery(person: PersonModel, added: boolean = true): void {
    const addIdx = this.findPersonIndex(person, this._guestQuery.add),
          delIdx = this.findPersonIndex(person, this._guestQuery.del),
          existIdx = this.findPersonIndex(person, this.entry.guests);

    if (added) {
      if (addIdx === -1 && existIdx === -1) {
        this._guestQuery.add.push(person);
      }

      if (delIdx !== -1) {
        this._guestQuery.del.splice(delIdx, 1);
      }

    } else {
      if (delIdx === -1 && existIdx !== -1) {
        this._guestQuery.del.push(person);
      }

      if (addIdx !== -1) {
        this._guestQuery.add.splice(addIdx, 1);
      }
    }
  }

  /**
   * Add a new guest
   * @param {PersonModel} person
   * @returns {boolean}
   */
  private addGuestToEntry(person: PersonModel): boolean {
    if (this.findPersonIndex(person, this.getFilteredGuest(this.entryEditable)) === -1) {
      this.entryEditable.guests.push(person);
      this.searchGuestList.dataFiltered = this.getSearchGuestListFiltered();
      this.updateGuestQuery(person);

      return true;
    }

    return false;
  }

  /**
   * Delete a guest
   * @param {PersonModel} person
   * @returns {boolean}
   */
  private deleteGuestToEntry(person: PersonModel): boolean {
    const idx = this.findPersonIndex(person, this.entryEditable.guests);

    if (idx !== -1) {
      this.entryEditable.guests.splice(idx, 1);
      this.searchGuestList.dataFiltered = this.getSearchGuestListFiltered();
      this.updateGuestQuery(person, false);

      return true;
    }

    return false;
  }


  /************************ DOM METHODS ************************/

  /**
   *
   * @returns {string}
   */
  public getButtonTitle(): string {
    return this.updateMode
      ? 'Actualizar'
      : 'Crear';
  }


  /**
   * Return if guest input is highlight or not
   * @returns {boolean}
   */
  public thereMustBeGuests(): boolean {
    return !this._updateFirstTry && this.entryEditable.conference_allocated && this.getFilteredGuest(this.entryEditable).length === 0;
  }


  /**
   * REturn a label switch input
   * @returns {string}
   */
  public getConferenceSwitchLabel(): string {
    return this.entryEditable.conference_allocated
      ? 'Si'
      : 'No';
  }


  /**
   * Return a person list filtered without actual guests
   * @returns {Array<Object>}
   */
  private getSearchGuestListFiltered(): Array<object> {
    return _.filter(this.searchGuestList.data, (guest) => {
      return guest.id !== this._authSrv.getPersonId() && _.findIndex(this.entryEditable.guests, (p: PersonModel) => {
        return p.id === guest['id'];
      }) === -1;
    });
  }

  /**
   * Return if an input has errors
   * @param input
   * @returns {boolean}
   */
  public hasErrorInput(input: NgModel): boolean {
    if (_.isNil(input)) {
      return false;
    }

    return this._updateFirstTry
      ? !_.isNull(input.errors)
      : !_.isNull(input.errors) && (input.dirty || input.touched);
  }


  /**
   * Returns if a datepicker has error
   * @returns {boolean}
   */
  public hasErrorDatePicker(): boolean {
    return this._updateFirstTry && moment(this.endDate).isBefore(this.startDate);
  }

  /**
   * Return if guest input has error
   * @returns {boolean}
   */
  public hasErrorGuests(): boolean {
    return this._updateFirstTry && this.entryEditable.conference_allocated && this.getFilteredGuest(this.entryEditable).length === 0;
  }

  /**
   * Return if the form has any error
   * @returns {boolean}
   */
  public hasFormError(): boolean {
    return this._updateFirstTry
      && ( this.hasErrorInput(this.entryTitleInput)
        || this.hasErrorInput(this.entryDescriptionInput)
        || this.hasErrorInput(this.entryUrlConference)
        || this.hasErrorDatePicker()
        || this.hasErrorGuests());
  }

  /**
   * Return an error css class if calendar is invalid
   * @returns {string}
   */
  public getClassCalendar(): string {
    return this._updateFirstTry && this.hasErrorDatePicker()
      ? 'invalid-field full-width'
      : 'full-width';
  }

  /**
   * Get css error class
   * @param input
   * @returns {string}
   */
  public getClassSearchLocationError(input: any): string {
    return this._updateFirstTry && this.hasErrorInput(input)
      ? 'invalid-field'
      : '';
  }

  /**
   * Get css error class
   * @returns {string}
   */
  public getClassSearchGuest(): string {
    return this.thereMustBeGuests()
      ? 'blue-highlight'
      : this.hasErrorGuests()
        ? 'invalid-field invalid-highlight'
        : '';
  }

  /**
   * REturn an error input
   * @param {NgModel} input
   * @returns {string}
   */
  public getErrorInput(input: NgModel): string {
    if (this.hasErrorInput(input)) {
      if (input.getError('required')) {
        return 'Campo requerido';
      }

      if (input.getError('minlength')) {
        return 'Mínimo ' + input.errors.minlength.requiredLength + ' caracteres';
      }

      if (input.getError('maxlength')) {
        return 'Máximo ' + input.errors.maxlength.requiredLength + ' caracteres';
      }
    }

    return '';
  }

  /**
   * Uninvited a determinated guest
   * @param {PersonModel} person
   */
  public unInviteGuest(person: PersonModel): void {
    this.deleteGuestToEntry(person);
  }
  /************************ DOM EVENT METHODS ************************/

  public onClickFormSubmit(event): void {
    event.preventDefault();
    event.stopPropagation();

    this._updateFirstTry = true;

    if (!this.hasFormError()) {

      if (this.entry.isEqual(this.entryEditable)) {
        this._alertSrv.warn('Calendario: no hubo cambios que actualizar');
        this.emitOutputEvent('backToView');

      } else {
        this.doSomethingWithEntry();
      }
    }
  }




  /**
   * On click over search location result list
   * @param {Object | any} event
   */
  public onClickSearchLocation(event: object | any): void {
    if (event['event'] === 'keyup' || event['event'] === 'focusout' || event['event'] === 'blur') {
      this.entryEditable.location_name = event['currentText'];
      this.entryEditable.location = null;

    } else if (event['event'] === 'unselect') {
      this.entryEditable.location_name = '';
      this.entryEditable.location = null;
      this.searchLocationList.restart++;
    }
  }

  /**
   * On click over search guest result list
   * @param {Object | any} event
   */
  public onClickSearchGuest(event: object | any): void {
    if (event['event'] === 'keyup' || event['event'] === 'focusout' || event['event'] === 'blur') {

    } else if (event['event'] === 'unselect') {
      this.searchGuestList.restart++;
    }
  }


  /**
   * On click location option. Double data bindin will update the searchs values default
   * @param {Object | any} event
   */
  public onSelectedSearchLocation(event: object | any): void {
    if (event && event.hasOwnProperty('element')) {
      this.entryEditable.location = event['element'];
      this.entryEditable.location_name = this.entryEditable.location.name;
    }
  }

  /**
   * On click guest option. Double data bindin will update the searchs values default
   * @param {Object | any} event
   */
  public onSelectedSearchGuest(event: object | any): void {
    if (event && event.hasOwnProperty('element')) {
      this.addGuestToEntry(new PersonModel(event['element']));
      this.searchGuestList.restart++;
    }
  }


  /**
   * On update startDate model
   */
  public updateStartDate(): void {
    this.entryEditable.start_date = this._datePipe.datetimeToISO(this.startDate.toUTCString());

  }


  /**
   * On update endDate model
   */
  public updateEndDate(): void {
    this.entryEditable.end_date = this._datePipe.datetimeToISO(this.endDate.toUTCString());
  }
  /************************ API METHODS ************************/


  private getLocationsFromApi(): void {
    this._httpSrv.get(
      'locations/',
      {
        page: 1,
        elements: 2000,
        orderBy: 'name',
        orderType: 'asc'
      },
      undefined,
      false,
      false
    ).subscribe(
      (response) => {
        if ( response && response.hasOwnProperty('success') && response['success']
          && response.hasOwnProperty('object') && response['object']
          && response['object'].hasOwnProperty('items') && _.isArray(response['object']['items'])) {
          this.searchLocationList.data = _.cloneDeep(response['object']['items']);

        } else {
          this.searchGuestList.data = [];
        }
      }
    );
  }


  /**
   * Return a list of admins cancidates to manage domain
   */
  private getGuestsFromApi(): void {
    this._httpSrv.get(
      'domains/persons/',
      {
        page: 1,
        elements: 2000
      },
      undefined,
      false,
      false
    ).subscribe(
      (response) => {
        if ( response && response.hasOwnProperty('success') && response['success']
          && response.hasOwnProperty('object') && response['object']
          && response['object'].hasOwnProperty('items') && _.isArray(response['object']['items'])) {
          this.searchGuestList.data = _.cloneDeep(
            response['object']['items'].map((item) => {
                return _.assign(item, EntryFormComponent.createGuestsBySearchKeys(item));
              }
            )
          );
          this.searchGuestList.dataFiltered = this.getSearchGuestListFiltered();

        } else {
          this.searchGuestList.data = [];
          this.searchGuestList.dataFiltered = [];
        }
      }
    );
  }


  /**
   * Return a API object format to send
   * @returns {Object}
   */
  private generateEntryApiObject(): object {
    return {
      title: this.entryEditable.title,
      description: this.entryEditable.description,
      start_date: this._datePipe.datetimeToISO(this.startDate.toUTCString()),
      end_date: this._datePipe.datetimeToISO(this.endDate.toUTCString()),
      url_conference: this.entryEditable.url_conference,
      conference_allocated: this.entryEditable.conference_allocated,
      location_name: !this.entryEditable.hasLocationModel() ? this.entryEditable.getLocationName() : null,
      location_id: this.entryEditable.hasLocationModel() ? this.entryEditable.location.id : null
    };
  }

  /**
   * Return a API guestobject format to send
   * @returns {Object}
   */
  private generateEntryGuestApiObject(guestList: Array<PersonModel>): object {
    const guestObjectList = [];

    _.forEach(guestList, (guest: PersonModel) => {
      guestObjectList.push({id: guest.id});
    });

    return guestObjectList;
  }

  /**
   * Api call actions: update o create
   */
  private doSomethingWithEntry(): void {

    if (this.updateMode) {
      this._loaderSrv.softUpdating();

      this._httpSrv
        .put(
          'calendarentries/' + this.entryEditable.id,
          this.generateEntryApiObject(),
          undefined,
          false,
          false
        )
        .subscribe(
          response => this.onSuccessDoSomethingWithEntry(response),
          fail => this.onFailDoSomethingWithEntry(fail)
        );

    } else {
      this._loaderSrv.softSaving();

      this._httpSrv
        .post(
          'calendarentries/',
          this.generateEntryApiObject(),
          undefined,
          false,
          false
        )
        .subscribe(
          response => this.onSuccessDoSomethingWithEntry(response),
          fail => this.onFailDoSomethingWithEntry(fail)
        );
    }
  }



  /**
   * Api call actions: add or delete guests
   */
  private doSomethingWithEntryGuests(add: boolean = true): void {
    if (add) {
      this._httpSrv
        .post(
          'calendarentries/' + this.entryEditable.id + '/relationships/guests/',
          {guests: this.generateEntryGuestApiObject(this._guestQuery.add)},
          undefined,
          false,
          false
        )
        .subscribe(
          (response) => {
            this._guestQuery.add = [];
            this.onSuccessDoSomethingWithEntry(response);
          },
          fail => this.onFailDoSomethingWithEntry(fail)
        );

    } else {
      this._httpSrv
        .delete(
          'calendarentries/' + this.entryEditable.id + '/relationships/guests/' + this._guestQuery.del[0]['id'],
          undefined,
          undefined,
          false,
          false
        )
        .subscribe(
          (response) => {
            this._guestQuery.del.splice(0, 1);
            this.onSuccessDoSomethingWithEntry(response);
          },
          fail => this.onFailDoSomethingWithEntry(fail)
        );
    }
  }


  /**
   * On success api response
   * @param response
   */
  private onSuccessDoSomethingWithEntry(response): void {
    if (response['success'] && response.hasOwnProperty('object') && !_.isNil(response['object'])) {
      if (this.entryEditable.id === 0) {
        this.entryEditable.id = response['object']['id'];
      }

      if (this._guestQuery.add.length !== 0 || this._guestQuery.del.length !== 0) {
        this.doSomethingWithEntryGuests(this._guestQuery.add.length !== 0);

      } else {
        this._loaderSrv.stopSoft();

        this.emitOutputEvent(
          this.updateMode ? 'updated' : 'created',
          {entry: new CalendarEntryModel(response['object'])}
        );
      }
    }
  }

  /**
   * On fail response api call
   * @param response
   */
  private onFailDoSomethingWithEntry(response): void {
    this._loaderSrv.stopSoft();

    if (response.hasOwnProperty('message')) {
      this._alertSrv.error(response['message']);
    }
  }


  /************************ EXTERNAL METHODS ************************/
  /**
   * Forced datetime dates
   */
  public updateForm(): void {
    this.initializeDatesInputs();
  }


  /**
   * Return if exist changes in data entry
   * @returns {boolean}
   */
  public hasChangesInEntry(): boolean {
    return !this.entry.isEqual(this.entryEditable);
  }
}
