/*GENERAL ASSETS*/
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';

import {DatePipe} from '@angular/common';
import {ColorGeneratorPipe} from '../pipes/colorGenerator.pipe';
import {DateLocalPipe} from '../pipes/dateLocal.pipe';

/*CUSTOM COMPONENTS*/
import {ButtonComponent} from './button/button.component';
import {SearchComponent} from './search/search.component';
import {FilterComponent} from './filter/filter.component';
import {ConfirmDialogComponent} from './confirm-dialog/confirm-dialog.component';
import {InputValidatorComponent} from './input-validator/input-validator.component';

import {WidgetContainerComponent} from './widget-container/widget-container.component';
import {WidgetBaseComponent} from './widget-container/widget-base/widget-base.component';
import {WidgetTypeBComponent} from './widget-container/widget-type-b/widget-type-b.component';
import {WidgetTypeCComponent} from './widget-container/widget-type-c/widget-type-c.component';
import {WidgetTypeDComponent} from './widget-container/widget-type-d/widget-type-d.component';

import {DndModule} from 'ng2-dnd';
import {DragulaModule} from 'ng2-dragula';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {DataTableModule, CalendarModule, DropdownModule, CheckboxModule, ScheduleModule,
  TooltipModule, OverlayPanelModule, ListboxModule, TabViewModule, SelectButtonModule,
  ChartModule, InputSwitchModule, MultiSelectModule
} from 'primeng/primeng';

import {GeneralDomainWidgetContainerComponent} from './general-domain-widget-container/general-domain-widget-container.component';

import {ScopeNotesBoardComponent} from './scope-notes/scope-notes-board/scope-notes-board.component';
import {ScopeNotesListComponent} from './scope-notes/scope-notes-list/scope-notes-list.component';
import {ScopeNotesFormComponent} from './scope-notes/scope-notes-form/scope-notes-form.component';
import {TeamWidgetComponent} from './general-domain-widget-container/team-widget/team-widget.component';
import {TeamGestorWidgetComponent} from './general-domain-widget-container/team-gestor-widget/team-gestor-widget.component';
import {PersonWidgetComponent} from './general-domain-widget-container/person-widget/person-widget.component';
import {TreeTeamGroupComponent} from './tree-team-group/tree-team-group.component';
import {DefaultTreeTeamGroupComponent} from './tree-team-group/default-tree-team-group/default-tree-team-group.component';
import {FavouriteTreeTeamGroupComponent} from './tree-team-group/favourite-tree-team-group/favourite-tree-team-group.component';
import {EditFavouriteTreeTeamGroupComponent} from './tree-team-group/edit-favourite-tree-team-group/edit-favourite-tree-team-group.component';
import {StatusPeriodsComponent} from './status-periods/status-periods.component';

// Calendar
import {CalendarComponent} from './calendar/calendar.component';
import {EntryFormComponent} from './calendar/entry-form/entry-form.component';
import {EntryDetailComponent} from './calendar/entry-detail/entry-detail.component';

// Datepicker
import {DpDatePickerModule} from 'ng2-date-picker';
import {MonthRangeDatePickerComponent} from './month-range-datepicker/month-range-datepicker.component';
import {DayRangeDatePickerComponent} from './day-range-datepicker/day-range-datepicker.component';
import { TableMenuComponent } from './table-menu/table-menu.component';

@NgModule({
  imports: [
    DpDatePickerModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    DndModule.forRoot(),
    DragulaModule,
    BrowserAnimationsModule,
    DataTableModule,
    CalendarModule,
    DropdownModule,
    CheckboxModule,
    ScheduleModule,
    TooltipModule,
    OverlayPanelModule,
    ListboxModule,
    TabViewModule,
    SelectButtonModule,
    ChartModule,
    InputSwitchModule,
    MultiSelectModule
    // ScopeNotesComponentsModule
  ],
  exports: [
    DpDatePickerModule,
    ButtonComponent,
    SearchComponent,
    DndModule,
    DragulaModule,
    TreeTeamGroupComponent,
    FilterComponent,
    DataTableModule,
    CalendarModule,
    DropdownModule,
    CheckboxModule,
    ConfirmDialogComponent,
    InputValidatorComponent,
    TooltipModule,
    OverlayPanelModule,
    ListboxModule,
    TabViewModule,
    SelectButtonModule,
    ChartModule,
    InputSwitchModule,
    WidgetContainerComponent,
    ScopeNotesBoardComponent,
    ScopeNotesListComponent,
    ScopeNotesFormComponent,
    GeneralDomainWidgetContainerComponent,
    MonthRangeDatePickerComponent,
    DayRangeDatePickerComponent,
    StatusPeriodsComponent,
    CalendarComponent,
    ScheduleModule,
    MultiSelectModule,
    TableMenuComponent
  ],
  declarations: [
    ButtonComponent,
    SearchComponent,
    FilterComponent,
    ConfirmDialogComponent,
    InputValidatorComponent,
    WidgetContainerComponent,
    WidgetBaseComponent,
    ScopeNotesBoardComponent,
    ScopeNotesListComponent,
    ScopeNotesFormComponent,
    WidgetTypeBComponent,
    WidgetTypeCComponent,
    WidgetTypeDComponent,
    GeneralDomainWidgetContainerComponent,
    TeamWidgetComponent,
    TeamGestorWidgetComponent,
    PersonWidgetComponent,
    TreeTeamGroupComponent,
    DefaultTreeTeamGroupComponent,
    FavouriteTreeTeamGroupComponent,
    EditFavouriteTreeTeamGroupComponent,
    StatusPeriodsComponent,
    MonthRangeDatePickerComponent,
    DayRangeDatePickerComponent,
    CalendarComponent,
    EntryFormComponent,
    EntryDetailComponent,
    TableMenuComponent,
  ],
  providers: [
    DatePipe,
    ColorGeneratorPipe,
    DateLocalPipe
  ]
})

export class ComponentsModule {
}
