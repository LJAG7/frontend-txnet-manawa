import {
  Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ViewChild, ElementRef
} from '@angular/core';

const _ = require('lodash');

@Component({
  selector: 'app-confirm-dialog-component',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})

/**
 * @author Elias Romero <elias.romero@steelmood.com>
 * @author Miguel Rico <miguel.rico@steelmood.com>
 */
export class ConfirmDialogComponent implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() public externalConfig: object;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() public outputEvent = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('confirmDialog') public confirmDialog: ElementRef;

  /************************ COMPONENT VARS  ************************/

  private _visible = false;
  private _params = null;
  private _alternativeHeader = null;
  private _alternativeMessage = null;

  /************************ CONFIG VARS  ************************/

  private _config = {
    id: 'confirm_dialog_component_n'.concat(Math.floor(Math.random() * 100000).toString()),
    header: 'Confirmación',
    message: 'Estas seguro de continuar con esta acción?',
    accept: {
      show: true,
      action: null,
      button: {
        text: 'Si',
        classes: {
          main: 'btn-secondary',
          extra: 'btn-regular'
        }
      }
    },
    reject: {
      show: true,
      action: null,
      button: {
        text: 'Cancelar',
        classes: {
          main: 'secondary',
          extra: 'btn-regular'
        }
      }
    },
    css: {
      extraClasses: ''
    }
  };

  /************************ NATIVE METHODS  ************************/

  constructor() { }

  ngOnInit() {
    _.merge(this._config, this.externalConfig);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!_.isNil(changes['externalConfig'])) {
      _.merge(this._config, this.externalConfig);
    }
  }

  /************************ SETUP METHODS ************************/

  public getId(): string {
    return this._config.id;
  }
  public getCss(): string {
    return this._config.css.extraClasses;
  }

  public getHeader(): string {
    return _.isEmpty(this._alternativeHeader)
      ? this._config.header
      : this._alternativeHeader;
  }

  public getMessage(): string {
    return _.isEmpty(this._alternativeMessage)
      ? this._config.message
      : this._alternativeMessage;
  }

  public getButtonClasses(buttonKey: string): string {
    return !_.isEmpty(this._config[buttonKey].button.classes)
      ? this._config[buttonKey].button.classes.main + ' ' + this._config[buttonKey].button.classes.extra
      : '';
  }

  public getButtonTtext(buttonKey: string): string {
    return this._config[buttonKey].button.text;
  }

  public getShowButton(buttonKey: string): boolean {
    return this._config[buttonKey].show;
  }

  public isVisible(): boolean {
    return this._visible;
  }

  /************************ EVENTS METHODS ************************/

  public onClick(action: boolean): void {
    const key = action ? 'accept' : 'reject';
    this.hideDialog();
    this.outputEvent.emit(
      {
        event: key,
        callBack: this._config[key]['action'],
        params: this._params
      }
    );
  }

  /**
   * Display the dialog component. Accept params for event call and optional header
   * @param {Object | any} params
   * @param {string} header
   * @param {string} message
   */
  public showDialog(params: object | any = null,
                    header: string = null,
                    message: string = null): void {
    this._visible = true;
    this._params = params;

    if (!_.isEmpty(header)) {
      this._alternativeHeader = header;
    }

    if (!_.isEmpty(message)) {
      this._alternativeMessage = message;
    }

    setTimeout(() => {
        this.confirmDialog.nativeElement.classList.add('fade-in');
        this.doSomethingWithHiddeClassTimer(false);
      }
      , 1);
  }

  public hideDialog(): void {

    this._alternativeHeader = null;
    this._alternativeMessage = null;
    this.confirmDialog.nativeElement.classList.remove('fade-in');
    setTimeout(() => {
      this._visible = false;
      this.doSomethingWithHiddeClassTimer();
    }, 300);
  }

  /************************ AUXILIAR METHODS ************************/

  private doSomethingWithHiddeClassTimer(add: boolean = true): void {
    setTimeout(() => {
      add
        ? this.confirmDialog.nativeElement.classList.add('hidden-dialog')
        : this.confirmDialog.nativeElement.classList.remove('hidden-dialog');
    }, 300);
  }
}
