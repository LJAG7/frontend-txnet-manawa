import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';

import {BaseComponentModel} from '../../models/base-component/base-component.class';
import {DATEPICKER} from '../../const/system.const';

import {StatusPeriodsRangeFilterModel} from '../../models/status-periods/status-periods-range-filter.model';

const moment = require('moment');
const _ = require('lodash');

@Component({
  selector: 'app-day-range-datepicker-component',
  templateUrl: './day-range-datepicker.component.html',
  styleUrls: ['./day-range-datepicker.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class DayRangeDatePickerComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() mode = 'init';
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() onChangeFilter = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('dayRangeDatepicker') dayRangeDatepicker;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() onChangeRangeEvent = new EventEmitter();

  /************************ COMPONENT VARS  ************************/
  private _actualDate = new Date();
  public isOpen = false;
  public rangeDates: Date[];

  /************************ COMPONENT CONFIG  ************************/

  public datePicker = {
    calendar: DATEPICKER.ESP,
    range: '1920:' + this._actualDate.getFullYear(),
    minDate: new Date(1920, 0, 1),
    maxDate: this._actualDate
  };

  /************************ NATIVE METHODS  ************************/

  constructor() {
    super();
  }

  ngOnInit() {
    this.config.id = 'day_range_datepicker_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    this.emitOnChangeRangeEvent(null, null,true);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('visible') && !_.isNil(changes.visible.currentValue)) {
      this.onUnselectDays();
    }
  }

  /************************ EVENTS METHODS ************************/

  /**
   * On change range
   * @param event
   */
  public onChangeRange() {
    if (!_.isNil(this.rangeDates[0]) && _.isNil(this.rangeDates[1])) {
      this.emitOnChangeRangeEvent(this.rangeDates[0], this.rangeDates[0], false, false);
    } else if (this.rangeDates.length === 2) {
      if (!_.isNil(this.rangeDates[0])
        && !_.isNil(this.rangeDates[1])) {
        this.emitOnChangeRangeEvent(this.rangeDates[0], this.rangeDates[1]);
      } else {
        this.emitOnChangeRangeEvent(null, null, true);
      }
    } else {
      this.emitOnChangeRangeEvent(null, null, true);
    }
  }

  /**
   * On close calendar
   */
  public onCloseCalendar(): void {
    this.isOpen = false;
  }

  /**
   * On open calendar
   */
  public onOpenCalendar(): void {
    this.isOpen = true;
  }

  /**
   * On unselect months
   */
  public onUnselectDays(): void {
    this.rangeDates = null;
    this.emitOnChangeRangeEvent(null, null, true);
  }

  /**
   * Check if show unselect days button
   * @returns {boolean}
   */
  public showUnselectDaysButton(): boolean {
    return !_.isNil(this.rangeDates) && !_.isEmpty(this.rangeDates);
  }

  /************************ AUXILIAR METHODS  ************************/

  /**
   * On change range
   * @param {boolean} reset
   */
  private emitOnChangeRangeEvent(dateInit: Date, dateEnd: Date, reset: boolean = false, closeCalendar: boolean = true) {
    let initDateMoment = null;
    let endDateMoment = null;

    if (!reset) {
      initDateMoment = moment(dateInit);
      initDateMoment = moment(initDateMoment.startOf('day'));

      endDateMoment = moment(dateEnd);
      endDateMoment = moment(endDateMoment.endOf('day'));
    }

    this.onChangeRangeEvent.emit(new StatusPeriodsRangeFilterModel({
      mode: this.mode,
      initDate: initDateMoment,
      endDate: endDateMoment,
      closeCalendar: closeCalendar
    }));
  }
}
