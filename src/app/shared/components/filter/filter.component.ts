import {
  Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter, ElementRef,
  QueryList, ViewChildren
} from '@angular/core';

import {HttpSrvService} from '../../services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-filter-component',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css'],
  providers: [HttpSrvService]
})

/**
 * @author Elias Romero <elias.romero@steelmood.com>
 * @author Miguel Rico <miguel.rico@steelmood.com>
 */
export class FilterComponent implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() externalConfig: object;
  @Input() selected = false;
  @Input() restart = false;
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() outputEvent = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChildren('filterPill') filterPill: QueryList<ElementRef>;

  /************************ COMPONENT VARS  ************************/

  private _expandedStatus = false;
  private _elementList: Array<any> = [];
  private _resultList: Array<any> = [];
  private _viewMaxHeight = 0;

  /************************ CONFIG VARS  ************************/

  private _config = {
    id: 'filter_component_n'.concat(Math.floor(Math.random() * 100000).toString()),
    endpoint: '',
    text: 'name',
    key: 'id',
    label: '', // Filtrar por...
    flags: {
      isMultiFilter: false,
      hasDefaultOption: true,
      showLabel: true
    }
  };

  private _defaultOption = {
    name: 'Todos',
    id: -1,
    selected: true
  };

  /************************ STATIC METHODS ************************/

  private static isValidObjectResponse(response: object | any): boolean {
    return response.hasOwnProperty('object')  && response['object']
      && response['object'].hasOwnProperty('items')  && response['object']['items'];
  }

  /************************ NATIVE METHODS ************************/

  constructor(private _httpSrv: HttpSrvService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (!_.isNil(changes['restart']) && !changes['restart']['firstChange']) {
      this.changeStatus(0, 'restart');
    }
    _.merge(this._config, this.externalConfig);
  }

  ngOnInit() {
    _.merge(this._config, this.externalConfig);
    this.getData();
  }

  /************************ SETUP METHODS ************************/

  /**
   * Get the single and unique identification for component instance
   * @returns {string}
   */
  public getComponentId(): string {
    return this.hasValidConfigurationInput() ? this._config['id'] : '';
  }

  /**
   * Check if must show label
   * @return {boolean}
   */
  public showLabel(): boolean {
    return this._config.flags.showLabel;
  }

  /************************ EVENTS METHODS ************************/

  /**
   * Change the current active/inactive status on component
   * @param {number} index
   * @param {string} status
   */
  public changeStatus(index: number, status: string = 'clicked'): void {
    if (_.isNil(this._elementList) || _.isNil(this._elementList[index]) || !this._elementList[index].hasOwnProperty('id')) {
     return;
    }


    if (this._elementList[index]['id'] === -1) {
      this.resetElementList();
      if (this.hasDefaultOption()) {
        this._elementList[0]['selected'] = true;
      }
      this._resultList = [];
    } else {

      if (!this.isMultiFilter()) {
        if (this._elementList[index]['selected'] === true) {
          this.resetElementList();
          this._elementList[0]['selected'] = true;
        } else {
          this.resetElementList();
          this._elementList[index]['selected'] = !this.getCurrentStatus(index);
        }
      } else if (this.hasDefaultOption()) {
        this._elementList[0]['selected'] = false;
        if (this._resultList.indexOf(this._defaultOption) !== -1) {
          this._resultList.pop();
        }
        this._elementList[index]['selected'] = !this.getCurrentStatus(index);
      }
    }

    if (this._elementList[index]['selected'] === true) {
      if (!this.isMultiFilter()) {
        this._resultList = [];
      }
      this._resultList.push(this._elementList[index]);
    } else {
      if (this._resultList.indexOf(this._elementList[index]) !== -1) {
        this._resultList.splice(this._resultList.indexOf(this._elementList[index]), 1);
      }
      if (this._resultList.length === 0) {
        this._elementList[0]['selected'] = true;
        this._resultList.push(this._defaultOption);
      }
    }

    this.emitOutputEvent(status);
  }

  /**
   * Get the current selected/unselected status on component from the DOM
   * @return {boolean}
   */
  public getCurrentStatus(index: number): boolean | any {
    return this._elementList[index]['selected'];
  }

  /**
   * Get element list
   * @returns {Array<any>}
   */
  public getElementList(): Array<any> {
    return this._elementList;
  }

  /**
   * Return the icon according status
   * @return {string|boolean|string}
   */
  public getIconAccordingStatus(index: number): string | any {
    return this._elementList[index]['selected']
      ? this.configHasUnselectIcon()
        ? this._config['icons']['deselect']
        : 'times'
      : this.configHasSelectIcon()
        ? this._config['icons']['select']
        : 'plus';
  }

  /**
   * Get label from widgetConfig
   * @return {boolean}
   */
  public getLabel(): string {
    return this._config.label;
  }

  /**
   * Get the text shown on pill
   * @return {any|string}
   */
  public getTextContent(index: number): string | any {
    return this._elementList[index][this._config.text];
  }

  /**
   * Get view max height
   * @return {boolean}
   */
  public getViewMaxHeight(): number {
    return this._viewMaxHeight;
  }

  /**
   * Check if the component has a valid configuration
   * @returns {boolean}
   */
  public hasValidConfigurationInput(): boolean {
    return !_.isNil(this._config)
      && _.size(this._config) > 0
      && this._config.hasOwnProperty('text')
      && this._config['text'].length > 0;
  }

  /**
   * Check if expanded
   * @returns {boolean}
   */
  public isExpanded(): boolean {
    return this._expandedStatus;
  }

  /**
   * Check if must show expand button
   * @return {boolean}
   */
  public showExpandButton(): boolean {
    return this.getViewMaxHeight() > 25;
  }

  /**
   * Toggle expanded status
   */
  public toggleExpandedStatus(): void {
    this._expandedStatus = !this._expandedStatus;
  }

  /************************ API METHODS ************************/

  /**
   * Get component data
   */
  private getData(): void {
    this._httpSrv
      .get(
        this._config.endpoint
      )
      .subscribe(
        (responseOk) => this.onSuccessGetData(responseOk)
      );
  }

  /**
   * On success get data
   * @param {Object | any} response
   */
  private onSuccessGetData(response: object | any): void {
    // this.hideSoftLoader();

    if (response['success'] && FilterComponent.isValidObjectResponse(response)) {
      this._elementList = response['object']['items'];
      if (this.hasDefaultOption()) {
        this._elementList.unshift(this.getDefaultOption());
      }

      setTimeout(() => {
        this.filterPill.toArray().forEach(element => {
          const rectParent = element.nativeElement.parentElement.getBoundingClientRect();
          const rectChild = element.nativeElement.getBoundingClientRect();
          this._viewMaxHeight = rectChild.bottom - rectParent.top;
        });
      }, 250);
    }
  }

  /************************ AUXILIAR METHODS ************************/

  /**
   * Check configuration to find if icons has been configured (deselect pill)
   * @return {boolean}
   */
  private configHasUnselectIcon(): boolean {
    return this._config.hasOwnProperty('icons')
      && this._config['icons'].hasOwnProperty('deselect')
      && this._config['icons']['deselect'].length > 0;
  }

  /**
   * Check configuration to find if icons has been configured (select pill)
   * @return {boolean}
   */
  private configHasSelectIcon(): boolean {
    return this._config.hasOwnProperty('icons')
      && this._config['icons'].hasOwnProperty('select')
      && this._config['icons']['select'].length > 0;
  }

  /**
   * Emit to the parent when a pill is un/selected
   * @param {string} event
   * @returns {EventEmitter<Object> | any}
   */
  private emitOutputEvent(event: string): EventEmitter<object> | any {
    this.outputEvent.emit({
      event: event,
      id: this.getComponentId(),
      elements: this._resultList,
      config: this._config
    });
  }

  /**
   * Get default option
   */
  private getDefaultOption(): object {
    return this._defaultOption;
  }

  /**
   * Check if component has default option
   * @returns {boolean}
   */
  private hasDefaultOption(): boolean {
    return this._config.flags.hasDefaultOption;
  }

  /**
   * Check if component is multi filter
   * @returns {boolean}
   */
  private isMultiFilter(): boolean {
    return this._config.flags.isMultiFilter;
  }

  /**
   * Unselect all element list
   */
  private resetElementList(): void {
    this._elementList.forEach(element => {
      element['selected'] = false;
    });
  }

}
