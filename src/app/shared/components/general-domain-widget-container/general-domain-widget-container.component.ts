import {Component, ElementRef, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild} from '@angular/core';

import {BaseComponentModel} from '../../../shared/models/base-component/base-component.class';
import {TeamWidgetComponent} from './team-widget/team-widget.component';
import {TeamGestorWidgetComponent} from './team-gestor-widget/team-gestor-widget.component';
import {PersonWidgetComponent} from './person-widget/person-widget.component';

import {HttpSrvService} from '../../services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-general-domain-widget-container-component',
  templateUrl: './general-domain-widget-container.component.html',
  styleUrls: ['./general-domain-widget-container.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class GeneralDomainWidgetContainerComponent extends BaseComponentModel implements OnInit, OnChanges, OnDestroy {

  @ViewChild('generalWidgetContainer') generalWidgetContainer: ElementRef;

  @ViewChild('teamGeneralWidget') private _teamGeneralWidget: TeamWidgetComponent;
  @ViewChild('personGeneralWidget') private _personGeneralWidget: PersonWidgetComponent;
  @ViewChild('teamGestorGeneralWidget') private _teamGestorGeneralWidget: TeamGestorWidgetComponent;

  /************************ COMPOENENT INPUTS  ************************/

  @Input() visible = true;

  public collapse = false;

  private _interval = {
    instance: undefined,
    cycle: 1000
  };

  /************************ NATIVE METHODS  ************************/

  constructor(private _httpSrv: HttpSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'general_domain_widget_container_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    this._interval.instance = setInterval(() => {
        this.listenerGeneralWidgets('GeneralDomainWidgetContainerComponent');
      }, this._interval.cycle
    );
  }

  ngOnChanges(changes: SimpleChanges): void { }

  ngOnDestroy() {
    if (!_.isNil(this._interval.instance)) {
      clearInterval(this._interval.instance);
    }
  }

  public toggleGeneralWidgets(): void {
    // case: to collapse (previous height calc)
    if (!this.collapse) {
      this.generalWidgetContainer.nativeElement.style.height =  this.generalWidgetContainer.nativeElement.offsetHeight + 'px';
    }

    this.collapse = !this.collapse;

    // case: to expand (ignore height calculated)
    if (!this.collapse) {
      this.generalWidgetContainer.nativeElement.style.height = '100%';
    }
  }

  private listenerGeneralWidgets(storageKey: string): void {
    if (localStorage.getItem(storageKey)) {
      switch (localStorage.getItem(storageKey)) {
        case 'team':
          this._teamGeneralWidget.resetWidget();
          this._teamGestorGeneralWidget.resetWidget();
          break;
        case 'person':
          this._personGeneralWidget.resetWidget();
          break;
        case 'gestor':
          this._teamGestorGeneralWidget.resetWidget();
          break;
      }

      localStorage.removeItem(storageKey);
    }
  }
}
