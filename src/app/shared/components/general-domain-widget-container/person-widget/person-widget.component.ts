import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

import {BaseComponentModel} from '../../../../shared/models/base-component/base-component.class';

import {HttpSrvService} from '../../../services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-person-widget-component',
  templateUrl: './person-widget.component.html',
  styleUrls: ['./person-widget.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class PersonWidgetComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT VARS  ************************/

  public total_mates = '###';
  public number_mates = '###';
  public number_mates_manager = '###';

  /************************ NATIVE METHODS  ************************/

  constructor(private _httpSrv: HttpSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'person_widget_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    this.getWidgetValues();
  }

  ngOnChanges(changes: SimpleChanges): void { }

  /************************ API METHODS  ************************/

  /**
   * Get widget values
   */
  private getWidgetValues(): void {
    this._httpSrv.get(
      'domains/totalMates/',
      { },
      'Widget general de personas',
      false,
      false
    ).subscribe(
      success => this.onSuccessGetWidgetValues(success)
    );
  }

  /**
   * On success get widget values
   * @param response
   */
  private onSuccessGetWidgetValues(response: any): void {
    if (response['success'] === true
      && !_.isNil(response.object)) {
      this.number_mates = (Number(response.object.number_mates) - 1).toString();
      this.number_mates_manager = response.object.number_mates_manager;
      this.total_mates = this.number_mates.toString();
    }
  }

  /**
   * Update widget by mandatory
   */
  public resetWidget() {
    this.getWidgetValues();
  }

}
