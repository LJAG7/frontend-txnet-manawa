import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

import {BaseComponentModel} from '../../../../shared/models/base-component/base-component.class';

import {HttpSrvService} from '../../../services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-team-gestor-widget-component',
  templateUrl: './team-gestor-widget.component.html',
  styleUrls: ['./team-gestor-widget.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TeamGestorWidgetComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT VARS  ************************/

  public number_unmanaged_teams = '###';

  /************************ NATIVE METHODS  ************************/

  constructor(private _httpSrv: HttpSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'team_gestor_widget_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    this.getWidgetValues();
  }

  ngOnChanges(changes: SimpleChanges): void { }

  /************************ API METHODS  ************************/

  /**
   * Get widget values
   */
  private getWidgetValues(): void {
    this._httpSrv.get(
      'domains/totalUnmanagedTeams/',
      { },
      'Widget general de personas',
      false,
      false
    ).subscribe(
      success => this.onSuccessGetWidgetValues(success)
    );
  }

  /**
   * On success get widget values
   * @param response
   */
  private onSuccessGetWidgetValues(response: any): void {
    if (response['success'] === true
      && !_.isNil(response.object)) {
      this.number_unmanaged_teams = response.object.number_unmanaged_teams;
    }
  }

  /**
   * Update widget by mandatory
   */
  public resetWidget() {
    this.getWidgetValues();
  }
}
