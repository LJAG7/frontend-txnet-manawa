import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

import {BaseComponentModel} from '../../../../shared/models/base-component/base-component.class';

import {HttpSrvService} from '../../../services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-team-widget-component',
  templateUrl: './team-widget.component.html',
  styleUrls: ['./team-widget.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TeamWidgetComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() visible = true;

  /************************ COMPONENT VARS  ************************/

  public number_teams =  '###';
  public number_teams_level_one = '###';

  /************************ NATIVE METHODS  ************************/

  constructor(private _httpSrv: HttpSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'team_widget_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    this.getWidgetValues();
  }

  ngOnChanges(changes: SimpleChanges): void { }

  /************************ API METHODS  ************************/

  /**
   * Get widget values
   */
  private getWidgetValues(): void {
    this._httpSrv.get(
      'domains/totalTeams/',
      { },
      'Widget general de personas',
      false,
      false
    ).subscribe(
      success => this.onSuccessGetWidgetValues(success)
    );
  }

  /**
   * On success get widget values
   * @param response
   */
  private onSuccessGetWidgetValues(response: any): void {
    if (response['success'] === true
      && !_.isNil(response.object)) {
      this.number_teams = response.object.number_teams;
      this.number_teams_level_one = response.object.number_teams_level_one;
    }
  }

  /**
   * Update widget by mandatory
   */
  public resetWidget() {
    this.getWidgetValues();
  }

}
