import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import {HttpSrvService} from '../../services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-input-validator',
  templateUrl: './input-validator.component.html',
  styleUrls: ['./input-validator.component.css'],
  providers: [HttpSrvService]
})

/**
 * @author Elias Romero <elias.romero@steelmood.com>
 * @author Miguel Rico <miguel.rico@steelmood.com>
 */
export class InputValidatorComponent implements OnInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() externalConfig: object;
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() outputEvent = new EventEmitter();
  @Output() errorEvent = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('validateInput') validateInput;

  /************************ COMPONENT VARS  ************************/

  // Static vars for static methods

  public hasError = InputValidatorComponent.hasError;
  public getValidationApiMessage = InputValidatorComponent.getValidationApiMessage;

  // Component vars

  private _timeout = 1000;

  /************************ CONFIG VARS  ************************/

  public config = {
    id: 'input_validator_component_n'.concat(Math.floor(Math.random() * 100000).toString()),
    field: {
      value: null,
      placeholder: '',
      capitalize: false,
      css: {
        empty: false
      },
      validations: {
        required: {
          active: false,
          value: true,
          message: ''
        },
        maxlength: {
          active: false,
          value: 255,
          message: ''
        },
        minlength: {
          active: false,
          value: 0,
          message: ''
        },
        pattern: {
          active: false,
          value: '',
          message: ''
        }
      },
      validationsApi: {
        url: null,
        message: null
      },
      checker: {
        timer: null,
        status: null
      },
      style: '',
      error: null
    }
  };

  /************************ STATIC METHODS ************************/

  /**
   * clear checker
   * @param {Object} field
   */
  private static clearCheckerStatus(field: object): void {
    field['checker']['status'] = null;
  }

  /**
   * Return the validation status object
   * @param {Object} field
   * @returns {any}
   */
  private static getValidationApiMessage(field: object): any {
    if (!InputValidatorComponent.isInitializeValidation(field)) {
      return '';
    }

    if (InputValidatorComponent.isValidatingInput(field)) {
      return '<i class="fa fa-spinner fa-pulse fa-fw"></i>';

    } else if (field['checker']['status']['valid']) {
      return '<i class="fa fa-check text-success"></i>';

    }

    return '<i class="fa fa-exclamation-triangle text-danger"></i>';
  }

  /**
   * Return if a input value is not allow by the API
   * @param {Object} field
   * @returns {boolean}
   */
  private static hasApiValidationError(field: object): boolean {
    return InputValidatorComponent.isInitializeValidation(field)
      && !InputValidatorComponent.isValidatingInput(field)
      && !field['checker']['status']['valid'];
  }

  /**
   * Return if an input has errors in object field
   * @param {Object} field
   * @returns {boolean}
   */
  private static hasError(field: object): boolean {
    return !_.isNull(field['error']);
  }

  /**
   * Return if an input has errors
   * @param input
   * @returns {boolean}
   */
  private static hasErrorInput(input: any): boolean {
    if (!_.isNil(input.errors) && (input.dirty || input.touched)) {
      return true;
    } else if (input.dirty || input.touched) {
      return input.value.trim() === '';
    }
  }

  /**
   * show the loading icon for a specific input
   * @param {Object} field
   */
  private static iniLoadingChecker(field: object): void {
    field['checker']['status'] = {checking: true};
  }

  /**
   * Return if a validation status is enabled
   * @param {Object} field
   * @returns {boolean}
   */
  private static isInitializeValidation(field: object): boolean {
    return field.hasOwnProperty('checker')
      && field['checker'].hasOwnProperty('status')
      && !_.isNull(field['checker']['status']);
  }

  /**
   * Return if a input value is pending to API response to receive the validation
   * @param {Object} field
   * @returns {boolean}
   */
  private static isValidatingInput(field: object): boolean {
    return !_.isNull(field['checker']['status'])
      && field['checker']['status'].hasOwnProperty('checking')
      && field['checker']['status']['checking'];
  }

  /************************ NATIVE METHODS ************************/

  constructor(private _httpSrv: HttpSrvService) { }

  ngOnInit() {
    _.merge(this.config, this.externalConfig);
  }

  /************************ SETUP METHODS ************************/

  /**
   * CHeck if must apply empty input css class
   * @returns {boolean}
   */
  public applyEmptyCss(): boolean {
    return this.config.field.css.empty && _.isNull(this.config.field.value);
  }

  /**
   * Get the single and unique identification for component instance
   * @returns {string}
   */
  public getComponentId(): string {
    return this.config.id;
  }

  /**
   * Get field from widgetConfig
   * @returns {string}
   */
  public getFieldFromConfig(): object {
    return this.config.field;
  }

  /**
   * Get field error from widgetConfig
   * @returns {string}
   */
  public getFieldErrorFromConfig(): object {
    return this.config.field.error;
  }

  /**
   * Return placeholder from widgetConfig
   * @returns {boolean}
   */
  public getPlaceHolder(): string {
    return !_.isNil(this.config.field.placeholder) ? this.config.field.placeholder : '';
  }

  /**
   * Return validation from widgetConfig
   * @param {string} name
   * @returns {string}
   */
  public getValidationStatus(name: string): any {
    for (const key of Object.keys(this.config.field.validations)) {
      if (key === name) {
        return this.config.field.validations[key].active
          ? this.config.field.validations[key].value.toString() : '';
      }
    }
    return '';
  }

  /**
   * Return required from widgetConfig
   * @returns {boolean}
   */
  public isRequired(): boolean {
    return this.config.field.validations.required.active
      ? Boolean(this.config.field.validations.required.value) : false;
  }

  /************************ VALIDATION METHODS ************************/

  /**
   * Validation for input. Generate the associated error if exist
   * @param {Object | any} event
   * @param {Object | any} input
   */
  public onLiveValidate(event: object | any,
                        input: object | any): void {
    event.stopPropagation();
    event.preventDefault();

    if (this.config.field['capitalize']) {
      this.config.field.value =  this.config.field.value.toLowerCase().replace(/\b\w/g, function(l) { return l.toUpperCase(); } );
    }

    if (Number(event['keyCode']) !== 9) {
      this.inputLiveValidate(this.config.field, input);
    }
  }

  /**
   * Return the message for input error
   * @param {Object} field
   * @param input
   * @returns {string}
   */
  public validateFieldMessage(field: object, input: object | any): string {

    for (const key of Object.keys(this.config.field.validations)) {
      if (this.config.field.validations[key].active
        && input.hasError(key)) {
        this.emitErrorEvent(this.config.field.validations[key].message);
        return this.config.field.validations[key].message;
      }
    }

    if (InputValidatorComponent.hasApiValidationError(field)) {
      this.emitErrorEvent(field['validationsApi']['message']);
      return field['validationsApi']['message'];
    } else {
      this.emitErrorEvent('Campo obligatorio');
      return 'Campo obligatorio';
    }
  }

  /************************ API METHODS ************************/

  /**
   * Api call to validate user data has unique in system
   * @param {Object} field
   * @param {Object | any} input
   */
  private validateFromApi(field: object,
                          input: object | any): void {
    InputValidatorComponent.iniLoadingChecker(field);
    this._httpSrv
      .get(
        field['validationsApi']['url'] + '?value=' + field['value']
      )
      .subscribe(
        success => this.onSuccessValidateFromApi(success, field, input),
        fail => this.onFailValidateFromApi(field, input)
      );
  }

  private onSuccessValidateFromApi(response: object,
                                   field: object,
                                   input: object | any): void {
    if (response.hasOwnProperty('object')
      && response['object'].hasOwnProperty('valid')) {
      field['checker']['status'] = response['object'];

      if (!response['object']['valid']) {
        field['error'] = this.validateFieldMessage(field, input);
      }

      this.emitOutputEvent();

    } else {
      InputValidatorComponent.clearCheckerStatus(field);
    }
  }

  private onFailValidateFromApi(field: object,
                                input: object | any): void {
    InputValidatorComponent.clearCheckerStatus(field);
    field['error'] = this.validateFieldMessage(field, input);
  }

  /************************ AUXILIAR METHODS ************************/

  /**
   * Apply style defined in field widgetConfig
   */
  private applyFieldStyle(): void {
    if (this.config.field.style === 'lowerCase' && !_.isNil(this.config.field.value)) {
      this.config.field.value = this.config.field.value.toLowerCase();
    }
  }

  /**
   * Emit current state of component to parent
   */
  private emitErrorEvent(message: string = 'ERROR'): void {
    this.errorEvent.emit({
      id: this.config.id,
      currentConfig: this.config,
      message: message,
      valid: false,
      value: this.config.field.value
    });
  }

  /**
   * Emit current state of component to parent
   */
  private emitOutputEvent(): void {
    this.outputEvent.emit({
      id: this.config.id,
      currentConfig: this.config,
      valid: true,
      value: this.config.field.value
    });
  }

  /**
   * Make the combo validation: native and apiCall
   * @param {Object} field
   * @param {Object | any} input
   */
  private inputLiveValidate(field: object,
                            input: object | any): void {

    this.applyFieldStyle();

    if (!InputValidatorComponent.hasErrorInput(input)) {
      field['error'] = null;
    }

    clearTimeout(field['checker']['timer']);
    InputValidatorComponent.clearCheckerStatus(field);

    field['checker']['timer'] = setTimeout(() => {
      if (field.hasOwnProperty('validationsApi')
        && !_.isNil(field['validationsApi']['url'])
        && !InputValidatorComponent.hasErrorInput(input)) {
        this.validateFromApi(field, input);
      } else if (!InputValidatorComponent.hasErrorInput(input)) {
        this.config.field.checker.status = {
          valid: true
        };
        this.emitOutputEvent();
      } else {
        field['error'] = this.validateFieldMessage(field, input);
      }
    }, this._timeout);
  }


  /************************ PUBLIC METHODS ************************/
  public restart(): void {
    this.config.field.value = null;
    this.config.field.error = null;
    this.config.field.checker.status = null;
    clearTimeout(this.config.field.checker.timer);
  }
}
