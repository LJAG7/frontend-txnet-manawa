import {Component, OnInit} from '@angular/core';
import {Loader, LoaderType, LoaderMessageType} from '../../models/loader';
import {LoaderService} from '../../services/loader.service';

const _ = require('lodash');

@Component({
  moduleId: module.id,
  selector: 'app-loader-component',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})

export class LoaderComponent implements OnInit {

  loader: Loader;

  constructor(private _loaderSrv: LoaderService) { }

  ngOnInit() {
    this._loaderSrv.getLoader().subscribe((loader: Loader) => {
       if (_.isNil(this.loader) || _.isNil(this.loader.type)) {
        this.loader = loader;
      } else if (loader.type === LoaderType.Denegation) {
        this.loader = loader;
      } else if (loader.type === LoaderType.General && this.loader.type !== LoaderType.Denegation) {
        this.loader = loader;
      } else {
        this.loader = loader;
      }
    });

    this._loaderSrv.getNeededFromApi().subscribe((neededFromApi: Array<string> = null) => {
      if (!_.isNil(neededFromApi)
        && !_.isNil(this._loaderSrv.getType())
        && this._loaderSrv.getType() === LoaderType.General
        && neededFromApi.length === 0) {
        this._loaderSrv.stopGeneral();
      }
    });
  }

  public show(): boolean {
    return this.isDenegation() || this.isGeneral() || this.isSoft();
  }

  public isDenegation(): boolean {
    return !_.isNil(this.loader) && !_.isEmpty(this.loader) && this.loader.type === LoaderType.Denegation;
  }

  public isGeneral(): boolean {
    return !_.isNil(this.loader) && !_.isEmpty(this.loader) && this.loader.type === LoaderType.General;
  }

  public isSoft(): boolean {
    return !_.isNil(this.loader) && !_.isEmpty(this.loader) && this.loader.type === LoaderType.SoftLoader;
  }

  public getMessage(): string {
    return _.isNil(this.loader) || _.isEmpty(this.loader) ? '' :
      this.loader.message === LoaderMessageType.Loading ? 'Cargando...' :
        this.loader.message === LoaderMessageType.Updating ? 'Actualizando...' :
          this.loader.message === LoaderMessageType.Saving ? 'Guardando...' :
            this.loader.message === LoaderMessageType.Deleting ? 'Eliminando...' :
              this.loader.message === LoaderMessageType.Verifying ? 'Verificando...' :
                this.loader.message === LoaderMessageType.Importing ? 'Importando...' :
                  this.loader.message === LoaderMessageType.Redirecting ? 'Redirigiendo...' :
                    this.loader.message === LoaderMessageType.NotAllowed ? 'Acceso denegado. Redirigiendo...' : '';
  }
}
