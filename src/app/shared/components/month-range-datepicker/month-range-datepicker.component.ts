import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {CalendarValue, IDatePickerConfig} from 'ng2-date-picker';

import {BaseComponentModel} from '../../models/base-component/base-component.class';
import {MONTHRANGEDATEPICKERCONFIG} from '../../const/system.const';

import {
  StatusPeriodsRangeFilterModel
} from '../../models/status-periods/status-periods-range-filter.model';

const moment = require('moment');
const _ = require('lodash');

@Component({
  selector: 'app-month-range-datepicker-component',
  templateUrl: './month-range-datepicker.component.html',
  styleUrls: ['./month-range-datepicker.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class MonthRangeDatePickerComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() mode = 'init';
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() onChangeFilter = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('monthRangeDatepicker') monthRangeDatepicker;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() onChangeRangeEvent = new EventEmitter();

  /************************ COMPONENT VARS  ************************/

  public selectedDate: object = null;
  public isOpen = false;

  private _hasSelectedRange = false;
  private _backupSelectedDate: Array<object> = null;

  /************************ COMPONENT CONFIG  ************************/

  public datePickerConfig: IDatePickerConfig = MONTHRANGEDATEPICKERCONFIG;

  /************************ NATIVE METHODS  ************************/

  constructor() {
    super();
  }

  ngOnInit() {
    this.config.id = 'month_range_datepicker_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    this.emitOnChangeRangeEvent(true);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('visible') && !_.isNil(changes.visible.currentValue)) {
      this.selectedDate = null;
    }
  }

  /************************ EVENTS METHODS ************************/

  /**
   * On change range
   * @param event
   */
  public onChangeRange(event: CalendarValue) {
    if (this.monthRangeDatepicker.selected.length === 1) {
      this._hasSelectedRange = true;
      this.monthRangeDatepicker.selected.push(this.monthRangeDatepicker.selected[0]);
      this.emitOnChangeRangeEvent(false, false);
      this._backupSelectedDate = _.cloneDeep(this.selectedDate);
    } else if (this.monthRangeDatepicker.selected.length === 2 || this.monthRangeDatepicker.selected.length === 3) {

      if (this.monthRangeDatepicker.selected.length === 3
        && this.monthRangeDatepicker.selected[0] === this.monthRangeDatepicker.selected[1]) {
        this.monthRangeDatepicker.selected.splice(1, 1);
      }

      // if (!this._hasSelectedRange) {

        // Check that the start date is greater than the selected end date

        if (this.monthRangeDatepicker.selected[1] < this.monthRangeDatepicker.selected[0]) {
          this.onUnselectMonths();
        } else if (this.monthRangeDatepicker.selected[1] > this.monthRangeDatepicker.selected[0]) {

          const initMoment = this.monthRangeDatepicker.selected[0];
          const endMoment = this.monthRangeDatepicker.selected[1];

          const newSelectedMonths = [];
          if (moment(initMoment).isBefore(moment(endMoment))) {

            const monthDifference = moment(endMoment).diff(moment(initMoment), 'months', true);
            if (monthDifference === 1) {
              newSelectedMonths.push(this.monthRangeDatepicker.selected[0]);
              newSelectedMonths.push(this.monthRangeDatepicker.selected[1]);
              this._hasSelectedRange = true;
              this.emitOnChangeRangeEvent();
            } else if (monthDifference > 1) {

              newSelectedMonths.push(this.monthRangeDatepicker.selected[0]);

              let count = 0;
              let date = _.cloneDeep(this.monthRangeDatepicker.selected[0]);

              while (moment(date).isBefore(moment(this.monthRangeDatepicker.selected[1]))) {
                count++;
                date = _.cloneDeep(this.monthRangeDatepicker.selected[0]);
                date.add(count, 'months');
                newSelectedMonths.push(date);
              }

              this._backupSelectedDate = _.cloneDeep(newSelectedMonths);
              this._hasSelectedRange = true;
              this.monthRangeDatepicker.selected = newSelectedMonths;
              this.emitOnChangeRangeEvent();
            }
          }
        }
      // }
    } else if (this._hasSelectedRange
      && !_.isNil(this._backupSelectedDate)
      && this._backupSelectedDate.length !== this.monthRangeDatepicker.selected.length) {
      this.onUnselectMonths();
    }
  }

  /**
   * On click calendar icon
   */
  public onClickCalendarIcon(): void {
    if (this.isOpen) {
      this.monthRangeDatepicker.hideCalendar();
    } else {
      this.monthRangeDatepicker.showCalendars();
    }
  }

  /**
   * On close calendar
   */
  public onCloseCalendar(): void {
    this.isOpen = false;
  }

  /**
   * On open calendar
   */
  public onOpenCalendar(): void {
    this.isOpen = true;
  }

  /**
   * On unselect months
   */
  public onUnselectMonths(): void {
    this._hasSelectedRange = false;
    this.monthRangeDatepicker.selected = [];
    this.monthRangeDatepicker.inputElementValue = '';
    this.monthRangeDatepicker.cd.detectChanges();
    this.emitOnChangeRangeEvent(true);
  }

  /**
   * Check if show unselect months button
   * @returns {boolean}
   */
  public showUnselectMonthsButton(): boolean {
    return !_.isNil(this.selectedDate) && !_.isEmpty(this.selectedDate);
  }

  /************************ AUXILIAR METHODS  ************************/

  /**
   * On change range
   * @param {boolean} reset
   */
  private emitOnChangeRangeEvent(reset: boolean = false, closeCalendar: boolean = true) {
    let initDateMoment = null;
    let endDateMoment = null;

    if (!reset) {
      initDateMoment = moment(this.monthRangeDatepicker.selected[0]);
      initDateMoment = moment(initDateMoment.startOf('month'));

      endDateMoment = moment(this.monthRangeDatepicker.selected[this.monthRangeDatepicker.selected.length - 1]);
      endDateMoment = moment(endDateMoment.endOf('month'));

      this.monthRangeDatepicker.inputElementValue = moment(initDateMoment).format('MMM YYYY')
        + ' - ' + moment(endDateMoment).format('MMM YYYY');
      this.monthRangeDatepicker.cd.detectChanges();
    }

    this.onChangeRangeEvent.emit(new StatusPeriodsRangeFilterModel({
      mode: this.mode,
      initDate: initDateMoment,
      endDate: endDateMoment,
      closeCalendar: closeCalendar
    }));
  }
}
