import {
  Component, OnInit, Input, SimpleChanges, OnChanges, ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';

import {NoteModel} from '../../../models/notes/note.model';
import {PersonModel} from '../../../models/person.model';
import {ScopeNotes} from '../scope-notes.class';

import {AuthSrvService} from '../../../services/auth.service';
import {HttpSrvService} from '../../../services/http.service';

const moment = require('moment');
const _ = require('lodash');

@Component({
  selector: 'app-scope-notes-board',
  templateUrl: './scope-notes-board.component.html',
  styleUrls: ['./scope-notes-board.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class ScopeNotesBoardComponent extends ScopeNotes implements OnInit, OnChanges {

  @Input() public person: PersonModel;

  private _notes = {
    data: [],
    totalCount: 0,
    query: {
      page: 1,
      elements: 3,
      status: 'inValues(NOT_STARTED,ONGOING,BLOCKED,CANCELLED,ENDED)',
      orderBy: 'endDate',
      endDate: null,
      orderType: 'asc'
    }
  };

  constructor(private _httpSrv: HttpSrvService,
              private _authSrv: AuthSrvService,
              private _changeDetectorRef: ChangeDetectorRef) {
    super();
    this._notes.query.endDate = this._datePipe.rangeCalcDateForApi(90, moment().startOf('day'));
  }

  ngOnInit() {
    this.getNotesFromApi();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('person')
      && changes['person']['id'] !== 0
      && changes['person']['id'] !== this.person.id) {
      this.resetComponent();
    }
  }

  /**
   * Reset component
   * @param {boolean} flag
   */
  resetComponent(): void {
    this.getNotesFromApi();
  }


  /**
   * Return if has notes in data
   * @returns {boolean}
   */
  public hasNotes(): boolean {
    return this.getNotes().length > 0;
  }


  /**
   * Get the note list limited by offset
   * @returns {Array<NoteModel>}
   */
  public getNotes(): Array<NoteModel> {
    return this._notes.data.length === 0 ? [] : this._notes.data.slice(0, this._notes.query.elements);
  }

  public getBoardTitle(): string {
    return this.isOwnScope()
      ? ''
      : this.person.getNameFirstSurname();
  }

  /**
   * Determine if the logged user is the mate in page
   * @returns {boolean}
   */
  private isOwnScope(): boolean {
    return this.person.id === this._authSrv.getPersonId();
  }

  /**
   * CHeck if mate has team
   * @returns {boolean}
   */
  public hasTeams(): boolean {
    if (this.person.isGestor()) {
      return (!_.isNil(this.person) && !_.isNil(this.person.managed_teams) && !_.isEmpty(this.person.managed_teams)
        || (!_.isNil(this.person) && !_.isNil(this.person.team_persons) && !_.isEmpty(this.person.team_persons)));
    } else {
      return (!_.isNil(this.person) && !_.isNil(this.person.team_persons) && !_.isEmpty(this.person.team_persons));
    }
  }
  /*********************** UI EVENTS ******************************/
  public showFullNotes(event: Event): void {
    event.preventDefault();
    event.stopPropagation();

    this.emitEvent(
      {
        component: 'notes-board',
        event: 'noteListMode'
      }
    );
  }

  /*********************** API CALLS ******************************/
  /**
   * Get the note list
   */
  private getNotesFromApi(): void {
    if (this.person.id !== 0) {
      this._httpSrv
        .get(
          'persons/' + this.person.id + '/notes/',
          this._notes.query
        )
        .subscribe(
          (responseOk) => this.onSuccessGetNotesFromApi(responseOk)
        );
    }
  }

  // persons/96/notes/?page=1&elements=10&orderBy=endDate&orderType=desc&status=inValues(NOT_STARTED,ONGOING,BLOCKED,CANCELLED,ENDED)

  /**
   * On success get data notes
   * @param {Object | any} response
   */
  private onSuccessGetNotesFromApi(response: object | any): void {
    if (this._validator.isSuccessResponse(response)
      && (this._validator.isValidResponseItemList(response) || _.isEmpty(response['object']))) {
      if (!_.isEmpty(response['object'])) {
        this._notes.data = _.concat(
          this._notes.query.page === 1 ? [] : this._notes.data,
          this.generateNotes(response['object']['items'])
        );
      }

      this._notes.totalCount = response['object']['total_count'];
      this._notes.query.page = response['object']['current_page_number'];
      this._changeDetectorRef.detectChanges();
    }
  }
}
