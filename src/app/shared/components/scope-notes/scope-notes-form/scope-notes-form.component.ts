import {
  Component, OnInit, Input, SimpleChanges, OnChanges, ViewChild, ChangeDetectionStrategy
} from '@angular/core';
import {NgModel} from '@angular/forms';
import {SelectItem} from 'primeng/primeng';

import {DATEPICKER} from '../../../const/system.const';

import {NoteModel} from '../../../models/notes/note.model';
import {PersonModel} from '../../../models/person.model';
import {ScopeNotes} from '../scope-notes.class';
import {TeamPersonModel} from '../../../models/team/teamPerson.model';

import {AlertService} from '../../../services/alert.service';
import {AuthSrvService} from '../../../services/auth.service';
import {HttpSrvService} from '../../../services/http.service';

import {TeamModel} from '../../../models/team/team.model';

const _ = require('lodash');

@Component({
  selector: 'app-scope-notes-form',
  templateUrl: './scope-notes-form.component.html',
  styleUrls: ['./scope-notes-form.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class ScopeNotesFormComponent extends ScopeNotes implements OnInit, OnChanges {

  @ViewChild('noteTitle') noteTitleInput;
  @ViewChild('noteDescription') noteDescriptionInput;
  @ViewChild('nodeTypeSelect') nodeTypeSelect;
  @ViewChild('nodeTeamsSelect') nodeTeamsSelect;

  @Input() public person: PersonModel;

  public note;
  public dropdownNoteTypeOptions: SelectItem[] = [
    {label: 'Tipo de mensaje', value: null},
    {label: 'Hito', value: 'TASK'},
    {label: 'Notificación', value: 'COMMUNICATION'}
  ];
  public selectedNoteTypeDropdownOption: any;

  public dropdownNoteTeamOptions = [
    {label: 'Todos tus equipos', value: null}
  ];
  public selectedNoteTeamDropdownOption: any;

  private _actualDate = new Date();
  private _updateFirstTry = false;

  // CALENDARIO

  public spanishCalendar = DATEPICKER.ESP;

  constructor(private _httpSrv: HttpSrvService,
              private _alertSrv: AlertService,
              private _authSrv: AuthSrvService) {
    super();
    this.resetComponent();
  }

  ngOnInit() {
    this.selectedNoteTeamDropdownOption = this.dropdownNoteTeamOptions[0].value;
    this.selectedNoteTypeDropdownOption = this.dropdownNoteTypeOptions[0];
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('person') && !_.isNil(changes.person.currentValue)) {
      if (!_.isNil(this.person.teams) && !_.isEmpty(this.person.teams)) {
        this.setDropdownTeamData();
        this.person.teams.forEach( team => {
          this.note.teams.push({id: team.id});
        });
      }
    }
  }


  resetComponent(flag: boolean = false): void {
    this.note = new NoteModel({
      title: '',
      description: '',
      type: null,
      status: 'NO_STARTED',
      teams: [],
      end_date: null
    });

    if (!_.isNil(this.person) && this.person.id !== 0 && !_.isEmpty(this.person.teams)) {
      this.person.teams.forEach(team => {
        this.note.teams.push({id: team.id});
      });
    }

    this._updateFirstTry = false;

    if (flag) {
      this.noteTitleInput.reset();
      this.noteDescriptionInput.reset();
      this.selectedNoteTypeDropdownOption = this.dropdownNoteTypeOptions[0];
      this.selectedNoteTeamDropdownOption = this.dropdownNoteTeamOptions[0].value;
    }
  }

  public setDropdownTeamData(): void {
    this.person.teams.forEach((team: TeamModel) => {
      const teamIndex = this.dropdownNoteTeamOptions.findIndex( element => {
        return element.label === team.name;
      });
      if (teamIndex === -1) {
        this.dropdownNoteTeamOptions.push({label: team.name, value: team.id});
      }
    });

    // @ToDo: Descomentar si deben agregarse los equipos que gestiona al dropdown

    // if (this.person.id === this._authSrv.getPersonLogged().id
    //   && this._authSrv.getPersonLogged().isGestor()) {
    //   this.person.managed_teams.forEach((team: TeamModel) => {
    //     const teamIndex = this.dropdownNoteTeamOptions.findIndex( element => {
    //       return element.label === team.name;
    //     });
    //     if (teamIndex === -1) {
    //       this.dropdownNoteTeamOptions.push({label: team.name, value: team.id});
    //     }
    //   });
    // }

    this.selectedNoteTeamDropdownOption = this.dropdownNoteTeamOptions[0].value;
  }

  /**
   * On change dropdwon option selected
   * @param {Object} event
   */
  public onChangeDropdownTypeOption(event: object): void {
    this.note.type = event;
  }

  /**
   * On change dropdwon option selected
   * @param {Object} event
   */
  public onChangeDropdownTeamOption(event: object): void {
    this.note.teams = [];
    if ( _.isNil(this.selectedNoteTeamDropdownOption.value)) {
      this.person.teams.forEach( team => {
        this.note.teams.push({id: team.id});
      });
    } else {
      this.note.teams.push({id: event});
    }
  }

  /**
   * Return if an input has errors
   * @param input
   * @returns {boolean}
   */
  public hasErrorInput(input: NgModel): boolean {
    return this._updateFirstTry
      ? !_.isNull(input.errors)
      : !_.isNull(input.errors) && (input.dirty || input.touched);
  }

  public getErrorInput(input: NgModel): string {
    if (this.hasErrorInput(input)) {
      if (input.getError('required')) {
        return 'Campo requerido';
      }

      if (input.getError('minlength')) {
        return 'Mínimo ' + input.errors.minlength.requiredLength + ' caracteres';
      }

      if (input.getError('maxlength')) {
        return 'Máximo ' + input.errors.maxlength.requiredLength + ' caracteres';
      }
    }

    return '';
  }

  public getMinStartDate(): Date {
    return this._actualDate;
  }

  /**
   * Return if a dropdown has errors
   * @returns {boolean}
   */
  public hasErrorTypeDropdown(): boolean {
    return this._updateFirstTry && _.isNil(this.note.type);
  }

  /**
   * Returns if a datepicker has error
   * @returns {boolean}
   */
  public hasErrorDatePicker(): boolean {
    return this._updateFirstTry
      && (
        _.isNil(this.note.end_date)
        || this._datePipe.compareDates(this.note.end_date) === -1
      );
  }


  public hasFormError(): boolean {
    return this.hasErrorInput(this.noteTitleInput)
      || this.hasErrorInput(this.noteDescriptionInput)
      || this.hasErrorTypeDropdown()
      || this.hasErrorDatePicker();
  }


  public onSubmitNote(event: Event): void {
    event.preventDefault();
    event.stopPropagation();

    if (!this.hasTeams()) {
      this._alertSrv.warn(this.isOwnScope()
        ? 'Debes pertenecer a algún equipo para poder publicar mensajes'
        : 'Se requiere pertenecer a algún equipo para poder publicar mensajes'
      );

    } else {
      this._updateFirstTry = true;

      if (!this.hasFormError()) {
        this.createNotesFromApi();
      }
    }

  }

  /**
   * CHeck if mate has team
   * @returns {boolean}
   */
  public hasTeams(): boolean {
    if (this.person.isGestor()) {
      return (!_.isNil(this.person) && !_.isNil(this.person.managed_teams) && !_.isEmpty(this.person.managed_teams)
        || (!_.isNil(this.person) && !_.isNil(this.person.team_persons) && !_.isEmpty(this.person.team_persons)));
    } else {
      return (!_.isNil(this.person) && !_.isNil(this.person.team_persons) && !_.isEmpty(this.person.team_persons));
    }
  }


  /**
   * Determine if the logged user is the mate in page
   * @returns {boolean}
   */
  private isOwnScope(): boolean {
    return this.person.id === this._authSrv.getPersonId();
  }

  /*********************** API CALLS ******************************/
  /**
   * Crate a note
   */
  private createNotesFromApi(): void {
    if (this.person.id !== 0) {

      const sendTeams = [];
      if (_.isNil(this.selectedNoteTeamDropdownOption)) {
        this.note.teams.forEach( teamId => {
          sendTeams.push({id: teamId});
        });
      } else {
        sendTeams.push({id: this.selectedNoteTeamDropdownOption});
      }

      this._httpSrv
        .post(
          'notes/',
          {
            title: this.note.title,
            description: this.note.description,
            type: this.note.type,
            teams: sendTeams,
            end_date: this._datePipe.datetimeToISO(this.note.end_date),
            person_id: this.person.id
          }
        )
        .subscribe(
          (responseOk) => this.onSuccessCreateNotesFromApi(responseOk)
        );
    }
  }

  /**
   * On success create data notes
   * @param {Object | any} response
   */
  private onSuccessCreateNotesFromApi(response: object | any): void {
    if (this._validator.isSuccessResponse(response) && this._validator.isValidResponse(response)) {
      this.resetComponent(true);
      this.emitEvent({event: 'newNoteCreated'});
    }
  }
}
