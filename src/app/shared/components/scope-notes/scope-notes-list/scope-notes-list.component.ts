import {
  Component, OnInit, Input, SimpleChanges, OnChanges, ViewChildren, QueryList, ElementRef, ViewChild,
  ChangeDetectionStrategy, ChangeDetectorRef
} from '@angular/core';

import {NoteModel} from '../../../models/notes/note.model';
import {PersonModel} from '../../../models/person.model';
import {ScopeNotes} from '../scope-notes.class';

import {HttpSrvService} from '../../../services/http.service';
import {AlertService} from '../../../services/alert.service';
import {SelectItem} from 'primeng/primeng';

const _ = require('lodash');

@Component({
  selector: 'app-scope-notes-list',
  templateUrl: './scope-notes-list.component.html',
  styleUrls: ['./scope-notes-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class ScopeNotesListComponent extends ScopeNotes implements OnInit, OnChanges {

  @ViewChild('confirmDialogComponent') public confirmDialogComponent;
  @ViewChildren('noteListCell') noteListCell:  QueryList<ElementRef>;

  @Input() public person: PersonModel;

  public _notes = {
    data: [],
    totalCount: 0,
    query: {
      page: 1,
      elements: 10,
      status: 'ONGOING',
      orderBy: 'endDate',
      endDate: null,
      orderType: 'asc'
    }
  };

  private _notesQuerySetting = {
    page: 1,
    elements: 10,
    orderBy: 'endDate',
    orderType: 'asc',
    status: 'ONGOING',
    endDate: null,
  };


  private _actionTitles = {
    cancel: 'Cancelar',
    start: 'Iniciar',
    block: 'Paralizar',
    reactivate: 'Reactivar',
    end: 'Finalizar',
    restart: 'Reiniciar',
    recover: 'Recuperar'
  };

  /** COMPONENT CONFIG **/

  private _confirmDialogConfig = {
    header: 'El mensaje será eliminado',
    message: '¿Desea continuar?',
    accept: {
      action: 'deleteNote',
      button: {
        text: 'Si'
      }
    }
  };

  public statusDropdownOptions: SelectItem[] = [
    {label: 'Todas las publicaciones', value: 'NOT_STARTED,ONGOING,BLOCKED,CANCELLED,ENDED'},
    {label: 'Pendientes (No iniciadas, iniciadas sin finalizar)', value: 'NOT_STARTED,ONGOING'},
    {label: 'Paralizadas', value: 'BLOCKED'},
    {label: 'Canceladas', value: 'CANCELLED'},
    {label: 'Finalizadas', value: 'ENDED'}
  ];
  public selectedStatusDropdownOptions: string = this.statusDropdownOptions[0].value;

  public dateDropdownOptions: SelectItem[] = [
    {label: 'Fecha de entrega más reciente primero', value: 'desc'},
    {label: 'Fecha de entrega más antigüa primero', value: 'asc'}
  ];
  public selectedDateDropdownOptions: SelectItem = this.dateDropdownOptions[0].value;


  constructor(private _httpSrv: HttpSrvService,
              private _alertSrv: AlertService,
              private _changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  ngOnInit() {
    this.resetNoteQueryString(true);
    // this.getNotesFromApi();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('person') && changes['person']['id'] !== 0 && changes['person']['id'] !== this.person.id) {
      this.resetComponent();
    }
  }

  resetComponent(): void {
    this.resetNoteQueryString(true);
    this.getNotesFromApi();
  }

  /**
   * Reset API endpoint params
   * @param {boolean} forced
   */
  private resetNoteQueryString(forced: boolean = false): void {
    if (forced) {
      this._notes.totalCount = 0;
    }

    this._notes.query = Object.assign({}, this._notesQuerySetting);
    this._notes.query.status = 'inValues(' + this.selectedStatusDropdownOptions + ')';
    this._notes.query.orderType = this.selectedDateDropdownOptions.toString();
  }


  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getConfirmDialogSetup(): object {
    return this._confirmDialogConfig;
  }

  /**
   * Return if has notes in data
   * @returns {boolean}
   */
  public hasNotes(): boolean {
    return this.getNotes().length > 0;
  }

  /**
   * Get the note list
   * @returns {Array<NoteModel>}
   */
  public getNotes(): Array<NoteModel> {
    return this._notes.data.length === 0 ? [] : this._notes.data;
  }

  public getActionTitle(action: string): string {
    return this._actionTitles.hasOwnProperty(action.toLowerCase())
      ? this._actionTitles[action.toLowerCase()]
      : '';
  }

  public getActionAvailables(actions: object): Array<any> {
    return _.keys(actions);
  }

  public getNoteCellId(id: number): string {
    return 'noteCell' + id;
  }


  public thereAreMostNotesToShow(): boolean {
    return this._notes.data.length < this._notes.totalCount;
  }

  public isLoadingNotes(): boolean {
    return this._loading;
  }


  /*********************** UI EVENTS ******************************/
  public hideFullNotes(event: Event): void {
    event.preventDefault();
    event.stopPropagation();

    this.emitEvent(
      {
        component: 'notes-list',
        event: 'dashboardMode'
      }
    );
  }

  public onDeleteNoteEvent(event: Event, note: NoteModel): void {
    event.preventDefault();
    event.stopPropagation();

    this.confirmDialogComponent.showDialog(
      {
        id: note.id,
        type: note.type,
        title: note.title
      },
      this.isNoteTypeTask(note)
        ? 'La tarea "<i>' + note.title + '</i>" va a ser eliminada'
        : 'El comunicado "<i>' + note.title + '</i>" va a ser eliminada'
    );
  }

  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']](event['params']['id']);
    }
  }

  public OnNextNotes (event: Event): void {
    event.preventDefault();
    event.stopPropagation();

    this._notes.query.page++;
    this.getNotesFromApi();
  }


  /*********************** API CALLS ******************************/
  /**
   * Get the note list
   */
  private getNotesFromApi(): void {
    if (this.person.id !== 0) {
      this._httpSrv
        .get(
          'persons/' + this.person.id + '/notes/',
          this._notes.query
        )
        .subscribe(
          (responseOk) => this.onSuccessGetNotesFromApi(responseOk)
        );
    }
  }

  /**
   * On success get data notes
   * @param {Object | any} response
   */
  private onSuccessGetNotesFromApi(response: object | any): void {
    if (this._validator.isSuccessResponse(response)
      && (this._validator.isValidResponseItemList(response) || _.isEmpty(response['object']))) {
      if (!_.isEmpty(response['object'])) {
        this._notes.data = _.concat(
          this._notes.query.page === 1 ? [] : this._notes.data,
          this.generateNotes(response['object']['items'])
        );
      }

      this._notes.totalCount = response['object']['total_count'];
      this._notes.query.page = response['object']['current_page_number'];

      this._changeDetectorRef.detectChanges();
    }
  }


  /**
   * Update a status note
   * @param {Event} event
   * @param {NoteModel} note
   * @param {string} action
   */
  public onNoteStatusUpdate(event: Event,
                            note: NoteModel,
                            action: string): void {
    event.preventDefault();
    event.stopPropagation();

    // @TODO: se descrimina accion segun estado de la nota hasta que se defina funcionalidad

    if (this._datePipe.diffDatesDays(note.end_date) >= 1) {
      this._alertSrv.prompt('Esta acción está pendiente de definición y requisitos');
     return;
    }

    this._httpSrv
      .put(
        'notes/' + note.id + '/workflow/',
        {action: action}
      )
      .subscribe(
        (responseOk) => this.onSuccessUpdateNotesStatusFromApi(responseOk)
      );
  }

  /**
   * On success get data notes
   * @param {Object | any} response
   */
  private onSuccessUpdateNotesStatusFromApi(response: object | any): void {
    if (this._validator.isSuccessResponse(response) && this._validator.isValidResponse(response)) {
      this.getNotesFromApi();
    }
  }


  /**
   * Delete a note
   * @param {number} id
   */
  public deleteNote(id: number): void {
    this._httpSrv
      .delete(
        'notes/' + id
      )
      .subscribe(
        (responseOk) => this.onSuccessDeleteNoteFromApi(responseOk, id)
      );
  }

  /**
   * On success get data notes
   * @param {Object | any} response
   * @param {number} id
   */
  private onSuccessDeleteNoteFromApi(response: object | any, id: number): void {
    if (this._validator.isSuccessResponse(response)) {
      const note = this.noteListCell.find((cell) => {
        return cell.nativeElement.id === this.getNoteCellId(id);
      });

      if (!_.isNil(note)) {
        note.nativeElement.classList.add('delete');

        setTimeout(() => {
          if (_.remove(this._notes.data, (item: NoteModel) => {
              return item.id === id;
            }).length > 0 ) {
            this._notes.totalCount--;
          }
        }, 500);
      }
    }
  }
}
