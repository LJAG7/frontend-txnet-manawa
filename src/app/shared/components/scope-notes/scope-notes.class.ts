import {Output, EventEmitter} from '@angular/core';
import {NoteModel} from '../../models/notes/note.model';
import {TeamModel} from '../../models/team/team.model';

import {DateLocalPipe} from '../../pipes/dateLocal.pipe';
import {ColorGeneratorPipe} from '../../pipes/colorGenerator.pipe';
import {ValidationObjectPipe} from '../../pipes/validation.pipe';

const _ = require('lodash');

export abstract class ScopeNotes {

  @Output() public outputEvent = new EventEmitter();
  protected _datePipe: DateLocalPipe;
  protected _colorPipe: ColorGeneratorPipe;
  protected _validator: ValidationObjectPipe;
  protected _loading = false;
  protected _originalNotes;

  /**
   * Reset the component
   * @param {boolean} flag
   * @param {Object} params
   */
  public abstract resetComponent(flag?: boolean, params?: object): void;

  constructor () {
    this._datePipe = new DateLocalPipe();
    this._colorPipe = new ColorGeneratorPipe();
    this._validator = new ValidationObjectPipe();
  }


  /**
   * Returns the team color associated
   * @param {TeamModel} team
   * @returns {string}
   */
  public getNoteTeamColor(team: TeamModel): string {
    return !_.isEmpty(team.name)
      ? this._colorPipe.transform(team.name)
      : 'rgb(0,0,0)';
  }

  /**
   * Returns if a note is type Task
   * @param {NoteModel} note
   * @returns {boolean}
   */
  public isNoteTypeTask(note: NoteModel): boolean {
    return note.type === 'TASK';
  }

  /**
   * Returns if a note is type Communication
   * @param {NoteModel} note
   * @returns {boolean}
   */
  public isNoteTypeCommunication(note: NoteModel): boolean {
    return note.type === 'COMMUNICATION';
  }


  public isNoteExpired(date: string): boolean {
    return this._datePipe.diffDatesDays(date) >= 1;
  }

  /**
   * Get text for note date
   * @param {string} date
   * @returns {string}
   */
  public getNoteDate(date: string): string {
    if (this._datePipe.compareDates(date) === 0) {
      return '<span class="date-today">hoy</span>';
    } else if (this._datePipe.diffDatesDays(date) === 1) {
      return '<span class="date-expired">ayer</span>';
    } else if (this._datePipe.diffDatesDays(date) === -1) {
      return 'mañana';
    } else {
      return this._datePipe.transform(date);
    }
  }

  /**
   * Style class for note date
   * @param {string} date
   * @returns {string}
   */
  public getNoteDateStyle(date: string): string {
    if (this._datePipe.compareDates(date) === 0) {
      return 'date-today';
    } else if (this._datePipe.diffDatesDays(date) === -1) {
        return 'date-tomorrow';

    } else if (this.isNoteExpired(date)) {
      return 'date-expired';
    }

    return '';
  }

  /**
   * Return the border color class for note associated
   * @param {NoteModel} note
   * @returns {string}
   */
  public getNoteStatusStyle(note: NoteModel): string {
    if (this.isNoteTypeTask(note)) {
      return 'note-task-'.concat(_.replace(note.status.toLowerCase(), '_', '-'));

    } else if (this.isNoteTypeCommunication(note)) {
      return 'note-communication';
    }

    return '';
  }

  /**
   * Return loading status
   * @returns {boolean}
   */
  public isLoading(): boolean {
    return this._loading;
  }

  /**
   * Create NoteModel instances for all items of data list
   * @param {Array<Object>} notes
   */
  protected generateNotes(notes: object | Array<object>): Array<NoteModel> {
    const _notesModel = [];
    const _noteListArray = !Array.isArray(notes) ? [notes] : notes;

    _noteListArray.forEach((note: any) => {
      if (!(note instanceof NoteModel)) {
        _notesModel.push(new NoteModel(note));
      }
    });

    // !Array.isArray(notes) ? Object.assign({}, _notesModel[0]) : Object.assign([], _notesModel);

    return Object.assign([], _notesModel);
  }


  /**
   * Output emit
   * @param {Object} params
   */
  protected emitEvent(params: object): void {
    this.outputEvent.emit(params);
  }
}
