import {
  Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges, ViewChild
} from '@angular/core';
import {HttpSrvService} from '../../services/http.service';
import {TextParserPipe} from '../../pipes/textParser.pipe';
import {isNull} from 'util';

const _ = require('lodash');

@Component({
  selector: 'app-search-component',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']

})

/**
 * Search Component class
 *
 * ANNOTATION: if ApiCall mode is active, don't use data (input) in component instance
 */
export class SearchComponent implements OnInit, OnChanges {

  /** COMPONENT INPUTS **/

  @Input() public css: string | any = '';
  @Input() public data: Array<any> = [];
  @Input() public defaultValue: any = null; // Object: if searched in data and title parsed if exist, String: is direct assinged
  @Input() public externalConfig: object;
  @Input() public disabled = false;
  @Input() public restart = false;
  @Input() public visible = true;

  /** COMPONENT OUTPUTS **/

  @Output() public onClickElementEvent = new EventEmitter();
  @Output() public onNoResults = new EventEmitter();
  @Output() public onRestartEvent = new EventEmitter();
  @Output() public outputEvent = new EventEmitter();

  /** COMPONENT VIEWCHILD **/

  @ViewChild('searchInput') searchInput;

  /** COMPONENT VARS **/

  public placeHolder = '';
  public wordToFilter = '';
  public dataFiltered: Array<any> = [];

  private _isFocusingInput = false;
  private _originalData = [];
  private _uniqueResultFound = false;
  private _optionSelected: object | any = null;
  private _optionDefault: object | any = null;
  private _showListUnderMandatory = false;
  private _callToApi = false;
  private _timeoutKeyup = {
    first: 250,
    last: 500
  };

  /** COMPONENT CONFIG **/

  private _config = {
    id: 'search_component_n'.concat(Math.floor(Math.random() * 100000).toString()),
    title: 'name',
    subtitle: '',
    keyToFilter: 'name',
    alternativeKeysToFilter: [],
    titleOption: null,
    separator: ' - ',
    api: {
      endpoint: null, // endpoint to API call
      queryString: {
        page: 1,
        elements: 50
      },
      queryStringExtra: null, // key for search value. This key is associated with wordToFilter always. Must be type string
      showError: true,
      timer: null // timeout for API call on live mode. Only used when flags.dataOnLive is true
    },
    error: {
      show: true,
      message: 'No se encontraron resultados para <strong>{{wordToFilter}}</strong>'
    },
    flags: {
      dataOnLive: false, // the data is reloading in real time
      checkBlurEvent: true,
      hasFilterOnRealTime: true,
      hasWhiteBackground: false, // background cover style
      isEnabledClickOnElementList: true, // dis/enabled click event in option list
      isMiniSearch: false, // alternative input style
      showCenterAttention: false, // Show black background cover style
      showResultList: true, // mandatory to show result when input is focused
      showResultListUnderClick: false, // mandatory to show result when input is clicked
      showResultListUnderUnselect: false, // mandatory to force show result when unselect icon is clicked
      showBackgroundCover: true, // show or hide the background cover,
      forcedUnselectIconShow: false // force show unselect icon (not required _optionSelected to show). Required flag showUnselectIcon
    },
    icon: {
      hasIconRight: false,
      hasSearchIcon: false,
      hasSelectableIcon: false,
      hideIconOnSearch: false,
      showUnselectIcon: false
    },
    input: {
      placeholder: 'Buscar...',
      value: null
    },
    css: {
      extraClasses: ''
    }
  };

  /** STATIC METHODS **/

  public static isValidObjectResponse(response: object | any): boolean {
    return response.hasOwnProperty('object')  && response['object']
      && response['object'].hasOwnProperty('items')  && response['object']['items'];
  }

  public static isValidObjectProperty(item: object, property: string): boolean {
    return item
      && item.hasOwnProperty(property)
      && item[property];
  }

  /**
   * Simulate a join method (array) but with keys definitions to joining
   * @param object
   * @param keys
   * @param separator
   * @returns {string}
   */
  public static joinByKeys(object: Object = null, keys: Array<any> = [], separator: string = ' '): string {
    const values: Array<any> = [];

    for (const index of keys) {
      if (object[index]) {
        values.push(object[index]);
      }
    }

    return values.join(separator);
  }

  /** NATIVE METHODS **/

  constructor(private _httpSrv: HttpSrvService) {}

  ngOnInit(): void {
    _.merge(this._config, this.externalConfig);
    this.wordToFilter = this._config.input.value;
    const inputValueElement = this.dataFiltered.filter( data => {
      return data[this._config.keyToFilter] === this.wordToFilter;
    });

    if (inputValueElement.length === 1) {
      this.selectElementFromResultList(inputValueElement[0]);
    }
    this.placeHolder = this._config.input.placeholder;

    if (!this.isApiLiveMode()) {
      if (!isNull(this._config.api.endpoint)) {
        this.getDataFromApi();
      } else {
        this.makeReadyDataIdentify();
      }
    }

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!_.isNil(changes['data']) && !_.isUndefined(changes['data']['currentValue'])) {
      this.data = changes['data']['currentValue'];
      this._originalData = [...this.data];
      if (!_.isNil(this._config.input.value)) {
        this.wordToFilter = this._config.input.value;
        const inputValueElement = this.dataFiltered.filter( data => {
          return data[this._config.keyToFilter] === this.wordToFilter;
        });

        if (inputValueElement.length === 1) {
          this.selectElementFromResultList(inputValueElement[0]);
        }
      }
      this.makeReadyDataIdentify();

      if (this._config.flags.dataOnLive) {
        this.dataFiltered = this.data;
        this.showResultList();
      }
    }

    if (!_.isNil(changes['defaultValue']) && !_.isUndefined(changes['defaultValue']['currentValue'])) {
      this.parseDefaultValue();
    }

    this.observeChangesOnRestart();
    _.merge(this._config, this.externalConfig);
  }

  /** PUBLIC METHODS **/

  /**
   * Clean the input search
   */
  public cleanInputSearch(): void {
    this.wordToFilter = !_.isEmpty(this._config.input.value) ? this._config.input.value : '';
    this.placeHolder = this._config.input.placeholder;
    this.outputEvent.emit({id: this._config.id, currentText: this.wordToFilter});
  }

  /**
   * Determine if it a plane array or Array of objects
   * @param {Object} element
   * @returns {string}
   */
  public getElementValue(element: Object = null): string {
    const keyValue = _.isEmpty(this._config.titleOption)
      ? 'title'
      : 'titleOption';

    return this.getTitleByKey(element, keyValue, !_.isEmpty(this._config.titleOption));
  }

  /**
   * Get the component id
   * @returns string
   */
  public getId(): string {
    return this._config.id;
  }

  /**
   * Get the schema of notes for input according to widgetConfig
   * @returns Object
   */
  public getSearchInputCss(): Object {
    const schema = {'form-control': true, 'text-overflow': true};

    if (this.showIcon()) {
      schema['has-right-icon'] = true;
    }
    if (this.wordToFilter && this.wordToFilter.length) {
      schema['normal-style'] = true;
    }
    if (this.showBackgroundCover()) {
      schema['search-active'] = true;
    }
    if (this._config.flags.isMiniSearch) {
      schema['mini-mode'] = true;
    }
    if (this.hasWhiteBackground()) {
      schema['white-background'] = true;
    }

    return schema;
  }


  public getCss(): string {
    let css = _.isString(this._config.css.extraClasses) && !_.isEmpty(this._config.css.extraClasses)
      ? this._config.css.extraClasses
      : '';

    if (_.isString(this.css) && !_.isEmpty(this.css)) {
      css += ' ' + this.css;
    }

    return css.trim();
  }

  /**
   * Get if search component has data to filter
   * @returns {boolean}
   */
  public hasDataToFilter(): boolean {
    return !_.isNil(this.dataFiltered) && this.dataFiltered.length > 0;
  }

  /**
   * Determine if it mini search component
   * @returns {boolean}
   */
  public isMiniSearch(): boolean {
    return this._config.flags.isMiniSearch;
  }


  /**
   * On click hover result list element. If the item selected is the same, dont run any action
   * @param e
   * @param {Object} element
   */
  public onClickResultListElement(e, element: object): void {
    e.stopPropagation();
    e.preventDefault();

    if (element !== this._optionSelected) {
      this.selectElementFromResultList(element);

    } else {
      this._isFocusingInput = false;
    }
  }

  /**
   * Background Cover event click
   */
  public onClickHover(e: Event = null): void {
    if (!_.isNil(e)) {
      e.stopPropagation();
      e.preventDefault();
    }
    this.wordToFilter = this.searchInput.nativeElement.value;
    this.placeHolder = this._config.input.placeholder;
    this._isFocusingInput = false;
    this.emitComponentState('blur');
  }

  /**
   * Replace and get the error message on DOM
   * @returns {string}
   */
  public getErrorMessage(): string {
    return this._config.error.message.replace('{{wordToFilter}}', this.wordToFilter);
  }

  /**
   * On focus input element event
   */
  public onFocus(): void {
    this._isFocusingInput = true;
    // this.placeHolder = this.config.flags.showBackgroundCover
    //   ? ''
    //   : this.config.input.placeholder;

    this.copyDataToFilterArray();

    if (this.isApiLiveMode() && !this.isValidWordToFilter()) {
      this.data = [];
    }

    this.filterData();
  }

  /**
   * On focusout component event
   */
  public onFocusout(): void {
    this.emitComponentState('focusout');
  }


  /**
   * Method what control on key up event filtering data
   */
  public onKeyUpEvent(): void {
    this._optionSelected = null;

    if (!this.isApiLiveMode()) {
      this.filterData();

    } else if (this.isValidWordToFilter()) {
      clearTimeout(this._config.api.timer);

      const searchTimer = _.isEmpty(this.data)
        ? this._timeoutKeyup.first
        : this._timeoutKeyup.last;

      this._config.api.timer = setTimeout(() => {
        this.getDataFromApi();
      }, searchTimer);

    } else {
      this.data = [];
      this.emitComponentState('keyup');
      // this.restartComponent();

    }
  }

  /**
   * Select a result from list to autocomplete the search process
   * @param element: Object
   */
  public selectElementFromResultList(element: object): void {
    if (this._config.flags.isEnabledClickOnElementList) {
      this.dataFiltered = [element];
      this.wordToFilter = this.getTitleByKey(element, 'title');
      this.onListElementClicked(element);
      this.observeChangesOnRestart();
      this._isFocusingInput = false;
      this._optionSelected = element;
    }
  }

  /**
   * Set the flag to show the list under any doubts
   */
  public setShowListUnderMandatory(): void {
    this.copyDataToFilterArray();
    this._showListUnderMandatory = !this._showListUnderMandatory;
  }

  /**
   * Return if component has focus in view
   * @returns {boolean}
   */
  public hasFocus(): boolean {
    return this._isFocusingInput;
  }

  /**
   * Determine if to show or not the background cover for close event click
   * @returns {boolean}
   */
  public showBackgroundCover(): boolean {
    return this.hasFocus() && this._config.flags.showBackgroundCover;
  }

  /**
   * Show/hide by widgetConfig error component
   * @returns {boolean}
   */
  public showComponentErrorMessage(): boolean {
    return this._config.error.show && this.thereIsNoResultsSearchedWriting();
  }

  /**
   * Determine if to show or not the background cover for close event click with especial attention
   * @returns {boolean}
   */
  public showFocusAttention(): boolean {
    return this.showBackgroundCover() && this._config.flags.showCenterAttention;
  }

  /**
   * Show list of item available for word used to filter in data
   * @returns {boolean}
   */
  public showResultList(): boolean {
    if (this._config.flags.showResultListUnderClick
      && this._isFocusingInput
      && this.hasDataToFilter()) {
      return true;
    } else {
      if (this._config.flags.showResultList
        && this._isFocusingInput
        && this.hasDataToFilter()
        && this.isEnabledDynamicSearchWhileWriting()
        && this.userIsSearchingWriting()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Determine if show/hide Button according to widgetConfig
   * @returns {boolean}
   */
  public showSearchIcon(): boolean {
    return this.showIcon()
      && this._config.icon.hasSearchIcon
      && !this._isFocusingInput
      && !this.showCloseOptionSelected();
  }

  /**
   * Show icon to display all the list with results
   * @returns {boolean}
   */
  public showSelectabletIcon(): boolean {
    return this._config.icon.hasSelectableIcon
      && !this.userIsSearchingWriting()
      && this.hasDataToFilter();
  }

  public showCloseOptionSelected(): boolean {
    return this._config.icon.showUnselectIcon
      && (this.hasOptionSelected() || (this._config.flags.forcedUnselectIconShow && !_.isEmpty(this.wordToFilter)));
  }

  /**
   * Show loading icon when component is calling to Api
   * @returns {boolean}
   */
  public isLoadingSearch(): boolean {
    return this.isApiLiveMode() && this._callToApi;
  }

  /**
   * Get if there are results after search event
   * @returns {boolean}
   */
  public thereIsResultsFound(): boolean {
    return !_.isNil(this.dataFiltered) && this.dataFiltered.length > 0;
  }


  public unselectOption(): void {
    this._optionSelected = null;
    if (this._config.flags.showResultListUnderUnselect) {
      this.searchInput.nativeElement.focus();
    } else {
      this._isFocusingInput = false;
    }
    this.cleanInputSearch();
    this.copyDataToFilterArray();
    this.emitComponentState('unselect');
  }

  /** PRIVATE METHODS **/

  /**
   * Return if all elements on array has id attribute
   * @return {boolean}
   */
  private allDataElementsHasSingleId(): boolean {
    return this.data.every((element) => {
      return element.hasOwnProperty('id');
    });
  }

  /**
   * Copy from original data to model filter
   */
  private copyDataToFilterArray(): void {
    if (!_.isNull(this.data) && !_.isUndefined(this.data)) {
      this.dataFiltered = [...this.data];
    }
  }

  /**
   * Check if the search component has data to filter
   */
  private dataHasElement(): boolean {
    return this.data
      ? this.data.length > 0
      : false;
  }

  /**
   * Emit current state of component to parent
   *
   * @param {string} event
   * @param {string} msg
   */
  private emitComponentState(event: string = '',
                             msg: string = null): void {
    this.outputEvent.emit(
      {
        id: this._config.id,
        resultsFound: this.thereIsResultsFound(),
        currentText: this.wordToFilter,
        data: this.dataFiltered,
        currentConfig: this._config,
        itHasACorrectValue: this.hasOptionSelected(),
        originalData: this.data,
        event: event,
        message: msg
      }
    );
  }

  /**
   * Emit restart status for parent component
   */
  private emitRestartStatus(): void {
    this.onRestartEvent.emit({
      id: this._config.id,
      event: 'restart',
      restartStatusOnComponent: this.restart,
      infoAtThisPoint: {
        currentText: this.wordToFilter,
        config: this._config,
        data: this.data
      }
    });
  }

  /**
   * Return index on data
   * @param element
   * @return {number}
   */
  private getIndexOnArray(element: object): number {
    return this.data.findIndex((eOnArray) => {
      return element['id'] === eOnArray['id'];
    });
  }

  /**
   * Get if search component must have white background
   * @returns {boolean}
   */
  private hasWhiteBackground(): boolean {
    return this._config.flags.hasWhiteBackground;
  }

  /**
   * Determine search has to been done while you are writing
   * @returns {boolean}
   */
  private isEnabledDynamicSearchWhileWriting(): boolean {
    return this._config.flags.hasFilterOnRealTime;
  }

  /**
   * Determine typeof of values on array to resolve if it is plain or with objects
   * @param list: Array<any>
   * @returns {boolean}
   */
  private isPlainArray(list: Array<any>): boolean {
    return _.filter(list, function (item) {
      return _.isString(item);
    }).length > 0;
  }

  /**
   * Give identification for each element on array if not provided
   */
  private makeReadyDataIdentify(): void {
    if (this.dataHasElement()
      && !this.allDataElementsHasSingleId()) {
      this.data = this.data.map((element, key) => {
        delete element['id'];
        element['id'] = key;
        return element;
      });
    }
    this.copyDataToFilterArray();
  }

  /**
   * Method to control changes on Input restart component
   */
  private observeChangesOnRestart(): void {
    if (this.restart) {
      this.restart = false;
      this.restartComponent();
    }
  }

  /**
   * Output event to parent when one element from list is clicked
   * @param element
   */
  private onListElementClicked(element: object): void {
    this.onClickElementEvent.emit({
      idComponent: this._config.id,
      event: 'click',
      element: element,
      index: this.getIndexOnArray(element)
    });
  }

  /**
   * Restart the component to an initial status
   */
  private restartComponent(): void {
    this.emitRestartStatus();
    this.cleanInputSearch();
    this.parseDefaultValue();
    this.copyDataToFilterArray();
  }
  /**
   * Determine if show/hide icon according to widgetConfig
   * @returns {boolean}
   */
  private showIcon(): boolean {
    return this._config.icon.hasIconRight;
  }

  /**
   * Return if the user is searching writing and there is no results
   * @returns {boolean}
   */
  private thereIsNoResultsSearchedWriting(): boolean {
    return !this.thereIsResultsFound() && this.userIsSearchingWriting();
  }

  /**
   * Determine if the user is searching
   * @returns {boolean}
   */
  private userIsSearchingWriting(): boolean {
    return _.size(this.wordToFilter) > 0;
  }

  /**
   * Return a title option
   *
   * @param {Object} element
   * @param {string} keyTitle
   * @param {boolean} isOptionTitle
   * @returns {string}
   */
  private getTitleByKey(element: Object = null,
                        keyTitle: string = 'title',
                        isOptionTitle: boolean = false): string {
    if (_.isNil(element)) {
      return '';

    } else if (this.isPlainArray(this.data)) {
      return element.toString();
    }

    const titleValue: string = Array.isArray(this._config[keyTitle])
      ? SearchComponent.joinByKeys(element, this._config[keyTitle])
      : TextParserPipe.noNullString(element[this._config[keyTitle]]);

    if (!_.isEmpty(this._config.subtitle) && !isOptionTitle) {
      const subtitleValue: string = Array.isArray(this._config.subtitle)
        ? SearchComponent.joinByKeys(element, this._config.subtitle, ', ')
        : TextParserPipe.noNullString(element[this._config.subtitle]);

      return TextParserPipe.noNullToStringArray([titleValue, subtitleValue], this._config.separator);
    }

    return titleValue;
  }


  /** API METHODS **/
  /**
   * Control all the fails responses coming from the API
   *
   * @param {Object} response
   * @param {string} msg
   */
  private emitOnErrorApiResponse(response: object,
                                 msg: string = null): void {
    if (this._config.api.showError) {
      this.emitComponentState(
        'loading_error',
        !isNull(msg)
          ? msg
          : SearchComponent.isValidObjectProperty(response, 'message')
          ? response['message']
          : 'Error desconocido. No se pudo procesar la petición'
      );
    }
  }

  /**
   * Notify the api call status through emitter event
   * @param {boolean} status
   */
  private emitShowLoader(status: boolean = true): void {
    this._callToApi = status;
    this.emitComponentState(
      status === true
        ? 'loading'
        : 'loaded'
    );
  }


  /**
   * Get component data
   */
  private getDataFromApi(): void {
    if (this.isValidWordToFilter()) {
      this.emitShowLoader();

      const query = this._config.api.queryString;

      if (!_.isNil(this._config.api.queryStringExtra)) {
        const wordQuery = _.isNil(this.wordToFilter) ? '' : '%' + this.wordToFilter + '%';
        const extraQueryList = _.isArray(this._config.api.queryStringExtra)
          ? this._config.api.queryStringExtra
          : [this._config.api.queryStringExtra];

        for (const extraQuery of extraQueryList) {
          query[extraQuery] = wordQuery;
          // @TODO: aqui debería construir la consultar como OR
        }
      }

      this._httpSrv
        .get(
          this._config.api.endpoint,
          query,
          undefined,
          false,
          false
        )
        .subscribe(
          (responseOk) => this.onSuccessGetData(responseOk),
          (responseFail) => this.emitOnErrorApiResponse(responseFail)
        );
    }
  }

  /**
   * On success get data
   * @param {Object | any} response
   */
  private onSuccessGetData(response: object | any): void {
    if (response['success'] && SearchComponent.isValidObjectResponse(response)) {
      this.data = _.cloneDeep(response['object']['items']);
      this.makeReadyDataIdentify();
      this.emitShowLoader(false);

    } else {
      this.emitOnErrorApiResponse(response);
    }
  }


  /**
   * Data filtering in search action
   */
  private filterData(): void {
    if (this.isEnabledDynamicSearchWhileWriting()) {
      const isPlainArray = this.isPlainArray(this.data),
        keyToFilter = this._config.keyToFilter,
        alternativeKeysToFilter = this._config.alternativeKeysToFilter,
        text = _.deburr(_.trim(this.wordToFilter)).toLowerCase();

      this.dataFiltered = _.filter(this.data, function (item, key) {
        if (!isPlainArray) {
          let textToFilter = item[keyToFilter];

          if (!_.isEmpty(alternativeKeysToFilter)) {
            for (const aKey of alternativeKeysToFilter) {
              textToFilter += item[aKey];
            }
          }
          return _.includes(_.deburr(_.trim(textToFilter)).toLowerCase(), text);
        }

        return _.includes(_.deburr(_.trim(item[key])).toLowerCase(), text);
      });

      this._uniqueResultFound = !_.isNil(this.dataFiltered) && this.dataFiltered.length === 1;
      this.emitComponentState('keyup');
    }
  }

  /**
   * Return if an option has been selected
   * @returns {boolean}
   */
  private hasOptionSelected(): boolean {
    return !_.isNil(this._optionSelected) && !_.isEmpty(this._optionSelected);
  }

  /**
   * Return if is active live mode for data update
   * @returns {boolean}
   */
  private isApiLiveMode(): boolean {
    return !_.isNull(this._config.api.endpoint)
      && this._config.flags.dataOnLive;
  }

  /**
   * Return if a string is valid format to search
   * @returns {boolean}
   */
  private isValidWordToFilter(): boolean {
    return !_.isNil(this.wordToFilter) && !_.isEmpty(this.wordToFilter.trim());
  }


  /**
   * Returns a defaultValue index (text generated) if exist in data
   * @param {string} defaultValue
   * @returns {boolean}
   */
  private getDefaultValueIndex(defaultValue: string): number {
    if (_.isEmpty(defaultValue)) {
      return -1;
    }

    return this.data.findIndex((option) => {
      return this.getElementValue(option) === defaultValue;
    });
  }

  /**
   * Save how object the defaultVale if exist in data array. Null other case
   */
  private parseDefaultValue(): void {
    const defaultIdx = this.getDefaultValueIndex(
      typeof this.defaultValue === 'object'
                  ? this.getElementValue(this.defaultValue)
                  : this.defaultValue
    );


    this._optionDefault = defaultIdx !== -1
      ? this.data[defaultIdx]
      : null;

    this.wordToFilter = isNull(this._optionDefault)
      ? typeof this.defaultValue === 'object'
        ? ''
        : this.defaultValue
      : this.getElementValue(this._optionDefault);

    this._config.input.value = this.wordToFilter;
    this._optionSelected = this._optionDefault;
  }
}
