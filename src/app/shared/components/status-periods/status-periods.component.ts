import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {SelectItem} from 'primeng/primeng';

import {BaseComponentModel} from '../../models/base-component/base-component.class';

import {HttpSrvService} from '../../services/http.service';
import {PersonModel} from '../../models/person.model';
import {
  StatusPeriodsRangeFilterModel
} from '../../models/status-periods/status-periods-range-filter.model';
import {TeamModel} from '../../models/team/team.model';
import {StatusPeriodsModel} from '../../models/status-periods/status-periods.model';
import {StatusPeriodsFilterModel} from '../../models/status-periods/status-periods-filter.model';
import {StatusPeriodsDateModel} from '../../models/status-periods/status-periods-date.model';
import {StatusPeriodsTeamModel} from '../../models/status-periods/status-periods-team.model';


const moment = require('moment');
const _ = require('lodash');

@Component({
  selector: 'app-status-periods-component',
  templateUrl: './status-periods.component.html',
  styleUrls: ['./status-periods.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class StatusPeriodsComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() person: PersonModel;
  @Input() team: TeamModel;
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() onChangeStatusPeriods = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('initDayRange') private _initDayRange;
  @ViewChild('endDayRange') private _endDayRange;
  @ViewChild('initMonthRange') private _initMonthRange;
  @ViewChild('endMonthRange') private _endMonthRange;

  /************************ COMPONENT VARS  ************************/

  public cost = '-';
  public workingTime = '-';

  /************************ COMPONENT CONFIG  ************************/

  public dropdownOptions: SelectItem[] = [
    {label: 'Comparar días', value: 'days'},
    {label: 'Comparar meses', value: 'months'}
  ];
  public selectedDropdownOption: string = this.dropdownOptions[0].value;

  private _initStatusPeriodsFilter: StatusPeriodsRangeFilterModel;
  private _endStatusPeriodsFilter: StatusPeriodsRangeFilterModel;

  private _initStatusPeriods: StatusPeriodsModel;
  private _comparativeStatusPeriods: StatusPeriodsModel;

  /************************ NATIVE METHODS  ************************/

  constructor(private _httpSrv: HttpSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'status_periods_filter_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  /**
   * Get title to the widget
   * @returns {string}
   */
  public getBillingOrSalaryTitle(): string {
    if (!_.isNil(this.person)
      && this.person.id !== 0
      && !_.isNil(this.person.cost)) {
      return this.person.cost.salary !== 0 ? 'Coste' : this.person.cost.billing !== 0 ? 'Tarifa' : 'Coste';
    }
    return 'Coste';
  }

  /**
   * Check if person has billing or salary
   * @returns {string}
   */
  public hasBillingOrSalary(): boolean {
    return !_.isNil(this.person)
      && this.person.id !== 0
      && !_.isNil(this.person.cost)
      && (this.person.cost.billing !== 0 || this.person.cost.salary !== 0);
  }

  /**
   * Check if person has billing or salary
   * @returns {string}
   */
  public hasWorkingTime(): boolean {
    return this.workingTime !== '-';
  }
  /**
   * Check if month
   * @returns {boolean}
   */
  public isMonthRange(): boolean {
    return this.selectedDropdownOption === 'months';
  }

  /**
   * On change selected dropdown option
   */
  public onChangeSelectedDropdownOption(): void {
    this.emitOnStatusPeriodsEvent(true);
  }

  /**
   * On change range
   * @param {StatusPeriodsFilterModel} event
   */
  public onChangeRange(event: StatusPeriodsRangeFilterModel, calendar: string) {
    if (event.mode === 'init') {
      this.workingTime = '-';
      if (!_.isNil(event.initDate) && !_.isNil(event.endDate)) {
        this._initStatusPeriodsFilter = event;

        if (calendar === 'initDayRange' && event.closeCalendar) {
          this._initDayRange.dayRangeDatepicker.overlayVisible = false;
        } else if (calendar === 'initMonthRange'  && event.closeCalendar) {
          setTimeout( () => {
            this._initMonthRange.monthRangeDatepicker.hideCalendar();
          }, 200);
        }

        this.getStatusPeriods('init', true);
      } else {
        this._initStatusPeriods = null;
        this.emitOnStatusPeriodsEvent();
      }
    } else if (event.mode === 'end') {
      if (!_.isNil(event.initDate) && !_.isNil(event.endDate)) {
        this._endStatusPeriodsFilter = event;

        if (calendar === 'endDayRange'  && event.closeCalendar) {
          this._endDayRange.dayRangeDatepicker.overlayVisible = false;
        } else if (calendar === 'endMonthRange'  && event.closeCalendar) {
          setTimeout( () => {
            this._endMonthRange.monthRangeDatepicker.hideCalendar();
          }, 200);
        }

        this.getStatusPeriods('end', false);
      } else {
        this._comparativeStatusPeriods = null;
        this.emitOnStatusPeriodsEvent();
      }
    }
  }

  /************************ API METHODS  ************************/

  /**
   * Get status periods to actual mate
   */
  private getStatusPeriods(type: string = 'init', workingTime: boolean = false): void {
    if (!_.isNil(this.person)
      && this.person.id !== 0
      && (!_.isNil(this._initStatusPeriodsFilter) || !_.isNil(this._endStatusPeriodsFilter))) {

      const startDate = type === 'init' ?
        'range(' + this._initStatusPeriodsFilter.initDate.format() + ',' +  this._initStatusPeriodsFilter.endDate.format() + ')' :
        'range(' + this._endStatusPeriodsFilter.initDate.format() + ',' +  this._endStatusPeriodsFilter.endDate.format() + ')';

      const endDate = type === 'init' ?
        'range(' + this._initStatusPeriodsFilter.initDate.format() + ',' +  this._initStatusPeriodsFilter.endDate.format() + ')' :
        'range(' + this._endStatusPeriodsFilter.initDate.format() + ',' +  this._endStatusPeriodsFilter.endDate.format() + ')';

      this._httpSrv
        .get(
          'statusperiods/person/' + this.person.id,
          {
            startDate: startDate,
            endDate: endDate,
          }
        )
        .subscribe(
          (responseOk) => this.onSuccessGetStatusPeriods(responseOk, type, workingTime)
        );
    } else if (!_.isNil(this.team)
      && !_.isNil(this._initStatusPeriodsFilter)) {
      // @ToDo: Incluir llamada para equipos cuando se defina su funcionalidad
    }
  }

  /**
   * On success get status periods
   * @param {Object | any} response
   */
  private onSuccessGetStatusPeriods(response: object | any, type: string = 'init', workingTime: boolean = false): void {
    if (!_.isNil(this.person)
      && response['success']
      && this.isValidObjectProperty(response, 'object')
      && this.isValidObjectProperty(response['object'], 'dates')
      && this.isValidObjectProperty(response['object'], 'teams')) {

      if (type === 'init') {
        if (this.isMonthRange()) {
          this._initStatusPeriods = Object.assign(new StatusPeriodsModel(),
            this.buildMonthData(new StatusPeriodsModel(response['object']), workingTime));
        } else {
          this._initStatusPeriods = Object.assign(new StatusPeriodsModel(),
            this.buildDayData(new StatusPeriodsModel(response['object']), workingTime));
        }
      } else if (type === 'end') {
        if (this.isMonthRange()) {
          this._comparativeStatusPeriods = Object.assign(new StatusPeriodsModel(),
            this.buildMonthData(new StatusPeriodsModel(response['object']), workingTime));
        } else {
          this._comparativeStatusPeriods = Object.assign(new StatusPeriodsModel(),
            this.buildDayData(new StatusPeriodsModel(response['object']), workingTime));
        }
      }

      this.emitOnStatusPeriodsEvent();

    } else if (!_.isNil(this.team)
      && response['success']
      && this.isValidObjectProperty(response, 'object')
      && this.isValidObjectProperty(response['object'], 'dates')
      && this.isValidObjectProperty(response['object'], 'teams')) {
      // @ToDo: Incluir construccion para equipos cuando se defina su funcionalidad
    }
  }

  /************************ AUXILIAR METHODS  ************************/

  /**
   * On change range
   * @param {boolean} reset
   */
  private emitOnStatusPeriodsEvent(reset: boolean = false) {
    if (!_.isNil(this.person) && this.person.id !== 0) {

      if (reset) {
        this._initStatusPeriods = null;
        this._comparativeStatusPeriods = null;
      }

      this.onChangeStatusPeriods.emit(
        new StatusPeriodsFilterModel(
          this.person,
          this._initStatusPeriods,
          this._comparativeStatusPeriods
        )
      );
    }
  }

  // Build day data
  private buildDayData(data: StatusPeriodsModel, workingTime: boolean = false): StatusPeriodsModel {

    // Format day labels
    data.dates =  _.map(data.dates, (date) => {
      date.date = moment(date.date).format('DD/MM/YYYY');
      return date;
    });

    // Insert teams in all dates
    data.dates.forEach( date => {
      data.teams.forEach( originalTeam => {
        const hasTeam = date.teams.filter(team => {
          return team.id === originalTeam.id;
        }).length > 0;
        if (!hasTeam) {
          date.teams.push(new StatusPeriodsTeamModel(originalTeam.id, {name: originalTeam.name}));
        }
      });
    });

    // Refresh working time

    if (workingTime) {
      let auxWorkingTime = 0;
      data.dates.forEach( scopeAnalyticsDateObj => {
        auxWorkingTime += scopeAnalyticsDateObj.status.working;
      });
      this.workingTime = auxWorkingTime !== 0 ? (Number(auxWorkingTime) / 60).toFixed(2) : '0.00';
    }

    return data;
  }

  // Build month data
  private buildMonthData(data: StatusPeriodsModel, workingTime: boolean = false): StatusPeriodsModel {

    const auxStatusPeriods = new StatusPeriodsModel();

    // Set total
    auxStatusPeriods.total += data.total;

    // Set total teams
    data.teams.forEach( originalTeam => {
      const hasTeam = auxStatusPeriods.teams.find( team => {
        return team.id === originalTeam.id;
      });

      if (!_.isNil(hasTeam)) {
        hasTeam.value += originalTeam.value;
      } else {
        auxStatusPeriods.teams.push(originalTeam);
      }
    });

    // Set month labels
    auxStatusPeriods.dates = this.extractMonths(data);

    // Insert teams in all dates
    auxStatusPeriods.dates.forEach( date => {
      data.teams.forEach( originalTeam => {
        date.teams.push(new StatusPeriodsTeamModel(originalTeam.id, {name: originalTeam.name}));
      });
    });

    // Find data by month
    auxStatusPeriods.dates.forEach( scopeAnalyticsDateObj => {
      const month =  moment(scopeAnalyticsDateObj.date).format('MMM YYYY');

      data.dates.forEach( originalScopeAnalyticsDateObj => {

        if (moment(month).isSame(moment(originalScopeAnalyticsDateObj.date).format('MMM YYYY'))) {

          // Set values
          scopeAnalyticsDateObj.total += originalScopeAnalyticsDateObj.total;
          scopeAnalyticsDateObj.withoutteam += originalScopeAnalyticsDateObj.withoutteam;
          scopeAnalyticsDateObj.status.lease += originalScopeAnalyticsDateObj.status.lease;
          scopeAnalyticsDateObj.status.meeting += originalScopeAnalyticsDateObj.status.meeting;
          scopeAnalyticsDateObj.status.travel += originalScopeAnalyticsDateObj.status.travel;
          scopeAnalyticsDateObj.status.working += originalScopeAnalyticsDateObj.status.working;
          scopeAnalyticsDateObj.status.pause += originalScopeAnalyticsDateObj.status.pause;

          // Set teams
          originalScopeAnalyticsDateObj.teams.forEach(originalTeam => {
            const hasTeam = scopeAnalyticsDateObj.teams.find(team => {
              return team.id === originalTeam.id;
            });

            if (!_.isNil(hasTeam)) {
              hasTeam.value = (Number(hasTeam.value) + Number(originalTeam.value)).toString();
            }
          });
        }
      });
    });

    // Refresh working time

    if (workingTime) {
      let auxWorkingTime = 0;
      data.dates.forEach( scopeAnalyticsDateObj => {
        auxWorkingTime += scopeAnalyticsDateObj.status.working;
      });
      this.workingTime = auxWorkingTime !== 0 ? (Number(auxWorkingTime) / 60).toFixed(2) : '0.00';
    }

    return auxStatusPeriods;
  }

  /**
   * Extract months
   * @param {ScopeAnalyticsDataModel} data
   * @returns {Array<string>}
   */
  private extractMonths(data: StatusPeriodsModel): Array<StatusPeriodsDateModel> {
    const months = [];
    data.dates.forEach( scopeAnalyticsDateObj => {
      const auxScopeAnalyticsDateModel = new StatusPeriodsDateModel(moment(scopeAnalyticsDateObj.date).format('MMM YYYY'));

      const hasMonth = months.find(month => {
        const dateFormat = moment(scopeAnalyticsDateObj.date).format('MMM YYYY');
        return month.date === dateFormat;
      });

      if (_.isNil(hasMonth)) {
        months.push(auxScopeAnalyticsDateModel);
      }
    });
    return months;
  }
}
