'use strict';

import {Component, OnInit, Input, Output, ViewChild, EventEmitter} from '@angular/core';
import {Router} from '@angular/router';

import {OverlayPanel} from 'primeng/primeng';
import {HttpSrvService} from '../../services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-table-menu',
  templateUrl: './table-menu.component.html',
  styleUrls: ['./table-menu.component.css']
})
export class TableMenuComponent implements OnInit {

  @Input() public id: number;
  @Input() public removable: boolean;
  @Input() public deleteUrl: string;
  @Input() public goToUrl: string;
  @Input() public editText = 'Editar';
  @Input() public deleteText = 'Eliminar';

  @Output() outputEvent = new EventEmitter();

  @ViewChild('confirmDialogComponent') public confirmDialogComponent;

  private _confirmDialogConfig = {
    header: 'Se eliminará el elemento seleccionado.',
    message: '¿Desea continuar?',
    accept: {
      action: 'onDelete',
      button: {
        text: 'Si'
      }
    }
  };


  constructor(private _httpSrv: HttpSrvService,
              private _router: Router) { }

  ngOnInit() {
  }


  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getConfirmDialogSetup(): object {
    return this._confirmDialogConfig;
  }



  /************************ EVENTS METHODS ************************/

  /**
   * Toggle overlaPanel
   * @param {any} event
   * @param {OverlayPanel} overlayPanel
   */
  public onToggleMenu(event, overlayPanel: OverlayPanel) {
    overlayPanel.toggle(event);
  }


  /**
   * On click edit button
   * @param {OverlayPanel} overlayPanel
   */
  public onClickEdit(overlayPanel?: OverlayPanel): void {
    if (overlayPanel) {
      overlayPanel.hide();
    }

    this._router.navigateByUrl(this.goToUrl + this.id.toString());
  }


  /**
   * On click delete button
   * @param {OverlayPanel} overlayPanel
   */
  public onClickDelete(overlayPanel?: OverlayPanel): void {
    if (overlayPanel) {
      overlayPanel.hide();
    }

    this.showConfirmDialog();
  }


  /**
   * Show confirm dialog for delete
   */
  public showConfirmDialog(): void {
    this.confirmDialogComponent.showDialog(event);
  }


  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']](event['params']);
    }
  }

  /************************ API METHODS ************************/

  /**
   * On delete entity
   */
  private onDelete(): void {
    if (!_.isNil(this.id) && !_.isNil(this.deleteUrl)) {

      this._httpSrv
        .delete(this.deleteUrl + this.id.toString())
        .subscribe(
        (response) => {
          if (response['success']) {
            this.outputEvent.emit({target: 'app-table-menu', action: 'reset'});
          }
        },
        () => {
          this.outputEvent.emit({target: 'app-table-menu', action: 'error'});
        }
      );
    }
  }
}
