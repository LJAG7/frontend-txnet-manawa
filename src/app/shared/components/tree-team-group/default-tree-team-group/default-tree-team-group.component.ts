import {Component, Input, Output, EventEmitter, OnChanges, OnInit, SimpleChanges} from '@angular/core';

import {BaseComponentModel} from '../../../models/base-component/base-component.class';
import {NodeModel} from '../../../models/tree-team-group/node.model';

import {AuthSrvService} from '../../../services/auth.service';

const _ = require('lodash');

@Component({
  selector: 'app-default-tree-team-group-component',
  templateUrl: './default-tree-team-group.component.html',
  styleUrls: ['./default-tree-team-group.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class DefaultTreeTeamGroupComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() data: Array<NodeModel> = [];
  @Input() mode: string;
  @Input() selectedNode: NodeModel;
  @Input() type: string;
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() onSelectNodeEvent = new EventEmitter();

  /************************ COMPONENT VARS  ************************/

  /************************ COMPONENT CONFIG  ************************/

  /************************ NATIVE METHODS  ************************/

  constructor(private _authSrv: AuthSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'default_tree_team_group_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void { }

  /************************ EVENTS METHODS ************************/

  /**
   * Get component mode
   * @returns {string}
   */
  public getComponentMode(): string {
    return this.mode === 'teams' ? 'equipo' : 'grupo';
  }

  /**
   * Get if tree-team-group has data
   * @returns {boolean}
   */
  public hasData(): boolean {
    return !_.isNil(this.data) && !_.isEmpty(this.data);
  }

  /**
   * Get if person logged is MATE
   * @returns {boolean}
   */
  public isMate(): boolean {
    return this._authSrv.getPersonLogged().isMate();
  }

  /**
   * Check if node is selected
   * @param node
   * @returns {boolean}
   */
  public isSelectedNode(node: NodeModel): boolean {
    return !_.isNil(this.selectedNode)
      && node.id === this.selectedNode.id;
  }

  /**
   * On toggle selected node
   * @param {Object} node
   */
  public onToggleSelectedNode(node: NodeModel): void  {
    if (!this.isMate()) {
      this.selectedNode = _.isNil(this.selectedNode) ?
        node : this.selectedNode.id !== node.id ? node : null;
      this.onSelectNodeEvent.emit({
        id: this.config.id,
        tree: this.mode,
        view: this.type,
        node: this.selectedNode
      });
    }
  }

  /************************ API METHODS  ************************/

  /************************ AUXILIAR METHODS  ************************/

}
