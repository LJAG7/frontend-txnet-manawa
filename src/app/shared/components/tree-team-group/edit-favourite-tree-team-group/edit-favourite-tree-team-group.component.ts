import {Component, Input, Output, EventEmitter, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';

import {BaseComponentModel} from '../../../models/base-component/base-component.class';
import {NodeModel} from '../../../models/tree-team-group/node.model';

import {HttpSrvService} from 'app/shared/services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-edit-favourite-tree-team-group-component',
  templateUrl: './edit-favourite-tree-team-group.component.html',
  styleUrls: ['./edit-favourite-tree-team-group.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class EditFavouriteTreeTeamGroupComponent  extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() allData: Array<object> = [];
  @Input() data: Array<NodeModel>;
  @Input() editMode = false;
  @Input() level = 0;
  @Input() mode: string;
  @Input() search = true;
  @Input() type: string;
  @Input() visible = true;
  @Input() addNode: NodeModel;
  @Input() deleteNode: NodeModel;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() onSelectNodeEvent = new EventEmitter();
  @Output() onChangeFavouriteEvent = new EventEmitter();
  @Output() onDeleteNodeFromChildEvent = new EventEmitter();
  @Output() onAddNodeFromChildEvent = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild ('notFavouriteSearch') private _notFavouriteSearch;

  /************************ COMPONENT VARS  ************************/

  public restartSearchFavouriteComponent = 0;
  public searchData: Array<object> = [];
  public selectedNode: NodeModel;

  private expandedNodes: Array<NodeModel> = [];

  /************************ COMPONENT CONFIG  ************************/

  /************************ NATIVE METHODS  ************************/

  constructor(private _httpSrv: HttpSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'edit_favourite_tree_team_group_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ( (changes.hasOwnProperty('allData') && !_.isNil(changes.allData.currentValue))
      || (changes.hasOwnProperty('editMode') && !_.isNil(changes.editMode.currentValue)) ) {
      if (!_.isNil(this.allData) && this.isEditingFavourite()) {
        this.setSearchData();
      }
    }

    if (changes.hasOwnProperty('deleteNode') && !_.isNil(changes.deleteNode.currentValue) && this.level > 0) {
      const node = changes.deleteNode.currentValue;
      this.deleteTreeNodeFromParent(node);
    }

    if (changes.hasOwnProperty('addNode') && !_.isNil(changes.addNode.currentValue) && this.level > 0) {
      const node = changes.addNode.currentValue;
      this.addTreeNodeFromParent(node);
    }
  }

  /************************ EVENTS METHODS ************************/

  /**
   * Get component mode
   * @returns {string}
   */
  public getComponentMode(): string {
    return this.mode === 'teams' ? 'equipos' : 'grupos';
  }

  /**
   * Get child node
   * @param {NodeModel} node
   * @returns {Array<NodeModel>}
   */
  public getChildNode(node: NodeModel): Array<NodeModel> {
    return node.child_teams;
  }

  /**
   * Configure general search component
   * @returns {Object}
   */
  public getSearchFavouriteConfig(): object {
    return {
      flags: {
        isMiniSearch: true,
        dataOnLive: true,
        showResultList: true
      },
      error: {
        show: false
      },
      icon: {
        hasIconRight: true,
        hasSearchIcon: true,
        hideIconOnSearch: true
      },
      input: {
        placeholder: this.isTeamType()
          ? 'Busca un team'
          : this.isGroupType()
            ? 'Busca un group'
            : 'ERROR DE CONFIGURACIÓN'
      }
    };
  }

  /**
   * Get if tree-team-group has data
   * @returns {boolean}
   */
  public hasData(): boolean {
    return !_.isNil(this.data) && !_.isEmpty(this.data);
  }

  /**
   * Check if has data
   * @returns {boolean}
   */
  public hasSearch(): boolean {
    return this.search;
  }

  /**
   * Check if favorites are being edited
   * @returns {boolean}
   */
  public isEditingFavourite(): boolean {
    return this.editMode;
  }

  /**
   * Check if is expanded node
   * @param {NodeModel} node
   * @returns {boolean}
   */
  public isExpandedNode(node: NodeModel): boolean {
    return this.expandedNodes.findIndex( expandedNode => {
      return expandedNode['id'] === node.id;
    }) !== -1;
  }

  /**
   * Determine if a node is favourite
   * @param {NodeModel} node
   * @returns {boolean}
   */
  public isFavouriteNode(node: NodeModel): boolean {
    return !_.isNil(node.favourite_team);
  }

  /**
   * Get if tree-team-group component is to groups
   * @returns {boolean}
   */
  public isGroupType(): boolean {
    return this.mode === 'groups';
  }

  /**
   * Get if tree-team-group component is to team
   * @returns {boolean}
   */
  public isTeamType(): boolean {
    return this.mode === 'teams';
  }

  /**
   * On add favourite node
   * @param {NodeModel} node
   */
  public onAddFavouriteNode(node: NodeModel): void {
    if (this.level > 0) {
      this.onAddNodeFromChildEvent.emit({
        node: node
      });
    } else {
      this.addFavourite(node);
    }
  }

  /**
   * On add node from child
   * @param {Object} event
   */
  public onAddNodeFromChild(event: object): void {
    if (event.hasOwnProperty('node')) {
      this.onAddFavouriteNode(event['node']);
    }
  }

  /**
   * On delete favourite node
   * @param {NodeModel} node
   */
  public onDeleteFavouriteNode(node: NodeModel): void {
    if (this.level > 0) {
      this.onDeleteNodeFromChildEvent.emit({
        node: node
      });
    } else {
      this.deleteFavourite(node);
    }
  }

  /**
   * On delete node from child
   * @param {Object} event
   */
  public onDeleteNodeFromChild(event: object): void {
    if (event.hasOwnProperty('node')) {
      this.onDeleteFavouriteNode(event['node']);
    }
  }

  /**
   * On select element from general search result list
   * @param event
   */
  public onSelectSearchFavouriteElement(event: any): void {
    if (this.isValidObjectProperty(event, 'element')) {
      this.addFavourite(event['element']);
      this._notFavouriteSearch.unselectOption();
    }
  }

  /**
   * On toggle expanded node
   * @param {NodeModel} node
   */
  public onToggleExpandedNode(node: NodeModel): void {
    const isExpandedIndex = this.expandedNodes.findIndex( expandedNode => {
      return expandedNode['id'] === node.id;
    });

    if (isExpandedIndex === -1
      && node.num_child_teams > 0) {
      this.getChild(node);
    } else {
      node.child_teams = [];
      this.expandedNodes.splice(isExpandedIndex, 1);
    }
  }

  /************************ API METHODS  ************************/

  /**
   * Add a favourite team or group
   * @param {NodeModel} node
   */
  private addFavourite(node: NodeModel): void {
    this._httpSrv
      .post(
        this.isTeamType()
          ? 'persons/my/teams/favourite/'
          : 'persons/my/groups/favourite/',
        {id: node.id}
      )
      .subscribe(
        (responseOk) => this.onSuccessAddFavourite(responseOk)
      );
  }

  /**
   * On success favourite added
   * @param {Object} response
   */
  private onSuccessAddFavourite(response: object): void {
    if (response['success']
      && this.isValidObjectProperty(response, 'object')) {
      this.data.push(new NodeModel(response['object']));
      this.addNode = Object.assign(new NodeModel(), response['object']);
      this.onChangeFavouriteEvent.emit({
        favourites: this.data
      });
      this.setSearchData();
    }
  }

  /**
   * Delete a favourite team or group
   * @param {NodeModel} node
   */
  private deleteFavourite(node: NodeModel): void {
    this._httpSrv
      .delete(
        this.isTeamType()
          ? 'persons/my/teams/favourite/' + node['favourite_team']['id']
          : 'persons/my/groups/favourite/' + node['favourite_team']['id']
      )
      .subscribe(
        (responseOk) => this.onSuccessDeleteFavourite(responseOk, node)
      );
  }

  /**
   * On success delete favourite
   * @param {Object} response
   * @param {NodeModel} node
   */
  private onSuccessDeleteFavourite(response: object, node: NodeModel): void {
    if (response['success']) {
      const deleteNodeIndex = this.data.findIndex( element => {
        return element['id'] === node.id;
      });
      if (deleteNodeIndex !== -1) {
        this.data.splice(deleteNodeIndex, 1);
        this.deleteNode = Object.assign(new NodeModel(), node);
        this.onChangeFavouriteEvent.emit({
          favourites: this.data
        });
      }
      this.setSearchData();
    }
  }

  /**
   * Get child from node
   * @param {NodeModel} node
   */
  private getChild(node: NodeModel): void {
    this._httpSrv
      .get(
        'persons/my/teams/' + node.id + '/favourite/')
      .subscribe(
        (responseOk) => this.onSuccessGetChild(responseOk, node)
      );
  }

  /**
   * On succes get child
   * @param {Object | any} response
   * @param {NodeModel} node
   */
  private onSuccessGetChild(response: object | any,
                            node: NodeModel): void {
    if (response['success']
      && this.isValidObjectResponse(response)) {
      const nodeIndex = this.data.findIndex( element => {
        return element.id === node.id;
      });

      this.data[nodeIndex].child_teams = [];
      response['object']['items'].forEach( element => {
        this.data[nodeIndex].child_teams.push(new NodeModel(element));
      });

      this.expandedNodes.push(node);
    }
  }

  /************************ AUXILIAR METHODS  ************************/

  /**
   * Add node from parent
   * @param {NodeModel} node
   */
  private addTreeNodeFromParent(node: NodeModel): void {
    if (!_.isNil(node)) {
      const addNodeIndex = this.data.findIndex( element => {
        return element['id'] === node.id;
      });
      if (addNodeIndex !== -1) {
        this.data[addNodeIndex].favourite_team = node.favourite_team;
      }
      this.setSearchData();
    }
  }

  /**
   * Delete node from parent
   * @param {NodeModel} node
   */
  private deleteTreeNodeFromParent(node: NodeModel): void {
    if (!_.isNil(node)) {
      const deleteNodeIndex = this.data.findIndex( element => {
        return element['id'] === node.id;
      });
      if (deleteNodeIndex !== -1) {
        this.data[deleteNodeIndex].favourite_team = null;
      }
      this.setSearchData();
    }
  }

  /**
   * Set search data
   */
  private setSearchData(): void {
    this.searchData = this.allData.filter( element => {
      return this.data.findIndex( favourite => {
        return element['id'] === favourite['id'];
      }) === -1;
    });
  }

  public functionalityNotImplemented(e): void {
  }

}
