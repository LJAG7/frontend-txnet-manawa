import {Component, Input, Output, EventEmitter, OnChanges, OnInit, SimpleChanges, OnDestroy} from '@angular/core';
import 'rxjs/add/operator/takeWhile';

import {BaseComponentModel} from '../../../models/base-component/base-component.class';
import {NodeModel} from '../../../models/tree-team-group/node.model';

import {AuthSrvService} from '../../../services/auth.service';
import {DragulaService} from 'ng2-dragula';
import {HttpSrvService} from 'app/shared/services/http.service';

const _ = require('lodash');

@Component({
  selector: 'app-favourite-tree-team-group-component',
  templateUrl: './favourite-tree-team-group.component.html',
  styleUrls: ['./favourite-tree-team-group.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class FavouriteTreeTeamGroupComponent extends BaseComponentModel implements OnInit, OnChanges, OnDestroy {

  /************************ COMPONENT INPUTS  ************************/

  @Input() data: Array<NodeModel> = [];
  @Input() editMode = false;
  @Input() favouriteBackup: Array<NodeModel> = [];
  @Input() mode: string;
  @Input() selectedNode: NodeModel;
  @Input() type: string;
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() onSelectNodeEvent = new EventEmitter();
  @Output() onReorderFavouriteEvent = new EventEmitter();

  /************************ COMPONENT VARS  ************************/

  /************************ COMPONENT CONFIG  ************************/

  /************************ NATIVE METHODS  ************************/

  constructor(private _authSrv: AuthSrvService,
              private _dragulaSrv: DragulaService,
              private _httpSrv: HttpSrvService) {
    super();
    const bag: any = this._dragulaSrv.find('bag-items');
    if (bag !== undefined ) {
      this._dragulaSrv.destroy('bag-items');
    }
    this._dragulaSrv.setOptions('bag-items', {
      revertOnSpill: true,
      copy: false,
      copySortSource: false,
      ignoreInputTextSelection: true
    });
  }

  ngOnInit() {
    this.config.id = 'favourite_tree_team_group_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    this.enableDragula();
  }

  ngOnChanges(changes: SimpleChanges): void { }

  // Necessary to destroy drag observer when leaving the view

  public ngOnDestroy() {
    this.editMode = true;
    this._dragulaSrv.destroy('bag-items');
  }

  /************************ EVENTS METHODS ************************/

  /**
   * Get component mode
   * @returns {string}
   */
  public getComponentMode(): string {
    return this.mode === 'teams' ? 'equipos' : 'grupos';
  }

  /**
   * Get if tree-team-group has data
   * @returns {boolean}
   */
  public hasData(): boolean {
    return !_.isNil(this.data) && !_.isEmpty(this.data);
  }

  /**
   * Get if tree-team-group component is to groups
   * @returns {boolean}
   */
  public isGroupType(): boolean {
    return this.mode === 'groups';
  }

  /**
   * Get if person logged is MATE
   * @returns {boolean}
   */
  public isMate(): boolean {
    return this._authSrv.getPersonLogged().isMate();
  }

  /**
   * Get if tree-team-group component is to team
   * @returns {boolean}
   */
  public isTeamType(): boolean {
    return this.mode === 'teams';
  }

  /**
   * Check if node is selected
   * @param {NodeModel} node
   * @returns {boolean}
   */
  public isSelectedNode(node: NodeModel): boolean {
    return !_.isNil(this.selectedNode)
      && node.id === this.selectedNode.id;
  }

  /**
   * On toggle selected node
   * @param {NodeModel} node
   */
  public onToggleSelectedNode(node: NodeModel): void  {
    if (!this.isMate()) {
      this.selectedNode = _.isNil(this.selectedNode) ?
        node : this.selectedNode.id !== node.id ? node : null;
      this.onSelectNodeEvent.emit({
        id: this.config.id,
        tree: this.mode,
        view: this.type,
        node: this.selectedNode
      });
    }
  }

  /************************ API METHODS  ************************/

  /**
   * Delete a favourite team or group
   * @param {NodeModel} node
   * @param {number} index
   */
  private onUpdateFavouriteNode(node: NodeModel, index: number): void {
    if (!_.isNil(node.favourite_team)) {
      node.favourite_team.position = index + 1;
      const endPoint = this.isTeamType()
        ? 'persons/my/teams/favourite/'
        : 'persons/my/groups/favourite/';

      this._httpSrv
        .patch(
          endPoint + node.favourite_team.id,
          {
            position: node.favourite_team.position
          })
        .subscribe(
          (responseOk) => this.onSuccessUpdateFavouriteNode(responseOk)
        );
    }
  }

  /**
   * On success updated favourite node
   * @param {Object | any} response
   */
  private onSuccessUpdateFavouriteNode(response: object | any): void {
    if (response['success']) {
      for (let i = 0; i < this.data.length; i++) {
        this.data[i].favourite_team.position = i + 1;
      }

      this.onReorderFavouriteEvent.emit({
        id: this.config.id,
        tree: this.mode,
        view: this.type,
        data: this.data
      });
    }
  }

  /************************ AUXILIAR METHODS  ************************/

  /**
   * Enable dragula
   */
  private enableDragula(): void {
    this._dragulaSrv.drop.takeWhile(() => !this.editMode).subscribe(value => {
      const changeNodeName = value[1].innerText.replace(/\n/ig, '');
      setTimeout( () => {
        const changeNodeIndex = this.data.findIndex( node => {
          return node.name === changeNodeName;
        });
        this.onUpdateFavouriteNode(this.data[changeNodeIndex], changeNodeIndex);
      }, 300);
    });
  }
}
