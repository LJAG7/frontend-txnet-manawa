import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

import {SelectItem} from 'primeng/primeng';

import {BaseComponentModel} from '../../models/base-component/base-component.class';
import {NodeModel} from '../../models/tree-team-group/node.model';

import {HttpSrvService} from '../../services/http.service';
import {AuthSrvService} from "../../services/auth.service";
import {ActivatedRoute, Router} from "@angular/router";

const _ = require('lodash');

@Component({
  selector: 'app-tree-team-group-component',
  templateUrl: './tree-team-group.component.html',
  styleUrls: ['./tree-team-group.component.css']
})

/**
 *  @Author Elías Romero <elias.romero@steelmood.com>
 *  @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TreeTeamGroupComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() generalSearch = true;
  @Input() mode: string;
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() onSelectNodeEvent = new EventEmitter();

  /************************ COMPONENT VARS  ************************/

  public data: Array<NodeModel>;
  public dropdownOptions: SelectItem[];
  public selectedDropdownOption: SelectItem;

  public editFavourite = false;

  public restartGeneralSearchComponent = 0;
  public searchData: Array<object> = [];
  public selectedNode: NodeModel;

  private _defaultDropdownOptions: SelectItem[] = [
    {label: 'Ver ' + this.mode === 'teams' ? 'equipos' : 'grupos' + ' más vistos', value: {id: 0, name: 'most_visited'}},
    {label: 'Ver ' + this.mode === 'teams' ? 'equipos' : 'grupos' + ' sin padres', value: {id: 1, name: 'teams_root'}},
    {label: 'Ver ' + this.mode === 'teams' ? 'equipos' : 'grupos' + ' con más mates', value: {id: 2, name: 'most_mates'}},
    {label: 'Ver mis favoritos', value: {id: 3, name: 'teams_favourites'}},
  ];
  private favouriteBackup: Array<NodeModel>;

  /************************ COMPONENT CONFIG  ************************/

  private _apiSettup = {
    teams: {
      most_visited: {
        endpoint: 'teams/matesnumbers/?orderBy=numVisitedByUser&orderType=desc',
        offset: 6,
        counterParam: 'num_pending_notifications',
        option: {title: 'Ver equipos más vistos', action: null, disabled: false}
      },
      teams_root: {
        endpoint: 'teams/matesnumbers/?level=1&orderBy=name&orderType=asc',
        offset: 6,
        counterParam: 'num_pending_notifications',
        option: {title: 'Ver equipos sin padres', action: null, disabled: false}
      },
      most_mates: {
        endpoint: 'teams/matesnumbers/?orderBy=num_mates&orderType=desc',
        offset: 6,
        counterParam: 'num_mates',
        option: {title: 'Ver equipos con más mates', action: null, disabled: false}
      },
      teams_favourites: {
        endpoint: 'persons/my/teams/favourite/?',
        offset: 100,
        counterParam: null,
        option: {title: 'Ver mis favoritos', action: {icon: 'fa-cog', success: 'onToggleGroupsFavoritesMode'}, disabled: false}
      },
      notification: {
        endpoint: 'teamswithteamt/?orderBy=numPendingNotifications&orderType=desc',
        offset: 6,
        option: {title: 'Ver equipos con más notificaciones', action: null, disabled: true}
      },
      search: {endpoint: 'teams/', offset: 2000}
    },
    groups: {
      most_visited: {
        endpoint: 'teamgroups/?orderBy=numVisitedByUser&orderType=desc',
        offset: 6,
        counterParam: 'num_pending_notifications',
        option: {title: 'Ver grupos más vistos', action: null, disabled: false}
      },
      teams_favourites: {
        endpoint: 'persons/my/teams/favourite/?',
        offset: 100,
        counterParam: null,
        option: {title: 'Ver mis favoritos', action: {icon: 'fa-cog', success: 'onToggleGroupsFavoritesMode'}, disabled: false}
      },
      notification: {
        endpoint: 'teamgroups/?orderBy=numPendingNotifications&orderType=desc',
        offset: 6,
        counterParam: 'num_pending_notifications',
        option: {title: 'Ver equipos con más notificaciones', action: null, disabled: true}
      },
      search: {
        endpoint: 'teamgroups/',
        offset: 2000
      }
    }
  };

  /************************ NATIVE METHODS  ************************/

  constructor(private _authSrv: AuthSrvService,
              private _router: Router,
              private _httpSrv: HttpSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'tree_team_group_component_n'.concat(Math.floor(Math.random() * 100000).toString());
    const retrievedObject = JSON.parse(localStorage.getItem('selectedNode'));
    if (!_.isNil(retrievedObject)
      && !_.isEmpty(retrievedObject)
      && retrievedObject.hasOwnProperty('node')
      && this._router.url === '/team-mate') {
      if (retrievedObject['tree'] === this.mode) {
        this.changeSelectedNode(new NodeModel(retrievedObject['node']));
      }
      // localStorage.removeItem('selectedNode');
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('mode') && !_.isNil(changes.mode.currentValue)) {
      const dropdownLabel = this.mode === 'teams' ? 'equipos' : 'grupos';
      this._defaultDropdownOptions = [
        {label: 'Ver ' + dropdownLabel + ' más vistos', value: {id: 0, name: 'most_visited'}},
        {label: 'Ver ' + dropdownLabel + ' sin padres', value: {id: 1, name: 'teams_root'}},
        {label: 'Ver ' + dropdownLabel + ' con más mates', value: {id: 2, name: 'most_mates'}},
        {label: 'Ver mis ' + dropdownLabel + ' favoritos', value: {id: 3, name: 'teams_favourites'}},
      ];
      this.getDropdownOptions();
    }
  }

  /************************ EVENTS METHODS ************************/

  /**
   * Get component title
   * @returns {string}
   */
  public getComponentTitle(): string {
    return this.mode === 'teams' ? 'Equipos' : 'Grupos';
  }

  /**
   * Get general search widgetConfig
   * @returns {Object}
   */
  public getGeneralSearchConfig(): object {
    return {
      flags: {
        showResultListUnderClick: true,
        dataOnLive: true
      },
      error: {
        show: false
      },
      icon: {
        hasIconRight: true,
        hasSearchIcon: true,
        hideIconOnSearch: true,
        showUnselectIcon: true
      },
      input: {
        placeholder: this.isTeamType()
          ? 'Busca un equipo'
          : 'Busca un grupo'
      }
    };
  }

  /**
   * Get if component has top search
   * @returns {boolean}
   */
  public hasGeneralSearch(): boolean {
    return this.generalSearch && !this._authSrv.getPersonLogged().isMate();
  }

  /**
   * Get if component has dropdown options
   * @returns {boolean}
   */
  public hasDropdownOptions(): boolean {
    return !_.isNil(this.dropdownOptions) && !_.isEmpty(this.dropdownOptions);
  }

  /**
   * Check if dropdown selected option is favourite
   * @returns {boolean}
   */
  public isFavouriteSelected(): boolean {
    return !_.isNil(this.selectedDropdownOption) && this.selectedDropdownOption['name'] === 'teams_favourites';
  }

  /**
   * Check if favorites are being edited
   * @returns {boolean}
   */
  public isEditingFavourite(): boolean {
    return this.editFavourite;
  }

  /**
   * Get if tree-team-group component widgetConfig is to groups
   * @returns {string}
   */
  public isGroupType(): boolean {
    return this.mode === 'groups';
  }

  /**
   * Get if person logged is MATE
   * @returns {boolean}
   */
  public isMate(): boolean {
    return this._authSrv.getPersonLogged().isMate();
  }

  /**
   * Get if tree-team-group component is to team
   * @returns {boolean}
   */
  public isTeamType(): boolean {
    return this.mode === 'teams';
  }

  /**
   * On click search mate filter
   * @param {Object | any} event
   */
  public onBlurGeneralSearch(event: object | any): void {
    if (event.hasOwnProperty('event')
      && (event['event'] === 'blur'
        && !event['itHasACorrectValue'])
      || event['event'] === 'unselect') {
      this.restartGeneralSearchComponent++;
    }
  }

  /**
   * On change dropdown selected option
   * @param {Object | any} event
   */
  public onChangeDropdownSelectedOption(event: object | any): void {
    if (event.hasOwnProperty('name')) {
      if (event['name'] !== 'teams_favourites') {
        this.editFavourite = false;
      }
      this.getTeamsOrGroups();
    }
  }

  /**
   * On change favourite
   * @param event
   */
  public onChangeFavourite(event: any): void {
    if (event.hasOwnProperty('favourite')) {
      this.data = event['favourite'];
      this.favouriteBackup = [...this.data];
    }
  }

  /**
   * On reorder favourite
   * @param event
   */
  public onReorderFavouriteEvent(event: any): void {
    if (this.isValidObjectProperty(event, 'data')) {
      this.data = event['data'];
      this.favouriteBackup = [...this.data];
    }
  }

  /**
   * On select element from general search result list
   * @param {Object} event
   */
  public onSelectGeneralSearchElement(event: any): void {
    if (this.isValidObjectProperty(event['element'], 'child_teams')
      || this.isValidObjectProperty(event['element'], 'num_components')) {
      this.changeSelectedNode(event['element']);
    }
  }

  /**
   * On toggle favorites mode
   */
  public onToggleFavouritesMode(): void {
    this.editFavourite = !this.editFavourite;
  }

  /**
   * On toggle selected node
   * @param {Object} event
   */
  public onToggleSelectedNode(event: object): void {
    if (event.hasOwnProperty('node')) {
      this.changeSelectedNode(event['node']);
    }
  }

  /**
   * Restart component
   * @param {Object} event
   */
  public restartComponent(): void {
    this.selectedNode = null;
    this.getTeamsOrGroups();
  }

  /************************ API METHODS  ************************/

  /**
   * Get teams or groups list
   */
  private getAllTeamsOrGroups(): void {
    this._httpSrv.get(
      this.isTeamType() ? 'teams/' : 'teamgroups/',
      {
        page: 1,
        elements: 2000
      }
    ).subscribe(
      success => this.onSuccessGetAllTeamsOrGroups(success)
    );
  }

  /**
   * On success get data
   * @param response
   */
  private onSuccessGetAllTeamsOrGroups(response: any): void {
    if (response['success']) {
      this.searchData = this.isValidObjectResponse(response)
        ? response['object']['items']
        : [];
    }
  }

  /**
   * Get teams or groups list
   */
  private getDropdownOptions(): void {
    this._httpSrv.get(
      this.isTeamType() ? 'teams/my/filterTeams/' : 'teams/my/filterTeams/',
      {
        page: 1,
        elements: 5
      }
    ).subscribe(
      success => this.onSuccessGetDropdownOptions(success)
    );
  }

  /**
   * On success get data
   * @param response
   */
  private onSuccessGetDropdownOptions(response: any): void {
    if (response['success']
      && response.hasOwnProperty('object')
      && !_.isNil(response['object'])
      && !_.isEmpty(response['object'])) {

      this.dropdownOptions = [];
      Object.keys(response['object']).forEach( name => {
        const addOption = this._defaultDropdownOptions.find( option => {
          return option.value.name === response['object'][name];
        });
        if (!_.isNil(addOption)) {
          this.dropdownOptions.push(addOption);
        }
      });
    } else {
      this.dropdownOptions = this._defaultDropdownOptions;
    }

    if (this.dropdownOptions.length !== this._defaultDropdownOptions.length) {
      this._defaultDropdownOptions.forEach( defaultOption => {
        const findOption = this.dropdownOptions.find( option => {
          return option.value['name'] === defaultOption.value['name'];
        });

        if (_.isNil(findOption)) {
          this.dropdownOptions.push(defaultOption);
        }
      });
    }

    this.selectedDropdownOption = this.dropdownOptions[0].value;
    this.getAllTeamsOrGroups();
    this.getTeamsOrGroups();
  }

  /**
   * Get teams or groups list
   */
  private getTeamsOrGroups(): void {
    if (this.isTeamType()) {
      this._httpSrv.get(
        this._apiSettup[this.mode][this.selectedDropdownOption['name']].endpoint,
        {
          page: 1,
          elements: this._apiSettup[this.mode][this.selectedDropdownOption['name']].offset,
          see_is_deleteable: true,
        }
      ).subscribe(
        success => this.onSuccessGetTeamsOrGroups(success)
      );
    } else {
      this._httpSrv.get(
        this._apiSettup[this.mode][this.selectedDropdownOption['name']].endpoint,
        {
          page: 1,
          elements: this._apiSettup[this.mode][this.selectedDropdownOption['name']].offset
        }
      ).subscribe(
        success => this.onSuccessGetTeamsOrGroups(success)
      );
    }
  }

  /**
   * On success get data
   * @param response
   */
  private onSuccessGetTeamsOrGroups(response: any): void {
    if (response['success'] && this.isValidObjectResponse(response)) {
      this.data = [];
      response['object']['items'].forEach( element => {
        this.data.push(new NodeModel(element));
      });

      if (this.selectedDropdownOption['name'] === 'teams_favourites') {
        this.favouriteBackup = [...this.data];
      }
    }
  }

  /************************ AUXILIAR METHODS  ************************/

  private changeSelectedNode(node: NodeModel): void {
    this.selectedNode = node;

    if (_.isNil(this.selectedNode)) {
      this.restartGeneralSearchComponent++;
    }

    this.onSelectNodeEvent.emit({
      id: this.config.id,
      tree: this.mode,
      node: this.selectedNode
    });
  }
}
