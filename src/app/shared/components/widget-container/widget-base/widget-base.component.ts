import {
  Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnDestroy, ViewChild
} from '@angular/core';
import {HttpSrvService} from '../../../services/http.service';
import {AuthSrvService} from '../../../services/auth.service';
import {ActivatedRoute} from '@angular/router';
import {OverlayPanel} from 'primeng/primeng';
import {BaseComponentModel} from '../../../models/base-component/base-component.class';
import {DashboardEntryModel} from '../../../models/widgets/dashboard-entry.model';
import {WidgetTypeBComponent} from '../widget-type-b/widget-type-b.component';
import {WidgetTypeCComponent} from '../widget-type-c/widget-type-c.component';
import {WidgetTypeDComponent} from '../widget-type-d/widget-type-d.component';

const _ = require('lodash');

@Component({
  selector: 'app-widget-base-component',
  templateUrl: './widget-base.component.html',
  styleUrls: ['./widget-base.component.css']
})
export class WidgetBaseComponent extends BaseComponentModel implements OnInit, OnChanges, OnDestroy {

  /** COMPONENT INPUTS **/

  @Input() dashboardMode = false;
  @Input() dashboardEntry: DashboardEntryModel;
  @Input() visible = true;
  @Input() widgetConfig = {};

  /** COMPONENT OUTPUTS **/

  @Output() onDeleteWidgetEvent = new EventEmitter();
  @Output() onEditWidgetEvent = new EventEmitter();

  /** COMPONENT VIEWCHILD **/

  @ViewChild('widgetTypeB') public widgetTypeB: WidgetTypeBComponent;
  @ViewChild('widgetTypeC') public widgetTypeC: WidgetTypeCComponent;
  @ViewChild('widgetTypeD') public widgetTypeD: WidgetTypeDComponent;

  /** COMPONENT VARS **/

  private _editMode = false;
  private _showOptions = false;
  private _showOptionsPanel = false;
  private _widgetValue = '-';
  private _subscribeIdParam;

  /** COMPONENT CONFIG **/

  public widgetOptions = [
    {label: 'Eliminar', value: 'Eliminar'}
  ];

  /** STATIC METHODS **/

  /** NATIVE METHODS **/

  constructor(private _httpSrv: HttpSrvService,
              private _route: ActivatedRoute,
              private _authSrv: AuthSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'widget_base_component_n'.concat(Math.floor(Math.random() * 100000).toString());

    if (!this.dashboardMode
      && !_.isEmpty(this.widgetConfig) && this.widgetConfig.hasOwnProperty('type')
      && (this.widgetConfig['type'] === 'B' || this.widgetConfig['type'] === 'D')) {
      this.widgetOptions.push({label: 'Editar', value: 'Editar'});
    }

    this.getWidgetValueFromApi();
  }

  ngOnChanges(changes: SimpleChanges): void { }

  ngOnDestroy() {
    if (!_.isNil(this._subscribeIdParam)) {
      this._subscribeIdParam.unsubscribe();
    }
  }


  /** PUBLIC METHODS **/

  /**
   * Close edit mode
   */
  public closeEditMode(): void {
    this._editMode = false;
    this.onEditWidgetEvent.emit({
      id: this.getComponentId(),
      dashboardEntry: {
        id: this.dashboardEntry.id
      },
      isEditMode: this.isEditMode()
    });
  }

  /**
   * Get widget text from widgetConfig
   * @returns {string}
   */
  public getWidgetText(): string {
    return !_.isEmpty(this.widgetConfig) && this.widgetConfig.hasOwnProperty('type') ? this.widgetConfig['text'] : '';
  }

  /**
   * Get widget type from config
   * @returns {string}
   */
  public getWidgetType(): string {
    return !_.isEmpty(this.widgetConfig) && this.widgetConfig.hasOwnProperty('type') ? this.widgetConfig['type'] : '';
  }


  /**
   * Get widget value
   * @returns {string}
   */
  public getWidgetValue(): string {
    return this._widgetValue;
  }

  /**
   * Check if widget in dashboard mode
   * @returns {boolean}
   */
  public isDashboardMode(): boolean {
    return this.dashboardMode;
  }

  /**
   * Check if widget in edit mode
   * @returns {boolean}
   */
  public isEditMode(): boolean {
    return this._editMode;
  }

  /**
   * Check if widget options are visible
   * @returns {boolean}
   */
  public isShowOptions(): boolean {
    return this._showOptions = (this._showOptionsPanel ? this._showOptionsPanel : this._showOptions);
  }

  /**
   * On before hide options panel
   */
  public onBeforeHideOptionsPanel(): void {
    this._showOptionsPanel = false;
    this._showOptions = false;
  }

  /**
   * Enable options on hover
   */
  public onEnabledShowOptions(): void {
    this._showOptions = true;
  }

  /**
   * Disabled options visibility on outter
   */
  public onDisabledShowOptions(): void {
    this._showOptions = false;
  }

  /**
   * On select option
   * @param {string} optionValue
   * @param {OverlayPanel} overlaypanel
   */
  public onSelectOption(optionValue: string, overlaypanel: OverlayPanel): void {
    if (optionValue === 'Eliminar') {
      overlaypanel.hide();
      this.onDeleteWidgetEvent.emit({
        id: this.getComponentId(),
        dashboardEntry: this.dashboardEntry
      });
    } else if (optionValue === 'Editar') {
      overlaypanel.hide();
      this._editMode = true;
      this.onEditWidgetEvent.emit({
        id: this.getComponentId(),
        dashboardEntry: {
          id: this.dashboardEntry.id
        },
        isEditMode: this.isEditMode()
      });
    }
  }

  /**
   * Set show options panel
   * @param event
   * @param {OverlayPanel} overlaypanel
   */
  public setShowOptionsPanel(event, overlaypanel: OverlayPanel) {
    this._showOptions = true;
    this._showOptionsPanel = true;
    overlaypanel.toggle(event);
  }

  /**
   * On change internal event in widget
   * @param event
   */
  public onChangeWidget(event): void {
    if (event.hasOwnProperty('type')) {
      if (event['type'] === 'D') {
        this._widgetValue = event['value'];
      }
    }
  }

  /**
   * Get widget value from API
   */
  private getWidgetValueFromApi(): void {
    // this.showSoftLoader();

    if (this.dashboardMode || _.isEmpty(this.widgetConfig) || !this.widgetConfig.hasOwnProperty('type')
      || !this.widgetConfig.hasOwnProperty('endpoint')) {
      this._widgetValue = '-';

    } else  if (!_.isNil(this.widgetConfig['endpoint']) && !_.isEmpty(this.widgetConfig['endpoint'])) {
        this._subscribeIdParam = this._route.params.subscribe(params => {
          if (this.isValidObjectProperty(params, 'id')) {
            const userId = this.widgetConfig['endpoint'].indexOf('gid') !== -1 && this._authSrv.isRoleGestor()
              ? this._authSrv.getPersonId()
              : params['id'];

            this._httpSrv
              .get(
                this.widgetConfig['endpoint'].replace(/id|gid/gi, userId)
              )
              .subscribe(
              (responseOk) => this.onSuccessGetWidgetValueFromApi(responseOk)
            );
          }
        });

    } else {
      this._widgetValue = '-';
    }
  }

  /**
   * On success get widget- value from API
   * @param {Object} response
   */
  private onSuccessGetWidgetValueFromApi(response: object): void {
    if (response['success']) {
      if (response.hasOwnProperty('object') && this.widgetConfig['type'] === 'A') {
        const key = Object.keys(response['object'])[0];
        this._widgetValue = response['object'][key];

      } else if (response.hasOwnProperty('object') && this.widgetConfig['type'] === 'C') {
        this._widgetValue = response['object']['occupied'];
        this.widgetConfig['extend']['value'] = response['object']['total'];

      } else if (response.hasOwnProperty('object') && this.widgetConfig['type'] === 'D') {
        this.widgetTypeD.initializeWidget(response['object']);
      }
    }
  }
}
