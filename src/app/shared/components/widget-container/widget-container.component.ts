import {
  Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ViewChild
} from '@angular/core';
import {HttpSrvService} from '../../services/http.service';
import {WidgetCatalog} from '../../models/widgets/widget-catalog.class';
import {BaseComponentModel} from '../../models/base-component/base-component.class';
import {DashboardEntryModel} from '../../models/widgets/dashboard-entry.model';
import {DashboardModel} from '../../models/widgets/dashboard.model';
import {WidgetModel} from '../../models/widgets/widget.model';
import {PersonModel} from '../../models/person.model';

const _ = require('lodash');

@Component({
  selector: 'app-widget-container-component',
  templateUrl: './widget-container.component.html',
  styleUrls: ['./widget-container.component.css']
})
export class WidgetContainerComponent extends BaseComponentModel implements OnInit, OnChanges {

  /** COMPONENT INPUTS **/

  @Input() dashboardMode: false;
  @Input() person: PersonModel;
  @Input() expanded: false;
  @Input() widgetType: null;
  @Input() visible = true;

  /** COMPONENT OUTPUTS **/

  @Output() outputEvent = new EventEmitter();
  @Output() onSelectWidgetEvent = new EventEmitter();

  /** COMPONENT VIEWCHILD **/

  /** COMPONENT VARS **/

  // Static vars for static methods

  public getWidgetConfig = WidgetContainerComponent.getWidgetConfig;

  // Component vars

  public myDashboardEntries: Array<DashboardEntryModel> = [];

  private _apiLoader = false;
  private _myDashboard: DashboardModel;
  private _widgetCatalog = new WidgetCatalog();
  private _widgetsEditing: Array<number> = [];

  /** COMPONENT CONFIG **/

  /** STATIC METHODS **/

  /**
   * Get widget config
   * @param {DashboardEntryModel} dashboardEntry
   * @returns {Object}
   */
  private static getWidgetConfig(dashboardEntry: DashboardEntryModel): object {
    return dashboardEntry.widget.widgetConfig;
  }

  /************************ NATIVE METHODS  ************************/

  constructor(private _httpSrv: HttpSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'widget_container_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ( (changes.hasOwnProperty('person') || changes.hasOwnProperty('widgetType'))
      && this.person.id !== 0) {
      if (this.dashboardMode
        && !_.isEmpty(this.person)
        && !_.isNil(this.widgetType)
        && !_.isEmpty(this.widgetType)) {
        this.getMyDashboard();
      } else if (!this.dashboardMode
        && !_.isEmpty(this.person)
        && !_.isNil(this.widgetType)
        && !_.isEmpty(this.widgetType)) {
        this.getMyDashboardEntries();
      }
    }
  }

  /** PUBLIC METHODS **/

  /**
   * Check if widget-container in dashboard mode
   * @returns {boolean}
   */
  public isDashboardMode(): boolean {
    return this.dashboardMode;
  }

  /**
   * Check if the widget is being edited
   * @param {number} id
   * @returns {boolean}
   */
  public isWidgetEditing(id: number): boolean {
    return this._widgetsEditing.indexOf(id) !== -1;
  }



  public onDeleteWidgetEvent(event: object): void {
    if (this.isValidObjectProperty(event, 'dashboardEntry')) {
      this.deleteWidget(event['dashboardEntry']);
    }
  }

  public onEditWidgetEvent(event: object): void {
    if (this.isValidObjectProperty(event, 'dashboardEntry')
      && this.isValidObjectProperty(event['dashboardEntry'], 'id')) {

      if (event['isEditMode'] === true) {
        this._widgetsEditing.push(event['dashboardEntry']['id']);
      } else if (event['isEditMode'] === false) {
        const pos = this._widgetsEditing.indexOf(event['dashboardEntry']['id']);
        this._widgetsEditing.splice(pos, 1);
      }
    }
  }

  /**
   * Change widget position
   * @param {number} widgetIndex
   */
  public onChangePosition(widgetIndex: number): void {
    if (this.isDashboardMode()) {

      if (this.myDashboardEntries[widgetIndex].position !== 0) {
        this.reorderWidgets(this.myDashboardEntries[widgetIndex].position);
        this.myDashboardEntries[widgetIndex].position = 0;
      } else {
        let newWidgetPosition = -1;
        for (let i = 0; i < this.myDashboardEntries.length; i++) {
          if (this.myDashboardEntries[i].widget.id !== this.myDashboardEntries[widgetIndex].widget.id
            && this.myDashboardEntries[i].position !== 0
            && this.myDashboardEntries[i].position > newWidgetPosition) {
            newWidgetPosition = this.myDashboardEntries[i].position;
          }
        }
        if (newWidgetPosition === -1) {
          this.myDashboardEntries[widgetIndex].position = 1;
        } else {
          this.myDashboardEntries[widgetIndex].position = newWidgetPosition + 1;
        }
      }

      this.onSelectWidgetEvent.emit({
        dashboardEntries: this.myDashboardEntries
      });
    }
  }

  /**
   * Reorder widget list
   * @param {number} fromPosition
   */
  private reorderWidgets(fromPosition: number): void {
    if (this.isDashboardMode()) {
      for (let i = 0; i < this.myDashboardEntries.length; i++) {
        if (this.myDashboardEntries[i].position !== 0 && this.myDashboardEntries[i].position >= fromPosition) {
          this.myDashboardEntries[i].position--;
        }
      }
    }
  }

  /** PRIVATE METHODS **/

  /**
   * Delete widget
   * @param {DashboardEntryModel} dashboardEntry
   */
  private deleteWidget(dashboardEntry: DashboardEntryModel): void {
    this._httpSrv
      .delete(
        'dashboards/' + this.widgetType + '/dashboardentries/' + dashboardEntry.id)
      .subscribe(
      (responseOk) => this.onSuccessDeleteWidget(responseOk, dashboardEntry),
      (responseFail) => this.onErrorApiResponse(responseFail)
    );
  }

  /**
   * On success delete widget
   * @param {Object | any} response
   * @param {DashboardEntryModel} dashboardEntry
   */
  private onSuccessDeleteWidget(response: object | any, dashboardEntry: DashboardEntryModel): void {
    // console.log(response)
    if (response['success']) {
      this.myDashboardEntries.splice(this.myDashboardEntries.indexOf(dashboardEntry), 1);
    }
  }


  /**
   * Get my dashboard
   */
  private getMyDashboard(): void {
    this._apiLoader = true;

    this._httpSrv
      .get(
        'dashboards/',
        {
          tag: this.widgetType
        })
      .subscribe(
      (responseOk) => this.onSuccessGetMyDashboard(responseOk),
      (responseFail) => this.onErrorApiResponse(responseFail)
    );
  }

  /**
   * On success get my dashboard
   * @param {Object | any} response
   */
  private onSuccessGetMyDashboard(response: object | any): void {
    if (response['success']
      && this.isValidObjectProperty(response, 'object')
      && !_.isEmpty(response['object'])
      && response['object']['items'].length === 1) {

      this._myDashboard = response['object']['items'][0];
      this.getMyDashboardEntries();

    } else {
      this._apiLoader = false;
    }
  }

  /**
   * Get my dashboard entries
   */
  private getMyDashboardEntries(): void {
    this._apiLoader = true;

    this._httpSrv.get(
      'dashboards/' + this.widgetType + '/dashboardentries/')
      .subscribe(
      (responseOk) => this.onSuccessGetMyDashboardEntries(responseOk),
      (responseFail) => this.onErrorApiResponse(responseFail)
    );
  }

  /**
   * On success get my dashboard entries
   * @param {Object | any} response
   */
  private onSuccessGetMyDashboardEntries(response: object | any): void {
    if (response['success']
      && this.isValidObjectProperty(response, 'object')) {

      if (!_.isNil(response['object']) && Array.isArray(response['object'])) {

        this.myDashboardEntries = response['object'];

        if (this.isDashboardMode()) {
          this.buildDataToDashboardMode();
        } else {
          this.buildDataToNormalMode();
        }

        this._apiLoader = false;
      }

    } else {
      this._apiLoader = false;
    }
  }

  /**
   * Control all the fails responses coming from the API
   *
   * @param response
   */
  private onErrorApiResponse(response: Object): void {
    this._apiLoader = false;
  }

  /**
   * Build data to dashboard mode
   */
  private buildDataToDashboardMode(): void {

    for (let i = 0; i < this._myDashboard.widgets.length; i++) {
      const widget = this._myDashboard.widgets[i];

      let dashboardEntry = new DashboardEntryModel({
        dashboard: {
          id: this._myDashboard.id,
          tag: this._myDashboard.tag
        },
        id: 0,
        position: 0,
        widget: {
          id: widget.id,
          tag: widget.tag,
          widgetConfig: this._widgetCatalog.getWidgetConfig(widget.tag)
        }
      });

      let findWidget = false;
      for (let j = 0; j < this.myDashboardEntries.length; j++) {
        const myDashboardEntry = this.myDashboardEntries[j];

        if (dashboardEntry.widget.id === myDashboardEntry.widget.id) {
          dashboardEntry = new DashboardEntryModel({
            dashboard: {
              id: myDashboardEntry.dashboard.id,
              tag: myDashboardEntry.dashboard.tag
            },
            id: myDashboardEntry.id,
            position: myDashboardEntry.position,
            widget: {
              id: myDashboardEntry.widget.id,
              tag: myDashboardEntry.widget.tag,
              widgetConfig: this._widgetCatalog.getWidgetConfig(myDashboardEntry.widget.tag)
            }
          });
          this.myDashboardEntries[j] = dashboardEntry;
          findWidget = true;
          break;
        }
      }

      if (!findWidget) {
        this.myDashboardEntries.push(dashboardEntry);
      }
    }

    this.onSelectWidgetEvent.emit({
      dashboardEntries: this.myDashboardEntries
    });
  }

  /**
   * Build data to normal mode
   */
  private buildDataToNormalMode(): void {
    const auxiliarDashboardEntries = [];

    for (let j = 0; j < this.myDashboardEntries.length; j++) {
      const myDashboardEntry = this.myDashboardEntries[j];

      const dashboardEntry = new DashboardEntryModel({
        dashboard: {
          id: myDashboardEntry.dashboard.id,
          tag: myDashboardEntry.dashboard.tag
        },
        id: myDashboardEntry.id,
        position: myDashboardEntry.position,
        widget: new WidgetModel({
          id: myDashboardEntry.widget.id,
          tag: myDashboardEntry.widget.tag,
          widgetConfig: this._widgetCatalog.getWidgetConfig(myDashboardEntry.widget.tag)
        })
      });

      auxiliarDashboardEntries.push(dashboardEntry);
    }

    this.myDashboardEntries = [];
    Object.assign(this.myDashboardEntries, auxiliarDashboardEntries);
  }

}
