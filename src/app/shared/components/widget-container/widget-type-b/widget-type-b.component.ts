import {Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {HttpSrvService} from '../../../services/http.service';
import {SelectItem} from 'primeng/primeng';
import {BaseComponentModel} from '../../../models/base-component/base-component.class';

const _ = require('lodash');

@Component({
  selector: 'app-widget-type-b-component',
  templateUrl: './widget-type-b.component.html',
  styleUrls: ['./widget-type-b.component.css'],
  providers: [HttpSrvService]
})
export class WidgetTypeBComponent extends BaseComponentModel implements OnInit, OnChanges {


  /** COMPONENT INPUTS **/

  @Input() editMode = false;
  @Input() visible = true;
  @Input() widgetConfig = {};

  /** COMPONENT OUTPUTS **/

  @Output() outputEvent = new EventEmitter();
  @Output() changeDropdownEvent = new EventEmitter();

  /** COMPONENT VIEWCHILD **/

  /** COMPONENT VARS **/

  public dropdownOptionSelectedValue: string;
  public dropdownOptionSelectedLabel = '';

  private _dropdown = [];
  private _dropdownOptionSelectedObject: object;
  private _dropdownOptions: SelectItem[] = [];

  /** COMPONENT CONFIG **/

  /** STATIC METHODS **/

  /** NATIVE METHODS **/

  constructor(private _httpSrv: HttpSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'widget_type_b_component_n'.concat(Math.floor(Math.random() * 100000).toString());

    if (this.widgetConfig.hasOwnProperty('dropdown')) {
      this._dropdown = this.widgetConfig['dropdown'];

      this._dropdown.forEach(element => {
        this._dropdownOptions.push({label: element.label, value: element.value});
      });

      this.dropdownOptionSelectedValue = this._dropdown[0].label;
      this._dropdownOptionSelectedObject = this._dropdown.find(o => o.label === this.dropdownOptionSelectedValue);
      this.dropdownOptionSelectedLabel = this._dropdownOptionSelectedObject['label'];
    }

  }

  ngOnChanges(changes: SimpleChanges): void { }

  /** PUBLIC METHODS **/

  /**
   * Get widget dropdown options from config
   * @returns {string}
   */
  public getWidgetDropdownOptions(): Array<object> {
    return this._dropdownOptions;
  }

  /**
   * Check if edit mode
   * @returns {boolean}
   */
  public isEditMode(): boolean {
    return this.editMode;
  }

  /**
   * On change widget dropdown value
   * @returns {string}
   */
  public onChangeWidgetDropdown(event: string): void {

    if (!_.isNil(this.dropdownOptionSelectedValue)) {
      this._dropdownOptionSelectedObject = this._dropdown.find(o => o.value === event['value']);
      this.dropdownOptionSelectedLabel = this._dropdownOptionSelectedObject['label'];
    }

    this.changeDropdownEvent.emit({
      id: this.getComponentId(),
      tag: this.widgetConfig['tag'],
      dropdownOptionSelected: this._dropdownOptionSelectedObject
    });
  }
}
