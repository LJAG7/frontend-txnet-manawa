import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import {BaseComponentModel} from '../../../models/base-component/base-component.class';

const _ = require('lodash');

@Component({
  selector: 'app-widget-type-c-component',
  templateUrl: './widget-type-c.component.html',
  styleUrls: ['./widget-type-c.component.css']
})
export class WidgetTypeCComponent extends BaseComponentModel implements OnInit, OnChanges {

  /** COMPONENT INPUTS **/

  @Input() dashboardMode = false;
  @Input() visible = true;
  @Input() widgetValue = '-';

  /** COMPONENT OUTPUTS **/

  @Output() outputEvent = new EventEmitter();

  /** COMPONENT VARS **/

  /** COMPONENT CONFIG **/

  /** STATIC METHODS **/

  /** NATIVE METHODS **/

  constructor() {
    super();
  }

  ngOnInit() {
    this.config.id = 'widget_type_c_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void { }

  /** PUBLIC METHODS **/

  /**
   * Get widget-container value
   * @returns {string}
   */
  public getWidgetValue(): string {
    return this.widgetValue;
  }

  /** PRIVATE METHODS **/

}
