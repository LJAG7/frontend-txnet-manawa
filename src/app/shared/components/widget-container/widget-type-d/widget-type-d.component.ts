import {Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {HttpSrvService} from '../../../services/http.service';
import {SelectItem} from 'primeng/primeng';
import {BaseComponentModel} from '../../../models/base-component/base-component.class';

const _ = require('lodash');

@Component({
  selector: 'app-widget-type-d-component',
  templateUrl: './widget-type-d.component.html',
  styleUrls: ['./widget-type-d.component.css'],
  providers: [HttpSrvService]
})
export class WidgetTypeDComponent extends BaseComponentModel implements OnInit, OnChanges {


  /** COMPONENT INPUTS **/

  @Input() editMode = false;
  @Input() visible = true;
  @Input() widgetConfig = {};
  @Input() options = [];

  /** COMPONENT OUTPUTS **/

  @Output() outputEvent = new EventEmitter();
  @Output() changeDropdownEvent = new EventEmitter();

  /** COMPONENT VIEWCHILD **/

  /** COMPONENT VARS **/

  public selectedDropdownOption: string;
  public dropdownOptionSelectedLabel = '';
  public dropdownOptionPlaceholder = '';

  private _dropdown = [];
  private _dropdownOptionSelectedObject: object;
  private _dropdownOptions: SelectItem[] = [];

  /** COMPONENT CONFIG **/

  /** STATIC METHODS **/

  /** NATIVE METHODS **/

  constructor(private _httpSrv: HttpSrvService) {
    super();
  }

  ngOnInit() {
    this.config.id = 'widget_type_d_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!_.isNil(changes['options']) && !_.isNil(changes['options']['currentValue'])
      && !_.isEmpty(changes['options']['currentValue'])) {
      this._dropdown = changes['options']['currentValue'];

      this._dropdown.forEach(element => {
        this._dropdownOptions.push({label: element.name, value: element.id});
      });

      this._dropdownOptionSelectedObject = this._dropdownOptions[0];
      this.selectedDropdownOption = this._dropdownOptionSelectedObject['value'];
      this.dropdownOptionSelectedLabel = this._dropdownOptionSelectedObject['label'];
    }
  }

  /** PUBLIC METHODS **/

  /**
   * Get widget dropdown options from config
   * @returns {string}
   */
  public getWidgetDropdownOptions(): Array<object> {
    return this._dropdownOptions;
  }

  /**
   * Check if edit mode
   * @returns {boolean}
   */
  public isEditMode(): boolean {
    return this.editMode;
  }

  /**
   * On change widget dropdown value
   * @returns {string}
   */
  public onChangeWidgetDropdown(event: string): void {
    if (!_.isNil(this.selectedDropdownOption)) {
      this._dropdownOptionSelectedObject = this._dropdown.find(o => o.id === event['value']);
      this.dropdownOptionSelectedLabel = this._dropdownOptionSelectedObject['name'];
    }

    this.updateWidget(event['value']);
  }

  /**
   * Initialize Widget method
   * @param data
   */
  public initializeWidget(data: any): void {
    this.widgetConfig['dropdown']['options'] = this.widgetConfig.hasOwnProperty('param') && !_.isNil(this.widgetConfig['param'])
      ? data[this.widgetConfig['param']]['items']
      : data['items'];

    if (!_.isEmpty(this.widgetConfig['dropdown']['options'])) {
      this.updateWidget(this.widgetConfig['dropdown']['options'][0]['id']);

    } else {
      this.widgetConfig['dropdown']['options'] = [];
      this.emitWidget('-');
    }
  }


  /** PRIVATE METHODS **/

  /**
   * Update the widget value according WidgetTypeDComponent logic
   * @param id
   */
  private updateWidget(id: any): void {
    this._httpSrv.get(
      this.widgetConfig['dropdown']['endpoint'].replace('id', id.toString())
    ).subscribe(
      (response) => {
        if (response.hasOwnProperty('success') && response['success'] && response.hasOwnProperty('object')
          && response['object'].hasOwnProperty(this.widgetConfig['dropdown']['param'])) {
          this.emitWidget(response['object'][this.widgetConfig['dropdown']['param']]);
        } else {
          this.emitWidget('-');
        }
      },
      () => {
        this.emitWidget('-');
      }
    );
  }

  /**
   * Emitter method
   * @param value
   */
  private emitWidget(value: any): void {
    this.changeDropdownEvent.emit({
      id: this.getComponentId(),
      type: this.widgetConfig['type'],
      tag: this.widgetConfig['tag'],
      value: value
    });
  }
}
