import {ROLESYSTEMS, ROLESYSTEMSGROUPS} from './rolesystems.const';

export const PERMISSIONS = {
  '': {
    access: ROLESYSTEMSGROUPS.ALL
  },
  'person':
    {
      access: ROLESYSTEMSGROUPS.ADMINSGESTOR,
      components: {
        'addPersonsContainer': ROLESYSTEMSGROUPS.ADMINSGESTOR,
        'personsTableContainer': ROLESYSTEMSGROUPS.ADMINSGESTOR
    }
  },
  'team': {
    access: ROLESYSTEMSGROUPS.ADMINSGESTOR,
    components: {
      'addTeamContainer': ROLESYSTEMSGROUPS.ADMINSGESTOR,
      'teamsTableContainer': ROLESYSTEMSGROUPS.ADMINSGESTOR
    }
  },
  'team/edit': {
    access: ROLESYSTEMSGROUPS.ADMINSGESTOR,
    components: {
      'all': ROLESYSTEMSGROUPS.ADMINSGESTOR
    }
  },
  'team-mate': {
    access: ROLESYSTEMSGROUPS.ADMINSGESTOR,
    components: {
      'all': ROLESYSTEMSGROUPS.ADMINSGESTOR
    }
  },
  'mate-profile': {
    access: ROLESYSTEMSGROUPS.ALL,
    components: {
      'all': ROLESYSTEMSGROUPS.ALL,
      'teamsTreeComponent': ROLESYSTEMSGROUPS.ALL,
      'groupsTreeComponent': ROLESYSTEMSGROUPS.ADMINSGESTOR,
      'tabDashboard': ROLESYSTEMSGROUPS.ALL,
      'tabAgend': ROLESYSTEMSGROUPS.ALL,
      'tabProfile': ROLESYSTEMSGROUPS.ALL,
    },
    config: {
      'tabs': [
        {
          value: [
            {label: 'Dashboard', value: 0},
            {label: 'Agenda', value: 1},
            {label: 'Perfil', value: 2}
          ],
          case: ROLESYSTEMSGROUPS.ALL
        }
      ],
      'selectedTab': [
        {value: 0, case: ROLESYSTEMSGROUPS.ALL}
      ]
    }
  },
  'tab-mate-profile-tab-laboral-academic': {
    access: ROLESYSTEMSGROUPS.ALL,
    components: {
      'all': ROLESYSTEMSGROUPS.ALL,
      'tabLaboral': ROLESYSTEMSGROUPS.ALL,
      'tabAcademic': ROLESYSTEMSGROUPS.ALL
    },
    config: {
      'tabs': [
        {
          value: [
            {label: 'Experiencia', value: 0},
            {label: 'Educación', value: 1}
          ],
          case: ROLESYSTEMSGROUPS.ALL
        }
      ],
      'selectedTab': [
        {value: 0, case: ROLESYSTEMSGROUPS.ALL}
      ]
    }
  },
  'mates-profile/mate-dashboard': {
    access: ROLESYSTEMSGROUPS.ALL
  },
  'master': {
    access: ROLESYSTEMSGROUPS.ADMINSGESTOR,
    components: {
      'addMasterContainer': ROLESYSTEMSGROUPS.ADMINSGESTOR,
      'mastersTableContainer': ROLESYSTEMSGROUPS.ADMINSGESTOR
    }
  },
  'master/edit':
    {
      access: ROLESYSTEMSGROUPS.ADMINSGESTOR,
      components: {
        'all': ROLESYSTEMSGROUPS.ADMINSGESTOR
      },

  },
  'admin': {
    access: ROLESYSTEMSGROUPS.ADMINS,
    components: {
      'all': ROLESYSTEMSGROUPS.ADMINS,
      'tabDomainComponent': [ROLESYSTEMS.ROLE_SUPER_ADMIN],
      'tabUsersComponent': ROLESYSTEMSGROUPS.ADMINS,
      'tabEntityComponent': ROLESYSTEMSGROUPS.ADMINS,
    },
    config: {
      'tabs': [
        {
          value: [{label: 'Domain', value: 0}, {label: 'Usuarios', value: 1}, {label: 'Entidades', value: 2}],
          case: [ROLESYSTEMS.ROLE_SUPER_ADMIN]
        },
        {
          value: [{label: 'Usuarios', value: 1}, {label: 'Entidades', value: 2}],
          case: [ROLESYSTEMS.ROLE_ADMIN]
        }
      ],
      'selectedTab': [
        {value: 0, case: [ROLESYSTEMS.ROLE_SUPER_ADMIN]},
        {value: 1, case: [ROLESYSTEMS.ROLE_ADMIN]},
      ]
    }
  },
  'admin/scope': {
    access: ROLESYSTEMSGROUPS.ADMINS,
    components: {
      'all': ROLESYSTEMSGROUPS.ADMINS
    }
  },
  'admin/user': {
    access: ROLESYSTEMSGROUPS.ADMINS,
    components: {
      'all': ROLESYSTEMSGROUPS.ADMINS
    }
  },
  'admin/master-class': {
    access: ROLESYSTEMSGROUPS.ADMINS,
    components: {
      'all': ROLESYSTEMSGROUPS.ADMINS
    }
  },
  'admin/team-group': {
    access: ROLESYSTEMSGROUPS.ADMINS,
    components: {
      'all': ROLESYSTEMSGROUPS.ADMINS
    }
  },
  'admin/role': {
    access: ROLESYSTEMSGROUPS.ADMINS,
    components: {
      'all': ROLESYSTEMSGROUPS.ADMINS
    }
  },
  'admin/role-category': {
    access: ROLESYSTEMSGROUPS.ADMINS,
    components: {
      'all': ROLESYSTEMSGROUPS.ADMINS
    }
  },
  'locations': {
    access: ROLESYSTEMSGROUPS.ALL,
    components: {
      'addLocationContainer': ROLESYSTEMSGROUPS.ALL,
      'locationsTableContainer': ROLESYSTEMSGROUPS.ALL
    }
  },
  'locations/edit': {
    access: ROLESYSTEMSGROUPS.ALL,
    components: {
      'all': ROLESYSTEMSGROUPS.ALL
    }
  },
  'issues': {
    access: ROLESYSTEMSGROUPS.ALL,
    components: {
      'addIssueContainer': ROLESYSTEMSGROUPS.ALL,
      'issuesTableContainer': ROLESYSTEMSGROUPS.ALL
    }
  },
  'search': {
    access: ROLESYSTEMSGROUPS.ALL,
    components: {
      'all': ROLESYSTEMSGROUPS.ALL
    }
  },
  'notes': {
    access: ROLESYSTEMSGROUPS.ALL,
    components: {
      'all': ROLESYSTEMSGROUPS.ALL
    }
  },
  'faq': {
    access: ROLESYSTEMSGROUPS.ALL,
    components: {
      'all': ROLESYSTEMSGROUPS.ALL
    }
  },
  'logout': {
    access: ROLESYSTEMSGROUPS.ALL,
    components: {
      'all': ROLESYSTEMSGROUPS.ALL
    }
  }
};

