import {IDatePickerConfig} from 'ng2-date-picker';

const moment = require('moment');

export const APP_NAME = {
  long: 'Team Experience Network',
  short: 'TxNet'
};

export const DOMAINS = {
  localhost: 'localhost:',
  local: 'www.tsf.com',
  develop: 'li1428-73.members.linode.com',
  preProduction: 'www.teamspaceframework.com',
  production: 'www.txnet.es'
};

export const REGEXPATTERNS = {
  email:               /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
  anyMin3:             /^.{3,}$/,
  anyMin6:             /^.{6,}$/,
  alphanumMin3:        /^\w{3,}$/,
  alphanumMin6:        /^\w{6,}$/,
  alphanumSpecialMin3: /^[a-zA-Z0-9_]{3,}$/,
  role_systems:        /^ROLE_USER$|ROLE_GESTOR$|ROLE_ADMIN$/,
  birthdate:           /^((19|20)\d\d([-]|[\/])(0[1-9]|1[012])([-]|[\/])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))$|((0[1-9]|1[0-9]|2[0-9]|3[0-1])([-]|[\/])(0[1-9]|1[012])([-]|[\/])(19|20)\d\d)$/,
  timeHHMM:             /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/,
  timeHHMMSS:             /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/,
  coordinates:         /^(-?)\d{1,3}(\.?)\d{0,9}$/,
  birthdateCsv:        /^((19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|1[0-9]|2[0-9]|3[0-1]))$|((0[1-9]|1[0-9]|2[0-9]|3[0-1])[\/](0[1-9]|1[012])[\/](19|20)\d\d)$/,
  moneyCsv:            /^\d+((\.)\d+)?$/,
  number:              /^[0-9]$/,
  numbers:             /^[0-9]+$/,
  costType:            /^salary$|billing$/
};

export const REGEXPATTERNSHTML = {
  email:                '[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}',
  username:             '[a-zA-Z0-9._]+$|[a-zA-Z0-9._#-]+@[a-z0-9.-]+\\.[a-z]{2,4}',
  password:             '[a-zA-Z0-9._@#-]{5,}$|[a-zA-Z0-9._#-]+@[a-z0-9.-]+\\.[a-z]{2,4}',
  alphanumSpecialPoint: '[a-zA-Z0-9._]+',
  alphanumSpecialMin3:  '[a-zA-Z0-9_]{3,}',
  alphanumSpecialMin6:  '[a-zA-Z0-9_]{6,}',
  range1_255:           '.{1,255}',
  range3_255:           '.{3,255}',
  phone:                '[0-9_#\\+\\-\\s()]{3,20}',
  birthdate:            '((19|20)\\d\\d([-]|[\\/])(0[1-9]|1[012])([-]|[\\/])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))$|((0[1-9]|1[0-9]|2[0-9]|3[0-1])([-]|[\\/])(0[1-9]|1[012])([-]|[\\/])(19|20)\\d\\d)',
  coordinates:          '(-?)\\d{1,3}(\\.?)\\d{0,9}',
  postalCode: {
    ESP: '[0-9]{5}$'
  },
  cost: '[0-9]+([,][0-9]{1,2})?$'
};

export const DATEPICKER = {
  ESP: {
    locale: 'es',
    firstDayOfWeek: 1,
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
    dayNamesMin: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
      'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    today: 'Hoy',
    clear: 'Borrar'
  }
};

export const EMPTYPROFILEPICTURE = 'assets/img/default_avatar.png';

export const AGES = {
  min: 16,
  default: 18,
  max: 100
};

export const SYSTEMLOCALPARAMS = {
  master: {
    rules: {
      person: {max: 500, default: 1, min: 0},
      team:   {max: 500, default: 1, min: 1}
    },
    multiRules: {
      items: 2,
      person: {max: 1, default: 1, min: 0}
    }
  }
};

export const MONTHRANGEDATEPICKERCONFIG: IDatePickerConfig = {
  locale: 'es',
  format: 'MMM YYYY',
  allowMultiSelect: true,
  max: moment(new Date())
}

// export const DAYSHORTLABELS: IMyDayLabels = {su: 'Dom', mo: 'Lun', tu: 'Mar', we: 'Mie', th: 'Jue', fr: 'Vie', sa: 'Sab'}


export const PUSHNOTIFICATIONSMAXHOURS = 2;
