const _ = require('lodash');

export class CostModel {
  id: number;
  billing: number;
  salary: number;

  constructor(data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;

    this.billing = !_.isNil(data) && data.hasOwnProperty('billing') && !_.isNil(data['billing']) ?
      data['billing'] : 0;

    this.salary = !_.isNil(data) && data.hasOwnProperty('salary') && !_.isNil(data['salary']) ?
      data['salary'] : 0;
  }
}
