export class BaseComponentModel {
// @TODO: agregar atributo VISIBLE y aplicar patron observable para cambios en strategy defaul o onPush

  /** COMPONENT VARS **/

  // Public var to static methods

  public isValidObjectResponse = BaseComponentModel.isValidObjectResponse;
  public isValidObjectProperty = BaseComponentModel.isValidObjectProperty;

  // Component vars

  public config = {
    id: 'component_n'.concat(Math.floor(Math.random() * 100000).toString())
  };

  /**
   * Check if the api response has the correct structure
   * @param {Object | any} response
   * @returns {boolean}
   */
  private static isValidObjectResponse(response: object | any): boolean {
    return response.hasOwnProperty('object') && response['object']
      && response['object'].hasOwnProperty('items') && response['object']['items'];
  }

  /**
   * Check if object has property
   * @param {Object} item
   * @param {string} property
   * @returns {boolean}
   */
  public static isValidObjectProperty(item: object, property: string): boolean {
    return item
      && item.hasOwnProperty(property)
      && item[property];
  }

  /** PUBLIC METHODS **/

  /**
   * Get the single and unique identification for component instance
   * @returns {string}
   */
  public getComponentId(): string {
    return this.config.id;
  }
}
