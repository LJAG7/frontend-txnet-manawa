import {BaseModel} from '../base.model';
import {DATEPICKER, REGEXPATTERNS} from '../../const/system.const';

const _ = require('lodash');

export class CalendarModel extends BaseModel {

  private _id: number;
  private _locale: string;
  private _headers: object;
  private _business_hours: Array<object>;
  private _time_format: string;
  private _slot_duration: string;
  private _min_time_hour: string;
  private _max_time_hour: string;

  private _now_indicator: boolean;
  private _nav_links: boolean;
  private _editable: boolean;
  private _droppable: boolean;
  private _height: number;

  private static isValidCalendarHeaders(headers: object): boolean {
    return !_.isNil(headers) && headers
      && headers.hasOwnProperty('left') && _.isString(headers['left'])
      && headers.hasOwnProperty('center') && _.isString(headers['center'])
      && headers.hasOwnProperty('right') && _.isString(headers['right']);
  }

  private static isValidCalendarBusinessHours(businessHours: Array<object>): boolean {
    return !_.isNil(businessHours) && _.isArray(businessHours)
      && _.filter(businessHours, (b) => {
          return !_.isNil(b)
            && b.hasOwnProperty('dow') && !_.isNil(b['dow']) && _.isArray(b['dow']) && !_.isEmpty(b['dow'])
            && b.hasOwnProperty('start') && !_.isNil(b['start']) && _.isString(b['start']) && REGEXPATTERNS.timeHHMM.test(b['start'])
            && b.hasOwnProperty('end') && !_.isNil(b['end']) && _.isString(b['end']) && REGEXPATTERNS.timeHHMM.test(b['end']);
      }).length === businessHours.length;
  }




  constructor(data?: object) {
    super();

    this._id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : Math.floor(Math.random() * 100000);
    this._locale = !_.isNil(data) && data.hasOwnProperty('locale') ? data['locale'] : DATEPICKER.ESP.locale;
    this._height = !_.isNil(data) && data.hasOwnProperty('height') ? data['height'] : DATEPICKER.ESP.locale;

    this._headers = !_.isNil(data) && data.hasOwnProperty('headers')
    && CalendarModel.isValidCalendarHeaders(data['headers'])
      ? data['headers']
      : {
          // left: '',
          // center: 'title',
          // right: ''
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        };


    this._business_hours = !_.isNil(data) && data.hasOwnProperty('business_hours')
    && CalendarModel.isValidCalendarBusinessHours(data['business_hours'])
      ? data['business_hours']
      : [{
          dow: [ 1, 2, 3, 4], // Monday - Thursday
          start: '8:00', // a start time (10am in this example)
          end: '20:00', // an end time (6pm in this example)
        },
          {
            dow: [ 5], // Monday - Thursday
            start: '8:00', // a start time (10am in this example)
            end: '15:00', // an end time (6pm in this example)
          }
        ];

    this._time_format = !_.isNil(data) && data.hasOwnProperty('time_format') ? data['time_format'] : 'H:mm';

    this._slot_duration = !_.isNil(data) && data.hasOwnProperty('slot_duration')
    && REGEXPATTERNS.timeHHMMSS.test(data['slot_duration'])
      ? data['slot_duration']
      : '00:15:00';

    this._min_time_hour = !_.isNil(data) && data.hasOwnProperty('min_time_hour')
    && REGEXPATTERNS.timeHHMMSS.test(data['min_time_hour'])
      ? data['min_time_hour']
      : '07:00:00';

    this._max_time_hour = !_.isNil(data) && data.hasOwnProperty('max_time_hour')
    && REGEXPATTERNS.timeHHMMSS.test(data['max_time_hour'])
      ? data['max_time_hour']
      : '22:00:00';

    this._now_indicator = !_.isNil(data) && data.hasOwnProperty('now_indicator') ? data['now_indicator'] : false;
    this._nav_links = !_.isNil(data) && data.hasOwnProperty('nav_links') ? data['nav_links'] : false;
    this._editable = !_.isNil(data) && data.hasOwnProperty('editable') ? data['editable'] : false;
    this._droppable = !_.isNil(data) && data.hasOwnProperty('droppable') ? data['droppable'] : false;
  }


  get id(): number {
    return this._id;
  }

  get locale(): string {
    return this._locale;
  }

  get headers(): Object {
    return this._headers;
  }

  get businessHours(): Array<Object> {
    return this._business_hours;
  }

  get timeFormat(): string {
    return this._time_format;
  }

  get slotDuration(): string {
    return this._slot_duration;
  }

  get minTimeHour(): string {
    return this._min_time_hour;
  }

  get maxTimeHour(): string {
    return this._max_time_hour;
  }

  get nowIndicator(): boolean {
    return this._now_indicator;
  }

  get navLinks(): boolean {
    return this._nav_links;
  }

  get editable(): boolean {
    return this._editable;
  }

  get droppable(): boolean {
    return this._droppable;
  }
}
