import {BaseModel} from '../base.model';
import {PersonModel} from '../person.model';
import {DomainModel} from '../domain.model';
import {LocationModel} from '../location.model';

const _ = require('lodash');

export class CalendarEntryModel extends BaseModel {
  id: number;
  title: string;
  description: string;
  start_date: string;
  end_date: string;
  url_conference: string;
  conference_allocated: boolean;
  created_by: PersonModel;
  domain: DomainModel;
  guests: Array<PersonModel>;
  location: LocationModel;
  location_name: string;


  constructor(data?: object) {
    super();

    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.title = !_.isNil(data) && data.hasOwnProperty('title') ? data['title'] : null;
    this.description = !_.isNil(data) && data.hasOwnProperty('description') ? data['description'] : null;
    this.start_date = !_.isNil(data) && data.hasOwnProperty('start_date') ? data['start_date'] : null;
    this.end_date = !_.isNil(data) && data.hasOwnProperty('end_date') ? data['end_date'] : null;

    this.url_conference = !_.isNil(data) && data.hasOwnProperty('url_conference') ? data['url_conference'] : null;
    this.conference_allocated = !_.isNil(data) && data.hasOwnProperty('conference_allocated') ? data['conference_allocated'] : false;
    this.location_name = _.isNil(data) || !data.hasOwnProperty('location_name') ? null : data['location_name'];

    this.guests = _.isNil(data) || !data.hasOwnProperty('guests') || !Array.isArray(data['guests'])
      ? []
      : data['guests'].map((guest) => {
        if (guest instanceof PersonModel) {
          return guest;
        }

        return new PersonModel(guest);
      });

    this.created_by = _.isNil(data) || !data.hasOwnProperty('created_by')
      ? null
      : data['created_by'] instanceof PersonModel
        ? data['created_by']
        : new PersonModel(data['created_by']);

    this.domain = _.isNil(data) || !data.hasOwnProperty('domain')
      ? null
      : data['domain'] instanceof DomainModel
        ? data['domain']
        : new DomainModel(data['domain']);

    this.location = _.isNil(data) || !data.hasOwnProperty('location') && !_.isEmpty(data['location'])
      ? null
      : data['location'] instanceof LocationModel
        ? data['location']
        : new LocationModel(data['location']);
  }


  /**
   * Return a XOR evaluate
   * @param a
   * @param b
   * @returns {boolean}
   * @constructor
   */
  private XOR(a: any, b: any): boolean {
    return ( a || b ) && !( a && b );
  }

  /**
   * Return if the guests are the same
   * @param {Array<PersonModel>} otherGuests
   * @returns {boolean}
   */
  private theyAreTheSameGuests(otherGuests: Array<PersonModel>): boolean {
    return _.isArray(this.guests) && _.isArray(otherGuests)
      && this.guests.length === otherGuests.length
      && _.filter(this.guests, (g) => {
            return _.findIndex(otherGuests, (o) => { return o.id === g.id; }) !== -1;
        }).length === this.guests.length;
  }


  /**
   * Return if are equals
   * @param {CalendarEntryModel} other
   * @returns {boolean}
   */
  public isEqual(other: CalendarEntryModel): boolean {
    if ( _.isNil(other)
      || this.title !== other.title
      || this.description !== other.description
      || this.start_date !== other.start_date
      || this.end_date !== other.end_date
      || this.url_conference !== other.url_conference
      || this.conference_allocated !== other.conference_allocated
      || this.getLocationName() !== other.getLocationName()
      || !this.theyAreTheSameGuests(other.guests)) {
      return false;
    }

    if ( this.XOR(this.created_by instanceof PersonModel, other.created_by instanceof PersonModel)
      || this.created_by.id !== other.created_by.id) {
      return false;
    }

    if ( this.XOR(this.domain instanceof DomainModel, other.domain instanceof DomainModel)
      || this.domain.id !== other.domain.id) {
      return false;
    }

    if (this.hasLocationModel()) {
      if ( this.XOR(this.location instanceof LocationModel, other.location instanceof LocationModel)
        || this.location.id !== other.location.id) {
        return false;
      }

    } else if (this.location_name !== other.location_name) {
       return false;
    }


    return true;
  }


  /**
   * Return if exist a location model in CalendarEntryModel
   * @returns {boolean}
   */
  public hasLocationModel(): boolean {
    return !_.isNil(this.location) && this.location.id !== 0;
  }

  /**
   * Get the location name to render
   * @returns {string}
   */
  public getLocationName(): string {
    return this.hasLocationModel()
      ? this.location.name
      : this.location_name !== ''
        ? this.location_name
        : null;
  }
}
