const _ = require('lodash');

export class Cookie {

  name: string;


  private static _setExpireDate(): Date {
    const d1 = new Date(),
      d2 = new Date(d1);
    d2.setHours(d1.getHours() + 1);
    return d2;
  }

  private static _getStringInstance(name: string): string {
    const cookieString = document.cookie
      .split(';')
      .find((element) => {
        return element.indexOf(name) !== -1;
      });

    return cookieString !== undefined
      ? cookieString.substring(cookieString.indexOf('=') + 1) // cookieString.split('=')[1]
      : null;
  }


  public static _delete(name: string): void {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
  }

  public static _getAllCookieNamesInSpace(): Array<any> {
    const cookies = document.cookie.split(';'),
      cookieNames = [];

    for (let i = 0, loops = cookies.length; i < loops; i++) {
      const eqPos = cookies[i].indexOf('='),
        name = eqPos > -1 ? cookies[i].substr(0, eqPos) : cookies[i];

      cookieNames.push(name);
    }

    return cookieNames;
  }

  constructor (name: string) {
    this.name = name;
  }


  public exist(): boolean {
    return Cookie._getStringInstance(this.name) !== null;
  }


  public getStringInstance(): string {
    return Cookie._getStringInstance(this.name);
  }

  public getJsonInstance(): any {
    return JSON.parse(Cookie._getStringInstance(this.name));
  }

  public delete(): void {
    Cookie._delete(this.name);
  }


  public generate(data: string | object): void {
    const stringData = typeof data === 'object'
      ? JSON.stringify(data)
      : data;

    document.cookie = this.name + '='.concat(stringData) + '; expires=' + Cookie._setExpireDate() + '; path=/';
  }

  public equalObjectToStringInstance(data: object) {
    const dataProperties = Object.getOwnPropertyNames(data);
    const cookieProperties = Object.getOwnPropertyNames(this.getJsonInstance());

    // If the number of properties are diferents then the objects are no equals.
    if (dataProperties.length !== cookieProperties.length) {
      return false;
    }

    // If the objects have same numbers of properties
    // We need to check if the properties values are the same
    for (let i = 0; i < dataProperties.length; i++) {
      const saveNameProperty = dataProperties[i]; // The name of the property name

      if ( data[saveNameProperty] !== Cookie._getStringInstance(this.name)[saveNameProperty]) {
        return false;
      }
    }

    return true;
  }


  public hasParam(key: string): boolean {
    return this.getJsonInstance().hasOwnProperty(key);
  }

  public getParam(key: string): any {
    return this.hasParam(key)
      ? this.getJsonInstance()[key]
      : undefined;
  }
}
