import {CSVRowModel} from './row.model';

const _ = require('lodash');

export class CSVModel {
  separator: string;
  headers: Array<any>;
  data: Array<CSVRowModel>;
  plainHeaders: Array<any>;
  plainData: Array<any>;
  error: string;

  constructor(data?: object) {
    this.separator = !_.isNil(data) && data.hasOwnProperty('separator') ? data['separator'] : undefined;
    this.error = !_.isNil(data) && data.hasOwnProperty('error') ? data['error'] : null;

    this.headers = !_.isNil(data) && data.hasOwnProperty('headers') ? data['headers'] : [];
    this.plainHeaders = !_.isNil(data) && data.hasOwnProperty('plainHeaders') ? data['plainHeaders'] : [];
    this.data = !_.isNil(data) && data.hasOwnProperty('data') ? data['data'] : [];
    this.plainData = !_.isNil(data) && data.hasOwnProperty('plainData') ? data['plainData'] : [];
  }

  /**
   * Add values in associated array format (header:value)
   * @param {Array<any>} plainData
   */
  generateValues(plainData: Array<any>): object {
    const values = {};

    if (plainData.length === this.plainHeaders.length) {

      for (let idx = 0, nloop = this.plainHeaders.length; idx < nloop; idx++) {
        if (this.headers.indexOf(this.plainHeaders[idx]) !== -1) {
          values[this.plainHeaders[idx]] = _.trim(plainData[idx]);
        }
      }
    }

    return values;
  }

  /**
   * Return a valid collection of csv rows
   * @returns {Array<CSVRowModel>}
   */
  getValidRows(): Array<CSVRowModel> {
    return this.data.filter((row: CSVRowModel) => {
      return row.isValid();
    });
  }

  /**
   * Return a invalid collection of csv rows
   * @returns {Array<CSVRowModel>}
   */
  getInvalidRows(): Array<CSVRowModel> {
    return this.data.filter((row: CSVRowModel) => {
      return !row.isValid();
    });
  }

  /**
   * Total valid rows
   * @returns {number}
   */
  getTotalValidRows(): number {
    return this.getValidRows().length;
  }

  /**
   * Total invalid rows
   * @returns {number}
   */
  getTotalInvalidRows(): number {
    return this.getInvalidRows().length;
  }

  /**
   * Total warning rows
   * @returns {number}
   */
  getTotalWarnings(): number {
    return this.data.filter((row: CSVRowModel) => {
      return !_.isEmpty(row.warnings);
    }).length;
  }
}
