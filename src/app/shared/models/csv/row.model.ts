const _ = require('lodash');

export class CSVRowModel {
  row: number;
  values: object;
  warnings: Array<string>;
  errors: Array<string>;

  constructor(data?: object) {
    this.row = !_.isNil(data) && data.hasOwnProperty('row') ? data['row'] : -1;

    this.errors = !_.isNil(data) && data.hasOwnProperty('errors') ? data['errors'] : [];
    this.warnings = !_.isNil(data) && data.hasOwnProperty('warnings') ? data['warnings'] : [];
    this.values = !_.isNil(data) && data.hasOwnProperty('values') ? data['values'] : {};
  }

  /**
   * Return if a valid row (no warnings no error)
   * @returns {boolean}
   */
  isValid(): boolean {
    return this.warnings.length === 0 && this.errors.length === 0;
  }
}
