import {BaseModel} from './base.model';
import {PersonModel} from './person.model';
import {LocationModel} from './location.model';
import {TeamModel} from './team/team.model';
import {RoleModel} from './team/role.model';
const _ = require('lodash');

// @TODO: pendiente modelacion: teamt, scope, vissible
export class DomainModel extends BaseModel{
  id: number;
  name: string;
  url_domain: string;
  administered_by: PersonModel;
  created_by: PersonModel;
  locations: Array<LocationModel>;
  main_location: LocationModel;

  parameters_value: Array<any>;
  roles: Array<RoleModel>;
  scope: string;
  teams: Array<TeamModel>;
  teamts: Array<object>;
  vissible: boolean;


  constructor(data?: object) {
    super();
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.name = !_.isNil(data) && data.hasOwnProperty('name') ? data['name'] : '';
    this.url_domain = !_.isNil(data) && data.hasOwnProperty('url_domain') ? data['url_domain'] : '';

    this.scope = !_.isNil(data) && data.hasOwnProperty('scope') ? data['scope'] : 'self';
    this.vissible = !_.isNil(data) && data.hasOwnProperty('vissible') ? data['vissible'] : false;

    this.administered_by = _.isNil(data) || !data.hasOwnProperty('administered_by')
      ? new PersonModel()
      : data['administered_by'] instanceof PersonModel
        ? data['administered_by']
        : new PersonModel(data['administered_by']);

    this.created_by = _.isNil(data) || !data.hasOwnProperty('created_by')
      ? new PersonModel()
      : data['created_by'] instanceof PersonModel
        ? data['created_by']
        : new PersonModel(data['created_by']);

    this.main_location = _.isNil(data) || !data.hasOwnProperty('main_location')
      ? new LocationModel()
      : data['main_location'] instanceof LocationModel
        ? data['main_location']
        : new LocationModel(data['main_location']);

    this.locations = _.isNil(data)
    || !data.hasOwnProperty('locations')
    || !Array.isArray(data['locations'])
      ? []
      : data['locations'].map((location) => {
        if (location instanceof LocationModel) {
          return location;
        }

        return new LocationModel(location);
      });

    this.parameters_value = _.isNil(data)
    || !data.hasOwnProperty('parameters_value')
    || !Array.isArray(data['parameters_value'])
      ? []
      : data['parameters_value'];

    this.roles = _.isNil(data)
    || !data.hasOwnProperty('roles')
    || !Array.isArray(data['roles'])
      ? []
      : data['roles'].map((role) => {
        if (role instanceof RoleModel) {
          return role;
        }

        return new RoleModel(role);
      });


    this.teams = _.isNil(data)
    || !data.hasOwnProperty('teams')
    || !Array.isArray(data['teams'])
      ? []
      : data['teams'].map((team) => {
        if (team instanceof TeamModel) {
          return team;
        }

        return new TeamModel(team);
      });

    this.teamts = _.isNil(data)
    || !data.hasOwnProperty('teamts')
    || !Array.isArray(data['teamts'])
      ? []
      : data['teamts'];
  }


  /**
   * Return if exist a valid main_location
   * @returns {boolean}
   */
  hasMainLocation(): boolean {
    return !_.isNil(this.main_location)
    && !_.isEmpty(this.main_location)
    && this.main_location instanceof LocationModel
    && this.main_location.id > 0;
  }

  /**
   * Return if exist a valid administered_by
   * @returns {boolean}
   */
  hasAdministeredBy(): boolean {
    return !_.isNil(this.administered_by)
      && !_.isEmpty(this.administered_by)
      && this.administered_by instanceof PersonModel
      && this.administered_by.id > 0;
  }

  /**
   * Return if exist a valid administered_by
   * @returns {boolean}
   */
  hasCreatedBy(): boolean {
    return !_.isNil(this.created_by)
      && !_.isEmpty(this.created_by)
      && this.created_by instanceof PersonModel
      && this.created_by.id > 0;
  }

  getMainLocationNameAndAddress(): string {
    return !_.isNil(this.main_location)
      ? this.main_location.getNameAndCompleteAddress()
      : '';
  }
}
