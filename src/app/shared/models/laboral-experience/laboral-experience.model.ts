const _ = require('lodash');

export class LaboralExperienceModel {
  id: number;
  name: string;
  company: string;
  location: string;
  start_date: Date;
  end_date: Date;
  is_current: boolean;
  country: string;
  summary: string;

  constructor(data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.name = !_.isNil(data) && data.hasOwnProperty('title') ? data['title'] : '';
    this.company = !_.isNil(data) && data.hasOwnProperty('company') && data['company'].hasOwnProperty('name')
      ? data['company']['name'] : '';
    this.is_current = !_.isNil(data) && data.hasOwnProperty('isCurrent') ? data['isCurrent'] : false;
    this.country = !_.isNil(data) && data.hasOwnProperty('location')
      && data['location'].hasOwnProperty('country')
        ? data['location']['country']['name'] : '';
    this.location = !_.isNil(data) && data.hasOwnProperty('location')
    && data['location'].hasOwnProperty('name')
      ? data['location']['name'] : '';
    this.start_date = !_.isNil(data) && data.hasOwnProperty('startDate')
      && data['startDate'].hasOwnProperty('month') && data['startDate'].hasOwnProperty('year')
        ? new Date(data['startDate']['year'], data['startDate']['month'], 1) : null;
    this.end_date = !_.isNil(data) && data.hasOwnProperty('endDate')
      && data['endDate'].hasOwnProperty('month') && data['endDate'].hasOwnProperty('year')
        ? new Date(data['endDate']['year'], data['endDate']['month'], 1) : null;
    this.summary = !_.isNil(data) && data.hasOwnProperty('summary') ? data['summary'] : '';
  }
}
