const _ = require('lodash');

export class LinkedinEndpointModel {
  url: string;
  response_type: string;
  client_id: string;
  state: string;
  scope: string;
  redirect_uri: string;
  backend_endpoint: string;

  constructor(data?: object) {
    this.url = !_.isNil(data) && data.hasOwnProperty('linkedin_endpoint')
    && data['linkedin_endpoint'].hasOwnProperty('url') ? data['linkedin_endpoint']['url'] : '';

    this.response_type = !_.isNil(data) && data.hasOwnProperty('linkedin_endpoint')
    && data['linkedin_endpoint'].hasOwnProperty('response_type') ? data['linkedin_endpoint']['response_type'] : '';

    this.client_id = !_.isNil(data) && data.hasOwnProperty('linkedin_endpoint')
    && data['linkedin_endpoint'].hasOwnProperty('client_id') ? data['linkedin_endpoint']['client_id'] : '';

    this.state = !_.isNil(data) && data.hasOwnProperty('linkedin_endpoint')
    && data['linkedin_endpoint'].hasOwnProperty('state') ? data['linkedin_endpoint']['state'] : '';

    this.scope = !_.isNil(data) && data.hasOwnProperty('linkedin_endpoint')
    && data['linkedin_endpoint'].hasOwnProperty('scope') ? data['linkedin_endpoint']['scope'] : '';

    this.redirect_uri = !_.isNil(data) && data.hasOwnProperty('linkedin_endpoint')
    && data['linkedin_endpoint'].hasOwnProperty('redirect_uri') ? data['linkedin_endpoint']['redirect_uri'] : '';

    this.backend_endpoint = !_.isNil(data) && data.hasOwnProperty('backend_endpoint') ? data['backend_endpoint'] : '';
  }

  public getAuthorizationEndpoint(): string {
    let authorizationEndpoint = this.url + '?';

    for (let i = 0; i < Object.keys(this).length; i++) {
      const key = Object.keys(this)[i];

      if (key !== 'url'
        && key !== 'backend_endpoint'
        && !_.isNil(this[key])
        && !_.isEmpty(this[key])) {

        authorizationEndpoint += key + '=' + this[key] + '&';
      }
    }

    return authorizationEndpoint;
  }
}
