export class Loader {
  type: LoaderType;
  message: LoaderMessageType;
}

export enum LoaderMessageType {
  Loading,
  Updating,
  Saving,
  Deleting,
  NotAllowed,
  Verifying,
  Importing,
  Redirecting
}

export enum LoaderType {
  General,
  SoftLoader,
  Denegation
}


