import {DomainModel} from './domain.model';

const _ = require('lodash');

export class LocationModel {
  id: number;
  name: string;
  description: string;
  location_type: string;
  street_type: string;
  street_address: string;
  external_number: string;
  internal_number: string;
  floor: string;
  address_extra_info: string;
  building: string;
  postal_code: string;
  city: string;
  province: string;
  country: string;
  latitude: number;
  longitude: number;
  domain: DomainModel;

  constructor(data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.name = !_.isNil(data) && data.hasOwnProperty('name') ? data['name'] : '';
    this.description = !_.isNil(data) && data.hasOwnProperty('description') ? data['description'] : '';
    this.location_type = !_.isNil(data) && data.hasOwnProperty('location_type') ? data['location_type'] : 'NORMAL';
    this.street_type = !_.isNil(data) && data.hasOwnProperty('street_type') ? data['street_type'] : '';
    this.street_address = !_.isNil(data) && data.hasOwnProperty('street_address') ? data['street_address'] : '';
    this.external_number = !_.isNil(data) && data.hasOwnProperty('external_number') ? data['external_number'] : '';
    this.internal_number = !_.isNil(data) && data.hasOwnProperty('internal_number') ? data['internal_number'] : '';
    this.floor = !_.isNil(data) && data.hasOwnProperty('floor') ? data['floor'] : '';
    this.address_extra_info = !_.isNil(data) && data.hasOwnProperty('address_extra_info') ? data['address_extra_info'] : '';
    this.building = !_.isNil(data) && data.hasOwnProperty('building') ? data['building'] : '';
    this.postal_code = !_.isNil(data) && data.hasOwnProperty('postal_code') ? data['postal_code'] : '';
    this.city = !_.isNil(data) && data.hasOwnProperty('city') ? data['city'] : '';
    this.province = !_.isNil(data) && data.hasOwnProperty('province') ? data['province'] : '';
    this.country = !_.isNil(data) && data.hasOwnProperty('country') ? data['country'] : '';
    this.latitude = !_.isNil(data) && data.hasOwnProperty('latitude') ? data['latitude'] : null;
    this.longitude = !_.isNil(data) && data.hasOwnProperty('longitude') ? data['longitude'] : null;

    this.domain = _.isNil(data) || !data.hasOwnProperty('domain')
      ? null
      : data['domain'] instanceof DomainModel
        ? data['domain']
        : new DomainModel(data['domain']);
  }

  public getCompleteAddress(): string {
    let completeAddress = this.street_type + ' ' + this.street_address;

    completeAddress += !_.isEmpty(this.external_number)
      ? ' nº' + this.external_number
      : ' s/n';

    if (!_.isEmpty(this.building)) {
      completeAddress += ', edf. ' + this.building;
    }

    if (!_.isEmpty(this.floor)) {
      completeAddress += ', planta ' + this.floor;
    }

    if (!_.isEmpty(this.internal_number)) {
      completeAddress += ', ' + this.internal_number;
    }

    if (!_.isEmpty(this.address_extra_info)) {
      completeAddress += ', ' + this.address_extra_info;
    }

    return completeAddress;
  }


  public getNameAndCompleteAddress(): string {
    return this.name + ', ' + this.getCompleteAddress();
  }
}
