const _ = require('lodash');

export class LogModel {
  error: string;
  action: string;
  description: string;
  time_miliseg_api: string;

  constructor(data?: object) {
    this.error = !_.isNil(data) && data.hasOwnProperty('error') ? data['error'] : '';
    this.action = !_.isNil(data) && data.hasOwnProperty('action') ? data['action'] : '';
    this.description = !_.isNil(data) && data.hasOwnProperty('description') ? data['description'] : '';
    this.time_miliseg_api = !_.isNil(data) && data.hasOwnProperty('time_miliseg_api') ? data['time_miliseg_api'] : '';
  }
}
