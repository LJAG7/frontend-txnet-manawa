const _ = require('lodash');

export class MasterClassModel {
  id: number;
  name: string;
  iconcss: string;

  constructor(data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.name = !_.isNil(data) && data.hasOwnProperty('name') ? data['name'] : '';
    this.iconcss = !_.isNil(data) && data.hasOwnProperty('iconcss') ? data['iconcss'] : '';
  }
}
