export class NoteActions {
  public static status = [
    'NOT_STARTED',
    'ONGOING',
    'BLOCKED',
    'CANCELLED',
    'ENDED'
  ];

  public static actions = {
    CANCEL: 'CANCELLED',
    START: 'ONGOING',
    BLOCK: 'BLOCKED',
    END: 'ENDED'
  };
}
