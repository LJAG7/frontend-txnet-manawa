import {NoteActions} from './note-actions.model';
import {TeamModel} from '../team/team.model';

const _ = require('lodash');

export class NoteModel {
  id: number;
  title: string;
  description: string;
  status: string;
  type: string;
  teams: Array<TeamModel>;
  end_date: string;
  deleted_at: string;
  workflow_available_actions: object;
  color: string;

  constructor (data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.title = !_.isNil(data) && data.hasOwnProperty('title') ? data['title'] : '';
    this.description = !_.isNil(data) && data.hasOwnProperty('description') ? data['description'] : '';
    this.status = !_.isNil(data) && data.hasOwnProperty('status') ? data['status'] : NoteActions.status[0];
    this.type = !_.isNil(data) && data.hasOwnProperty('type') ? data['type'] : '';
    this.end_date = !_.isNil(data) && data.hasOwnProperty('end_date') ? data['end_date'] : '';
    this.deleted_at = !_.isNil(data) && data.hasOwnProperty('deleted_at') ? data['deleted_at'] : '';
    this.workflow_available_actions = !_.isNil(data) && data.hasOwnProperty('workflow_available_actions')
      ? data['workflow_available_actions']
      : null;

    this.teams = _.isNil(data)
    || !data.hasOwnProperty('teams')
    || !Array.isArray(data['teams'])
      ? []
      : data['teams'].map((team) => {
        if (team instanceof TeamModel) {
          return team;
        }

        return new TeamModel(team);
      });


    this.color = '#fefefe';
  }
}
