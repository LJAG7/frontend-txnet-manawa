import {BaseModel} from './base.model';
import {LocationModel} from './location.model';
import {RoleSystemModel} from './role-system.model';
import {DomainModel} from './domain.model';
import {UserModel} from './user.model';
import {TeamModel} from './team/team.model';
import {DashboardModel} from './widgets/dashboard.model';
import {TeamPersonModel} from './team/teamPerson.model';
import {RoleModel} from './team/role.model';
import {ROLESYSTEMS} from '../const/rolesystems.const';
import {EMPTYPROFILEPICTURE} from '../const/system.const';
import {CostModel} from './administration/cost.model';

const _ = require('lodash');

export class PersonModel extends BaseModel {
  id: number;
  domain: DomainModel;
  name: string;
  surname: string;
  email: string;
  phone: string;
  birthdate: Date;
  status: string;
  activity_status: string;
  administered_domains: Array<DomainModel>;
  managed_teams: Array<TeamModel>;
  location: LocationModel;
  role_systems: Array<RoleSystemModel>;
  roles: Array<RoleModel>;
  user: UserModel;
  profile_picture: string;

  dashboard_entries: Array<DashboardModel>;
  team_persons: Array<TeamPersonModel>;
  teams: Array<TeamModel>;
  delete_at: string;
  invited_by: PersonModel;
  cost: CostModel;


  constructor(data?: object) {
    super();
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.name = !_.isNil(data) && data.hasOwnProperty('name') ? data['name'] : '';
    this.surname = !_.isNil(data) && data.hasOwnProperty('surname') ? data['surname'] : '';
    this.email = !_.isNil(data) && data.hasOwnProperty('email') ? data['email'] : '';
    this.phone = !_.isNil(data) && data.hasOwnProperty('phone') ? data['phone'] : '';
    this.birthdate = !_.isNil(data) && data.hasOwnProperty('birthdate') ? new Date(data['birthdate']) : null;
    this.status = !_.isNil(data) && data.hasOwnProperty('status') ? data['status'] : '';
    this.activity_status = !_.isNil(data) && data.hasOwnProperty('activity_status') ? data['activity_status'] : '';
    this.delete_at = !_.isNil(data) && data.hasOwnProperty('deleted_at') ? data['deleted_at'] : '';

    this.cost = !_.isNil(data) && data.hasOwnProperty('cost') ? new CostModel(data['cost']) : null;

    this.profile_picture = !_.isNil(data) && data.hasOwnProperty('profile_picture')
      ? data['profile_picture'] : EMPTYPROFILEPICTURE;

    this.user = _.isNil(data) || !data.hasOwnProperty('user')
      ? null
      : data['user'] instanceof UserModel
        ? data['user']
        : new UserModel(data['user']);

    this.roles = _.isNil(data) || !data.hasOwnProperty('roles')
      ? null
      : data['roles'] instanceof UserModel
        ? data['roles']
        : new RoleModel(data['roles']);


    this.domain = _.isNil(data) || !data.hasOwnProperty('domain')
      ? null
      : data['domain'] instanceof DomainModel
        ? data['domain']
        : new DomainModel(data['domain']);

    this.location = _.isNil(data) || !data.hasOwnProperty('location')
      ? null
      : data['location'] instanceof LocationModel
        ? data['location']
        : new LocationModel(data['location']);

    this.invited_by = _.isNil(data) || !data.hasOwnProperty('invited_by')
      ? null
      : data['invited_by'] instanceof PersonModel
        ? data['invited_by']
        : new PersonModel(data['invited_by']);

    this.administered_domains = _.isNil(data)
    || !data.hasOwnProperty('administered_domains')
    || !Array.isArray(data['administered_domains'])
      ? []
      : data['administered_domains'].map((domain) => {
          if (domain instanceof DomainModel) {
            return domain;
          }

          return new DomainModel(domain);
      });

    this.managed_teams = _.isNil(data)
    || !data.hasOwnProperty('managed_teams')
    || !Array.isArray(data['managed_teams'])
      ? []
      : data['managed_teams'].map((team) => {
        if (team instanceof TeamModel) {
          return team;
        }

        return new TeamModel(team);
      });

    this.role_systems = _.isNil(data)
    || !data.hasOwnProperty('main_role_systems')
    || !Array.isArray(data['main_role_systems'])
      ? []
      : data['main_role_systems'].map((role) => {
        if (role instanceof RoleSystemModel) {
          return role;
        }

        return new RoleSystemModel(role);
      });

    this.dashboard_entries = _.isNil(data)
    || !data.hasOwnProperty('dashboard_entries')
    || !Array.isArray(data['dashboard_entries'])
      ? []
      : data['dashboard_entries'].map((dashboard) => {
        if (dashboard instanceof DashboardModel) {
          return dashboard;
        }

        return new DashboardModel(dashboard);
      });

    this.team_persons = _.isNil(data)
    || !data.hasOwnProperty('team_persons')
    || !Array.isArray(data['team_persons'])
      ? []
      : data['team_persons'].map((teamPerson) => {
        if (teamPerson instanceof TeamPersonModel) {
          return teamPerson;
        }

        return new TeamPersonModel(teamPerson);
      });

    this.teams = _.isNil(data)
    || !data.hasOwnProperty('teams')
    || !Array.isArray(data['teams'])
      ? []
      : data['teams'].map((team) => {
        if (team instanceof TeamModel) {
          return team;
        }

        return new TeamModel(team);
      });
  }

  public getFullName(): string {
    return _.isNil(this.name)
      ? ''
      : _.isNil(this.surname)
        ? this.name
        : this.name + ' ' + this.surname;
  }

  public getNameFirstSurname(): string {
    return _.isNil(this.name)
      ? ''
      : _.isNil(this.surname)
        ? this.name
        : _.trim(this.name.concat(' ', _.split(this.surname, ' ', 1)[0]));
  }


  public getRoleTag() {
    return !_.isNil(this.role_systems)
    && !_.isEmpty(this.role_systems)
    && this.role_systems.length > 0 ?
      this.role_systems[0].tag
      : '';
  }

  public isSuperAdmin(): boolean {
    return this.hasRoleSystem(ROLESYSTEMS.ROLE_SUPER_ADMIN);
  }

  public isAdmin(): boolean {
    return this.hasRoleSystem(ROLESYSTEMS.ROLE_ADMIN);
  }

  public isGestor(): boolean {
    return this.hasRoleSystem(ROLESYSTEMS.ROLE_GESTOR);
  }

  public isMate(): boolean {
    return this.hasRoleSystem(ROLESYSTEMS.ROLE_USER)
      && !(this.isGestor() || this.isAdmin() || this.isSuperAdmin());
  }


  /**
   * Return a determinated role system or null if not exists in Person
   * @param {string} roleSystem
   * @returns {RoleSystemModel}
   */
  public getRoleSystem(roleSystem: string): RoleSystemModel {
    for (const role of this.role_systems) {
      if (role.tag === roleSystem) {
        return role;
      }
    }

    return null;
  }


  /**
   * Return if a person has a rolesystem o rolesystem list (only one rolesystem in list return true)
   * @param {Array<string> | any} roleSystems
   * @returns {boolean}
   */
  public hasRoleSystem(roleSystems: Array<string> | any): boolean {
    const userRoles = !_.isArray(roleSystems)
      ? [roleSystems]
      : roleSystems;


    for (const role of userRoles) {
      if (!_.isNull(this.getRoleSystem(role))) {
        return true;
      }
    }

    return false;
  }
}
