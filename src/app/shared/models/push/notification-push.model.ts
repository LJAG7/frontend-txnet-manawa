const moment = require('moment');
const _ = require('lodash');

export class NotificationPushModel {
  action: string;
  description: string;
  titulo: string;
  datetime: any;
  created_by: any;

  constructor (data?: object) {
    this.titulo = !_.isNil(data) && data.hasOwnProperty('titulo') ? data['titulo'] : '';
    this.action = !_.isNil(data) && data.hasOwnProperty('action') ? data['action'] : '';
    this.description = !_.isNil(data) && data.hasOwnProperty('description') ? data['description'] : '';
    this.datetime = !_.isNil(data) && data.hasOwnProperty('datetime') ? moment(data['datetime']) : '';
    this.created_by = !_.isNil(data) && data.hasOwnProperty('created_by') ? data['created_by'] : '';
  }

  public getNotificationKey(): string {
    return this.datetime.format() + '@' + this.created_by;
  }
}
