const _ = require('lodash');

export class RoleSystemModel {
  id: number;
  name: string;
  tag: string;
  description: string;

  constructor(data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.name = !_.isNil(data) && data.hasOwnProperty('name') ? data['name'] : '';
    this.tag = !_.isNil(data) && data.hasOwnProperty('tag') ? data['tag'] : '';
    this.description = !_.isNil(data) && data.hasOwnProperty('description') ? data['description'] : '';
  }
}
