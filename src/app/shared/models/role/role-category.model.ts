const _ = require('lodash');

export class RoleCategoryModel {
  id: number;
  name: string;

  constructor(data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.name = !_.isNil(data) && data.hasOwnProperty('name') ? data['name'] : '';
  }
}
