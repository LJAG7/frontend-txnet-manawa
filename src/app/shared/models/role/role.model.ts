import {RoleCategoryModel} from './role-category.model';

const _ = require('lodash');

export class RoleModel {
  id: number;
  name: string;
  description: string;
  role_class: RoleCategoryModel;

  constructor(data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.name = !_.isNil(data) && data.hasOwnProperty('name') ? data['name'] : '';
    this.description = !_.isNil(data) && data.hasOwnProperty('description') ? data['description'] : '';
    this.role_class = !_.isNil(data) && data.hasOwnProperty('role_class') ? new RoleCategoryModel(data['role_class']) : null;
  }

  getRoleCategory(): RoleCategoryModel {
    return this.role_class;
  }
}
