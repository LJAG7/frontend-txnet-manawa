import {BarChartElementModel} from './bar-chart-element.model';

const _ = require('lodash');

export class BarChartDataModel {

  public labels: Array<string>;
  public datasets: Array<BarChartElementModel>;

  constructor() {
    this.labels = [];
    this.datasets = [];
  }

  public addLabel(label: string): void {
    this.labels.push(label);
  }

  public addDataset(teamName: string): void {
    let findObject = false;
    for (let i = 0; i < this.datasets.length; i++) {
      if (this.datasets[i].label === teamName) {
        findObject = true;
        break;
      }
    }

    if (!findObject) {
      this.datasets.push(new BarChartElementModel(teamName));
    }
  }

  public addDataToDataset(teamName: string, data: number): void {
    this.datasets.forEach( team => {
      if (team.label === teamName) {
        const value = data > 0 ? Number(data / 60).toFixed(2) : '0.00';
        team.data.push(value);
      }
    });
  }

  public addValueToDataset(label: string, data: number): void {
    this.datasets.forEach( actualLabel => {
      if (actualLabel.label === label) {
        actualLabel.data.push(data.toString());
      }
    });
  }

  public hasData(): boolean {
    return _.isNil(this.datasets) || _.isEmpty(this.datasets) || this.datasets.length === 0 ? false
      : _.isNil(this.datasets[0].data) || _.isEmpty(this.datasets[0].data) || this.datasets[0].data.length === 0 ? false : true;
  }
}
