import {LineChartElementModel} from './line-chart-element.model';

const _ = require('lodash');

export class LineChartDataModel {

  public labels: Array<string>;
  public datasets: Array<LineChartElementModel>;

  constructor() {
    this.labels = [];
    this.datasets = [];
  }

  public addLabel(label: string): void {
    this.labels.push(label);
  }

  public addDataset(teamName: string): void {
    let findObject = false;
    for (let i = 0; i < this.datasets.length; i++) {
      if (this.datasets[i].label === teamName) {
        findObject = true;
        break;
      }
    }

    if (!findObject) {
      this.datasets.push(new LineChartElementModel(teamName));
    }
  }

  public addDataToDataset(teamName: string, data: number): void {
    this.datasets.forEach( team => {
      if (team.label === teamName) {
        const value = data > 0 ? Number(data / 60).toFixed(2) : '0.00';
        team.data.push(value);
      }
    });
  }

  public hasData(): boolean {
    return this.datasets.length > 0;
  }
}
