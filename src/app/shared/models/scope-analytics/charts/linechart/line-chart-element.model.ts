import {ColorGeneratorPipe} from '../../../../pipes/colorGenerator.pipe';

const _ = require('lodash');

export class LineChartElementModel {

  public label: string;
  public borderColor: string;
  public backgroundColor: string;
  public hoverBackgroundColor: string;
  public data: Array<string> = [];
  public fill: boolean;

  public colorGeneratorPipe: ColorGeneratorPipe;

  constructor(name: string) {
    this.colorGeneratorPipe = new ColorGeneratorPipe();
    this.fill = false;

    if (!_.isNil(name) && !_.isEmpty(name)) {
      this.label = name;
      this.borderColor = this.colorGeneratorPipe.transform(this.label);
      this.backgroundColor = this.borderColor;
      this.hoverBackgroundColor = this.colorGeneratorPipe.transformToHover(this.backgroundColor);
    }
  }
}
