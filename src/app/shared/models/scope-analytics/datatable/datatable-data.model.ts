import {DatatableElementModel} from './datatable-element.model';
import {DatatableLabelModel} from './datatable-label.model';

const _ = require('lodash');

export class DatatableDataModel {

  public labels: Array<DatatableLabelModel>;
  public datasets: Array<DatatableElementModel>;

  constructor() {
    this.labels = [];
    this.datasets = [];
  }

  public addLabel(label: DatatableLabelModel): void {
    this.labels.push(label);
  }

  public addDataset(teamName: string): void {
    let findObject = false;
    for (let i = 0; i < this.datasets.length; i++) {
      if (this.datasets[i].label === teamName) {
        findObject = true;
        break;
      }
    }

    if (!findObject) {
      this.datasets.push(new DatatableElementModel(teamName));
    }
  }

  public addDataToDataset(teamName: string, value: string): void {
    this.datasets.forEach( team => {
      if (team.label === teamName) {
        team.data.push(value);
      }
    });
  }

  public hasData(): boolean {
    return _.isNil(this.datasets) || _.isEmpty(this.datasets) || this.datasets.length === 0 ? false
      : _.isNil(this.datasets[0].data) || _.isEmpty(this.datasets[0].data) || this.datasets[0].data.length === 0 ? false : true;
  }
}
