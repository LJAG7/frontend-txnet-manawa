import {ColorGeneratorPipe} from '../../../pipes/colorGenerator.pipe';

const _ = require('lodash');

export class DatatableElementModel {

  public label: string;
  public borderColor: string;
  public backgroundColor: string;
  public hoverBackgroundColor: string;
  public data: Array<string> = [];
  public colorGeneratorPipe: ColorGeneratorPipe;

  constructor(name: string) {
    this.colorGeneratorPipe = new ColorGeneratorPipe();

    if (!_.isNil(name) && !_.isEmpty(name)) {
      this.label = name;
      this.borderColor = this.colorGeneratorPipe.transform(this.label);
      this.backgroundColor = this.borderColor;
      this.hoverBackgroundColor = this.colorGeneratorPipe.transformToHover(this.backgroundColor);
    }
  }
}
