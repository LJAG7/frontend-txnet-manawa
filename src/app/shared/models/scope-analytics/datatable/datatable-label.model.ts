import {ColorGeneratorPipe} from '../../../pipes/colorGenerator.pipe';

const _ = require('lodash');

export class DatatableLabelModel {

  public value: string;
  public comparative: boolean;
  public color: string;

  constructor(data?: object) {
    this.value = !_.isNil(data) && data.hasOwnProperty('value') ? data['value'] : '';
    this.comparative = !_.isNil(data) && data.hasOwnProperty('comparative') ? data['comparative'] : false;
    this.color = this.comparative ? 'gray' : 'white';
  }
}
