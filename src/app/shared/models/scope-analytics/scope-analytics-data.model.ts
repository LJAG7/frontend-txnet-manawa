import {ScopeAnalyticsTeamModel} from './scope-analytics-team.model';
import {ScopeAnalyticsDateModel} from './scope-analytics-date.model';

const _ = require('lodash');

export class ScopeAnalyticsDataModel {

  dates: Array<ScopeAnalyticsDateModel>;
  teams: Array<ScopeAnalyticsTeamModel>;
  total = 0;

  constructor(data?: object) {
    if (!_.isNil(data) && data.hasOwnProperty('dates') && !_.isEmpty(data['dates'])) {
      this.dates = [];
      Object.keys(data['dates']).forEach((date) => {
       this.dates.push(new ScopeAnalyticsDateModel(date, data['dates'][date]));
      });
    } else {
      this.dates = [];
    }

    if (!_.isNil(data) && data.hasOwnProperty('teams') && !_.isEmpty(data['teams'])) {
      this.teams = [];
      Object.keys(data['teams']).forEach((team) => {
        this.teams.push(new ScopeAnalyticsTeamModel(Number(team), data['teams'][team]));
      });
    } else {
      this.teams = [];
    }

    this.total = !_.isNil(data) && data.hasOwnProperty('total') ? data['total'] : 0;
  }
}
