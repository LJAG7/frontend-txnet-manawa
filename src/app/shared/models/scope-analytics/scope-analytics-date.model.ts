import {ScopeAnalyticsTeamModel} from './scope-analytics-team.model';
import {ScopeAnalyticsStatusModel} from './scope-analytics-status.model';

const _ = require('lodash');

export class ScopeAnalyticsDateModel {
  date: string;
  status: ScopeAnalyticsStatusModel;
  teams: Array<ScopeAnalyticsTeamModel>;
  total: number;
  withoutteam: number;

  constructor(date: string, data?: object) {
    this.date = !_.isNil(date) ? date : '';
    this.total = !_.isNil(data) && data.hasOwnProperty('total') ? data['total'] : 0;
    this.withoutteam = !_.isNil(data) && data.hasOwnProperty('withoutteam') ? data['withoutteam'] : 0;
    this.status = !_.isNil(data) && data.hasOwnProperty('status') ?
      new ScopeAnalyticsStatusModel(data['status']) : new ScopeAnalyticsStatusModel();

    this.teams = [];
    if (!_.isNil(data) && data.hasOwnProperty('teams')) {
      Object.keys(data['teams']).forEach((team) => {
        this.teams.push(new ScopeAnalyticsTeamModel(parseInt(team, 0), data['teams'][team]));
      });
    }
  }
}
