const _ = require('lodash');

export class ScopeAnalyticsFilterValuesModel {
  id: string;
  dropdownSelectedOption = {
    label: '',
    value: ''
  };
  dates = {
    selectRange: '',
    compareRange: ''
  };

  constructor(data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : '';
    this.dropdownSelectedOption.label = !_.isNil(data) && data.hasOwnProperty('dropdownSelectedOption') ?
      data['dropdownSelectedOption']['label'] : '';
    this.dropdownSelectedOption.value = !_.isNil(data) && data.hasOwnProperty('dropdownSelectedOption') ?
      data['dropdownSelectedOption']['value'] : '';
    this.dates.selectRange = !_.isNil(data) && data.hasOwnProperty('dates') ?
      data['dates']['selectRange'] : '';
    this.dates.compareRange = !_.isNil(data) && data.hasOwnProperty('dates') ?
      data['dates']['compareRange'] : '';
  }
}
