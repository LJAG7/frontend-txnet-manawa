const _ = require('lodash');

export class ScopeAnalyticsStatusModel {
  pause: number;
  working: number;
  meeting: number;
  travel: number;
  lease: number;

  constructor(data?: object) {
    this.pause = !_.isNil(data) && data.hasOwnProperty('pause') ? data['pause'] : 0;
    this.working = !_.isNil(data) && data.hasOwnProperty('working') ? data['working'] : 0;
    this.meeting = !_.isNil(data) && data.hasOwnProperty('meeting') ? data['meeting'] : 0;
    this.travel = !_.isNil(data) && data.hasOwnProperty('travel') ? data['travel'] : 0;
    this.lease = !_.isNil(data) && data.hasOwnProperty('lease') ? data['lease'] : 0;
  }
}
