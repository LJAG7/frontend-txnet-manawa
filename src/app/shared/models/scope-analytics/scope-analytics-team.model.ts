const _ = require('lodash');

export class ScopeAnalyticsTeamModel {
  id: number;
  name: string;
  value: string;

  constructor(id: number, data?: object) {
    this.id = !_.isNil(id) ? id : 0;
    this.name = !_.isNil(data) && data.hasOwnProperty('name') ? data['name'] : '';
    this.value = !_.isNil(data) && data.hasOwnProperty('value') ? data['value'].toFixed(2) : '0.00';
  }
}
