import {PersonModel} from './person.model';

export class SessionSingletonModel {
  static instance: SessionSingletonModel = new SessionSingletonModel();
  token: string;
  person: PersonModel;
  active: boolean;

  public static getInstance(): SessionSingletonModel {
    return SessionSingletonModel.instance;
  }

  constructor() {
    if (SessionSingletonModel.instance) {
      throw new Error('Error: Instantiation failed: Use SingletonClass.getInstance() instead of new.');
    }
    SessionSingletonModel.instance = this;
  }


  public initialize(token: string, person: PersonModel, active: boolean = true) {
    this.active = active
    this.token = token;
    this.person = person;
  }

}
