import {BaseModel} from '../base.model';
import {StatusPeriodsStatusModel} from './status-periods-status.model';
import {StatusPeriodsTeamModel} from './status-periods-team.model';

const _ = require('lodash');

export class StatusPeriodsDateModel extends BaseModel {

  date: string;
  status: StatusPeriodsStatusModel;
  teams: Array<StatusPeriodsTeamModel>;
  total: number;
  withoutteam: number;

  constructor(date: string, data?: object) {
    super();
    this.date = !_.isNil(date) ? date : '';
    this.total = !_.isNil(data) && data.hasOwnProperty('total') ? data['total'] : 0;
    this.withoutteam = !_.isNil(data) && data.hasOwnProperty('withoutteam') ? data['withoutteam'] : 0;
    this.status = !_.isNil(data) && data.hasOwnProperty('status') ?
      new StatusPeriodsStatusModel(data['status']) : new StatusPeriodsStatusModel();

    this.teams = [];
    if (!_.isNil(data) && data.hasOwnProperty('teams')) {
      Object.keys(data['teams']).forEach((team) => {
        this.teams.push(new StatusPeriodsTeamModel(parseInt(team, 0), data['teams'][team]));
      });
    }
  }

}
