import {BaseModel} from '../base.model';
import {PersonModel} from '../person.model';
import {TeamModel} from '../team/team.model';
import {StatusPeriodsModel} from './status-periods.model';

const _ = require('lodash');

export class StatusPeriodsFilterModel extends BaseModel {
  element: PersonModel | TeamModel;
  initialStatusPeriods: StatusPeriodsModel;
  comparativeStatusPeriods: StatusPeriodsModel;

  constructor(element?: PersonModel | TeamModel, initialStatusPeriods?: StatusPeriodsModel, comparativeStatusPeriods?: StatusPeriodsModel) {
    super();

    this.element = !_.isNil(element) ? element : null;
    this.initialStatusPeriods = !_.isNil(initialStatusPeriods) ? initialStatusPeriods : null;
    this.comparativeStatusPeriods = !_.isNil(comparativeStatusPeriods) ? comparativeStatusPeriods : null;
  }
}
