import {BaseModel} from '../base.model';

const moment = require('moment');
const _ = require('lodash');

export class StatusPeriodsRangeFilterModel extends BaseModel {
  mode: string;
  initDate: any;
  endDate: any;
  closeCalendar: boolean;

  constructor(data?: object) {
    super();
    this.mode = !_.isNil(data) && data.hasOwnProperty('mode') ? data['mode'] : '';
    this.initDate = !_.isNil(data) && data.hasOwnProperty('initDate') && !_.isNil(data['initDate']) ? moment(data['initDate']) : null;
    this.endDate = !_.isNil(data) && data.hasOwnProperty('endDate') && !_.isNil(data['endDate']) ? moment(data['endDate']) : null;
    this.closeCalendar = !_.isNil(data) && data.hasOwnProperty('closeCalendar') ? data['closeCalendar'] : true;
  }
}
