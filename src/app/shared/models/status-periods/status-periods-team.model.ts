import {BaseModel} from '../base.model';

const _ = require('lodash');

export class StatusPeriodsTeamModel extends BaseModel {

  id: number;
  name: string;
  value: string;

  constructor(id: number, data?: object) {
    super();
    this.id = !_.isNil(id) ? id : 0;
    this.name = !_.isNil(data) && data.hasOwnProperty('name') ? data['name'] : '';
    this.value = !_.isNil(data) && data.hasOwnProperty('value') ? data['value'].toFixed(2) : '0.00';
  }

}
