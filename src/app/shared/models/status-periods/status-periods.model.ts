import {BaseModel} from '../base.model';
import {StatusPeriodsDateModel} from './status-periods-date.model';
import {StatusPeriodsTeamModel} from './status-periods-team.model';

const _ = require('lodash');

export class StatusPeriodsModel extends BaseModel {

  dates: Array<StatusPeriodsDateModel>;
  teams: Array<StatusPeriodsTeamModel>;
  total = 0;

  constructor(data?: object) {
    super();
    if (!_.isNil(data) && data.hasOwnProperty('dates') && !_.isEmpty(data['dates'])) {
      this.dates = [];
      Object.keys(data['dates']).forEach((date) => {
        this.dates.push(new StatusPeriodsDateModel(date, data['dates'][date]));
      });
    } else {
      this.dates = [];
    }

    if (!_.isNil(data) && data.hasOwnProperty('teams') && !_.isEmpty(data['teams'])) {
      this.teams = [];
      Object.keys(data['teams']).forEach((team) => {
        this.teams.push(new StatusPeriodsTeamModel(Number(team), data['teams'][team]));
      });
    } else {
      this.teams = [];
    }

    this.total = !_.isNil(data) && data.hasOwnProperty('total') ? data['total'] : 0;
  }

  public addNullDate(label: string, position: number) {
    this.dates.splice(position, 0, new StatusPeriodsDateModel(label));
    this.dates[position].teams = [];
    this.teams.forEach( team => {
      this.dates[position].teams.push(new StatusPeriodsTeamModel(team.id, {name: team.name, value: 0}));
    });
  }

}
