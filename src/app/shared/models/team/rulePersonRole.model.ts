import {RoleModel} from './role.model';
const _ = require('lodash');

export class RulePersonRoleModel {
  id: number;
  category_name: string;
  description: string;
  min: number;
  max: number;
  child_roles: Array<RoleModel>;
  alias: string;

  constructor (data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.category_name = !_.isNil(data) && data.hasOwnProperty('category_name') ? data['category_name'] : '';
    this.alias = !_.isNil(data) && data.hasOwnProperty('alias') ? data['alias'] : '';
    this.description = !_.isNil(data) && data.hasOwnProperty('description') ? data['description'] : '';
    this.min = !_.isNil(data) && data.hasOwnProperty('min') ? data['min'] : null;
    this.max = !_.isNil(data) && data.hasOwnProperty('max') ? data['max'] : null;


    this.child_roles = _.isNil(data)
    || !data.hasOwnProperty('child_roles')
    || !Array.isArray(data['child_roles'])
      ? []
      : data['child_roles'].map((role) => {
        if (role instanceof RoleModel) {
          return role;
        }

        return new RoleModel(role);
      });
  }
}
