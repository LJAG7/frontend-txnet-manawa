const _ = require('lodash');

export class TeamGroupModel {
  id: number;
  name: string;
  num_pending_notifications: number;
  num_visited_by_user: number;

  constructor(data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.name = !_.isNil(data) && data.hasOwnProperty('name') ? data['name'] : '';
    this.num_pending_notifications = !_.isNil(data) && data.hasOwnProperty('num_pending_notifications') ?
      data['num_pending_notifications'] : 0;
    this.num_visited_by_user = !_.isNil(data) && data.hasOwnProperty('num_visited_by_user') ?
      data['num_visited_by_user'] : 0;
  }
}
