import {PersonModel} from '../person.model';
import {LocationModel} from '../location.model';
import {TeamStatisticsModel} from './teamStatistics.model';

const _ = require('lodash');

export class TeamModel {
  id: number;
  name: string;
  level: number;
  description: string;
  status: number;
  created_at: string;
  deleted_at: string;
  updated_at: string;
  start_date: string;
  end_date: string;
  child_teams: Array<TeamModel>;
  groups: Array<object>;
  managed_by: PersonModel;
  num_components: number;
  num_pending_notifications: number;
  num_visited_by_user: number;
  team_locations: Array<LocationModel>;
  team_rule_person: Array<object>;
  team_rule_teamt: Array<object>;
  teamt: object;
  statistics: TeamStatisticsModel;
  is_deleteable: boolean;

  constructor(data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.name = !_.isNil(data) && data.hasOwnProperty('name') ? data['name'] : '';
    this.level = !_.isNil(data) && data.hasOwnProperty('level') ? data['level'] : 0;
    this.description = !_.isNil(data) && data.hasOwnProperty('description') ? data['description'] : 0;
    this.status = !_.isNil(data) && data.hasOwnProperty('status') ? data['status'] : 0;

    this.created_at = !_.isNil(data) && data.hasOwnProperty('created_at') ? data['created_at'] : '';
    this.deleted_at = !_.isNil(data) && data.hasOwnProperty('deleted_at') ? data['deleted_at'] : '';
    this.updated_at = !_.isNil(data) && data.hasOwnProperty('updated_at') ? data['updated_at'] : '';
    this.start_date = !_.isNil(data) && data.hasOwnProperty('start_date') ? data['start_date'] : '';
    this.end_date = !_.isNil(data) && data.hasOwnProperty('end_date') ? data['end_date'] : '';
    this.is_deleteable = !_.isNil(data) && data.hasOwnProperty('is_deleteable') ? data['is_deleteable'] : false;

    this.num_components = !_.isNil(data) && data.hasOwnProperty('num_components') ? data['num_components'] : 0;
    this.num_pending_notifications = !_.isNil(data) && data.hasOwnProperty('num_pending_notifications')
      ? data['num_pending_notifications']
      : 0;
    this.num_visited_by_user = !_.isNil(data) && data.hasOwnProperty('num_visited_by_user')
      ? data['num_visited_by_user']
      : 0;

    this.managed_by = _.isNil(data) || !data.hasOwnProperty('managed_by')
      ? null
      : data['managed_by'] instanceof PersonModel
        ? data['managed_by']
        : new PersonModel(data['managed_by']);

    this.child_teams = _.isNil(data)
    || !data.hasOwnProperty('child_teams')
    || !Array.isArray(data['child_teams'])
      ? []
      : data['child_teams'].map((team) => {
        if (team instanceof TeamModel) {
          return team;
        }

        return new TeamModel(team);
      });

    this.team_locations = _.isNil(data)
    || !data.hasOwnProperty('child_teams')
    || !Array.isArray(data['child_teams'])
      ? []
      : data['child_teams'].map((location) => {
        if (location instanceof LocationModel) {
          return location;
        }

        return new LocationModel(location);
      });

    this.groups = !_.isNil(data) && data.hasOwnProperty('groups') ? data['groups'] : [];
    this.teamt = !_.isNil(data) && data.hasOwnProperty('teamt') ? data['teamt'] : null;
    this.team_rule_person = !_.isNil(data) && data.hasOwnProperty('team_rule_person')
      ? data['team_rule_person'] :
      [];
    this.team_rule_teamt = !_.isNil(data) && data.hasOwnProperty('team_rule_teamt')
      ? data['team_rule_teamt'] :
      [];

    this.statistics = _.isNil(data) || !data.hasOwnProperty('statistics')
      ? null
      : data['statistics'] instanceof TeamStatisticsModel
        ? data['statistics']
        : new TeamStatisticsModel(data['statistics']);
  }

  /**
   * Get team assign persons
   * @param {object} team
   * @returns {string}
   */
  public getTeamAssignPersons(): string {
    return this.statistics.numPersons + '(' + this.statistics.numPersonsRecursive + ')';
  }

  /**
   * Get subteams of team
   * @param {object} team
   * @returns {string}
   */
  public getTeamSubTeams(): string {
    return this.statistics.numTeams + '(' + this.statistics.numTeamsRecursive + ')';
  }
}
