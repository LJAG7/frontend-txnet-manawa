import {TeamModel} from './team.model';
import {RulePersonRoleModel} from './rulePersonRole.model';
const _ = require('lodash');

export class TeamPersonModel {
  id:  number;
  rule_person_role: RulePersonRoleModel;
  team: TeamModel;

  constructor (data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;

    this.rule_person_role = _.isNil(data) || !data.hasOwnProperty('rule_person_role')
      ? null
      : data['rule_person_role'] instanceof RulePersonRoleModel
        ? data['rule_person_role']
        : new RulePersonRoleModel(data['rule_person_role']);

    this.team = _.isNil(data) || !data.hasOwnProperty('team')
      ? null
      : data['team'] instanceof TeamModel
        ? data['team']
        : new TeamModel(data['team']);
  }
}
