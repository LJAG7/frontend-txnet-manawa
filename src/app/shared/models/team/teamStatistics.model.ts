const _ = require('lodash');

export class TeamStatisticsModel {
  umInvited: number;
  numInvitedRecursive: number;
  numPersons: number;
  numPersonsRecursive: number;
  numTeams: number;
  numTeamsRecursive: number;
  numTotal: number;
  numTotalRecursive: number;

  constructor (data?: object) {
    this.umInvited = !_.isNil(data) && data.hasOwnProperty('umInvited') ? data['umInvited'] : 0;
    this.numInvitedRecursive = !_.isNil(data) && data.hasOwnProperty('numInvitedRecursive') ? data['numInvitedRecursive'] : 0;
    this.numPersons = !_.isNil(data) && data.hasOwnProperty('numPersons') ? data['numPersons'] : 0;
    this.numPersonsRecursive = !_.isNil(data) && data.hasOwnProperty('numPersonsRecursive') ? data['numPersonsRecursive'] : 0;
    this.numTeams = !_.isNil(data) && data.hasOwnProperty('numTeams') ? data['numTeams'] : 0;
    this.numTeamsRecursive = !_.isNil(data) && data.hasOwnProperty('numTeamsRecursive') ? data['numTeamsRecursive'] : 0;
    this.numTotal = !_.isNil(data) && data.hasOwnProperty('numTotal') ? data['numTotal'] : 0;
    this.numTotalRecursive = !_.isNil(data) && data.hasOwnProperty('numTotalRecursive') ? data['numTotalRecursive'] : 0;
  }
}
