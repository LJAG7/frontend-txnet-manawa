const _ = require('lodash');

export class FavouriteTeamModel {

  id: number;
  position: number;

  constructor(data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.position = !_.isNil(data) && data.hasOwnProperty('name') ? data['name'] : 0;
  }

}
