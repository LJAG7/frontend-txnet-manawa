import {FavouriteTeamModel} from './favourite-team.model';
import {ColorGeneratorPipe} from '../../pipes/colorGenerator.pipe';

const _ = require('lodash');

export class NodeModel {

  id: number;
  colour: string;
  favourite_team: FavouriteTeamModel;
  name: string;
  num_child_teams: number;
  child_teams: Array<NodeModel> = [];
  num_mates: number;
  num_pending_notifications: number;
  is_deleteable: boolean;

  private _colorGeneratorPipe = new ColorGeneratorPipe();

  constructor(data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.name = !_.isNil(data) && data.hasOwnProperty('name') ? data['name'] : '';
    this.colour = !_.isNil(this.name) ? this._colorGeneratorPipe.transform(this.name) : '';
    this.num_child_teams = !_.isNil(data) && data.hasOwnProperty('num_child_teams') ? data['num_child_teams'] : 0;
    this.favourite_team = !_.isNil(data) && data.hasOwnProperty('favourite_team') ? new FavouriteTeamModel(data['favourite_team']) : null;
    this.num_mates = !_.isNil(data) && data.hasOwnProperty('num_mates') ? data['num_mates'] : 0;
    this.num_pending_notifications = !_.isNil(data) && data.hasOwnProperty('num_pending_notifications') ? data['num_pending_notifications'] : 0;
    this.is_deleteable = !_.isNil(data) && data.hasOwnProperty('is_deleteable') ? data['is_deleteable'] : false;

    if (!_.isNil(data) && data.hasOwnProperty('child_teams')) {
      this.child_teams = [];
      data['child_teams'].forEach( node => {
        this.child_teams.push(new NodeModel(node));
      });
    }
  }
}
