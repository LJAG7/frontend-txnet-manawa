const _ = require('lodash');

export class UserModel {
  id: number;
  username: string;
  locale: string;
  enabled: boolean;

  constructor(data?: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.username = !_.isNil(data) && data.hasOwnProperty('username') ? data['username'] : '';
    this.locale = !_.isNil(data) && data.hasOwnProperty('locale') ? data['locale'] : 'es';
    this.enabled = !_.isNil(data) && data.hasOwnProperty('enabled') ? data['enabled'] : false;
  }
}
