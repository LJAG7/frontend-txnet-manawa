import {DashboardModel} from './dashboard.model';
import {WidgetModel} from './widget.model';

const _ = require('lodash');

export class DashboardEntryModel {

  public id: number;
  public position: number;
  public dashboard: DashboardModel;
  public widget: WidgetModel;

  constructor(data: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.position = !_.isNil(data) && data.hasOwnProperty('position') ? data['position'] : 0;
    this.dashboard = !_.isNil(data) && data.hasOwnProperty('dashboard') ? data['dashboard'] : null;
    this.widget = !_.isNil(data) && data.hasOwnProperty('widget') ? data['widget'] : null;
  }
}
