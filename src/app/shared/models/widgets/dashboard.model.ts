import {RoleSystemModel} from '../role-system.model';
import {WidgetModel} from './widget.model';

const _ = require('lodash');

export class DashboardModel {

  public id: number;
  public name: string;
  public tag: string;
  public roleSystemType: RoleSystemModel;
  public widgets: Array<WidgetModel> = [];

  constructor(data: object) {
    this.id = !_.isNill(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.name = !_.isNill(data) && data.hasOwnProperty('name') ? data['name'] : '';
    this.tag = !_.isNill(data) && data.hasOwnProperty('tag') ? data['tag'] : '';
    this.roleSystemType = !_.isNill(data) && data.hasOwnProperty('role_system_type') ? data['role_system_type'] : null;
    this.widgets = !_.isNill(data) && data.hasOwnProperty('widgets') ? data['widgets'] : [];
  }
}
