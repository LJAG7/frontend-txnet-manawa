const _ = require('lodash');

export class WidgetCatalog {

  private widgetCatalog: object = {

    // [Dashboard] MATE_MATE - Dashboard de un Mate visto por el propio Mate

    WMATE_TIME_01: {
      tag: 'WMATE_TIME_01',
      type: 'B',
      endpoint: '',
      text: 'horas trabajadas',
      dropdown: [
        {label: 'hoy', value: '0'},
        {label: 'esta semana', value: '7'},
        {label: 'este mes', value: '30'},
        {label: 'este año', value: '365'},
      ]
    },
    WMATE_TIME_02: {
      tag: 'WMATE_TIME_02',
      type: 'A',
      endpoint: '',
      text: 'días de vacaciones pendientes'
    },
    WMATE_TIME_03: {
      tag: 'WMATE_TIME_03',
      type: 'A',
      endpoint: '',
      text: 'días de ausencia justificada'
    },
    WMATE_ROLE_01: {
      tag: 'WMATE_ROLE_01',
      type: 'C',
      endpoint: 'persons/id/totalRoles/',
      text: 'roles ocupados sobre',
      extend: {
        value: '-'
      }
    },
    WMATE_ROLE_02: {
      tag: 'WMATE_ROLE_02',
      type: 'A',
      endpoint: 'persons/id/totalMatesSameRole/',
      text: 'usuarios con el mismo role'
    },
    WMATE_ACTIVITY_01: {
      tag: 'WMATE_ACTIVITY_01',
      type: 'B',
      endpoint: '',
      text: 'viajes',
      dropdown: [
        {label: 'hoy', value: '0'},
        {label: 'esta semana', value: '7'},
        {label: 'este mes', value: '30'},
        {label: 'este año', value: '365'},
      ]
    },
    WMATE_SALARY_01: {
      tag: 'WMATE_SALARY_01',
      type: 'A',
      endpoint: '',
      text: 'sueldo neto'
    },
    WMATE_SALARY_02: {
      tag: 'WMATE_SALARY_02',
      type: 'A',
      endpoint: '',
      text: 'cantidad a percibir este mes'
    },
    WMATE_TEAM_01: {
      tag: 'WMATE_TEAM_01',
      type: 'A',
      endpoint: 'persons/id/totalTeams/',
      text: 'equipos a los que pertenezco'
    },
    WMATE_GESTOR_01: {
      tag: 'WMATE_GESTOR_01',
      type: 'A',
      endpoint: 'persons/id/totalMatesManager/',
      text: 'mates que son gestores'
    },

    // [Dashboard] GESTOR_MATE - Dashboard de un Mate visto por un Gestor

    WGESTOR_TIME_01: {
      tag: 'WGESTOR_TIME_01',
      type: 'A',
      endpoint: '',
      text: 'años de experiencia'
    },
    WGESTOR_TIME_02: {
      tag: 'WGESTOR_TIME_02',
      type: 'A',
      endpoint: '',
      text: 'años de antigüedad'
    },
    WGESTOR_TIME_03: {
      tag: 'WGESTOR_TIME_03',
      type: 'B',
      endpoint: '',
      text: 'días de permiso',
      dropdown: [
        {label: 'hoy', value: '0'},
        {label: 'esta semana', value: '7'},
        {label: 'este mes', value: '30'},
        {label: 'este año', value: '365'},
      ]
    },
    WGESTOR_ROLE_01: {
      tag: 'WGESTOR_ROLE_01',
      type: 'A',
      endpoint: '',
      text: 'puestos de responsabilidad'
    },
    WGESTOR_MATE_01: {
      tag: 'WGESTOR_MATE_01',
      type: 'D',
      endpoint: 'persons/gid/teams/managedBy/',
      param: 'paginate_data', // null if no required param in response callback
      text: 'mates en el equipo',
      dropdown: {
        endpoint: 'teams/id/totalMates/',
        param: 'number_mates',
        placeholder: 'selecciona un equipo',
        options: []
      }
    },
    WGESTOR_TEAM_01: {
      tag: 'WGESTOR_TEAM_01',
      type: 'A',
      endpoint: 'persons/id/totalTeamsManaged/',
      text: 'equipos gestionados'
    },
    // [Dashboard] GESTOR_SCOPE - Dashboard de un Scope visto por un Gestor

  };

  public getWidgetConfig(tag: string): object {
    if (!_.isNil(tag) && !_.isEmpty(tag)) {
      const properties = Object.keys(this.widgetCatalog);
      if (properties.indexOf(tag) !== -1) {
        return this.widgetCatalog[tag];
      }
    }
    return null;
  }
}
