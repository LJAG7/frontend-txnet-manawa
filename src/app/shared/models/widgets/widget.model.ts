const _ = require('lodash');

export class WidgetModel {

  public id: number;
  public tag: string;
  public position: number;
  public widgetConfig: object = {
    tag: '',
    type: '',
    endpoint: '',
    text: ''
  }

  constructor(data: object) {
    this.id = !_.isNil(data) && data.hasOwnProperty('id') ? data['id'] : 0;
    this.tag = !_.isNil(data) && data.hasOwnProperty('tag') ? data['tag'] : '';
    this.widgetConfig = !_.isNil(data) && data.hasOwnProperty('widgetConfig') ? data['widgetConfig'] : null;
    this.position = !_.isNil(data) && data.hasOwnProperty('position') ? data['position'] : 0;
  }
}
