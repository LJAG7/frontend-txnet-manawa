/**
 * Created by sebastian on 14/06/17.
 */
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'capitalize'})

export class CapitalizePipe implements PipeTransform {

  transform(string: string): string {

    return string.substring(0, 1).toUpperCase() + string.substring(1, string.length).toLowerCase();

  }
}
