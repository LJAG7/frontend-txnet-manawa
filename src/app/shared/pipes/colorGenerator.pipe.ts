import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'colorGenerator'
})
export class ColorGeneratorPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return '#' + this.intToRGB(this.hashCode(value));
  }

  transformToHover(value: any, args?: any): any {

    // Color y cantidad a restar
    const color = value;
    const cant = 30;

    // Extraer las tres partes del color
    let red = color.substr(1, 2);
    let green = color.substr(3, 2);
    let blue = color.substr(5, 2);

    // Convertir a enteros los string, que tengo en hexadecimal
    let intRed = parseInt(red, 16);
    let intGreen = parseInt(green, 16);
    let intBlue = parseInt(blue, 16);

    // Verificar que no quede como negativo y resto
    intRed = intRed - cant >= 0 ? intRed - cant : intRed;
    intGreen = intGreen - cant >= 0 ? intGreen - cant : intGreen;
    intBlue = intBlue - cant >= 0 ? intBlue - cant : intBlue;

    // Convertir enteros a hexadecimal
    red = intRed.toString(16);
    green = intGreen.toString(16);
    blue = intBlue.toString(16);

    // Validar que los string hexadecimales tengan dos caracteres
    red = red.length < 2 ? '0' + red : red;
    green = green.length < 2 ? '0' + green : green;
    blue = blue.length < 2 ? '0' + blue : blue;

    // Construir el color hexadecimal
    const hoverColor = '#' + red + green + blue;

    // La función devuelve el valor del color hexadecimal resultante
    return hoverColor;
  }

  hashCode(str) {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
  }

  intToRGB(i) {
    const c = (i & 0x00FFFFFF)
      .toString(16)
      .toUpperCase();

    return '00000'.substring(0, 6 - c.length) + c;
  }

}
