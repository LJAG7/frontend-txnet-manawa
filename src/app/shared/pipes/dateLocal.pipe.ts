import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common';
import {isNull, isUndefined} from 'util';

@Pipe({name: 'dateLocalPipe'})

export class DateLocalPipe implements PipeTransform {

  private _locale = 'en-US';

  private _patterns = {
    date: {
      'es-ES':      'dd-MM-yyyy',
      'en-EN':      'yyyy-MM-dd',
      'medium':     'medium',
      'shortTime':  'shortTime',
      'mmss':       'mmss',
      'month-year': 'MMM yyyy',
      iso:          'yyyy-MM-dd'
    },
    datetime: {
      iso: 'yyyy-MM-ddTHH:mm:ss'
    }
  };


  private static getDate(hours: number = 0,
                         minutes: number = 0,
                         seconds: number = 0,
                         ms: number = 0,
                         day?: number,
                         month?: number,
                         year?: number): Date {

    const d = new Date();

    return new Date(
      isUndefined(year) ? d.getUTCFullYear() : year,
      isUndefined(month) ? d.getUTCMonth() : month,
      isUndefined(day) ? d.getUTCDate() : day,
      hours,
      minutes,
      seconds,
      ms
    );
  }

  /**
   * Return the date for first day in the month and year given
   * @param {boolean} UTC
   * @param {number} hours
   * @param {number} minutes
   * @param {number} seconds
   * @param {number} ms
   * @param {number} month
   * @param {number} year
   * @returns {string}
   */
  public static getMonthIniDay(UTC: boolean = true,
                               hours: number = 0,
                               minutes: number = 0,
                               seconds: number = 0,
                               ms: number = 0,
                               month?: number,
                               year?: number): string {

    const day = DateLocalPipe.getDate(hours, minutes, seconds, ms, 1, month, year);

    return UTC
      ? day.toUTCString()
      : day.toLocaleString();
  }

  /**
   * Return the date for last day in the month and year given
   * @param {boolean} UTC
   * @param {number} hours
   * @param {number} minutes
   * @param {number} seconds
   * @param {number} ms
   * @param {number} month
   * @param {number} year
   * @returns {string}
   */
  public static getMonthLastDay(UTC: boolean = true,
                                hours: number = 0,
                                minutes: number = 0,
                                seconds: number = 0,
                                ms: number = 0,
                                month?: number,
                                year?: number): string {

    const d = new Date();
    const nextMonth = isUndefined(month) ? d.getUTCMonth() + 1 : month + 1;
    const day = DateLocalPipe.getDate(hours, minutes, seconds, ms, 0, nextMonth, year);

    return UTC
      ? day.toUTCString()
      : day.toLocaleString();
  }


  /**
   * Return a valid pattern
   * @returns {string}
   */
  private getPattern(pattern: string): string {
    if (isUndefined(pattern)) {
      return undefined;
    } else {
      if (this._patterns.date.hasOwnProperty(pattern)) {
        return this._patterns.date[pattern];
      } else {
        return pattern;
      }
    }
  }


  /**
   * Set locale for DatePipe
   * @param {string} value
   */
  setLocale(value: string): void {
    this._locale = value;
  }


  transform(date: string, pattern?: string): string {
    const datePipe = new DatePipe(this._locale);
    return datePipe.transform(date, this.getPattern(pattern));
  }


  /**
   * Transform a datetime to ISO format (use for API calls)
   * @param {string} date
   * @returns {string}
   */
  datetimeToISO(date: string): string {
    const datePipe = new DatePipe(this._locale);
    const zoneOffset = new Date().getTimezoneOffset() / -60;
    const zoneOffsetString = (zoneOffset > 0 ? '+' : '-') + ('0' + (zoneOffset)).slice(-2) + ':00';

    return datePipe.transform(date, this._patterns.datetime.iso) + zoneOffsetString;
  }


  /**
   * Compare dates. 0 equal, -1 date1 < date2, 1 date1 > date2
   * @param {string} date1
   * @param {string} date2: null -> now()
   * @returns {number}
   */
  compareDates(date1: string, date2: string = null): number {
    const datePipe = new DatePipe(this._locale);
    const d1 = new Date(datePipe.transform(date1, this._patterns.date.iso));
    const d2 = new Date(
      datePipe.transform(
        isNull(date2) ? new Date().toUTCString() : date2,
        this._patterns.date.iso
      )
    );

    return +d1 === +d2
      ? 0
      : d1 > d2
        ? 1
        : -1;
  }


  /**
   * Total days between date1 and date2
   * @param {string} date1
   * @param {string} date2
   * @returns {number}
   */
  diffDatesDays(date1: string, date2: string = null): number {
    const datePipe = new DatePipe(this._locale);
    const d1 = new Date(datePipe.transform(date1, this._patterns.date.iso));
    const d2 = new Date(
      datePipe.transform(
        isNull(date2) ? new Date().toUTCString() : date2,
        this._patterns.date.iso
      )
    );

    return Math.round((d2.getTime() - d1.getTime()) / (24 * 60 * 60 * 1000));
  }


  /**
   * Return a datetime range for Api calls
   * @param {string} iniDate
   * @param {string} endDate
   * @returns {string}
   */
  rangeDateForApi(iniDate: string, endDate: string): string {
    return 'range('
      + this.datetimeToISO(iniDate)
      + ','
      + this.datetimeToISO(endDate)
      + ')';
  }


  /**
   * @param {number} days
   * @param {string | Date} iniDate
   * @returns {string}
   */
  rangeCalcDateForApi(days: number, iniDate: string | Date = null): string {
    const datePipe = new DatePipe(this._locale);
    const fromDate = iniDate instanceof Date
      ? iniDate
      : new Date(datePipe.transform(
        isNull(iniDate) ? new Date().toUTCString() : iniDate,
        this._patterns.date.iso
        )
      );

    if (days > 0) {
      return this.rangeDateForApi(
        fromDate.toUTCString(),
        this.dateCalc(days, fromDate).toUTCString()
      );

    } else {
      return this.rangeDateForApi(
        this.dateCalc(days, fromDate).toUTCString(),
        fromDate.toUTCString()
      );
    }
  }

  /**
   * Calc new date for value diff
   * @param {number} value
   * @param {string} type
   * @param {string | Date} iniDate
   * @param {string} pattern
   * @returns {string | Date}
   */
  private calc(value: number, type: string = 'day', iniDate: any = null, pattern?: string): any {
    const datePipe = new DatePipe(this._locale);
    const startDate = iniDate instanceof Date
      ? iniDate
      : new Date(datePipe.transform(
        isNull(iniDate) ? new Date().toUTCString() : iniDate,
        this._patterns.date.iso
        )
      );

    const difValues = {
      hour: 0,
      minute: 0,
      second: 0,
      ms: 0,
      day: 0,
      month: 0,
      year: 0
    };

    difValues[type] += value;

    const calcDate =  DateLocalPipe.getDate(
      startDate.getHours() + difValues.hour,
      startDate.getMinutes() + difValues.minute,
      startDate.getSeconds() + difValues.second,
      startDate.getMilliseconds() + difValues.ms,
      startDate.getDate() + difValues.day,
      startDate.getMonth() + difValues.month,
      startDate.getFullYear() + difValues.year
    );

    return iniDate instanceof Date
      ? calcDate
      : isUndefined(pattern)
        ? calcDate.toUTCString()
        : datePipe.transform(calcDate, this.getPattern(pattern));
  }

  /**
   * Return a date for a iniDate and days diff (positive or negative)
   * @param {number} days
   * @param {string|Date} iniDate
   * @param {string} pattern: no applicable for iniDate string
   * @returns {any} (string|Date)
   */
  dateCalc(days: number, iniDate: any = null, pattern?: string): any {
    return this.calc(days, 'day', iniDate, pattern);
  }


  /**
   * Return a date for a iniDate and months diff (positive or negative)
   * @param {number} months
   * @param {string|Date} iniDate
   * @param {string} pattern: no applicable for iniDate string
   * @returns {any} (string|Date)
   */
  monthCalc(months: number, iniDate: any = null, pattern?: string): any {
    return this.calc(months, 'month', iniDate, pattern);
  }


  /**
   * Return a date for a iniDate and years diff (positive or negative)
   * @param {number} years
   * @param {string|Date} iniDate
   * @param {string} pattern: no applicable for iniDate string
   * @returns {any} (string|Date)
   */
  yearCalc(years: number, iniDate: any = null, pattern?: string): any {
    return this.calc(years, 'year', iniDate, pattern);
  }
}
