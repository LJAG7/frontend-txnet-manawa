import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberPipe'
})
export class NumberPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return null;
  }

  public formatMoney(value: string, edit: boolean = false): string {
    let numberStr;
    if (edit) {
      numberStr = value.replace('.', '');
    }
    numberStr = parseFloat(value).toFixed(2).toString();
    const numFormatDec = numberStr.slice(-2);
    numberStr = numberStr.substring(0, numberStr.length - 3);
    const numFormat = new Array;
    while (numberStr.length > 3) {
      numFormat.unshift(numberStr.slice(-3));
      numberStr = numberStr.substring(0, numberStr.length - 3);
    }
    numFormat.unshift(numberStr);
    return numFormat.join('.') + ',' + numFormatDec;
  }

  public unformatMoney(value: string): number {
    value = value.replace('.', '');
    value = value.replace(',', '.');
    return Number(Number(value).toFixed(2));
  }

}
