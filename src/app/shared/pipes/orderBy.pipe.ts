import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'orderBy'})

export class OrderByPipe implements PipeTransform {


  transform(items: Array<object | any>,
            filter: string): any {

    return items.sort((a: string, b: string) => {
      return a[filter] < b[filter]
        ? -1
        : a[filter] > b[filter]
          ? 1
          : 0;
    });
  }

}
