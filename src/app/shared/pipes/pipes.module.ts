/*GENERAL ASSETS*/
import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';

/*CUSTOM COMPONENTS*/
import {CapitalizePipe} from './capitalize.pipe';
import {OrderByPipe} from './orderBy.pipe';
import {ColorGeneratorPipe} from './colorGenerator.pipe';
import {ValidationObjectPipe} from './validation.pipe';
import {DateLocalPipe} from './dateLocal.pipe';
import {TextParserPipe} from './textParser.pipe';
import { NumberPipe } from './number.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    CapitalizePipe,
    OrderByPipe,
    ColorGeneratorPipe,
    ValidationObjectPipe,
    DateLocalPipe,
    TextParserPipe
  ],
  declarations: [
    CapitalizePipe,
    OrderByPipe,
    ColorGeneratorPipe,
    ValidationObjectPipe,
    DateLocalPipe,
    TextParserPipe,
    NumberPipe
  ],
  providers: [DatePipe]
})

export class PipesModule {
}
