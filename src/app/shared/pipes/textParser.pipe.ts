import {Pipe, PipeTransform} from '@angular/core';
const _ = require('lodash');

@Pipe({name: 'textParser'})

export class TextParserPipe implements PipeTransform {

  private static toPipeString(value: any, replace?: string): string {
    return _.isEmpty(value)
      ? _.isUndefined(replace)
          ? ''
          : replace
      : value.toString().trim();
  }

  static noNullString(value: string, replace?: string): string {
    return TextParserPipe.toPipeString(value, replace);
  }

  static noNullToStringArray(list: Array<any>, separator: string = ' ', replace?: string): string {
    return _.map(list, (element) => {
      return TextParserPipe.toPipeString(element, replace);
    }).join(separator).trim();
  }

  transform(value: any | Array<any>, separator: string = ' ', replace?: string): string {
    return _.isArray(value)
      ? TextParserPipe.noNullToStringArray(value, separator, replace)
      : TextParserPipe.noNullString(value, replace);
  }
}
