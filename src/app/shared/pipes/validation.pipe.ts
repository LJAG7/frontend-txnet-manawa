import {Pipe, PipeTransform} from '@angular/core';

const _ = require('lodash');

@Pipe({name: 'validationObject'})

export class ValidationObjectPipe implements PipeTransform {

  public isValidObjectProperty(item: object, property: string): boolean {
    return item && item.hasOwnProperty(property);
  }

  transform(item: object, property: string, subProperty: string = null): boolean {

    return this.isValidObjectProperty(item, property);
  }

  public isValidResponse(item: object | any): boolean {
    return this.isValidObjectProperty(item, 'object');
  }

  public isValidResponseItemList(item: object | any): boolean {
    return this.isValidObjectProperty(item, 'object')
      && this.isValidObjectProperty(item['object'], 'items');
  }

  public isSuccessResponse(item: object | any): boolean {
    return this.isValidObjectProperty(item, 'success') && item['success'];
  }

  public isValidValueObjectProperty(item: object, property: string): boolean {
    return this.isValidObjectProperty(item, property) && !_.isNil(item[property]);
  }

  public getValidOrNullObjectProperty(item: object, property: string): any {
    return this.isValidValueObjectProperty(item, property)
      ? item[property]
      : null;
  }

  /**
   * Return if two fields are equals
   * @param a
   * @param b
   * @returns {boolean}
   */
  public equal(a: any, b: any): boolean {
    return (
      ((_.isUndefined(a) || _.isEmpty(a)) && (_.isUndefined(b) || _.isEmpty(b)))
      || a === b) ;
  }
}
