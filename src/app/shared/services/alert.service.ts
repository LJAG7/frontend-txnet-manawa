import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';

import {Alert, AlertType} from '../models/alert';

const _ = require('lodash');

@Injectable()
export class AlertService {
  private subject = new Subject<Alert>();
  private keepAfterRouteChange = false;

  constructor(private router: Router) {
    // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.keepAfterRouteChange) {
          // only keep for a single route change
          this.keepAfterRouteChange = false;
        } else {
          // clear alert messages
          this.clear();
        }
      }
    });
  }

  getAlert(): Observable<any> {
    return this.subject.asObservable();
  }

  success(message: string, keepAfterRouteChange = false) {
    this.alert(AlertType.Success, message, keepAfterRouteChange);
  }

  prompt(message?: string, keepAfterRouteChange = false) {
    this.alert(
      AlertType.Prompt,
      !_.isNil(message) && !_.isEmpty(message)
        ? message
        : 'Funcionalidad pendiente de desarrollo.',
      keepAfterRouteChange);
  }

  error(message: string, keepAfterRouteChange = false) {
    this.alert(
      AlertType.Error,
      !_.isNil(message) && !_.isEmpty(message)
        ? message
        : 'Ha habido un error intentando procesar su solicitud. Por favor, inténtelo de nuevo mas tarde.',
      keepAfterRouteChange);
  }

  info(message: string, keepAfterRouteChange = false) {
    this.alert(AlertType.Info, message, keepAfterRouteChange);
  }

  warn(message: string, keepAfterRouteChange = false) {
    this.alert(AlertType.Warning, message, keepAfterRouteChange);
  }

  alert(type: AlertType, message: string, keepAfterRouteChange = false) {
    this.keepAfterRouteChange = keepAfterRouteChange;
    this.subject.next(<Alert>{ type: type, message: message });
  }


  clear() {
    // clear alerts
    this.subject.next();
  }
}
