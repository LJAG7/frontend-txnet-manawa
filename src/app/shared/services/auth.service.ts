import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Router} from '@angular/router';
import {setInterval, clearInterval} from 'timers';

import {HttpSrvService} from './http.service';

import {Cookie} from '../models/cookie.model';
import {SessionSingletonModel} from '../models/session.model';
import {PersonModel} from '../models/person.model';
import {DomainModel} from '../models/domain.model';
import {RoleSystemModel} from '../models/role-system.model';
import {LoaderService} from './loader.service';

import {PERMISSIONS} from '../const/permissions.const';
import {DOMAINS} from '../const/system.const';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';


const _ = require('lodash');


@Injectable()
export class AuthSrvService {

  private _apiUrl = location.origin.indexOf(DOMAINS.localhost) === -1
    ? location.origin + '/api/v1/tokens/'
    : '//' + DOMAINS.local + '/api/v1/tokens/';
  // private _apiUrl = 'http://' + DOMAINS.develop + '/api/v1/tokens/';

  private _session: SessionSingletonModel;
  private _sessionCookie;
  private _jwtCookie;

  private _sessionCheckerInterval = undefined;
  private _sessionInterval = 2 * 60 * 1000; // 2 minutes 2 * 60 * 1000

  constructor(private _http: Http,
              private _httpSrv: HttpSrvService,
              private _router: Router,
              private _loaderSrv: LoaderService) {
    this._sessionCookie = new Cookie('userSession');
    this._jwtCookie = new Cookie('jwt');
    this.cronSession();
  }


  /**
   * Login via email and password
   * @param {string} username
   * @param {string} password
   * @returns {Observable<Response>}
   */
  public login(username: string,
               password: string): Observable<Response> {
    return this._http
      .post(
        this._apiUrl.concat('login/'), {user: username, password: password})
      .map((success) => this.onSuccessLogin(success))
      .catch(this.onFailLogin);
  }


  /**
   * Login via social buttons
   * @param {Object | any} options
   * @returns {Observable<Response>}
   */
  public loginWithSn(options: object | any): Observable<Response> {
    return this._http
      .post(this._apiUrl.concat('social/'), options)
      .map((success) => this.onSuccessLogin(success))
      .catch(this.onFailLoginWithSn);
  }


  /**
   * Logout
   */
  public logout(): void {
    localStorage.removeItem('logout');
    clearInterval(this._sessionCheckerInterval);
    this._session = null;

    this._sessionCookie.delete();
    this._jwtCookie.delete();
    this.deleteAllCookies();

    this._router.navigateByUrl('/login');
  }

  public updateSession(): void {
    this.checkSession();
  }

  /**
   * Regenerate the session model
   * @param {Response | any} response
   * @returns {boolean}
   */
  private generateSession(response: Response | any): boolean {
    if (response.hasOwnProperty('object')
      && response['object'].hasOwnProperty('token')
      && response['object'].hasOwnProperty('person')) {
      this._sessionCookie.generate(JSON.stringify(response['object']['person']));
      this._jwtCookie.generate(response['object']['token']);

      return true;
    }

    return false;
  }


  private deleteAllCookies(): void {
    const cookies = Cookie._getAllCookieNamesInSpace();

    for (let i = 0; i < cookies.length; i++) {
      Cookie._delete(cookies[i]);
    }
  }

  private expiredSession() {
    // @TODO: mostrar aviso de sesion, poner timeout para redireccion
    this.logout();
  }

  /**
   * Launch interval session check
   */
  private cronSession(): void {
    this.checkSession();
    clearInterval(this._sessionCheckerInterval);

    this._sessionCheckerInterval = setInterval(() => {
      this.checkSession();
    }, this._sessionInterval);
  }



  // ******************* API METHODS ******************* //
  private responseSuccess(response: Response | any): boolean {
    return response.hasOwnProperty('status')
      && Number(response['status']) === 200;
  }


  private onSuccessLogin(response: Response | any): any {
    const user = response.json();
    if (this.responseSuccess(response) && this.generateSession(user)) {
      this._session = SessionSingletonModel.getInstance();
      this._session.initialize(this._jwtCookie.getStringInstance(), this._sessionCookie.getJsonInstance());
      this.cronSession();

      return {
        auth: this.responseSuccess(response),
        token: user['object']['token'],
        user: user['object']['mate'],
        success: true
      };
    }

    return {
      success: false,
      auth: false,
      message: user.hasOwnProperty('message')
        ? user['message']
        : 'Hubo un problema con el login. Inténtelo de nuevo.'
    };
  }


  private checkSession(): void {
    try {
      this._httpSrv
        .get(
        'tokens/session/',
          undefined,
          null,
          false,
          false
        )
        .subscribe(
        (success) => this.onGetSession(success),
        (fail) => this.expiredSession()
      );
    } catch (e) {
      // console.log('Error');
      // console.log(e);
    }
  }

  private onGetSession(response: object | any): void {
    if (response['success'] && response.hasOwnProperty('object')) {
      this._session = SessionSingletonModel.getInstance();
      this._session.person = new PersonModel(this._sessionCookie.getJsonInstance());
      this._session.active  = response['success'];

      if (!this._sessionCookie.equalObjectToStringInstance(response['object'])) {
        this._sessionCookie.generate(response['object']);
        // @TODO: realizar aqui acciones por cambio de session en user: ir al index
      }

    } else {
      this.expiredSession();
    }
  }


  private onFailLogin(error: Response | any): Observable<Response> {
    // console.log('error');
    // console.log(error);
    return Observable.throw({
      success: false,
      auth: false,
      message: error
    });
  }

  private onFailLoginWithSn(error: Response | any): Observable<Response> {
    // console.log('error');
    // console.log(error);
    return Observable.throw({
      success: false,
      auth: false,
      message: error
    });
  }

  // ********** session data getters ********** //

  isLogged(): boolean {
    return this._jwtCookie.exist() && this._sessionCookie.exist();
  }

  getPersonLogged(): PersonModel {
    this._session = SessionSingletonModel.getInstance();
    this._session.person = new PersonModel(this._sessionCookie.getJsonInstance());

    return this._session.person;
  }



  hasDomain(): boolean {
    return this.isLogged
      && !_.isNull(this.getPersonLogged().domain)
      && !_.isEmpty(this.getPersonLogged().domain.name);
  }

  getDomain(): DomainModel {
    return this.getPersonLogged().domain;
  }

  getDomainName(): string {
    return this.hasDomain()
      ? this.getDomain().name
      : '-';
  }

  getDomainId(): number {
    return this.hasDomain()
      ? this.getDomain().id
      : 0;
  }


  hasSubDomain(): boolean {
    return false;
  }

  getSubDomain(): DomainModel {
    return null;
  }

  getSubDomainName(): string {
    return this.hasSubDomain()
      ? this.getSubDomain().name
      : '-';
  }

  getRoles(): Array<RoleSystemModel> {
    return this.getPersonLogged().role_systems;
  }

  getFullName(): string | any {
    return this.getPersonLogged().getFullName();
  }

  getPersonId(): number {
    return this.getPersonLogged().id;
  }


  // ************** REDIRECT METHODS ******************** //
  /**
   * Verify status session. IF invalid then redirect to login page
   */
  verifySession(): void {
    if (!this.isLogged()) {
      this._router.navigateByUrl('/login');
    }
  }

  goToProfile() {
    if (this.isLogged()) {
      localStorage.setItem('activeGeneralTab', '1');
      this._router.navigateByUrl('/mate-profile/' + this.getPersonId());
    } else {
      this._router.navigateByUrl('/login');
    }
  }

  goToAdminPanel() {
    if (this.isLogged()) {
      this.isRoleSuperAdmin() || this.isRoleAdmin()
        ? this._router.navigateByUrl('/admin')
        : this.goToProfile();

    } else {
      this._router.navigateByUrl('/login');
    }
  }


  // ********************* ROLE SYSTEM METHODS **************************** //
  hasRoleSystemsList(roleSystems: Array<string> | any): boolean {
    return this.getPersonLogged().hasRoleSystem(roleSystems);
  }


  getManagedTeams(): Array<object> {
    return this.getPersonLogged().managed_teams;
  }

  isRoleSuperAdmin(): boolean {
    return this.getPersonLogged().isSuperAdmin();
  }

  isRoleAdmin(): boolean {
    return this.getPersonLogged().isAdmin();
  }

  isRoleGestor(): boolean {
    return this.getPersonLogged().isGestor();
  }

  isRoleUser(): boolean {
    return this.getPersonLogged().isMate();
  }


  isRoleAdminsGestor(): boolean {
    return this.isRoleAdmin() || this.isRoleSuperAdmin() || this.isRoleGestor();
  }

  // ******************** PERMISSION METHODS ************************* //

  /**
   * Check for a view if a logged user has permission to visualize it. If not, redirect page
   * @param {string} url
   */
  verifyViewPermission(url: string): void {
    if (!this.hasViewPermission(url)) {
      this._loaderSrv.denegation();

      setTimeout(() => {
        this.goToProfile();
      }, 5000);
    }
  }


  /**
   * Return if exist a determinate view in permission list
   * @param {string} url
   * @returns {boolean}
   */
  private existViewPermission(url: string): boolean {
    if (_.isNil(url)) {
      return false;
    }

    const view = _.trim(url, '/');

    return _.isEmpty(view) || PERMISSIONS.hasOwnProperty(view);
  }


  /**
   * Return if the logged has permission to visit a determinate view. Index is always available
   * @param {string} url
   * @returns {boolean}
   */
  hasViewPermission(url: string): boolean {
    if (!this.existViewPermission(url)) {
      return false;
    }

    return this.hasRoleSystemsList(PERMISSIONS[_.trim(url, '/')]['access']);
  }


  /**
   * Return a complete permission object
   * @param {string} url
   * @returns {Object}
   */
  private getViewPermission(url: string): object {
    if (!this.existViewPermission(url)) {
      return {};
    }

    return PERMISSIONS[_.trim(url, '/')];
  }


  /**
   * Return if a logged user has permission to access a determinate component
   * @param {string} url
   * @param {string} tag
   * @returns {boolean}
   */
  hasComponentPermission(url: string, tag: string): boolean {
    const permission = this.getViewPermission(url);

    if (_.isNil(permission) || _.isEmpty(permission) || _.isNil(tag)
      || !permission.hasOwnProperty('components')) {
      return false;
    }

    return permission['components'].hasOwnProperty(tag)
      && this.hasRoleSystemsList(permission['components'][tag]);
  }

  /**
   * Return if exist setup for a view
   * @param {string} url
   * @returns {boolean}
   */
  private hasViewConfig(url: string): boolean {
    const permission = this.getViewPermission(url);

    return !_.isNil(permission)
      && !_.isEmpty(permission)
      && permission.hasOwnProperty('config')
      && !_.isEmpty(permission['config']);
  }


  /**
   * Return a view configuration
   * @param {string} url
   * @returns {Object}
   */
  public getViewConfig(url: string): object {
    if (!this.hasViewConfig(url)) {
      return {};
    }

    const viewConfig = {};

    _.forIn(this.getViewPermission(url)['config'], (config, key) => {
      config.forEach(item => {
        if (this.hasRoleSystemsList(item['case'])) {
          viewConfig[key] = item['value'];
        }
      });
    });

    return _.isEmpty(viewConfig)
      ? {}
      : viewConfig;
  }
}
