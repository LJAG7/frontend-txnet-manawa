import {Injectable} from '@angular/core';
import {Http, Response, Headers, URLSearchParams, RequestOptions} from '@angular/http';
import {Router} from '@angular/router';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';

import {LogModel} from '../models/log/log.model';

import {AlertService} from './alert.service';
import {LoaderService} from './loader.service';

import {DOMAINS} from '../const/system.const';

const _ = require('lodash');

@Injectable()
export class HttpSrvService {

  private _apiUrl = location.origin.indexOf(DOMAINS.localhost) === -1
    ? location.origin + '/api/'
    : '//' + DOMAINS.local + '/api/';
  // private _apiUrl = 'http://' + DOMAINS.develop + '/api/';

  private _baseUrl = this._apiUrl.concat('v1/');

  private _headers: RequestOptions;

  private _callOwnerMethod: string;

  private _callTime = 0;
  private _dateStartApiCall: number;
  private _dateEndApiCall: number;
  private _logList: Array<LogModel> = [];
  private _timeToSendLog = 60000;

  private _sendLog = false;

  /**
   * Return if a domain is like than location
   * @param {string} domainIdx
   * @returns {boolean}
   */
  private static isDomainMode(domainIdx: string): boolean {
    return DOMAINS.hasOwnProperty(domainIdx)
      && !_.isNil(DOMAINS[domainIdx])
      && location.origin.indexOf(DOMAINS[domainIdx]) !== -1;
  }


  /**
   * Returns if the domain is between those considered to received logs
   * @returns {boolean}
   */
  public static isDebugMode(): boolean {
    const debugDomains = _.omit(DOMAINS, ['preProduction', 'production']);

    for (const domainList in debugDomains) {
      if (HttpSrvService.isDomainMode(domainList)) {
        return true;
      }
    }
    return false;
  }

  /**
   * The domain is preProduction
   * @returns {boolean}
   */
  public static isPreMode(): boolean {
    return HttpSrvService.isDomainMode('preProduction');
  }

  /**
   * The domain is production
   * @returns {boolean}
   */
  public static isProdMode(): boolean {
    return HttpSrvService.isDomainMode('production');
  }

  /**
   * Generate the URL string params
   * @param query
   * @returns {URLSearchParams}
   */
  private static createQueryString(query: any): URLSearchParams {
    const paramsQueryString = new URLSearchParams();

    if (query) {
      for (const param in query) {
        if (query.hasOwnProperty(param)) {
          paramsQueryString.set(param, query[param]);
        }
      }
    }
    return paramsQueryString;
  }

  /**
   * Generate the Json Web Token
   * @returns {RequestOptions}
   */
  private static jwt(): RequestOptions {
    // create authorization header with jwt token
    const jwtHash = document.cookie
      .split(';')
      .find((element) => {
        return element.indexOf('jwt=') !== -1;
      });

    if (jwtHash && jwtHash !== undefined) {
      return new RequestOptions({headers: new Headers({'Authorization': 'Bearer ' + jwtHash.split('=')[1].trim()})});
    }
  }


  private static createLogRegistryObject(method: string, url: string): LogModel {
    const logRegistry = new LogModel();
    logRegistry.action = method;
    logRegistry.description = url;
    return logRegistry;
  }

  private static generateMessage(title: string, description: string): string {
    return !_.isNil(title) && !_.isEmpty(title)
      ? `${title}: ${_.lowerFirst(description)}`
      : _.upperFirst(description);
  }


  constructor(private _http: Http,
              private _alertSrv: AlertService,
              private _router: Router,
              private _loaderSrv: LoaderService) {
    this._http = _http;
    // this._sendLog = HttpSrvService.isDebugMode();
    this.createHeaders();
    this.intervalLogs();
  }


  public getUrlApi(): string {
    return this._apiUrl;
  }

  /**
   *
   * @param {string} method
   * @param {string} endpoint
   * @param {Object} query
   * @param {string} alertTitle
   * @param {boolean} alertShow
   * @param {boolean} loaderShow
   * @returns {Observable<Response>}
   */
  public call(method: string,
              endpoint: string,
              query?: object,
              alertTitle?: string,
              alertShow: boolean = true,
              loaderShow: boolean = true): Observable<Response> {

    this._callTime = 0;
    this._dateStartApiCall = Date.now();
    this._callOwnerMethod = alertTitle || '';

    const url = this._baseUrl.concat(encodeURI(endpoint));
    const logRegistry = HttpSrvService.createLogRegistryObject(method, url);
    let methodObject;

    if (HttpSrvService.jwt() !== undefined) {
      if (loaderShow) {
        this.showLoader(method);
      }

      if (_.includes(method, 'get')) {
        methodObject = this._http.get(
          url,
          Object.assign(HttpSrvService.jwt(),
            {search: HttpSrvService.createQueryString(query)})
        );

      } else if (_.includes(method, 'delete')) {
        methodObject = this._http[method](url, HttpSrvService.jwt());

      } else {
        methodObject = this._http[method](url, query, HttpSrvService.jwt());
      }

      return methodObject
        .map((response) => this.extractData(response, logRegistry, alertShow))
        .catch((response) => this.handleError(response, this, logRegistry));

    } else {
      this._alertSrv.error('No token JWT to do calls to API');
      return Observable.throw({
        success: false,
        error: 'No token JWT to do calls to API'
      });
    }
  }

  /**
   *
   * @param {string} endpoint
   * @param {Object} query
   * @param {string} alertTitle
   * @param {boolean} alertShow
   * @param {boolean} loaderShow
   * @returns {Observable<Response>}
   */
  public get(endpoint: string,
             query?: object,
             alertTitle?: string,
             alertShow: boolean = false,
             loaderShow: boolean = true): Observable<Response> {
    return this.call('get', endpoint, query, alertTitle, alertShow, loaderShow);
  }

  /**
   *
   * @param {string} endpoint
   * @param {Object} query
   * @param {string} alertTitle
   * @param {boolean} alertShow
   * @param {boolean} loaderShow
   * @returns {Observable<Response>}
   */
  public post(endpoint: string,
              query?: object,
              alertTitle?: string,
              alertShow: boolean = true,
              loaderShow: boolean = true): Observable<Response> {
    return this.call('post', endpoint, query, alertTitle, alertShow, loaderShow);
  }

  /**
   *
   * @param {string} endpoint
   * @param {Object} query
   * @param {string} alertTitle
   * @param {boolean} alertShow
   * @param {boolean} loaderShow
   * @returns {Observable<Response>}
   */
  public patch(endpoint: string,
               query?: object,
               alertTitle?: string,
               alertShow: boolean = true,
               loaderShow: boolean = true): Observable<Response> {
    return this.call('patch', endpoint, query, alertTitle, alertShow, loaderShow);
  }


  /**
   *
   * @param {string} endpoint
   * @param {Object} query
   * @param {string} alertTitle
   * @param {boolean} alertShow
   * @param {boolean} loaderShow
   * @returns {Observable<Response>}
   */
  public put(endpoint: string,
             query?: object,
             alertTitle?: string,
             alertShow: boolean = true,
             loaderShow: boolean = true): Observable<Response> {
    return this.call('put', endpoint, query, alertTitle, alertShow, loaderShow);
  }


  /**
   *
   * @param {string} endpoint
   * @param {Object} query
   * @param {string} alertTitle
   * @param {boolean} alertShow
   * @param {boolean} loaderShow
   * @returns {Observable<Response>}
   */
  public delete(endpoint: string,
                query?: object,
                alertTitle?: string,
                alertShow: boolean = true,
                loaderShow: boolean = true): Observable<Response> {
    return this.call('delete', endpoint, query, alertTitle, alertShow, loaderShow);
  }

  /**
   *
   * @param {string} method
   * @param {string} endpoint
   * @param {Object} query
   * @param {string} alertTitle
   * @param {boolean} alertShow
   * @param {boolean} loaderShow
   * @returns {Observable<Response>}
   */
  public outCall(method: string,
                 endpoint: string,
                 query?: object,
                 alertTitle?: string,
                 alertShow: boolean = true,
                 loaderShow: boolean = true): Observable<Response> {

    this._callTime = 0;
    this._dateStartApiCall = Date.now();
    this._callOwnerMethod = alertTitle || '';

    const logRegistry = HttpSrvService.createLogRegistryObject(method, endpoint);

    let methodObject;

    if (HttpSrvService.jwt() !== undefined) {
      if (loaderShow) {
        this.showLoader(method);
      }

      if (_.includes(method, 'get')) {
        methodObject = this._http.get(
          endpoint,
          Object.assign(HttpSrvService.jwt(),
            {search: HttpSrvService.createQueryString(query)})
        );

      }

      return methodObject
        .map((response) => this.extractData(response, logRegistry, alertShow))
        .catch((response) => this.handleError(response, this, logRegistry));

    } else {
      this._alertSrv.error('No token JWT to do calls to API');
      return Observable.throw({
        success: false,
        error: 'No token JWT to do calls to API'
      });
    }
  }

  /**
   *
   * @param {string} endpoint
   * @param {Object} query
   * @param {string} alertTitle
   * @param {boolean} alertShow
   * @param {boolean} loaderShow
   * @returns {Observable<Response>}
   */
  public outGet(endpoint: string,
             query?: object,
             alertTitle?: string,
             alertShow: boolean = false,
             loaderShow: boolean = true): Observable<Response> {
    return this.outCall('get', endpoint, query, alertTitle, alertShow, loaderShow);
  }


  // ********************* PRIVATE METHODS ************************* //
  /**
   * XHR header
   */
  private createHeaders(): void {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'GET, POST, HEAD, OPTIONS, PUT, DELETE, PATCH');
    headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');

    this._headers = new RequestOptions({headers: headers});
  }

  /**
   * Initializa the soft loader component
   * @param {string} method
   */
  private showLoader(method: string): void {
    switch (method.toLowerCase()) {
      case 'post':
            this._loaderSrv.softSaving();
            break;
      case 'put':
      case 'patch':
            this._loaderSrv.softUpdating();
            break;
      case 'delete':
            this._loaderSrv.softDeleting();
            break;
      default:
            this._loaderSrv.softLoading();
    }
  }


  /**
   * Generate the alert message
   * @param response
   */
  private showResponseAlert(response: any): void {
    const message = HttpSrvService.generateMessage(
      this._callOwnerMethod,
      response.hasOwnProperty('success')
        ? response.message
        : 'error inesperado. Por favor, inténtalo de nuevo más tarde'
    );

    if (Number(response.code_action) === 7) {
      this._loaderSrv.denegation();
      setTimeout(() => {
        this._router.navigateByUrl('/');
      }, 2000);

    } else if (response.hasOwnProperty('success') && response.success) {
      if (Number(response.code_action) === 6) {
        this._alertSrv.warn(message);
      } else {
        this._alertSrv.success(message);
      }
    } else {
      this._alertSrv.error(message);
    }
  }


  private mustBeShowAlert(cast: any, showAlert: boolean): boolean {
    return showAlert
      || (cast.hasOwnProperty('code_action')
      && (Number(cast.code_action) === 7 || Number(cast.code_action) === 6));
  }
  /**
   * Cast the response
   * @param {Response} response
   * @param {Object | any} logRegistry
   * @param {boolean} showAlert
   * @returns {any}
   */
  private castResponse(response: Response,
                       logRegistry: LogModel,
                       showAlert: boolean): any {
    const cast = {latency: 2000};

    _.forIn(response.json(), (value, key) => {
      cast[key] = value;
    });

    this._loaderSrv.stopSoft();

    if (this.mustBeShowAlert(cast, showAlert)) {
      if (this._sendLog) {
        logRegistry.error = cast['message'];
        this.createLogRegistry(logRegistry);
      }
      this.showResponseAlert(cast);
    }

    if (this._sendLog) {
      this.createLogRegistry(logRegistry);
    }

    return cast;
  }


  /**
   * Response Handler
   * @param {Response} response
   * @param {Object | any} logRegistry
   * @param {boolean} alertShow
   * @returns {Observable<Response>}
   */
  private extractData(response: Response,
                      logRegistry: LogModel,
                      alertShow: boolean): Observable<Response> {
    this._dateEndApiCall = Date.now();
    this._callTime = this._dateEndApiCall - this._dateStartApiCall;
    logRegistry.time_miliseg_api = this._callTime.toString();

    return this.castResponse(response, logRegistry, alertShow);
  }


  /**
   * Error handler
   * @param {Response | any} error
   * @param {HttpSrvService} http
   * @param {Object | any} logRegistry
   * @returns {ErrorObservable}
   */
  private handleError(error: Response | any,
                      http: HttpSrvService,
                      logRegistry: LogModel) {
    // In a real world app, we might use a remote logging infrastructure
    http._dateEndApiCall = Date.now();
    http._callTime = http._dateEndApiCall - http._dateStartApiCall;
    logRegistry.time_miliseg_api = this._callTime.toString();

    const errMsg = error instanceof Response
      ? `${error.status} - ${error.statusText || ''}`
      : error.message ? error.message : error.toString();

    logRegistry.error = errMsg;

    if (http._sendLog) {
      http._logList.push(logRegistry);
    }

    http._loaderSrv.stopSoft();
    http._alertSrv.error(HttpSrvService.generateMessage(http._callOwnerMethod, errMsg));

    return Observable.throw(errMsg);
  }

  // *********************** LOGS METHODS ************************* //


  private intervalLogs(): void {
    if (this._sendLog) {
      setInterval(() => {
        if (this._logList.length > 0) {
          this.sendLog();
        }
      }, this._timeToSendLog);
    }
  }


  private createLogRegistry(logRegistry: object | any): void {
    if (this._sendLog) {
      this._logList.push(logRegistry);
    }
  }


  private sendLog(): void {
    // console.log('Envío de logs pospuesto hasta modificación del endpoint para recibir una lista en lugar de un objeto');
    // this._logList = [];
    this._logList.forEach(log => {
      this.call(
        'post',
        location.origin.indexOf(DOMAINS.localhost) === -1 ? location.origin + 'logs/' : 'logs/',
        log,
        undefined,
        false
        ).subscribe(
        (responseOk) => { this._logList = []; },
        (responseFail) => console.log('Error al crear el log'));
    });
  }

}
