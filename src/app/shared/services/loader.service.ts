import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';

import {Loader, LoaderType, LoaderMessageType} from '../models/loader';

const _ = require('lodash');

@Injectable()
export class LoaderService {

  private subject = new Subject<Loader>();
  private neededFromApi = new Subject<Array<string>>();

  private type: LoaderType;

  constructor(private router: Router) {
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.stop();
      }
    });
    this.type = null;
  }

  loader(type: LoaderType, message: LoaderMessageType): void {
    this.subject.next(<Loader>{ type: type, message: message });
  }

  getLoader(): Observable<Loader> {
    return this.subject.asObservable();
  }

  getNeededFromApi(): Observable<Array<string>> {
    return this.neededFromApi.asObservable();
  }

  setNeededFromApi(neededFromApi: Array<string> = null): void {
    this.neededFromApi.next(neededFromApi);
  }

  getType(): LoaderType {
    return this.type;
  }

  denegation(): void {
    this.type = LoaderType.Denegation;
    this.loader(LoaderType.Denegation, LoaderMessageType.NotAllowed);
  }

  general(neededFromApi: Array<string> = null): void {
    if (_.isNil(this.type) || (!_.isNil(this.type) && this.type !== LoaderType.Denegation)) {
      this.type = LoaderType.General;
      this.setNeededFromApi(neededFromApi);
      this.loader(LoaderType.General, LoaderMessageType.Loading);
    }
  }

  soft(loaderMessageType: LoaderMessageType): void {
    if (_.isNil(this.type) || (!_.isNil(this.type) && this.type !== LoaderType.General)) {
      this.type = LoaderType.SoftLoader;
      this.loader(LoaderType.SoftLoader, loaderMessageType);
    }
  }

  softLoading(): void {
    this.soft(LoaderMessageType.Loading);
  }

  softUpdating(): void {
    this.soft(LoaderMessageType.Updating);
  }

  softSaving(): void {
    this.soft(LoaderMessageType.Saving);
  }

  softDeleting(): void {
    this.soft(LoaderMessageType.Deleting);
  }

  softVerifying(): void {
    this.soft(LoaderMessageType.Verifying);
  }

  softImporting(): void {
    this.soft(LoaderMessageType.Importing);
  }

  softRedirecting(): void {
    this.soft(LoaderMessageType.Redirecting);
  }


  stop(): void {
    this.type = null;
    this.loader(null, null);
  }

  stopGeneral(): void {
    if (_.isNil(this.type)
      || (!_.isNil(this.type) && this.type === LoaderType.General)) {
      this.type = null;
      this.loader(null, null);
    }
  }

  stopSoft(): void {
    if (_.isNil(this.type)
      || (!_.isNil(this.type) && this.type === LoaderType.SoftLoader)) {
      this.type = null;
      this.loader(null, null);
    }
  }
}
