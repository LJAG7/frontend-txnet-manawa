import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, URLSearchParams, RequestOptions} from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';

import {DOMAINS} from '../const/system.const';

const _ = require('lodash');

@Injectable()
export class HttpNoJwtSrvService {

  private _baseUrl = location.origin.indexOf(DOMAINS.localhost) === -1
    ? location.origin + '/api/v1/'
    : '//' + DOMAINS.local + '/api/v1/';
  // private _baseUrl = 'http://' + DOMAINS.develop + '/api/v1/';

  private _headers: RequestOptions;

  private _callTime = 0;
  private _dateStartApiCall: number;
  private _dateEndApiCall: number;

  /**
   * Generate the URL string params
   * @param query
   * @returns {URLSearchParams}
   */
  private static createQueryString(query: any): URLSearchParams {
    const paramsQueryString = new URLSearchParams();

    if (query) {
      for (const param in query) {
        if (query.hasOwnProperty(param)) {
          paramsQueryString.set(param, query[param]);
        }
      }
    }
    return paramsQueryString;
  }


  constructor(private _http: Http) {
    this._http = _http;
    this.createHeaders();
  }

  /**
   *
   * @param {string} method
   * @param {string} endpoint
   * @param {Object} query
   * @returns {Observable<Response>}
   */
  public call(method: string,
              endpoint: string,
              query?: object): Observable<Response> {
    this._callTime = 0;
    this._dateStartApiCall = Date.now();

    const url = this._baseUrl.concat(endpoint);
    let methodObject;

    if (!_.includes(method, 'get') && !_.includes(method, 'post')) {
      return Observable.throw({
        success: false,
        error: 'Method unsupported by this service'
      });

    } else {
      if (_.includes(method, 'get')) {
        methodObject = this._http.get(
          url,
          {search: HttpNoJwtSrvService.createQueryString(query)}
        );

      } else {
        methodObject = this._http[method](url, query);
      }

      return methodObject
        .map((response) => this.extractData(response))
        .catch(this.handleError);
    }
  }


  private createHeaders(): void {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'GET, POST, HEAD, OPTIONS, PUT, DELETE, PATCH');
    headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    this._headers = new RequestOptions({headers: headers});
  }


  private extractData(res: Response): Observable<Response> {
    this._dateEndApiCall = Date.now();
    this._callTime = this._dateEndApiCall - this._dateStartApiCall;
    return this.castResponse(res);
  }

  private castResponse(response: Response): any {
    const cast = {latency: 2000};

    _.forIn(response.json(), (value, key) => {
      cast[key] = value;
    });

    return cast;
  }

  private handleError(error) {
    const errMsg = error instanceof Response
      ? `${error.status} - ${error.statusText || ''}`
      : error.message ? error.message : error.toString();

    return Observable.throw(errMsg);
  }

}
