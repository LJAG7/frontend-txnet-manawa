import {Injectable} from '@angular/core';
import { Subject } from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';

import {HttpSrvService} from './http.service';
import {LoaderService} from './loader.service';
import {DateLocalPipe} from '../pipes/dateLocal.pipe';
import {CSVModel} from '../models/csv/csv.model';
import {CSVRowModel} from '../models/csv/row.model';

const _ = require('lodash');

let parseCsvService: any;

@Injectable()
export class ParseCsvService {

  private _subject = new Subject<CSVModel>();

  private _csv: CSVModel;
  private _separators = [',', ';', '\t'];
  private _typeSupported = ['text/csv', 'application/vnd.ms-excel'];
  private _maxFileSize = 4;  // MB

  private _strict = false;
  private _verifyFields = {};

  private _warningMessages = {
    base: 'Fila :row: ',
    invalidRow: ':col no válido',
    required: ':col obligatorio',
    duplicity: ':value duplicado',
    failVerify: ':value ya existe en el sistema'
  };

  private _errorMessages = {
    invalidFileType: 'Error: Tipo de archivo no válido. Descargue el CSV prototipo.',
    invalidFileSize: 'Error: El archivo supera los 4Mb permitidos.',
    invalidSeparators: 'Error: CSV no válido. Separadores permitidos: :sep',
    invalidHeaders: 'Error: CSV con cabeceras inválidas. Requeridas: :heads',
    duplicityHeaders: 'Error: CSV con cabeceras duplicadas',
    empty: 'Error: CSV vacío o sin datos bien formados.',
    separatorsInHeader: 'Error: Detectado separador en nombre de cabecera ( :col )',
    separatorsInText: 'Error: Detectado separador en texto ( :sep ). Revise fila :row',
    emptyValidRows: 'Error: 0 registro(s) válidos a importar de un total de :total',
    verifyErrorApi: 'Error: el servicio de verificación para el campo :col no responde. Intentalo más tarde.',
    strict: 'Error: CSV con datos no válidos. Revise campo :col, fila :row.',
    strictDuplicity: 'Error: El campo :col debe tener valores únicos. Revise fila :row.',
    strictVerifyFail: 'Error: :col :value ya existe en el sistema. Revise fila :row.'
  };

  /**
   *
   * @param {string} birthdate
   * @returns {any}
   */
  private static parseDate(birthdate: string): any {
    if (_.includes(birthdate, '/')) {
      const dateFormatted = birthdate.split('/');
      return dateFormatted[2].concat('-' + dateFormatted[1]).concat('-' + dateFormatted[0]);
    }

    return birthdate;
  }


  /**
   * Return the header messages that are required
   * @param {Object} headersRules
   * @returns {string}
   */
  private static getMessageHeadersRequired(headersRules: object): string {
    const required = [];

    _.forIn(headersRules, (rule, header) => {
      if (rule['required']) {
        required.push(header);
      }
    });

    return required.join(', ');
  }

  /**
   * Return if a row is valid according pattern test
   * @param data
   * @param pattern
   * @param {boolean} isRequired
   * @returns {boolean}
   */
  private static validateRow(data: any, pattern: any, isRequired: boolean): boolean {
    return isRequired
      ? pattern.test(data) && !_.isEmpty(data)
      : pattern.test(data);
  }

  /**
   * Return if is an empty row not required
   * @param data
   * @param {boolean} isRequired
   * @returns {boolean}
   */
  private static isEmptyNoRequired(data: any, isRequired: boolean): boolean {
    return !isRequired && _.isEmpty(data);
  }


  constructor (private _httpSrv: HttpSrvService,
               private _datePipe: DateLocalPipe,
               private _loaderSrv: LoaderService) {
    this._csv = new CSVModel();
    parseCsvService = this;

    const sepString = this._separators.join(' ').replace('\t', 'tabulador');
    this._errorMessages.invalidSeparators = this._errorMessages.invalidSeparators.replace(':sep', sepString);
    this._errorMessages.separatorsInText = this._errorMessages.separatorsInText.replace(':sep', sepString);
  }

  getCsv(): Observable<CSVModel> {
    return this._subject.asObservable();
  }


  /**
   * Determine if the separator is valid
   * @returns {boolean}
   */
  private isValiSeparators(): boolean {
    for (let idx = 0, nloop = this._separators.length; idx < nloop; idx++) {
      if (this._csv.plainData[0].lastIndexOf(this._separators[idx]) !== -1) {
        this._csv.separator = this._separators[idx];
        break;
      }
    }

    if (this._csv.separator === undefined) {
      this._csv.error = this._errorMessages.invalidSeparators;
      return false;
    }

    return true;
  }


  /**
   * Determine if the csv headers is valid
   * @param {Object} hRules
   * @returns {boolean}
   */
  private isValidHeaders(hRules: object): boolean {
    // const csvHeader =
    this._csv.plainHeaders = _.trim(this._csv.plainData[0].replace(new RegExp('"', 'g'), '')).split(this._csv.separator);
    for (const header in hRules) {

      if (hRules.hasOwnProperty(header)) {
        let existKeyInCSV = false;

        for (const key of this._csv.plainHeaders) {
          if (this._separators.filter((sep) => { return key.indexOf(sep) !== -1; }).length > 0) {
            this._csv.headers = [];
            this._csv.error = this._errorMessages.separatorsInHeader.replace(':col', key);
            return false;
          }

          if (!_.isEmpty(key) && _.snakeCase(header) === _.trim(_.snakeCase(key))) {
            existKeyInCSV = true;
            break;
          }
        }

        if (existKeyInCSV) {
          this._csv.headers.push(header);

          if (hRules[header].hasOwnProperty('validation')) {
            this._verifyFields[header] = {
              endpoint: hRules[header]['validation'],
              processed: 0
            };
          }

        } else if (hRules[header].required) {
          this._csv.headers = [];
          this._csv.error = this._errorMessages.invalidHeaders.replace(':heads', ParseCsvService.getMessageHeadersRequired(hRules));
          return false;
        }
      }
    }

    return true;
  }


  /**
   * Return if exist duplicates headers
   * @returns {boolean}
   */
  private hasDuplicatedHeaders(): boolean {

    if (_.uniq(this._csv.plainHeaders).length !== this._csv.plainHeaders.length) {
      this._csv.headers = [];
      this._csv.error = this._errorMessages.duplicityHeaders;
      return true;
    }

    return false;
  }


  /**
   * Return if exist duplicates entry for a header values
   * @param {CSVRowModel} rowData
   * @param {string} header
   * @returns {boolean}
   */
  private isDuplicatedValue(rowData: CSVRowModel, header: string): boolean {
    return this._csv.data.filter((row: CSVRowModel) => {
      return row.values[header] === rowData.values[header];
    }).length > 0;
  }


  /**
   * Process a row data
   * @param {CSVRowModel} data
   * @param {Object} hRules
   * @returns {boolean}
   */
  private processRow(data: CSVRowModel, hRules: object): boolean {
    for (const header of this._csv.headers) {

      if (ParseCsvService.validateRow(data.values[header], hRules[header].pattern, hRules[header].required)) {
        if (header === 'birthdate') {
          data.values[header] = this._datePipe.datetimeToISO(ParseCsvService.parseDate(data.values[header]));
        }

        if (header === 'name' || header === 'surname') {
          data.values[header] = data.values[header].toLowerCase().replace(/\b\w/g, function(l) { return l.toUpperCase(); } );
        }

        // add queue validation
        if (this._verifyFields.hasOwnProperty(header) && this.isDuplicatedValue(data, header)) {
          if (this._strict) {
            data.errors.push(this._errorMessages.strictDuplicity.replace(':col', header));
            return false;

          } else {
            data.warnings.push(this._warningMessages.duplicity.replace(':value', data.values[header]));
          }
        }

      } else if (ParseCsvService.isEmptyNoRequired(data.values[header], hRules[header].required)) {
        data.values[header] = null;

      } else if (this._strict) {
        data.errors.push(this._errorMessages.strict.replace(':col', header));
        return false;

      } else {
        data.warnings.push(hRules[header].required
          ? this._warningMessages.required.replace(':col', header)
          : this._warningMessages.invalidRow.replace(':col', header)
        );
      }
    }

    return true;
  }

  /**
   * Extract the csv data to array. The " is deleted in all text
   * @param {Object} hRules
   * @returns {boolean}
   */
  private extractData(hRules: object): boolean {
    for (let idxRow = 1, rloop =  this._csv.plainData.length; idxRow < rloop; idxRow++) {
      const plainRow = _.trim(this._csv.plainData[idxRow].replace(new RegExp('"', 'g'), '')).split(this._csv.separator);

      // Empty row case
      if (_.isEmpty(plainRow) || (plainRow.length === 1 && _.isEmpty(plainRow[0]))) {
        break;
      }

      // Errors separator in text case
      if (plainRow.length !== this._csv.plainHeaders.length) {
        this._csv.error = this._errorMessages.separatorsInText.replace(':row', idxRow.toString());
        return false;
      }

      const data = new CSVRowModel({row: idxRow, values: this._csv.generateValues(plainRow)});

      if (!this.processRow(data, hRules)) {
        this._csv.error = data.errors[0].replace(':row', data.row.toString());
        return false;
      }

      this._csv.data.push(data);
    }

    return !this.isEmptyProcessedFile();
  }


  /**
   * Return if a processed csv file is empty or not
   * @returns {boolean}
   */
  private isEmptyProcessedFile(): boolean {
    if (this._csv.getTotalValidRows() === 0) {
      this._csv.error = this._csv.getTotalInvalidRows() > 0
        ? this._errorMessages.emptyValidRows.replace(':total', this._csv.data.length.toString()) + this.standardWarningMessage(this._csv)
        : this._errorMessages.empty;
      return true;
    }

    return false;
  }


  /**
   * Return if the file is valid type and size
   * @param files
   * @returns {boolean}
   */
  private isValidFile(files): boolean {
    const isValid = files === undefined || _.filter(this._typeSupported, function (object) {
      return _.lowerCase(object) === _.lowerCase(files.item(0).type);
    }).length > 0;

    if (!isValid) {
      this._csv.error = this._errorMessages.invalidFileType;
      this.updateObserver();
      return false;
    }

    if ((Number(files.item(0).size) / 1024 / 1024) > this._maxFileSize) {
      this._csv.error = this._errorMessages.invalidFileSize;
      this.updateObserver();
      return false;
    }

    return true;
  }


  /**
   * Process csv file
   * @param {FileReader} fileReader
   * @param {Object} headersRules
   * @returns {boolean}
   */
  private parseFile(fileReader: FileReader, headersRules: object): boolean {
    this._csv.plainData = fileReader.result.split('\n');

    if (!this.isValiSeparators() || !this.isValidHeaders(headersRules) || this.hasDuplicatedHeaders() || !this.extractData(headersRules)) {
      this.updateObserver();
      return false;
    }

    if (!_.isEmpty(this._verifyFields)) {
      this.verifyFields();
    } else {
      this.updateObserver();
    }

    return false;
  }


  // VERIFY METHODS *******************************************************************/
  /**
   * Return if exist verifiable to process
   * @param actualKey
   * @returns {boolean}
   */
  private thereArePendingVerifies (actualKey): boolean {
    return !_.isEmpty(this._verifyFields)
      && Object.keys(this._verifyFields).indexOf(actualKey) < (Object.keys(this._verifyFields).length - 1);
  }

  /**
   * Launch the verification fields process, if is required
   */
  private verifyFields(): void {

    _.forEach(this._verifyFields, (field, key) => {
      if (field.hasOwnProperty('endpoint') && !_.isEmpty(field.endpoint)) {
        this.validatorFromApi(field, key);
      }
    });
  }


  /**
   * General method to verify call in API
   * @param {Object} field
   * @param {string} header
   */
  private validatorFromApi (field: object, header: string): void {

    if (!this._csv.data[field['processed']].isValid()) {
      this.nextValidationFromApi(field, header);

    } else {
      this._loaderSrv.softVerifying();
      this._httpSrv.get(
        field['endpoint'],
        {value: this._csv.data[field['processed']].values[header]},
        null,
        null,
        false
      ).subscribe(
        (response) => {
          this.onValidatorFromApi (
            response.hasOwnProperty('object') && response['object'].hasOwnProperty('valid')
              ? response['object']['valid']
              : false,
            field,
            header
          );

        },
        () => {
          this._errorMessages.verifyErrorApi.replace(':col', header);
          this.updateObserver();
        }
      );
    }
  }


  /**
   * Callback for validatorFromApi
   * @param {boolean} isValid
   * @param {Object} field
   * @param {string} header
   */
  private onValidatorFromApi (isValid: boolean, field: object, header: string): void {
    if (!isValid) {
      const idx = field['processed'];

      if (this._strict) {
        this._csv.error = this._errorMessages.strictVerifyFail
          .replace(':col', header)
          .replace(':value', this._csv.data[idx].values[header])
          .replace(':row', this._csv.data[idx].row.toString());
        this.updateObserver();
        return;

      } else {
        this._csv.data[idx].warnings.push(this._warningMessages.failVerify.replace(':value', header));
      }
    }

    this.nextValidationFromApi(field, header);
  }


  private nextValidationFromApi(field: object, header: string): void {
    if (++field['processed'] < this._csv.data.length) {
      this.validatorFromApi(field, header);

    } else if (!this.thereArePendingVerifies(header)) {
      this.isEmptyProcessedFile();
      this.updateObserver();
    }
  }


  private updateObserver(): void {
    this._loaderSrv.stopSoft();
    this._subject.next(<CSVModel>this._csv);
  }


  private printWarnings(limit: number = 3): string {
    const warnings = [];
    let count = 0;

    for (const line of this._csv.data) {
      if (!_.isEmpty(line.warnings)) {
        warnings.push(
          '<li>'
          + this._warningMessages.base.replace(':row', line.row.toString())
          + line.warnings.join(', ')
          + '</li>');
      }

      if (warnings.length === limit || ++count === this._csv.data.length) {
        const totalWarnings = this._csv.getTotalWarnings();

        return '<ul class="pb-0 mb-0 pl-4">'
          + warnings.join('')
          + (warnings.length < totalWarnings
              ? '<li>+' + (totalWarnings - limit) + ' fila(s) más...</li>'
              : ''
            )
          + '</ul>';
      }
    }

    return '';
  }


  /******************** PUBLIC METHODS *******************************************/


  public resetCsv(): void {
    this._strict = false;
    this._verifyFields = {};
    this._csv = new CSVModel();
  }


  /**
   *
   * @param event
   * @param {Object} headersRules
   * @param {boolean} strict
   */
  processFile(event, headersRules: object, strict: boolean = false): void {

    if (this.isValidFile(event.target.files)) {
      this._loaderSrv.softLoading();
      this._strict = strict;
      const fileReader = new FileReader();

      fileReader.readAsText(event.target.files.item(0));

      fileReader.onload = function() {
        parseCsvService.parseFile(fileReader, headersRules);
      };
    }
  }


  /**
   * Generate a common import message
   * @param {CSVModel} csv
   * @returns {string}
   */
  public standardImportMessage(csv: CSVModel): string {
    return 'Va a importar ' + csv.getTotalValidRows() + ' registro(s)'
      + this.standardWarningMessage(csv)
      + '<br>¿Desea continuar?.';
  }

  /**
   * Generate a common warning message
   * @param {CSVModel} csv
   * @returns {string}
   */
  public standardWarningMessage(csv: CSVModel): string {
    const totalInvalid = csv.getTotalInvalidRows();

    return totalInvalid > 0
      ? '<br><span class="text-warning">Excluidos: ' + totalInvalid + ', detalle:' + this.printWarnings() + '</span>'
      : '';
  }

}
