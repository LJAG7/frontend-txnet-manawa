import {Injectable} from '@angular/core';
import {Http, Response, Headers, URLSearchParams, RequestOptions} from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';

import {PersonModel} from '../models/person.model';

@Injectable()
export class PersonService {

  // private commonUrl: string | any = self != top
  //   ? parent.location.origin + '/api/users/'
  //   : 'http://www.tsf.com/api/users/';

  constructor(private http: Http) {
    this.http = http;
  }

  // public getAll(): object | any {
  //   return this.http.get(this.commonUrl, this.jwt())
  //     .map(this.mapResponse)
  //     .catch(this.catchError);
  // }
  //
  // public getById(id: number = -1): object | any {
  //   return this.http.get(this.commonUrl.concat(id), this.jwt())
  //     .map(this.mapResponse)
  //     .catch(this.catchError);
  // }
  //
  // public getAllFilterBy(page: number = 1,
  //                       elements: number = 10): object | any {
  //   return this.http.get(this.commonUrl,
  //     Object.assign(this.jwt(), {search: {page: page, elements: elements}}))
  //     .map(this.mapResponse)
  //     .catch(this.catchError);
  // }
  //
  // public create(user: User): object | any {
  //   return this.http
  //     .post(this.commonUrl,
  //       Object.assign(this.jwt(), new RequestOptions({body: user}))
  //     ).map(this.mapResponse)
  //     .catch(this.catchError);
  // }
  //
  // public update(user: User): object | any {
  //   return this.http
  //     .put(this.commonUrl.concat(user['id']),
  //       Object.assign(this.jwt(), new RequestOptions({body: user}))
  //     ).map(this.mapResponse)
  //     .catch(this.catchError);
  // }
  //
  // public delete(id: number = -1): object | any {
  //   return this.http
  //     .delete(this.commonUrl.concat(id),
  //       this.jwt()
  //     ).map(this.mapResponse)
  //     .catch(this.catchError);
  // }
  //
  // private jwt(): RequestOptions {
  //   // create authorization header with jwt token
  //   let currentUser = JSON.parse(localStorage.getItem('currentUser'));
  //   if (currentUser && currentUser.token) {
  //     let headers = new Headers({'Authorization': 'Bearer ' + currentUser.token});
  //     return new RequestOptions({headers: headers});
  //   }
  // }
  //
  // private mapResponse(response: Response | any): Observable<Response> {
  //   return response.json() || {};
  // }
  //
  // private catchError(error: Response | any): Observable<Response> {
  //   return Observable.throw(error);
  // }

}
