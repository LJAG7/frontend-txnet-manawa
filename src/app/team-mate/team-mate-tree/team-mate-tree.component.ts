import {
  Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, AfterContentInit
} from '@angular/core';

const _ = require('lodash');

@Component({
  selector: 'app-team-mate-tree-component',
  templateUrl: './team-mate-tree.component.html',
  styleUrls: ['./team-mate-tree.component.css']
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TeamMateTreeComponent implements OnInit, OnChanges, AfterContentInit {

  /************************ COMPONENT INPUTS  ************************/

  @Input() public externalConfig: object;
  @Input() public data: Array<any> = [];
  @Input() public visible = true;
  @Input() public parent = true;
  @Input() public css = '';

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() public outputEvent = new EventEmitter();
  @Output() public onReady = new EventEmitter();
  @Output() public onActionSelectedEvent = new EventEmitter();
  @Output() public onActionChildSelectedEvent = new EventEmitter();

  /************************ COMPONENT VARS  ************************/

  public dataFiltered = [];
  private _originalData = [];

  /************************ CONFIG VARS  ************************/

  public config = {
    id: 'tree_mates_component_n'.concat(Math.floor(Math.random() * 100000).toString()),
  };

  /************************ NATIVE METHODS  ************************/

  constructor() {
  }

  ngOnInit() {
    // @TODO: falta agregar filtrado
    _.merge(this.config, this.externalConfig);
    this._originalData = [...this.data];
    this.dataFiltered = [...this.data];
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!_.isNull(changes['data']) && !_.isUndefined(changes['data'])) {
      this.data = changes['data']['currentValue'];
      this._originalData = changes['data']['currentValue'];
      this.dataFiltered = changes['data']['currentValue'];
    }
    _.merge(this.config, this.externalConfig);
  }

  ngAfterContentInit(): void {
    this.emitOnReadyState();
  }

  /************************ EVENTS METHODS ************************/

  public getMateLocation(mate: object): string {
    return this.hasMateLocation(mate)
      ? mate['person_location']['name']
      : '-';
  }

  public getMateName(mate: object): string {
    return this.isValidObjectProperty(mate, 'person_name')
      ? this.isValidObjectProperty(mate, 'person_surname')
        ? mate['person_name'] + ' ' + mate['person_surname']
        : mate['person_name']
      : '-';
  }

  public getMateRoles(mate: object): string {
    return this.isValidObjectProperty(mate, 'roles')
      ? mate['roles'].map((rol) => {
          return rol.hasOwnProperty('alias') && !_.isNil(rol['alias']) && rol['alias'] !== ''
            ? rol['alias']
            : rol['name'];
        }).join(', ')
      : '-';
  }

  public hasMateLocation(mate: object): boolean {
    return this.isValidObjectProperty(mate, 'person_location')
      && this.isValidObjectProperty(mate['person_location'], 'name')
      && mate['person_location']['name'].length > 0;
  }

  public hasMatesTeam(team: object): boolean {
    return this.isValidObjectProperty(team, 'team_mates')
      && team['team_mates'].length > 0;
  }

  public hasTeamChildTeam(team: object): boolean {
    return this.isValidObjectProperty(team, 'child_mates')
      && team['child_mates'].length > 0;
  }

  public isEmptyData(): boolean {
    return !(this.dataFiltered && this.dataFiltered.length > 0);
  }

  public isExpandedChild(team: object): boolean {
    return team && team.hasOwnProperty('expanded')
      ? team['expanded']
      : true;
  }

  /**
   * Output event to parent when one element from list is clicked
   * @param {Object} event
   * @param {Object} mate
   */
  public onActionSelectedClicked(event: object, mate: object): void {
    if (this.parent === true) {
      this.onActionSelectedEvent.emit({
        idComponent: this.config.id,
        event: 'click',
        element: !_.isNil(mate) ? mate : event['element']
      });
    } else {
      this.onActionChildSelectedEvent.emit({
        idComponent: this.config.id,
        event: 'click',
        element: !_.isNil(mate) ? mate : event['element']
      });
    }
  }

  /**
   * Output event to parent when one element from list is clicked
   * @param {Object} event
   * @param {Object} team
   */
  public onClickToogleEvent(event: object, team: object): void {
    team['expanded'] =  team && team.hasOwnProperty('expanded')
      ? !team['expanded']
      : false;
  }

  /************************ EMIT METHODS ************************/

  private emitOnReadyState(): void {
    this.onReady.emit({
      id: this.config['id'],
      ready: true
    });
  }

  /************************ VALIDATION METHODS ************************/

  private isValidObjectProperty(item: object, property: string): boolean {
    return item
      && item.hasOwnProperty(property)
      && item[property];
  }
}
