'use strict';

import {Component, OnInit, OnChanges, SimpleChanges, ViewChild, DoCheck, AfterViewInit} from '@angular/core';
import {Router} from '@angular/router';

import {ValidationObjectPipe} from '../shared/pipes/validation.pipe';

import {NodeModel} from '../shared/models/tree-team-group/node.model';

import {AlertService} from '../shared/services/alert.service';
import {AuthSrvService} from '../shared/services/auth.service';
import {HttpSrvService} from '../shared/services/http.service';
import {LoaderService} from '../shared/services/loader.service';

const _ = require('lodash');

@Component({
  selector: 'app-team-mate-component',
  templateUrl: './team-mate.component.html',
  styleUrls: ['./team-mate.component.css'],
  providers: [HttpSrvService, ValidationObjectPipe]
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TeamMateComponent implements OnInit, OnChanges, DoCheck, AfterViewInit {

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('groupsTreeComponent') private _groupsTreeComponent;
  @ViewChild('teamsTreeComponent') private _teamsTreeComponent;
  @ViewChild('confirmDialogComponent') public confirmDialogComponent;

  /************************ COMPONENT VARS  ************************/

  public selectedNode: NodeModel;
  public restartTeamRoleFilter = 0;
  public softLoader = false;
  public mainFullPage = false;
  public statusHeaderMainInfo = false;
  public teamMatesFiltered: Array<any> = [];

  private _matesFiltered: Array<any> = [];
  private _restartSearchMateFilter = 0;
  private _idMateSelected = -1;
  private _idTeamMateRolSelected = -1;
  private _teamMateSelected: object = null;
  private _teamMates: Array<any> = [];

  private _originalMates: Array<any> = [];
  private _apiOffset = 100;

  private _oldChangeValues = {
    idMateSelected: -1,
    idTeamMateRolSelected: -1,
    teamMateSelected: null
  };

  /************************ CONFIG VARS ************************/

  private _neededFromApi: Array<string> = ['person'];

  private _roleFilterConfig = {
    endpoint: 'teams/roles/',
    text: 'name',
    key: 'id',
    label: 'Filtrar por rol:'
  };

  private _searchMateFilterConfig = {
    title: 'name',
    subtitle: 'surname',
    keyToFilter: 'name',
    alternativeKeysToFilter: ['surname'],
    separator: ' ',
    flags: {
      hasWhiteBackground: true,
      showResultList: true,
      showResultListUnderClick: true
    },
    error: {
      show: false
    },
    icon: {
      hasIconRight: true,
      hasSearchIcon: true,
      hideIconOnSearch: true,
      showUnselectIcon: true
    },
    input: {
      placeholder: 'Busca un mate'
    }
  };

  private _confirmDialogConfig = {
    header: 'Se eliminará el equipo seleccionado.',
    message: '¿Desea continuar?',
    accept: {
      action: 'deleteTeam',
      button: {
        text: 'Si'
      }
    }
  };

  /************************ NATIVE METHODS ************************/

  constructor(private _alertSrv: AlertService,
              private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _loaderSrv: LoaderService,
              private _router: Router,
              private _validationObjectPipe: ValidationObjectPipe) {
    this._authSrv.verifyViewPermission('team-mate');
  }

  ngOnChanges(changes: SimpleChanges) { }

  ngDoCheck() {
    if (this._oldChangeValues.teamMateSelected !== this._teamMateSelected
    || this._oldChangeValues.idMateSelected !== this._idMateSelected
    || this._oldChangeValues.idTeamMateRolSelected !== this._idTeamMateRolSelected) {

      this._oldChangeValues = {
        idMateSelected: this._idMateSelected,
        idTeamMateRolSelected: this._idTeamMateRolSelected,
        teamMateSelected: this._teamMateSelected
      };

      this.generateTeamMatesFiltered();
    }
  }

  ngOnInit() {
    if (this.isAllowed()) {
      this._neededFromApi.push('teamMates');
      this._loaderSrv.general(this._neededFromApi);
      this.getAllNeededInfoFromApi();
    }

  }

  ngAfterViewInit() { }

  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('team-mate', tag);
  }

  /************************ SETUP METHODS ************************/

  /**
   * Get search mate widgetConfig
   * @returns {Object}
   */
  public getSearchMateFilterConfig(): object {
    return this._searchMateFilterConfig;
  }

  /**
   * Get filter role widgetConfig
   * @returns {Object}
   */
  public getTeamRoleFilterConfig(): object {
    return this._roleFilterConfig;
  }

  /************************ EVENTS METHODS ************************/

  /**
   * Get date of unique team selected
   * @param {string} field
   * @returns {string}
   */
  public getDateOfUniqueSelected(field: string): string {
    if (this.hasUniqueTeamSelected()
      && this._validationObjectPipe.isValidValueObjectProperty(this.getTeamsMates()[0], field)) {
      const d = new Date(this.getTeamsMates()[0][field]);
      return d.toLocaleString();
    }

    return '-';
  }

  /**
   * Get group of unique team selected
   * @returns {string}
   */
  public getGroupOfUniqueSelected(): string {
    return this.hasUniqueTeamSelected()
    && this._validationObjectPipe.isValidValueObjectProperty(this.getTeamsMates()[0], 'groups')
    && this.getTeamsMates()[0]['groups'].length > 0
      ? this.getTeamsMates()[0]['groups'].map((group) => {
        if (group.hasOwnProperty('name') && group['name'].length > 0) {
          return group['name'];
        }
      }).join(', ')
      : '-';
  }

  /**
   * Get locations of unique team selected
   * @returns {string}
   */
  public getLocationsOfUniqueSelected(): string {
    const locations = [];

    if (this._validationObjectPipe.isValidValueObjectProperty(this.getTeamsMates()[0], 'team_mates')
      && this.getTeamsMates()[0]['team_mates'].length > 0) {
      this.getTeamsMates()[0]['team_mates'].forEach((mate) => {
        if (this._validationObjectPipe.isValidValueObjectProperty(mate, 'person_location')
            && this._validationObjectPipe.isValidValueObjectProperty(mate['person_location'], 'name')
            && mate['person_location']['name'].length > 0
            && locations.indexOf(mate['person_location']['name']) === -1) {
          locations.push(mate['person_location']['name']);
        }
      });
    }
    return locations.length > 0
      ? locations.join(', ')
      : '-';
  }

  /**
   * Get master of unique team selected
   * @returns {string}
   */
  public getMasterOfUniqueSelected(): string {
    return this.hasUniqueTeamSelected()
    && this._validationObjectPipe.isValidValueObjectProperty(this.getTeamsMates()[0], 'teamt')
    && this._validationObjectPipe.isValidValueObjectProperty(this.getTeamsMates()[0]['teamt'], 'name')
      ? this.getTeamsMates()[0]['teamt']['name']
      : '-';
  }

  /**
   * Get name of unique team selected
   * @returns {any}
   */
  public getNameOfUniqueSelected(): any {
    return this.hasUniqueTeamSelected()
    && this._validationObjectPipe.isValidValueObjectProperty(this.getTeamsMates()[0], 'name')
      ? this.getTeamsMates()[0]['name']
      : '-';
  }

  /**
   * Get search mate data
   * @returns {Array<Object>}
   */
  public getSearchMateData(): Array<object> {
    return this._matesFiltered;
  }

  /**
   * Get unique team selected
   * @returns {boolean}
   */
  public getUniqueTeamSelected(): object {
    return _.isNil(this._teamMateSelected) ? null : this._teamMateSelected;
  }

  /**
   * Check if has only one team selected
   * @returns {boolean}
   */
  public hasUniqueTeamSelected(): boolean {
    return this.getTeamsMates().length === 1;
  }

  /**
   * Restart search mate
   * @returns {number}
   */
  public restartSearchMate(): number {
    return this._restartSearchMateFilter;
  }

  /**
   * On blur search mate filter
   * @param {Object | any} event
   */
  public onBlurSearchMateFilter(event: object | any): void {
    if (event.hasOwnProperty('event')
      && (event['event'] === 'unselect'
        || event['event'] === 'blur' && !event['itHasACorrectValue'])) {
      this._idMateSelected = -1;
    }
  }

  /**
   * On change role filter
   * @param {Object} event
   */
  public onChangeTeamRoleFilter(event: object): void {
    if (this._validationObjectPipe.isValidValueObjectProperty(event, 'elements')
      && !_.isNil(event['elements'])) {

      this._idTeamMateRolSelected = Number(event['elements'][0]['id']);
    }
  }

  public onChangeTreeComponentSelectedNode(event: object) {
    localStorage.setItem('selectedNode', JSON.stringify(event));
    if (this._validationObjectPipe.isValidValueObjectProperty(event, 'node')
      && this._validationObjectPipe.isValidValueObjectProperty(event, 'tree')
      && !_.isNil(event['node'])) {
      this.getOneTeamMatesFromApi(event['node']);
    } else {
      this._teamMateSelected = event['node'];
    }
  }

  /**
   * Go to mate profile when clicked
   * @param {Object} event
   */
  public onClickMate(event: object): void {
    if (this._validationObjectPipe.isValidValueObjectProperty(event, 'event')
      && event['event'] === 'click'
      && this._validationObjectPipe.isValidValueObjectProperty(event, 'element')
      && this._validationObjectPipe.isValidValueObjectProperty(event['element'], 'person_id')
      && _.isNumber(event['element']['person_id'])) {
      localStorage.setItem('activeGeneralTab', '0');
      this._router.navigateByUrl('/mate-profile/' + event['element']['person_id']);
    }
  }

  /**
   * On click over search mate result list
   * @param {Object | any} event
   */
  public onClickSearchMateFilter(event: object | any): void {
    if (this._validationObjectPipe.isValidValueObjectProperty(event, 'element')
      && !_.isNil(event['element'])) {

      this._idMateSelected = Number(event['element']['id']);
    }
  }


  /**
   * On toggle full main
   */
  public onToggleFullMain(): void {
    this.mainFullPage = !this.mainFullPage;
  }

  /**
   * Toggle header main info
   */
  public toggleHeaderMainInfo(): void {
    this.statusHeaderMainInfo = !this.statusHeaderMainInfo;
  }

  /**
   * Get confirm dialog setup
   * @returns {Object}
   */
  public getConfirmDialogSetup(): object {
    return this._confirmDialogConfig;
  }

  /**
   * Redirect to edit team view
   */
  public goToEditTeam(): void {
    if (this.hasUniqueTeamSelected() && this.getTeamsMates()[0].hasOwnProperty('id')) {
      this._router.navigateByUrl('/team/edit/'.concat(this.getTeamsMates()[0]['id'].toString()));
    }
  }

  /**
   * Redirect to new team view
   */
  public goToNewTeam(): void {
    this._router.navigateByUrl('/team/edit');
  }

  /**
   * Check if team is deleteable
   * @returns {boolean}
   */
  public isDeleteableTeam(): boolean {
    if (this.hasUniqueTeamSelected()) {
      return this.getTeamsMates()[0]['is_deleteable'];
    }
    return false;
  }

  /**
   * Delete team
   * @returns {boolean}
   */
  public onClickDeleteTeam(): void {
    this.confirmDialogComponent.showDialog(event);
  }
  /**
   * Event for dialogConfirm response
   * @param {Object | any} event
   */
  public onConfirmDialogResponse(event: object | any): void {
    if (!_.isNull(event['callBack'])) {
      this[event['callBack']](event['params']);
    }
  }


  /************************ API METHODS ************************/

  /**
   * Get all needed info from api
   */
  private getAllNeededInfoFromApi(): void {
    this.getMates();
    this.getTeamsMatesFromApi();
  }

  /**
   * Load all mates
   */
  private getMates(): void {
    if (this._originalMates.length === 0 || this._idTeamMateRolSelected === -1) {
      this._httpSrv
        .get(
          'persons/',
          {
            page: 1,
            elements: this._apiOffset,
            name: '%'
          }
        )
        .subscribe(
          (responseOk) => this.onSuccessGetMates(responseOk)
        );

    } else {
      this._httpSrv
        .get(
          'persons/role/' + this._idTeamMateRolSelected
        )
        .subscribe(
          (responseOk) => this.onSuccessGetMates(responseOk)
        );
    }
  }

  /**
   * On success get mates
   * @param {Object | any} response
   * @param {number} teamId
   */
  private onSuccessGetMates(response: object | any, teamId: number = -1): void {
    this._matesFiltered = [];
    this.checkNeededFromApi('person');
    if (response['success'] && this._validationObjectPipe.isValidResponseItemList(response)) {
      if (teamId === -1) {
        this._originalMates = [...response['object']['items']];
      }
      this._matesFiltered = _.cloneDeep(this._originalMates);
    }
  }

  /**
   * Load all team mates for the current user
   */
  private getTeamsMatesFromApi(): void {
    this._httpSrv
      .get(
        'teams/mates/',
        {page: 1, elements: this._apiOffset}
      )
      .subscribe(
        (responseOk) => this.onSuccessGetTeamsMates(responseOk)
      );
  }

  /**
   * On success get team mates
   * @param {Object | any} response
   */
  private onSuccessGetTeamsMates(response: object | any): void {
    if (response['success'] && this._validationObjectPipe.isValidResponseItemList(response)) {
      this._teamMates = _.cloneDeep(response['object']['items']);
      this.checkNeededFromApi('teamMates');
      this.statusHeaderMainInfo = true;
      this.generateTeamMatesFiltered();
    }
  }

  /**
   * Load all team mates for the current user
   */
  private getOneTeamMatesFromApi(team: object): void {
    this._httpSrv
      .get(
        'teams/' + team['id'] + '/mates/'
      )
      .subscribe(
        (responseOk) => this.onSuccessGetOneTeamsMates(responseOk, team)
      );
  }

  /**
   * On success get ine team mate
   * @param {Object | any} response
   */
  private onSuccessGetOneTeamsMates(response: object | any, team: object): void {
    if (response['success'] && this._validationObjectPipe.isValidValueObjectProperty(response, 'object')) {
      this._teamMateSelected = response['object'];
      this._teamMateSelected['is_deleteable'] = team['is_deleteable'];
      this.statusHeaderMainInfo = true;
      this.generateTeamMatesFiltered();
      this.checkNeededFromApi('oneTeamMates');
    }
  }

  /**
   * On delete entity
   */
  private deleteTeam(): void {
    if (this.hasUniqueTeamSelected()
      && this.getTeamsMates()[0].hasOwnProperty('id')) {
      this._httpSrv
        .delete('teams/' + this.getTeamsMates()[0]['id'])
        .subscribe(
          (responseOk) => this.onSuccessDeleteTeam(responseOk)
        );
    }
  }

  /**
   * On success get ine team mate
   * @param {Object | any} response
   */
  private onSuccessDeleteTeam(response: object | any): void {
    if (response['success']) {
      localStorage.setItem('selectedNode', null);
      this._teamMateSelected = null;
      this._teamsTreeComponent.restartComponent();
    }
  }

  /************************ VALIDATION METHODS  ************************/

  /**
   * Check needed from api
   * @param {string} value
   */
  private checkNeededFromApi(value: string): void {
    if (this._neededFromApi.indexOf(value) !== -1) {
      this._neededFromApi.splice(this._neededFromApi.indexOf(value), 1);
      this._loaderSrv.setNeededFromApi(this._neededFromApi);
    }
  }

  /************************ AUXILIAR METHODS  ************************/

  /**
   * Filter team mate selected by mate and role selected (Recursive algorithm)
   * @param {Object | any} teamMatesList
   */
  private filterTeamMatesRecursive(teamMatesList: object | any): any {

    teamMatesList.forEach(teamMate => {
      if (teamMate.hasOwnProperty('team_mates') && teamMate['team_mates'].length > 0) {
        teamMate['team_mates'] = teamMate['team_mates'].filter((mate) => {

          if (!_.isNil(this._idMateSelected) && this._idMateSelected !== -1
            && mate.hasOwnProperty('person_id') && Number(mate['person_id']) !== this._idMateSelected) {
            return false;
          }

          if (!_.isNil(this._idTeamMateRolSelected) && this._idTeamMateRolSelected !== -1
            && mate.hasOwnProperty('roles') && _.isArray(mate['roles']) && !_.isEmpty(mate['roles'])) {
            return mate['roles'].findIndex((rol) => {
              return rol.hasOwnProperty('id') && Number(rol['id']) === this._idTeamMateRolSelected;
            }) !== -1;
          }

          return true;
        });
      }

      if (teamMate.hasOwnProperty('child_mates') && !_.isEmpty(teamMate['child_mates'])) {
        this.filterTeamMatesRecursive(teamMate['child_mates']);
      }
    });
  }

  /**
   * Update the teamMates array
   */
  private generateTeamMatesFiltered(): void {
    this.teamMatesFiltered = this.getTeamsMates();

    if (this._idMateSelected !== -1 || this._idTeamMateRolSelected !== -1) {
      this.filterTeamMatesRecursive(this.teamMatesFiltered);
    }
  }

  /**
   * Return a deep clone team mates list.
   * If a teamMate has been selected, only return this team (array length 1)
   * @returns {Array<Object>}
   */
  private getTeamsMates(): Array<object> {
    return _.cloneDeep(
      !_.isNil(this._teamMateSelected)
        ? [this._teamMateSelected]
        : this._teamMates
    );
  }

  /**
   * Restart role filter component
   */
  private restartTeamRoleFilterComponent(): void {
    this.restartTeamRoleFilter++;
    this._idTeamMateRolSelected = -1;
  }

  /**
   * Restart search mate component
   */
  private restartSearchByMateComponent(): void {
    this._restartSearchMateFilter++;
    this._idMateSelected = -1;
  }


  public functionalityNotImplemented(msg?: string): void {
    this._alertSrv.prompt(msg);
  }
}
