/*GENERAL ASSETS*/
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';

/*MODULE ASSETS*/
import {TeamMateComponent} from './team-mate.component';
import {TeamMateTreeComponent} from './team-mate-tree/team-mate-tree.component';

/*SERVICES*/
import {HttpSrvService} from '../shared/services/http.service';
import {ComponentsModule} from '../shared/components/components.module';
import {DragulaModule} from 'ng2-dragula';
import { TeamNotesChartComponent } from './team-notes-chart/team-notes-chart.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    DragulaModule,
    ComponentsModule
  ],
  exports: [
  ],
  declarations: [
    TeamMateComponent,
    TeamMateTreeComponent,
    TeamNotesChartComponent
  ],
  providers: [
    HttpSrvService
  ]
})

export class TeamMateModule {}
