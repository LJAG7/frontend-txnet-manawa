import {
  Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ViewChild
} from '@angular/core';

import {ValidationObjectPipe} from '../../shared/pipes/validation.pipe';

import {BaseComponentModel} from '../../shared/models/base-component/base-component.class';
import {NoteModel} from '../../shared/models/notes/note.model';
import {BarChartDataModel} from '../../shared/models/scope-analytics/charts/barchart/bar-chart-data.model';

import {AlertService} from '../../shared/services/alert.service';
import {AuthSrvService} from '../../shared/services/auth.service';
import {HttpSrvService} from '../../shared/services/http.service';
import {TeamModel} from '../../shared/models/team/team.model';

const _ = require('lodash');

@Component({
  selector: 'app-team-notes-chart-component',
  templateUrl: './team-notes-chart.component.html',
  styleUrls: ['./team-notes-chart.component.css']
})

/**
 * @Author Elias Romero Martin <elias.romero@steelmood.com>
 * @Author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TeamNotesChartComponent extends BaseComponentModel implements OnInit, OnChanges {

  /************************ COMPONENT INPUTS  ************************/

  @Input() team: TeamModel;
  @Input() visible = true;

  /************************ COMPONENT OUTPUTS  ************************/

  @Output() onChangeShowByTeams = new EventEmitter();
  @Output() onChangeShowDetails = new EventEmitter();

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('barChart') barChart;

  /************************ COMPONENT VARS  ************************/

  public barChartNotes: BarChartDataModel;
  public notes: Array<NoteModel> = [];

  private _statusOfNotes: Array<string> = [
    'NOT_STARTED',
    'ONGOING',
    'BLOCKED',
    'CANCELLED',
    'ENDED'
  ];

  /************************ CONFIG VARS  ************************/

  public barChartOptions = {
    responsive: true,
    maintainAspectRatio: true,
    scales: {
      yAxes: [{
        ticks: {
          min: 0
        },
        scaleLabel: {
          display: true
        }
      }],
      xAxes: [{
        scaleLabel: {
          display: false
        }
      }]
    }
  };

  /************************ NATIVE METHODS  ************************/

  constructor(private _alertSrv: AlertService,
              private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _validationObjectPipe: ValidationObjectPipe) {
    super();
  }

  ngOnInit() {
    this.config.id = 'team_notes_chart_component_n'.concat(Math.floor(Math.random() * 100000).toString());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ((!_.isNil(changes['team']) && !_.isNil(changes['team']['currentValue']))) {
      this.getTeamNotes();
    }
  }

  /**
   * Get the note list
   */
  private getTeamNotes(): void {
    if (this.team.id !== 0) {
      this._httpSrv
        .get(
          'teams/' + this.team.id + '/notes/',
        )
        .subscribe(
          (responseOk) => this.onSuccessGetNotesFromApi(responseOk)
        );
    }
  }

  /**
   * On success get data notes
   * @param {Object | any} response
   */
  private onSuccessGetNotesFromApi(response: object | any): void {
    this.notes = [];

    if (this._validationObjectPipe.isSuccessResponse(response)
      && !_.isEmpty(response['object'])) {
      response['object'].forEach( note => {
        this.notes.push(new NoteModel(note));
      });
      this.buildChartData();
    }
  }

  /**
   * Build chart data
   */
  private buildChartData(): void {
    this.barChartNotes = new BarChartDataModel();

    if (!_.isNil(this.notes) && !_.isEmpty(this.notes)) {

      this._statusOfNotes.forEach( status => {

        // Add label to collection
        switch (status) {
          case 'NOT_STARTED':
            this.barChartNotes.addLabel('No iniciadas');
            break;
          case 'ONGOING':
            this.barChartNotes.addLabel('Iniciadas');
            break;
          case 'BLOCKED':
            this.barChartNotes.addLabel('Bloqueadas');
            break;
          case 'CANCELLED':
            this.barChartNotes.addLabel('Canceladas');
            break;
          case 'ENDED':
            this.barChartNotes.addLabel('Finalizadas');
            break;
        }

      });

      this.barChartNotes.addDataset('Hitos');

      this._statusOfNotes.forEach( status => {

        // Total value
        let value = 0;

        // Add values
        this.notes.forEach(note => {
          if (note.status === status) {
            value++;
          }
        });

        this.barChartNotes.addValueToDataset('Hitos', value);
      });

      this.barChartNotes.datasets[0].backgroundColor = '#2196F3';
      this.barChartNotes.datasets[0].borderColor = '#2196F3';
      this.barChartNotes.datasets[0].hoverBackgroundColor = '#0D47A1';
    }
  }

  // // Build month data
  // private buildMonthData(data: ScopeAnalyticsDataModel, workingTime: boolean = false): ScopeAnalyticsDataModel {
  //
  //   const auxStatusPeriods = new ScopeAnalyticsDataModel();
  //
  //   // Set total
  //   auxStatusPeriods.total += data.total;
  //
  //   // Set total teams
  //   data.teams.forEach( originalTeam => {
  //     const hasTeam = auxStatusPeriods.teams.find( team => {
  //       return team.id === originalTeam.id;
  //     });
  //
  //     if (!_.isNil(hasTeam)) {
  //       hasTeam.value += originalTeam.value;
  //     } else {
  //       auxStatusPeriods.teams.push(originalTeam);
  //     }
  //   });
  //
  //   // Set month labels
  //   auxStatusPeriods.dates = this.extractMonths(data);
  //
  //   // Insert teams in all dates
  //   auxStatusPeriods.dates.forEach( date => {
  //     data.teams.forEach( originalTeam => {
  //       date.teams.push(new ScopeAnalyticsTeamModel(originalTeam.id, {name: originalTeam.name}));
  //     });
  //   });
  //
  //   // Find data by month
  //   auxStatusPeriods.dates.forEach( scopeAnalyticsDateObj => {
  //     const month = scopeAnalyticsDateObj.date;
  //
  //     data.dates.forEach( originalScopeAnalyticsDateObj => {
  //       const dateSplit = originalScopeAnalyticsDateObj.date.split('-');
  //       if (month === dateSplit[dateSplit.length - 2]) {
  //
  //         // Set values
  //         scopeAnalyticsDateObj.total += originalScopeAnalyticsDateObj.total;
  //         scopeAnalyticsDateObj.withoutteam += originalScopeAnalyticsDateObj.withoutteam;
  //         scopeAnalyticsDateObj.status.lease += originalScopeAnalyticsDateObj.status.lease;
  //         scopeAnalyticsDateObj.status.meeting += originalScopeAnalyticsDateObj.status.meeting;
  //         scopeAnalyticsDateObj.status.travel += originalScopeAnalyticsDateObj.status.travel;
  //         scopeAnalyticsDateObj.status.working += originalScopeAnalyticsDateObj.status.working;
  //         scopeAnalyticsDateObj.status.pause += originalScopeAnalyticsDateObj.status.pause;
  //
  //         // Set teams
  //         originalScopeAnalyticsDateObj.teams.forEach(originalTeam => {
  //           const hasTeam = scopeAnalyticsDateObj.teams.find(team => {
  //             return team.id === originalTeam.id;
  //           });
  //
  //           if (!_.isNil(hasTeam)) {
  //             hasTeam.value = (Number(hasTeam.value) + Number(originalTeam.value)).toString();
  //           }
  //         });
  //       }
  //     });
  //   });
  //
  //   // Add year to label
  //   data.dates.forEach( scopeAnalyticsDateObj => {
  //     const dateSplit = scopeAnalyticsDateObj.date.split('-');
  //
  //     const month = auxStatusPeriods.dates.find( date => {
  //       return date.date === dateSplit[dateSplit.length - 2];
  //     });
  //     if (!_.isNil(month)) {
  //       month.date = dateSplit[dateSplit.length - 3] + '-' + moment(month.date, 'MM').format('MMMM');
  //     }
  //   });
  //
  //   // Refresh working time
  //
  //   if (workingTime) {
  //     let auxWorkingTime = 0;
  //     data.dates.forEach( scopeAnalyticsDateObj => {
  //       auxWorkingTime += scopeAnalyticsDateObj.status.working;
  //     });
  //     this.workingTime = auxWorkingTime !== 0 ? (Number(auxWorkingTime) / 60).toFixed(2) : '0.00';
  //   }
  //
  //   return auxStatusPeriods;
  // }

  // /**
  //  * Extract months
  //  * @param {ScopeAnalyticsDataModel} data
  //  * @returns {Array<string>}
  //  */
  // private extractMonths(data: ScopeAnalyticsDataModel): Array<ScopeAnalyticsDateModel> {
  //   const months = [];
  //   data.dates.forEach( scopeAnalyticsDateObj => {
  //     const dateSplit = scopeAnalyticsDateObj.date.split('-');
  //     const auxScopeAnalyticsDateModel = new ScopeAnalyticsDateModel(dateSplit[dateSplit.length - 2]);
  //     const hasMonth = months.filter( month => {
  //       return month.date === dateSplit[dateSplit.length - 2];
  //     }).length;
  //
  //     if (hasMonth === 0) {
  //       months.push(auxScopeAnalyticsDateModel);
  //     }
  //   });
  //   return months;
  // }
}
