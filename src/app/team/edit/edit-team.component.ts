import {AfterViewInit, Component, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {setTimeout} from 'timers';

import {TranslatePipe, TranslateService} from 'ng2-translate/ng2-translate';
import {HttpSrvService} from '../../shared/services/http.service';
import {AlertService} from '../../shared/services/alert.service';
import {AuthSrvService} from '../../shared/services/auth.service';

import {ROLESYSTEMS} from '../../shared/const/rolesystems.const';
import {ConfirmDialogComponent} from '../../shared/components/confirm-dialog/confirm-dialog.component';

const _ = require('lodash');

@Component({
  selector: 'app-edit-team-component',
  templateUrl: './edit-team.component.html',
  styleUrls: ['./edit-team.component.css'],
  exportAs: 'editComponent',
  providers: [
    HttpSrvService,
    NgForm,
    TranslateService,
    TranslatePipe
  ],
})
export class EditTeamComponent implements OnInit, AfterViewInit {

  @ViewChildren('pseudonymNameInput') public pseudonymNameInput: QueryList<any>;
  @ViewChildren('pseudonymNameText') public pseudonymNameText: QueryList<any>;
  @ViewChild('searchPersonInput') public searchPersonInput;
  @ViewChild('teamTypeName') public teamTypeName;
  @ViewChild('teamTypeDescription') public teamTypeDescription;

  @ViewChild('searchGestor') private searchGestor;
  @ViewChild('searchTeamTypesTemplate') private searchTeamTypesTemplate;

  @ViewChild('teamGroupList') private teamGroupList;
  @ViewChild('confirmCreateGroupDialog') confirmCreateGroupDialog: ConfirmDialogComponent;

  // PUBLIC

  public deepTeamsChildRecursivity = [];
  public apiCalled = false;

  public team: any = {
    name: '',
    type_team: 2,
    num_components: 0,
    rule_team: [],
    team_rule_person: [],
    team_person: [],
    child_team: [],
    statistics: [],
    initialized: false,
    managed_by: ''
  };

  public teamTypes: Array<object> = [];
  public teamTypesFiltered: Array<object> = [];
  public rolsSuggested: Array<object> = [];

  public apiLoader = {
    teamTypeRulePersons: false,
    teamTypeRuleTeams: false,
    teamTypeRecommendedPersons: false
  };


  // public groups: Array<object | any> = [];
  public teamGroups: Array<any> = [];
  public selectedGroups: Array<any> = [];
  private _originalSelectedGroups: Array<any> = [];

  public gestors: Array<object | any> = [];

  public persons = [];
  public rule_person = [];
  public rule_teamt = [];
  public restartSearchPersons = 0;
  public restartSearchGestors = 0;

  // PRIVATE

  private _teamGroupFilterSelected: Array<object> = [{id: -1}];

  private _teamGroupFilterConfig = {
    endpoint: 'teamtgroups/',
    text: 'name',
    key: 'id',
    label: 'Filtrar por clase: ',
    flags: {
      isMultiFilter: true
    }
  };

  /** FIN ÚLTIMOS CAMBIOS **/


  private _originalTeam;

  private _rolBoxIndexActive = -1;
  private _firstLoadApi = false;

  private _recommendMultiple = 8;

  private _toggleSearchSuggestionArea = true;

  private _visibleCollapsibleIndex = -1;
  private _teamTypeSelected: object = {};


  constructor(private _httpSrv: HttpSrvService,
              private _translate: TranslateService,
              private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _alertSrv: AlertService,
              private _authSrv: AuthSrvService) {

    this._authSrv.verifyViewPermission('team/edit');

    if (this.isAllowed()) {
      this.getAllTeamsGroups();
      if (this.isTeamCreated()) {
        this.getTeamInformation(-1, true);

      } else {
        this.getTeamTypes();
      }
    }

    _translate.setDefaultLang('es');
    _translate.use('es');
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
  }


  isAllowed(tag: string = 'all'): boolean {
    return this._authSrv.hasComponentPermission('team/edit', tag);
  }

  /**
   * Return to admin view
   */
  public goBackTo(): void {
    this._router.navigateByUrl('team');
  }

  public onChangeGroupInput(event: any): void {
    if (this.selectedGroups.length > this._originalSelectedGroups.length) {
      const addGroups = [];
      this.selectedGroups.forEach( group => {
        addGroups.push(group['id']);
      });
      this.addTeamGroup(addGroups);
    } else if (this.selectedGroups.length < this._originalSelectedGroups.length) {
      if (_.isEmpty(this.selectedGroups)) {
        const deleteGroups = [];
        this._originalSelectedGroups.forEach( group => {
          deleteGroups.push(group['id']);
        });
        this.deleteTeamGroup(deleteGroups);
      } else {
        const deleteGroups = [];
        this._originalSelectedGroups.forEach(group => {
          const findIndex = this.selectedGroups.findIndex(selectedGroup => {
            return selectedGroup['name'] === group['name'];
          });
          if (findIndex === -1) {
            deleteGroups.push(group['id']);
          }
        });
        this.deleteTeamGroup(deleteGroups);
      }
    }
  }

  private deleteTeamGroup(groups: Array<any>): void {
    this._httpSrv
      .patch(
        'teams/' + this.team['id'] + '/relationships/remove/teamgroups/',
        {
          groups: groups
        }
      )
      .subscribe(
        success => this.onSuccessDeleteTeamGroup(success),
        fail => this.onFailResponse(fail)
      );
  }

  private onSuccessDeleteTeamGroup(response: object): void {
    if (response['success']) {
      this.team['groups'] = response['object']['groups'];
      this.updateLocalTeam(this.team);
    }
  }

  private addTeamGroup(groups: Array<any>): void {
    this._httpSrv
      .patch(
        'teams/' + this.team['id'] + '/relationships/add/teamgroups/',
        {
          groups: groups
        }
      )
      .subscribe(
        success => this.onSuccessAddTeamGroup(success),
        fail => this.onFailResponse(fail)
      );
  }

  private onSuccessAddTeamGroup(response: object): void {
    if (response['success']) {
      this.team['groups'] = response['object']['groups'];
      this.updateLocalTeam(this.team);
    }
  }

  public isTeamCreated(): boolean {
    return this._activatedRoute.params['value'].hasOwnProperty('id')
      && Number(this._activatedRoute.params['value']['id']) > 0;
  }

  /**
   * Get filter group widgetConfig
   * @returns {Object}
   */
  public getTeamGroupFilterConfig(): object {
    return this._teamGroupFilterConfig;
  }

  /**
   * On change group filter
   * @param {Object} event
   */
  public onChangeTeamGroupFilter(event: object): void {
    if (event && event.hasOwnProperty('elements')
      && !_.isNil(event['elements'])
      && !_.isEmpty(event['elements'])) {

      this.restartSearchTTTemplatesComponent();
      this._teamGroupFilterSelected = event['elements'];
      this.filterTeamTypesTemplates();
      this.teamTypesFiltered = this.getAllSearchableTeamTypes();
    }
  }

  private updateOriginalTeam(): void {
    if (_.isUndefined(this._originalTeam) || this._originalTeam['id'] === this.team['id']) {
      this._originalTeam = {...this.team};
    }
  }

  private updateLocalTeam(team: any,
                          simpleChange: string = null): any {
    if (_.isNull(simpleChange)) {
      this.team = {...team};
      if (this.team['name'] === 'Nuevo equipo') {
        this.team['name'] = '';
      }
    } else {
      this.team[simpleChange] = team[simpleChange];
      if (this.team['name'] === 'Nuevo equipo') {
        this.team['name'] = '';
      }
    }

    if (!_.isNil(this.team['groups'])
      && !_.isEmpty(this.team['groups'])) {
      const selectedGroupIndex = [];

      this.team['groups'].forEach( group => {
        const findIndex = this.teamGroups.findIndex( element => {
          return element['label'] === group['name'];
        });
        if (findIndex !== -1) {
          selectedGroupIndex.push(findIndex);
        }
      });

      this.selectedGroups = [];
      const valuesAsString = [];
      selectedGroupIndex.forEach(index => {
        // const element = {label: this.teamGroups[index]['name'], value: this.teamGroups[index]};
        this.selectedGroups.push(this.teamGroups[index].value);
        valuesAsString.push(this.teamGroups[index].label);
      });
      this.teamGroupList.valuesAsString = valuesAsString.join();
      if (this.selectedGroups.length === this.teamGroups.length) {
        this.teamGroupList.showToggleAll = true;
      }
    }
    this._originalSelectedGroups = _.cloneDeep(this.selectedGroups);

    this.updateOriginalTeam();
  }

  /**
   * Call to API: all teamTypes (GET)
   */
  private getTeamTypes(): void {
    this._httpSrv
      .get(
      'teamts/complete/',
      {
          page: 1,
          elements: 100
        }
      )
      .subscribe(
      success => this.onSuccessTeamTypes(success),
      fail => this.onFailResponse(fail)
    );
  }

  /**
   * Call to API: all teamTypes (GET)
   */
  private getRulesTeamTeam(): void {
    this._httpSrv
      .get('teams/' + this.team['id'] + '/ruleteamtteamts/')
      .subscribe(
      success => this.onSuccessGetRuleTeamTeam(success),
      fail => this.onFailResponse(fail, 'teamTypeRuleTeams')
    );
  }

  private onSuccessGetRuleTeamTeam(response: object): void {
    this.apiLoader.teamTypeRuleTeams = false;
    if (response['success']) {
      this.team['team_rule_teamt'] = response['object']['team_rule_teamt'];
      this.updateLocalTeam(this.team);

    } else {
      this.onFailResponse(response);
    }
  }


  /**
   * Call to API: all teamTypes (GET) after add new child team
   */
  private updateRulesTeamAfterNewTeamChild(childId: number = -1): void {
    this._httpSrv
      .get('teams/' + this.team['id'] + '/ruleteamtteamts/')
      .subscribe(
      success => this.onSuccessUpdateRulesTeamAfterNewTeamChild(success, childId),
      fail => this.onFailResponse(fail, 'teamTypeRuleTeams')
    );
  }

  private onSuccessUpdateRulesTeamAfterNewTeamChild(response: object,
                                                    childId: number = -1): void {
    this.apiLoader.teamTypeRuleTeams = false;
    if (response['success']) {
      this.team['team_rule_teamt'] = response['object']['team_rule_teamt'];
      this.updateLocalTeam(this.team);
      this.addNewChildTeam(childId);

    } else {
      this.onFailResponse(response);
    }
  }

  /**
   * Get all recommended roles for rule mate role
   */
  private getRecommendedRoles(index: number = -1): void {
    const nElements = this._recommendMultiple * (this.hasValidMinMaxValues(this.team['team_rule_person'][index])
      ? Number(this.team['team_rule_person'][index]['max'])
      : 1);

    if (this.someRolBoxIsActive()) {
      this.apiLoader.teamTypeRecommendedPersons = true;

      this._httpSrv.get(
        'teams/'
        + ( this.team['id'] || this._activatedRoute['params']['value']['id'] )
        + '/rulepersons/'
        + this.getRolBoxActive()['rule_person']['id']
        + '/recommended/',
        {
          page: 1,
          elements: nElements
        }
      )
      .subscribe(
        success => this.onSuccessGotRecommendedRoles(success, index),
        fail => this.onFailResponse(fail)
      );
    }
  }

  private onSuccessGotRecommendedRoles(response: object,
                                       index: number = -1): void {
    this.apiLoader.teamTypeRecommendedPersons = false;

    if (response['success']) {
      this.rolsSuggested = response['object']['items'];
      this.team['team_rule_person'][index]['recommended'] = response['object']['items'];
      // @TODO: Continuar con la correcta implementación del LOG
      // this.createLog(response);
    } else {
      this.rolsSuggested = [];
    }
  }

  /**
   * Get all mate for search
   */
  private getAllPersons(): void {
    this._httpSrv.get(
      'domains/persons/',
      {
          page: 1,
          elements: 2000
        }
      )
      .subscribe(
      success => this.onSuccessGetAllPersons(success),
      fail => this.onFailResponse(fail)
    );
  }

  private onSuccessGetAllPersons(response: object): void {
    if (response['success']) {
      this.persons = response['object']['items'];
    } else {
      // BAD
    }
  }

  /**
   * Call to API: team info (GET)
   */
  private getTeamInformation(id: number = -1,
                             iniLoad: boolean = false,
                             showLoader: boolean = true): void {
    this.apiLoader.teamTypeRulePersons = showLoader;
    this.apiLoader.teamTypeRuleTeams = showLoader;
    this._httpSrv
      .get('teams/' + (id !== -1 ? id : this._activatedRoute['params']['value']['id']))
      .subscribe(
      success => this.onSuccessGetTeamInfo(success, iniLoad),
      fail => this.onFailResponse(fail, 'teamTypeRulePersons')
    );
  }

  /**
   * Get the response on get team types from api
   * @param response
   * @param {boolean} iniLoad
   */
  private onSuccessGetTeamInfo(response: any, iniLoad: boolean = false): void {
    if (!_.isNil(response.object)
      && response['success']) {
      this.apiLoader.teamTypeRulePersons = false;
      this._firstLoadApi = true;
      this.apiCalled = false;

      let idActiveRolBox = -1;
      if (this.someRolBoxIsActive()) {
        idActiveRolBox = this.getRolBoxActive()['id'];
      }

      for (let i = 0; i < this.team['team_rule_person'].length; i++) {
        if (this.team['team_rule_person'][i].hasOwnProperty('id')
          && idActiveRolBox !== -1
          && this.team['team_rule_person'][i]['id'] === idActiveRolBox) {
          this._rolBoxIndexActive = i;
          break;
        }
      }

      this.updateLocalTeam(response['object']);

      if (this.hasTeamTeamType()) {
        this.getRulesTeamTeam();
      }

      if (this.isChildTeamEditionSelected(this.team['id'])) {
        this.addTeamToRecursiveDeepArray(this.team);
      }

      if (iniLoad) {
        this.getAllGestor();

        if (this.hasTeamTeamType()) {
          this.getAllPersons();
        } else {
          this.getTeamTypes();
        }
      }

      if (this._rolBoxIndexActive !== -1) {
        this.onClickRolBox(this._rolBoxIndexActive);
      }
    }
  }

  public changeTeamEntity(simpleChange: string = null): void {

    if (simpleChange === 'description') {
      this.team['description'] = this.teamTypeDescription.nativeElement.value;
    }
    if (simpleChange === 'name') {
      this.team['name'] = this.teamTypeName.nativeElement.value;
    }

    const aux = _.cloneDeep(this.team);

    this._httpSrv
      .put(
      'teams/' + this.team['id'] || this._activatedRoute['params']['value']['id'],
        aux
      )
      .subscribe(
      success => this.onSuccessChangeTeamEntity(success, simpleChange),
      fail => this.onFailResponse(fail)
    );
  }

  private onSuccessChangeTeamEntity(response: any,
                                    simpleChange: string = null): void {

    if (!_.isNil(response.object)
      && response['success']) {
      this.updateLocalTeam(response['object'], simpleChange);
      // this.getAllPersons();

      if (this.someRolBoxIsActive()) {
        this.deactivateAllAssignMaskFlags();
      }

      this.apiCalled = false;
      this.deactivateAllRolBox();
      this.resetSearchSuggestionAreaStatus();

    } else {
      this.onFailResponse(response);
    }
  }

  public getGestorName(): string {
    return this.team
    && this.team.hasOwnProperty('managed_by')
    && !_.isNil(this.team['managed_by'])
    && this.team['managed_by'].hasOwnProperty('name')
    && !_.isNil(this.team['managed_by']['name'])
      ? this.team['managed_by']['name']
      : 'Sin gestor asignado';
  }

  public setSearchGestorConfig(): object {
    return {
      flags: {
        showResultList: true,
        showResultListUnderClick: true,
        forcedUnselectIconShow: true
      },
      error: {
        show: false
      },
      input: {
        placeholder: 'Busca un gestor',
        value: this.getSearchGestorDefaultValue()
      },
      icon: {
        hasIconRight: true,
        hasSearchIcon: true,
        hideIconOnSearch: true,
        showUnselectIcon: true
      }
    };
  }

  public selectedNewGestor(event: object | any): void {
    if (event
      && event.hasOwnProperty('element')
      && event['element'].hasOwnProperty('id')
      && event['element']['id'] > -1) {

      this._httpSrv
        .patch(
        'teams/' + this.team['id'] + '/relationships/managedBy/',
        {
            person_id: event['element']['id'],
            action: 'add'
          }
        )
        .subscribe(
        success => this.onSuccessChangedGestor(success),
        fail => this.onFailResponse(fail)
      );
    } else if (event
      && event.hasOwnProperty('event')
      && event['event'] === 'blur'
      && event.hasOwnProperty('itHasACorrectValue')
      && event['itHasACorrectValue'] === false
      && this.team.hasOwnProperty('managed_by')
      && !_.isNil(this.team['managed_by'])) {
      this._httpSrv
        .patch(
        'teams/' + this.team['id'] + '/relationships/managedBy/',
        {
            person_id: this.team['managed_by']['id'],
            action: 'delete'
          }
        )
        .subscribe(
        success => this.onSuccessChangedGestor(success),
        fail => this.onFailResponse(fail)
      );
    } else if (event
      && event.hasOwnProperty('event')
      && event['event'] === 'unselect') {
      this._httpSrv
        .patch(
          'teams/' + this.team['id'] + '/relationships/managedBy/',
          {
            person_id: this.team['managed_by']['id'],
            action: 'delete'
          }
        )
        .subscribe(
          success => this.onSuccessChangedGestor(success),
          fail => this.onFailResponse(fail)
        );
    }
  }

  private onSuccessChangedGestor(response: object): void {
    if (response['success']) {
      localStorage.setItem('GeneralDomainWidgetContainerComponent', 'gestor');
      this.updateLocalTeam(response['object'], 'managed_by');
      if (_.isNil(response['object']['managed_by'])) {
        this.restartSearchGestorsInput();
      }
    } else {
      this.onFailResponse(response);
    }
  }

  /**
   * Filters the teamType list to show by gestor-scope option
   * @param {number} scope
   * @param {Array<Object>} options
   */
  private filterTeamTypesList(scope: number = -1, options: Array<Object>): void {
    this.teamTypesFiltered = _.filter(options, (teamType) => {
      return teamType['scope'] !== 0 &&
        (scope === -1 || teamType['scope'] === scope);
    });
  }

  /**
   * Get the response on get team types from api
   * @param response
   */
  private onSuccessTeamTypes(response: any): void {
    if (!_.isNil(response.object)
      && response['success']
      && response.object.items.length > 0) {
      this.teamTypes = response['object']['items'];
      this.filterTeamTypesList(-1, this.teamTypes);
    }
  }


  private hasValidMinMaxValues(rule: object): boolean {
    return rule.hasOwnProperty('min')
      && rule.hasOwnProperty('max')
      && !_.isNull(rule['max'])
      && !_.isNull(rule['min'])
      && Number(rule['min']) > -2
      && Number(rule['max']) < 9999;
  }

  /**
   * Return the team type of the team applied
   * @return {string}
   */
  public getTeamTeamType(): string {
    return this.hasTeamTeamType()
      ? 'Master: ' + this.team['teamt']['name']
      : this.getTagTranslated('no_type_team');
  }


  private getAllRulePersonRolesConcatenated(teamRule: object): string {
    if (!_.isObject(teamRule)) {
      return 'sin categoria';
    }

    let roles = '';
    if (!teamRule.hasOwnProperty('child_roles')
      || teamRule['child_roles'].length === 0
      && teamRule.hasOwnProperty('description')) {
      roles = '<i class="fa fa-user-secret"></i> ' + teamRule['description'];

    } else {
      for (const rol of teamRule['child_roles']) {
        if (rol.hasOwnProperty('name')) {
          roles += rol['name'] + ', ';
        }
      }
      roles = roles.slice(0, roles.length - 2);
    }

    return roles;
  }

  /**
   * Get the category name's team type rule
   * @param teamRule
   * @return {string}
   */
  public getCategoryName(teamRule: object): string {
    return !_.isNil(teamRule)
    && teamRule.hasOwnProperty('category_name')
    && teamRule['category_name'].length > 0
      ? teamRule['category_name']
      : this.getAllRulePersonRolesConcatenated(teamRule);
  }

  /**
   * Parse roles and show them on list
   * @param rule
   * @return {any}
   */
  public convertRulePersonRoleMaxMin(rule: object): string {
    return this.hasValidMinMaxValues(rule)
      ? Number(rule['min']) === Number(rule['max'])
        ? this.stringifyAndReplace(rule['min'], '-1', '∞')
        : this.stringifyAndReplace(rule['min'], '-1', '∞')
        + '-'
        + this.stringifyAndReplace(rule['max'], '-1', '∞')
      : _.isNull(rule['max']) || _.isNull(rule['min'])
         ? '# ?'
         : '';
  }

  /**
   * Set as string any value and replace word passed as param
   * @param value
   * @param search
   * @param replacement
   * @return {string}
   */
  private stringifyAndReplace(value: any,
                              search: string = '',
                              replacement: string = ''): string {
    return value
      .toString()
      .replace(search, replacement);
  }

  /**
   * Concatenate all the roles to show as title
   * @param rule
   * @return {string}
   */
  public getAllRolesConcatenated(rule: object): string {
    let roles = '';
    if (_.isObject(rule)
      && rule.hasOwnProperty('rule_person')
      && rule['rule_person'].hasOwnProperty('roles')) {

      if (rule['rule_person']['roles'].length === 0
        && rule['rule_person'].hasOwnProperty('description')) {
        roles = '<i class="fa fa-user-secret"></i> ' + rule['rule_person']['description'];

      } else {
        for (const rol of rule['rule_person']['roles']) {
          if (rol.hasOwnProperty('name')) {
            roles += rol['name'] + ', ';
          }
        }
        roles = roles.slice(0, roles.length - 2);
      }
    }
    return roles;
  }

  /**
   * Config for search mate component
   * @returns {Object}
   */
  public setConfigForPersonsSearch(): object {
    return {
      title: ['name', 'surname'],
      subtitle: 'email',
      keyToFilter: 'name',
      alternativeKeysToFilter: ['surname', 'email'],
      flags: {
        showCenterAttention: true,
        showResultList: true,
        showResultListUnderClick: true
      },
      error: {
        show: false,
        message: '{{wordToFilter}}'
      },
      input: {
        placeholder: 'busca una persona por su nombre, apellidos, email...'
      },
      icon: {
        hasIconRight: true,
        hasSearchIcon: true,
        hideIconOnSearch: true
      }
    };
  }

  /**
   * Call api to add new mate to role box active passing id mate as param
   * @param personId
   */
  private connectApiOnAddNewPerson(personId: number = -1): void {
    this._httpSrv
      .patch(
        'teams/'
        + ( this.team['id'] || this._activatedRoute['params']['value']['id'] )
        + '/rulepersons/'
        + this.getRolBoxActive()['rule_person']['id']
        + '/relationships/persons/',
        {
          persons: [
            {
              id: personId
            }
          ]
        }
      )
      .subscribe(
      success => this.onSuccessAddPersonToPersonRule(success),
      fail => this.onFailResponse(fail)
    );
  }

  /**
   * Control the callback function on when user clicks on one of the list's elements
   * @param event
   */
  public onSearchClick(event: object): void {
    if (event.hasOwnProperty('element')
      && event['element']['id']
      && this.rolBoxActiveHasNotThisPerson(event['element']['id'])) {
      this.searchPersonInput.restartComponent();
      this.apiCalled = true;
      this.connectApiOnAddNewPerson(event['element']['id']);
    } else {
      this.onFailResponse({
        success: false,
        message: this.getTagTranslated('person_add_error')
      });
    }
  }

  public getFullName(person: object | any): string {
    return person.hasOwnProperty('name')
    || person.hasOwnProperty('surname')
      ? person.hasOwnProperty('surname')
        ? person['name'] + ' ' + person['surname']
        : person['name']
      : person['email'];
  }

  /**
   * Callback method on add mate
   * @param response
   */
  private onSuccessAddPersonToPersonRule(response: object = {}) {
    this.resetSearchSuggestionAreaStatus();

    response['success']
      ? this.getMaskWithAssignFlagEnabled() !== undefined
        ? this.removeMaskWithAssignFlagEnabled()
        : this.getTeamInformation(this.team['id'])
      : this.onFailResponse({
      success: false,
      message: response['message']
        ? response['message']
        : this.getTagTranslated('person_add_generic_error')
    });
  }

  private removeMaskWithAssignFlagEnabled(): void {
    this._httpSrv
      .delete(
        'teams/'
        + ( this.team['id'] || this._activatedRoute['params']['value']['id'] )
        + '/rulepersons/'
        + this.getRolBoxActive()['rule_person']['id']
        + '/relationships/masks/'
        + this.removeMaskWithAssignFlagEnabled()['id']
      )
      .subscribe(
      success => this.onSuccessRemovedMaskFromRulePersonActive(success),
      fail => this.onFailResponse(fail)
    );
  }

  private onSuccessRemovedMaskFromRulePersonActive(response: object = {}): void {
    if (response['success']) {
      this.deactivateAllAssignMaskFlags();
      this.getTeamInformation();
    } else {
      this.onFailResponse({
        success: false,
        message: this.getTagTranslated('error_remove_mask')
      });
    }
  }

  /**
   * Detect if role box actives has the mate id passed as param
   * @param id
   * @return {boolean}
   */
  private rolBoxActiveHasNotThisPerson(id: number = -1): boolean {
    return typeof this.getPersonsFromTeamRulePerson(this.getRolBoxActive()['id']).find((person) => {
        return Number(person['id']) === Number(id);
      }) === 'undefined';
  }


  /** ADD HERE THE RESULT CLICKED **/

  /**
   * Control restart component from search component
   * @param event
   */
  public onRestartSearchComponent(event: object): void {
    if (event.hasOwnProperty('restartStatusOnComponent')) {
    }
  }

  /**
   * Control no results from search component
   * @param event
   */
  public addNewPseudonymRoPersonRule(event: object): void {
    if (event.hasOwnProperty('actionMsg') && event['actionMsg'] === this.setConfigForPersonsSearch()['error']['actionMsg']
      && event.hasOwnProperty('currentText') && event['currentText'] !== ''
    ) {

      this._httpSrv
        .post(
          'masks/',
          {name: event['currentText']}
        )
        .subscribe(
        success => this.onSuccessAddNewMask(success),
        fail => this.onFailResponse(fail)
      );
    } else {
      this.onFailResponse({
        success: false, message: this.getTagTranslated('pseudonym_invalid')
      });
    }
  }

  private onSuccessAddNewMask(response: object = {}): void {
    response['success']
      ? this.addPseudonymToPersonRule(response)
      : this.onFailResponse({
      success: false, message: response['message']
        ? response['message']
        : this.getTagTranslated('pseudonym_error_create')
    });
  }


  private addPseudonymToPersonRule(response: object = {}): void {
    if (response.hasOwnProperty('object') && response['object']
      && response['object'].hasOwnProperty('id') && response['object']['id'] > 0) {
      this._httpSrv
        .patch(
          'teams/'
          + ( this.team['id'] || this._activatedRoute['params']['value']['id'] )
          + '/rulepersons/'
          + this.getRolBoxActive()['rule_person']['id']
          + '/relationships/masks/',
          {
            masks: [
              {
                id: response['object']['id']
              }
            ]
          }
        )
        .subscribe(
        success => this.onSuccessAddMaskToPersonRule(success),
        fail => this.onFailResponse(fail)
      );

    } else {
      this.onFailResponse({
        success: false, message: this.getTagTranslated('pseudonym_error_create')
      });
    }
  }


  /**
   * Call back from modify pseudonym on rule active
   * @param response
   */
  private onSuccessAddMaskToPersonRule(response: object = {}): void {
    response['success']
      ? this.getTeamInformation()
      : this.onFailResponse({
      success: false, message: response['message']
        ? response['message']
        : this.getTagTranslated('pseudonym_error_create')
    });
  }

  /**
   * Check if the current team has team type different from 0
   * @return {boolean}
   */
  private hasTeamTeamType(): boolean {
    try {
      return this.team
        && this.team.hasOwnProperty('teamt')
        && this.team['teamt']
      && typeof this.team['teamt'] === 'object'
      && this.team['teamt'].hasOwnProperty('id')
        && this.team['teamt'].hasOwnProperty('id')
        && Number.isInteger(this.team['teamt']['id'])
        && Number(this.team['teamt']['id']) > 0;
    } catch (e) {}
  }

  /**
   * Catch error on API Communication
   * @param response
   */
  private onFailResponse(response: object = {}, apiLoaderIdx: string = null): void {
    if (!_.isNull(apiLoaderIdx)) {
      this.apiLoader[apiLoaderIdx] = false;
    }
  }


  public getStatusClassForRule(rule: object = {}): string {
    return rule.hasOwnProperty('status')
      ? {
        1: 'incomplete',
        2: 'valid',
        3: 'complete'
      }[rule['status']]
      : '';
  }

  /***************** search-component & tab-element-container ***************************/


  /**
   * Updates the attribue throw an increment.
   * The increment launchs the restart event for search-component
   * @param value
   */
  public restartSearchPersonInput(): void {
    this.restartSearchPersons++;
  }

  /**
   * Updates the attribue throw an increment.
   * The increment launchs the restart event for search-component
   * @param value
   */
  public restartSearchGestorsInput(): void {
    this.restartSearchGestors++;
  }

  /**
   * Returns the data
   */
  public serializeTeamTypesList(): Array<object> {
    return this.teamTypes;
  }

  public getSearchGestorDefaultValue(): string {
    if (this.team.hasOwnProperty('managed_by')
      && !_.isNil(this.team['managed_by'])
      && this.team['managed_by'].hasOwnProperty('name')
      && !_.isEmpty(this.team['managed_by']['name'])) {
      return this.team['managed_by']['name'];
    } else {
      return '';
    }
  }

  /**
   * Returns if no exists rulePerson for this edit teamType
   * @returns {any|boolean}
   */
  public isEmptyTeamTypeOfRulePersons(): boolean {
    return _.isNil(this.getTeamRulesPerson()) || this.getTeamRulesPerson().length === 0;
  }

  private deactivateAllRolBox(): void {
    this._rolBoxIndexActive = -1;
    this.rolsSuggested = [];
    this.team['team_rule_person'] = _.map(this.team['team_rule_person'], (rule) => {
      delete rule['active'];
      return rule;
    });
  }


  private getRolBoxActive(): object {
    return _.find(this.team['team_rule_person'], (rule) => {
      return rule['active'] === true;
    });
  }

  /**
   * Return the index inside of the array on team rule mate attribute
   */
  private getRolBoxActiveIndexOnArray(): number {
    return _.findIndex(this.team['team_rule_person'], (rule) => {
      return this.getRolBoxActive() === rule;
    });
  }

  /**
   * Generate order flex attribute according to desktop grid to show section where edit the rule
   * @return {number}
   */
  public generateOrderIndexForEditRolSection(): number {
    return Math.ceil((this.getRolBoxActiveIndexOnArray() + 1) / 4) * 4;
  }

  /**
   * Check if some rule mate is active
   * @return {boolean}
   */
  public someRolBoxIsActive(): boolean {
    return !_.isUndefined(this.getRolBoxActive());
  }

  private resetSearchSuggestionAreaStatus(): void {
    this._toggleSearchSuggestionArea = true;
    this.restartSearchPersonInput();
  }

  public isVisibleSearchSuggestionArea(): boolean {
    return this._toggleSearchSuggestionArea;
  }


  /**
   * Return a recommended mate list for a rule mate.
   * The persons added are filtered in this list
   * @returns {Array<Object>}
   */
  public generateSuggestionAccordingRolBox(): Array<object> {
    return this.rolsSuggested.length > 0
      ? this.rolsSuggested.slice(0, this._recommendMultiple).filter((person) => {
          return this.rolBoxActiveHasNotThisPerson(person['id']);
        })
      : [];
  }

  /**
   * Add recommended mate to mate rule selected
   * @param rol
   */
  public addRecommendedRolToRulePerson(rol: object = {}): void {
    this.apiCalled = true;
    this.someRolBoxIsActive();
    rol.hasOwnProperty('id')
    && rol['id'] > 0
    && this.rolBoxActiveHasNotThisPerson(rol['id'])
      ? this.connectApiOnAddNewPerson(rol['id'])
      : this.onFailResponse({
      success: false,
      message: 'No se ha podido añadir la persona recomendada ya que esta repetida o no existe.'
    });
  }

  /**
   * Translate according key passed as param
   * @param tag
   * @return {any}
   */
  public getTagTranslated(tag: string): string {
    return this._translate.instant('TEAM_EDIT')[tag];
  }

  /**
   * On click some box for show a rule mate
   * @param index
   */
  public onClickRolBox(index: number = -1): void {
    if (!_.isNull(this.team['team_rule_person'])
      && this.team['team_rule_person'].length > 0
      && !_.isUndefined(this.team['team_rule_person'][index])
      && this.team['team_rule_person'][index].hasOwnProperty('active')
      && this.team['team_rule_person'][index]['active']) {
      this.deactivateAllRolBox();
      this.resetSearchSuggestionAreaStatus();
    } else {
      if (this.someRolBoxIsActive()) {
        this.deactivateAllAssignMaskFlags();
      }
      this.deactivateAllRolBox();
      this.resetSearchSuggestionAreaStatus();
      if (index !== -1 && !_.isUndefined(this.team['team_rule_person'][index])) {
        this.team['team_rule_person'][index]['active'] = true;
      }

      if (!this.isRolBoxActiveComplete()) {
        this.valorateGetRecommendedRoles(index);
      }
    }
  }

  private valorateGetRecommendedRoles(index: number = -1): void {
    if (!this.team['team_rule_person'][index].hasOwnProperty('recommended')
        || !Array.isArray(this.team['team_rule_person'][index]['recommended'])) {
      this.getRecommendedRoles(index);

    } else {
      this.rolsSuggested = this.team['team_rule_person'][index]['recommended'];
    }
  }

  public getAliasFromRolBoxSelected(): string {
    return this.someRolBoxIsActive()
      ? this.getRolBoxActive()['rule_person']['category_name']
      : '';
  }

  public rolBoxIndexIsActive(index: number = -1) {
    return index === this.getRolBoxActiveIndexOnArray();
  }

  public hasRolBoxAlias(): boolean {
    return this.someRolBoxIsActive()
      && this.getRolBoxActive().hasOwnProperty('rule_person')
      && this.getRolBoxActive()['rule_person'].hasOwnProperty('category_name')
      && this.getRolBoxActive()['rule_person']['category_name'].length > 0;
  }

  public isEmptyRulePersonBoxSelected(): boolean {
    return this.someRolBoxIsActive()
      && this.hasTeamRulesPerson()
      && this.getRolBoxActive()['rule_person'].hasOwnProperty('roles')
      && this.getRolBoxActive()['rule_person']['roles'].length === 0;
  }

  public rolBoxActiveHasRoles(): boolean {
    return this.someRolBoxIsActive()
      && this.hasTeamRulesPerson()
      && this.getRolBoxActive()['rule_person'].hasOwnProperty('roles')
      && this.getRolBoxActive()['rule_person']['roles'].length > 0
      || this.isJokersFromTeamRulePerson(this.getRolBoxActive());
  }

  public isRolBoxActive(personRule: object): boolean {
    return this.rolBoxActiveHasRoles()
      && Number(this.getRolBoxActive()['id']) === Number(personRule['id']);
  }

  public hasPersonsOrPseudonyms(): boolean {
    if (this.someRolBoxIsActive()) {
      const ruleId = this.getRolBoxActive()['id'];

      return this.getPseudonymFromTeamRulePerson(ruleId).length > 0
        || this.getPersonsFromTeamRulePerson(ruleId).length > 0;
    }
    return false;
  }

  /**
   * returns the message for empty rule
   * @returns {string|string}
   */
  public emptyRulePersonBoxSelectedMessage(): string {
    return 'No hay roles definidos para esta regla';
  }

  public personIsAdmin(): boolean {
    return this._authSrv.getPersonLogged().isAdmin() || this._authSrv.getPersonLogged().isSuperAdmin();
  }


  /*************** methods to role item edit *********************************************/

  /**
   * Check if the team on wizard has mate rule active
   *
   * @returns {boolean}
   */
  private hasTeamRulesPerson(): boolean {
    return !_.isNil(this.team)
      && this.team.hasOwnProperty('team_rule_person')
      && this.team['team_rule_person'].length > 0;
  }

  /**
   * Get all the team rule persons associated to the team
   * @return {Array}
   */
  public getTeamRulesPerson(): Array<object> {
    return this.hasTeamRulesPerson()
      ? this.team['team_rule_person']
      : [];
  }

  /**
   * Returns the index of a rule mate from team's rules
   * @param id
   */
  private getIndexOfRuleInTeam(id: number): number {
    return _.findIndex(this.getTeamRulesPerson(), (rule) => {
      return rule['id'] === id;
    });
  }

  /**
   * Returns the personList from a rule of Team
   * @param ruleId
   * @returns {Array}
   */
  private getPersonsFromTeamRulePerson(ruleId: number): Array<object> {
    const indexRule = this.getIndexOfRuleInTeam(ruleId);

    return indexRule !== -1
    && this.getTeamRulesPerson()[indexRule]['rule_person'].hasOwnProperty('person')
      ? this.getTeamRulesPerson()[indexRule]['rule_person']['person']
      : [];
  }

  /**
   * Returns if is a comodin rule of Team
   * @param ruleId
   * @returns {Array}
   */
  private isJokersFromTeamRulePerson(rulePerson: object): boolean {
    return !_.isUndefined(rulePerson)
    && rulePerson.hasOwnProperty('rule_person')
    && rulePerson['rule_person'].hasOwnProperty('roles')
      && rulePerson['rule_person']['roles'].length === 0;
  }

  /**
   * Returns the PseudonymList from a rule of Team
   * @param ruleId
   * @returns {Array}
   */
  private getPseudonymFromTeamRulePerson(ruleId: number): Array<object> {
    const indexRule = this.getIndexOfRuleInTeam(ruleId);
    return indexRule !== -1
    && this.getTeamRulesPerson()[indexRule]['rule_person'].hasOwnProperty('mask')
      ? this.getTeamRulesPerson()[indexRule]['rule_person']['mask']
      : [];
  }


  /**
   * Returns the mate list from a selected rule in team
   * @param rulePerson
   * @returns {Array<Object>|Array}
   */
  public getRolesFromTeamByRuleSelected(): Array<object> {
    return this.someRolBoxIsActive()
    && this.hasTeamRulesPerson()
      ? this.getPersonsFromTeamRulePerson(this.getRolBoxActive()['id'])
      : [];
  }


  /**
   * Delete the mate in rule
   * @param person
   */
  public deleteRol(person: object): void {
    if (person.hasOwnProperty('id')) {
      this.apiCalled = true;
      this._httpSrv
        .delete(
            'teams/'
          + ( this.team['id'] || this._activatedRoute['params']['value']['id'] )
          + '/rulepersons/'
          + this.getRolBoxActive()['rule_person']['id']
          + '/relationships/persons/'
          + person['id'])
        .subscribe(
        success => this.onSuccessRolRemoved(success),
        fail => this.onFailResponse(fail)
      );
    } else {
      this.onFailResponse({
        success: false,
        message: 'La persona que intentas borrar no existe o no tiene id.'
      });
    }
  }

  /**
   * Callback function when you remove a mate from one rule
   * @param response
   */
  private onSuccessRolRemoved(response: object = {}): void {
    if (response['success']) {
      if (this.childTeamEditionViewIsEnabled()) {
        return this.getTeamInformation(this.deepTeamsChildRecursivity[this.deepTeamsChildRecursivity.length - 1]['id']);
      } else {
        return this.getTeamInformation();
      }
    } else {
      return this.onFailResponse(response);
    }
  }


  private getIndexForAliasOnArrayInRulePersonActiveRolBox(id: number = -1): number {
    return this.getPseudonymFromTeamRulePerson(this.getRolBoxActive()['id']).findIndex((mask) => {
      return mask['id'] === id;
    });
  }

  private deactivateAllAssignMaskFlags(): void {
    if (this.getPseudonymFromTeamRulePerson(this.getRolBoxActive()['id'])) {
      this.getPseudonymFromTeamRulePerson(this.getRolBoxActive()['id']).map((mask) => {
        delete mask['assignFlag'];
        return mask;
      });
    }
  }

  private getMaskWithAssignFlagEnabled(): object {
    return this.getPseudonymFromTeamRulePerson(this.getRolBoxActive()['id']).find((mask) => {
      return mask['assignFlag'] === true;
    });
  }

  /********************************** TEAMS ***************************************/

  private ruleHasTeamConfigured(rule: object): boolean {
    return this.ruleTeamTeamHasTeams(rule)
      && rule['teamts'][0].hasOwnProperty('teams')
      && rule['teamts'][0]['teams'].length > 0
      && rule['teamts'][0]['teams'][0].hasOwnProperty('id');
  }


  public onClickTeamBox(index: number = -1): void {
    const rule = this.getTeamRulesTeam()[index];
    this.deactivateAllRolBox();
    this.resetSearchSuggestionAreaStatus();

    if (this.ruleHasTeamConfigured(rule)) {
      this._firstLoadApi = false;
      this.addNewChildTeam(rule['teamts'][0]['teams'][0]['id']);
    } else {
      this.createNewComodinTeam(rule['teamts'][0]['id'], rule['id']);
    }
  }

  private addNewChildTeam(id: number = -1,
                          loadOtherData: boolean = false): void {
    this.getTeamInformation(
      id,
      loadOtherData,
      false
    );
  }

  public getDeepTeamName(index: number): string {
    return index === 0 ? this._originalTeam['name'] : this.deepTeamsChildRecursivity[index - 1]['name'];
  }

  private createNewComodinTeam(idRuleTypeTeam: number = -1,
                               idRuleTeamTeam: number = -1): void {
    if (idRuleTeamTeam) {
      this._httpSrv
        .post(
          'teams/',
        {
            name: 'Nuevo equipo',
            teamt_id: idRuleTypeTeam
          }
        )
        .subscribe(
        (success) => this.onSuccessCreateNewComodinTeam(success, idRuleTeamTeam),
        (fail) => this.onFailResponse(fail),
      );
    }
  }

  private onSuccessCreateNewComodinTeam(response: object | any,
                                        idRuleTeamTeam: number = -1): void {
    if (response.hasOwnProperty('success') && response.success && idRuleTeamTeam > 0) {
      this.addTeamToRuleTeamTeam(response['object'], idRuleTeamTeam);
    } else {
      this.onFailResponse(response);
    }
  }

  private addTeamToRuleTeamTeam(team: object,
                                idRuleTeamTeam: number = -1): void {
    if (idRuleTeamTeam) {
      this._httpSrv.patch(
        'teams/' + this.team['id'] + '/ruleteamtteamts/' + idRuleTeamTeam + '/relationships/teams/',
        {teams: [{id: team['id']}]}
        )
        .subscribe(
        (success) => this.onSuccessSetupNewTeamToRuleTeamTeam(team['id'], success),
        (fail) => this.onFailResponse(fail),
      );
    }
  }

  private onSuccessSetupNewTeamToRuleTeamTeam(childId: number = -1,
                                              response: object | any): void {
    if (response['success']) {
      this.team['child_teams'] = response['object']['child_teams'];
      this.updateRulesTeamAfterNewTeamChild(childId);

    } else {
      this.onFailResponse(response);
    }
  }

  private isChildTeamEditionSelected(id): boolean {
    return Number(id) !== Number(this._originalTeam['id'])
      && this.deepTeamsChildRecursivity.filter((team) => {
        return Number(team['id'] === Number(id));
      }).length === 0;
  }

  public childTeamEditionViewIsEnabled(): boolean {
    return this.deepTeamsChildRecursivity.length > 0;
  }

  private addTeamToRecursiveDeepArray(item: object): void {
    this.deepTeamsChildRecursivity.push(item);
  }

  public editRecursiveTeam(index: number = -1) {
    this.deepTeamsChildRecursivity = this.deepTeamsChildRecursivity.slice(0, index + 1);
    this.popLastElementOnDeepArray();
  }


  /**
   * Update the ancestor with actual team changes (child)
   */
  public updateAncestorOnDeepArray(): void {
    const ancestor = this.deepTeamsChildRecursivity.length > 1
      ? this.deepTeamsChildRecursivity[this.deepTeamsChildRecursivity.length - 1]
      : this._originalTeam;

    if (ancestor.hasOwnProperty('child_teams')
      && ancestor['child_teams']
      && ancestor['child_teams'].length > 1) {

      const childIdx = ancestor['child_teams'].findIndex(
        (item) => {
          return Number(item['id']) === Number(this.team['id']);
        }
      );

      ancestor['child_teams'][childIdx] = {...this.team};
    }
  }


  public backToOnDeepArray(): void {
    this.updateAncestorOnDeepArray();
    this.popLastElementOnDeepArray();
  }

  private popLastElementOnDeepArray(): void {
    this.deepTeamsChildRecursivity.pop();
    this.deactivateAllRolBox();
    this.resetSearchSuggestionAreaStatus();
    this._firstLoadApi = false;

    if (!this.childTeamEditionViewIsEnabled()) {
      this.team = {...this._originalTeam};
      if (this.team['name'] === 'Nuevo equipo') {
        this.team['name'] = '';
      }
    } else {
      this.team = {...this.deepTeamsChildRecursivity[this.deepTeamsChildRecursivity.length - 1]};
      if (this.team['name'] === 'Nuevo equipo') {
        this.team['name'] = '';
      }
    }
  }


  /**
   * Check if the team on wizard has team rule active
   *
   * @returns {boolean}
   */
  public hasTeamRulesTeam(): boolean {
    return !_.isNil(this.team)
      && this.team.hasOwnProperty('team_rule_teamt')
      && this.team['team_rule_teamt'].length > 0;
  }


  public getTeamRulesTeam(): Array<object> {
    return this.hasTeamRulesTeam()
      ? this.team['team_rule_teamt']
      : [];
  }

  public getNameTeamRule(teamT: object | any): string {
    return teamT.hasOwnProperty('category_name')
    && teamT['category_name'].length > 0
      ? teamT['category_name']
      : teamT.hasOwnProperty('teamts')
      && teamT['teamts'].length > 0
      && teamT['teamts'][0].hasOwnProperty('name')
        ? teamT['teamts'][0]['name']
        : 'Tipo de equipo hijo';
  }

  public getNameTeamTeamtRule(teamT: object | any): string {
    return teamT.hasOwnProperty('category_name')
    && teamT['category_name'].length > 0
      ? teamT['category_name']
      : teamT.hasOwnProperty('child_teamts')
      && teamT['child_teamts'].length > 0
      && teamT['child_teamts'][0].hasOwnProperty('name')
        ? teamT['child_teamts'][0]['name']
        :  'Sin alias';
  }

  /**
   * Return the level for a teamType rule
   * @param teamt
   * @returns {string}
   */
  public getTeamTypeLevels(teamt: object): number {
    return this.ruleTeamTeamHasRuleTeamTeam(teamt)
      ? teamt['teamts'][0]['rule_teamt_teamt'].length
      : 0;
  }

  public ruleTeamTeamHasTeamtsAndRulePerson(teamt: object): boolean {
    return this.ruleTeamTeamHasTeams(teamt)
      && teamt['teamts'][0].hasOwnProperty('rule_person_role')
      && Array.isArray(teamt['teamts'][0]['rule_person_role'])
      && teamt['teamts'][0]['rule_person_role'].length > 0;
  }

  private ruleTeamTeamHasTeams(teamt: object): boolean {
    return teamt
      && teamt.hasOwnProperty('teamts')
      && Array.isArray(teamt['teamts'])
      && teamt['teamts'].length > 0;
  }

  public ruleTeamTeamHasRuleTeamTeam(teamt: object): boolean {
    return teamt
      && teamt.hasOwnProperty('teamts')
      && Array.isArray(teamt['teamts'])
      && teamt['teamts'].length > 0
      && teamt['teamts'][0].hasOwnProperty('rule_teamt_teamt')
      && Array.isArray(teamt['teamts'][0]['rule_teamt_teamt'])
      && teamt['teamts'][0]['rule_teamt_teamt'].length > 0;
  }

  public getRulePersonRolesTeamTeam(teamt: object): Array<object | any> {
    return this.ruleTeamTeamHasTeamtsAndRulePerson(teamt)
      ? teamt['teamts'][0]['rule_person_role']
      : [];
  }


  /**
   * Detect if the role box active is complete
   * @return {boolean}
   */
  public isRolBoxActiveComplete(): boolean {
    return this.someRolBoxIsActive()
      ? Number(this.getRolBoxActive()['status']) === 3
      : false;
  }


  /**
   * Return to list of team type
   */
  public goBackToList(): void {
    this._router.navigateByUrl('/team');
  }


  /**********************************************************************
   ***********************************************************************
   ***************************** NEW *************************************
   ***********************************************************************
   **********************************************************************/

  public getAllTeamsGroups(): void {
    this._httpSrv
      .get(
      'teams/groups/',
      {
          page: 1,
          elements: 100
        }
      )
      .subscribe(
      (success) => this.onSuccessGetTeamsGroups(success),
      (fail) => this.onFailsGetTeamsGroups(fail),
    );
  }

  private onSuccessGetTeamsGroups(response: object | any): void {
    if (response['success']) {
      // this.teamGroups = response['object']['items'];
      response['object']['items'].forEach(group => {
        this.teamGroups.push({label: group['name'], value: group});
      });
      // this.setSearchGroupValue();
    } else {
      this.onFailsGetTeamsGroups(response);
    }
  }

  private onFailsGetTeamsGroups(response: object | any): void {
    setTimeout(() => {
      this.goBackToList();
    }, 2500);
  }

  public getAllGestor(): void {
    this._httpSrv.get(
      'persons/rolesystemtype/' + ROLESYSTEMS.ROLE_GESTOR,
      {
          page: 1,
          elements: 100
        }
      )
      .subscribe(
      (success) => this.onSuccessGetAllGestor(success),
      (fail) => this.onFailsGetAllGestor(fail),
    );
  }

  private onSuccessGetAllGestor(response: object | any): void {
    if (response['success']) {
      this.gestors = response['object']['items'];
      this.setSearchGestorValue();
    } else {
      this.onFailsGetAllGestor(response);
    }
  }

  private onFailsGetAllGestor(response: object | any): void {
    setTimeout(() => {
      this.goBackToList();
    }, 2500);
  }

  private setSearchGestorValue(): void {
    if (this.searchGestor) {
      this.searchGestor.wordToFilter = this.getGestorName();
    }
  }

  public searchTeamTypeTemplatesConfig(): object {
    return {
      flags: {
        showResultList: false,
      },
      error: {
        show: false
      },
      input: {
        placeholder: 'Busca un master por su nombre'
      },
      icon: {
        hasIconRight: true,
        hasSearchIcon: true,
        hideIconOnSearch: true
      }
    };
  }

  private restartSearchTTTemplatesComponent(): void {
    this.searchTeamTypesTemplate.restartComponent();
  }

  private clearSearchableFilter(): void {
    this.teamTypes = this.teamTypes.map((tt) => {
      delete tt['searchable'];
      return tt;
    });
  }

  private filterTeamTypesTemplates(): void {
    for (const group of this._teamGroupFilterSelected) {
      if (group.hasOwnProperty('selected')
        && group['selected']) {
        this.teamTypes = this.teamTypes.map((tt) => {
          if (tt.hasOwnProperty('groups') && tt['groups'].length > 0) {
            tt['searchable'] = this.getKeysSelectedGroups().includes(tt['groups'][0]['id']);
          }

          return tt;
        });
      }
    }
  }

  private isAllFilterEnabled(): boolean {
    return !_.isEmpty(this._teamGroupFilterSelected) && this._teamGroupFilterSelected[0]['id'] === -1;
  }

  private resetTeamTypes(): void {
    this.clearSearchableFilter();
    this.teamTypesFiltered = this.teamTypes;
  }

  private getAllSearchableTeamTypes(): Array<object> {
    if (this.isAllFilterEnabled()) {
      this.resetTeamTypes();
      return this.teamTypesFiltered;
    } else {
      return this.teamTypes.filter((tt) => {
        return tt['searchable'];
      });
    }
  }

  private getKeysSelectedGroups(): Array<number> {
    return this._teamGroupFilterSelected.filter((tt) => {
      return tt['selected'];
    }).map((tt) => {
      return tt['id'];
    });
  }

  public filterTeamTypeTemplates(event: object | any): void {
    if (event.hasOwnProperty('data')) {
      this.teamTypesFiltered = event['currentText'] === ''
        ? this.getAllSearchableTeamTypes()
        : event['data'];
    }
  }


  public rulePHasMinMaxQuantities(ruleP: object | any): boolean {
    return ruleP
      && ruleP.hasOwnProperty('min')
      && ruleP.hasOwnProperty('max')
      && !_.isNull(ruleP['min'])
      && !_.isNull(ruleP['max'])
      && ruleP['min'] > -2
      && ruleP['max'] > -2;
  }

  public getQuantityFromRulePerson(ruleP: object | any): string {
    return this.rulePHasMinMaxQuantities(ruleP)
      ? (ruleP['min'] === ruleP['max']
        ? ruleP['min']
        : ruleP['min'] + '/' + ruleP['max']).toString().replace('-1', '∞')
      : _.isNull(ruleP['min']) || _.isNull(ruleP['max'])
        ? '# ?'
        : '';
  }

  public getQuantityTeamTypesChildren(ruleTeamt: object | any): string {
    return ruleTeamt.hasOwnProperty('child_teamts')
    && ruleTeamt['child_teamts'].length > 0
      ? ruleTeamt['child_teamts'].length
      : '0';
  }


  /**
   * Collapse/hide team type list elements
   * @param id
   */
  public showTeamTypeCollapsibleArea(teamType: object): void {
    const isCollapsing = teamType['id'] === this._visibleCollapsibleIndex;
    this._visibleCollapsibleIndex = isCollapsing ? -1 : teamType['id'];
    this._teamTypeSelected = isCollapsing ? {} : teamType;
  }

  /**
   * Show collapsible area when index on click event matches with team type id
   * @param teamType
   */
  public showWhenThisTeamTypeIsSelected(id: number): boolean {
    return Number(id) === this._visibleCollapsibleIndex;
  }


  /**
   * Determine if a team type passed as param has rule mate
   * @param teamType
   * @return {Object|any|boolean}
   */
  public teamTypeHasRuleP(teamType: object | any): boolean {
    return teamType
      && teamType.hasOwnProperty('rule_person')
      && teamType['rule_person'].length > 0;
  }

  /**
   * Determine if one rule mate has roles
   * @param ruleP
   * @return {Object|any|boolean}
   */
  public rulePHasRoles(ruleP: object | any): boolean {
    return ruleP
      && ruleP.hasOwnProperty('child_roles')
      && ruleP['child_roles'].length > 0;
  }

  /**
   * Return a string of all roles concatenaded for one rule mate passed as parameter
   * @param ruleP
   * @return {any}
   */
  public getAllRolesFromRulePerson(ruleP: object | any): string {
    if (this.rulePHasRoles(ruleP)) {
      let concatenated = '';
      for (const rule of ruleP['child_roles']) {
        concatenated += rule['name'].length > 0 ? rule['name'] + ', ' : '';
      }
      return concatenated.substring(0, concatenated.length - 2);
    } else if (ruleP.hasOwnProperty('description') && !_.isNil(ruleP['description'])) {
      return 'Comodín';
    }
    return '';
  }


  /**
   * Determine if one team type has at least one sub-team as children
   * @param teamType
   * @return {boolean}
   */
  public teamTypeHasOneSubTeamAtLeast(teamType: object | any): boolean {
    return teamType['rule_teamt'].length > 0;
  }

  /**
   * Set widgetConfig for create team type on team button component
   * @return {{button: {text: string, notes: {main: string, extra: string}}}}
   */
  public setButtonCopyTeamTypeConfig(): object {
    return {
      button: {
        text: 'Crear equipo',
        classes: {
          main: 'secondary',
          extra: 'text-center col-12',
          hasOutlineBorder: true
        }
      }
    };
  }


  /**
   * Call to API: create a team (POST)
   */
  public createTeam(): void {
    this._httpSrv
      .post(
        'teams/',
        {
          name: 'Nuevo equipo',
          teamt_id: this._teamTypeSelected['id']
        }
      )
      .subscribe(
        (response) => {
          if (response.hasOwnProperty('success') && response['success']) {
            localStorage.setItem('GeneralDomainWidgetContainerComponent', 'team');
            this._router.navigateByUrl('/team/edit/' + response['object']['id']);
            this.getTeamInformation(response['object']['id'], true);
          } else {
            this._alertSrv.error('Error al crear el equipo para su edición');
          }
        }
      );
  }



  /********************************** ALIAS **********************************/
  /**
   * On click alias event. Prevent the rule activation
   * @param event
   */
  public onClickAlias(event): void {
    event.stopPropagation();
    event.preventDefault();
  }



  /**
   * On change event handler for ruleperson alias
   * @param {Object} personRule
   */
  public onChangeAlias(personRule: object): void {
    this.updateAlias(personRule);
  }


  /**
   * Update Api endpoint
   * @param {Object} personRule
   */
  private updateAlias(personRule: object): void {
    const teamId = _.isNil() ? this.team['id'] : this._teamTypeSelected['id'];
    this._httpSrv
      .patch(
        'teams/' + teamId.toString() + '/teamrulepersons/' + personRule['id'].toString(),
        {
          alias: personRule['alias']
        }
      )
      .subscribe(
        (response) => {
          if (!response.hasOwnProperty('success') || !response['success']) {
            this.getTeamInformation(teamId, false, false);
          }
        },
        (fail) => {
          this.getTeamInformation(teamId, false, false);
        }
      );
  }
}
