'use strict';

import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {LazyLoadEvent, DataTable} from 'primeng/primeng';

import {ROLESYSTEMSGROUPS} from '../shared/const/rolesystems.const';

import {TeamModel} from '../shared/models/team/team.model';

import {AuthSrvService} from '../shared/services/auth.service';
import {HttpSrvService} from '../shared/services/http.service';
import {LoaderService} from '../shared/services/loader.service';
import {SelectItem} from 'primeng/api';

const moment = require('moment');
const _ = require('lodash');

enum dropdownAvailableOptions {edit, create}

@Component({
  selector: 'app-team-component',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css'],
  providers: [HttpSrvService]
})

/**
 * @whatItDoes Contains the methods for Team to list and create this entity via API-endpoint
 * @author Elias Romero <elias.romero@steelmood.com>
 * @author Miguel Rico <miguel.rico@steelmood.com>
 */
export class TeamComponent implements OnInit, AfterViewInit {

  public static staticSelectedDropdownOption: number;
  public isTeamRemovable = TeamComponent.isTeamRemovable;

  /************************ COMPONENT VIEWCHILD  ************************/

  @ViewChild('teamsDatatable') private _teamsDatatable: DataTable;

  /** COMPONENT VARS **/

  // Static vars for static methods

  public getTeamSubTeams = TeamComponent.getTeamSubTeams;
  public getTeamAssignPersons = TeamComponent.getTeamAssignPersons;
  public getTeamDate = TeamComponent.getTeamDate;

  // Component vars

  public teams: Array<TeamModel> = [];
  public teamType = 4;
  public existMasters = false;


  public dropdownOptions: SelectItem[] = [
    {label: 'Fecha de modificación', value: dropdownAvailableOptions.edit},
    {label: 'Fecha de creación', value: dropdownAvailableOptions.create},
  ];
  public selectedDropdownOption: number;
  public teamDateType = 'Fecha de modificación';

  /************************ CONFIG VARS  ************************/

  public pagination = {
    page: 1,
    rows: 10,
    sortField: 'name',
    sortOrder: 'asc',
    totalRecords: 0,
  };


  /************************ STATIC METHODS  ************************/

  /**
   * Get team assign persons
   * @param {object} team
   * @returns {string}
   */
  private static getTeamAssignPersons(team: TeamModel): string {
    return team.getTeamAssignPersons();
  }

  /**
   * Get subteams of team
   * @param {object} team
   * @returns {string}
   */
  private static getTeamSubTeams(team: TeamModel): string {
    return team.getTeamSubTeams();
  }

  /**
   * Get date of team
   * @param {object} team
   * @returns {string}
   */
  private static getTeamDate(team: TeamModel): string {
    if (TeamComponent.staticSelectedDropdownOption === dropdownAvailableOptions.create
      && moment(team.created_at).isValid()) {
      return moment(team.created_at).format('DD/MM/YYYY').toString();
    } else if (TeamComponent.staticSelectedDropdownOption === dropdownAvailableOptions.edit
      && moment(team.updated_at).isValid()) {
      return moment(team.updated_at).format('DD/MM/YYYY').toString();
    }
    return '-';
  }

  /**
   * Return if a team is removable
   * @param {Object} team
   * @returns {boolean}
   */
  public static isTeamRemovable(team: TeamModel): boolean {
    return !_.isNil(team) && team.is_deleteable;
  }


  /************************ NATIVE METHODS  ************************/

  constructor(private _authSrv: AuthSrvService,
              private _httpSrv: HttpSrvService,
              private _loaderSrv: LoaderService,
              private _router: Router) {
    this._authSrv.verifyViewPermission('team');
    this._loaderSrv.general();
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.selectedDropdownOption = this.dropdownOptions[0].value;
    TeamComponent.staticSelectedDropdownOption = this.selectedDropdownOption;
    this.getAllMasters();
  }

  ngAfterViewInit(): void {
    if (!_.isNil(this._teamsDatatable)) {
      this._teamsDatatable.resolveFieldData = function (data, field) {
        if (data && field) {
          if (field === 'assignPersons') {
            return TeamComponent.getTeamAssignPersons(data);
          } else if (field === 'subteams') {
            return TeamComponent.getTeamSubTeams(data);
          } else if (field === 'teamDate') {
            return TeamComponent.getTeamDate(data);
          } else if (field.indexOf('.') === -1) {
            return data[field];
          } else {
            const fields = field.split('.');
            let value = data;
            for (let i = 0, len = fields.length; i < len; ++i) {
              if (value == null) {
                return '';
              }
              value = value[fields[i]];
            }
            return value;
          }
        } else {
          return null;
        }
      };
    }
  }

  isAllowed(tag: string): boolean {
    return this._authSrv.hasComponentPermission('team', tag);
  }

  /************************ EVENTS METHODS ************************/

  public createTeam(): void {
    this._router.navigateByUrl('/team/edit/');
  }


  /**
   * Return if the create team button is available for user logged
   * @returns {boolean}
   */
  public isAvailableCreateTeam(): boolean {
    return this._authSrv.hasRoleSystemsList(ROLESYSTEMSGROUPS.ADMINSGESTOR);
  }


  /**
   * LOad teasm lazy
   * @param {LazyLoadEvent} event
   */
  public loadTeamsLazy(event: LazyLoadEvent) {
    this.pagination.page = (event.first / event.rows) + 1;
    this.pagination.sortField = !_.isNil(event.sortField) ? event.sortField : this.pagination.sortField;
    const order = !_.isNil(event.sortOrder) ? event.sortOrder : 1;
    this.pagination.sortOrder = order === 1 ? 'asc' : 'desc';

    setTimeout(() => {
      if (this.teams) {
        this.getAllTeams();
      }
    }, 250);
  }


  /**
   * On click export datatable to csv
   */
  public onClickExportToCsv(): void {
    this.getAllTeamsToExport();
  }


  public onChangeDropdownOption(): void {
    TeamComponent.staticSelectedDropdownOption = this.selectedDropdownOption;
    if (this.selectedDropdownOption === dropdownAvailableOptions.create) {
      this.teamDateType = 'Fecha de creación';
    } else if (this.selectedDropdownOption === dropdownAvailableOptions.edit) {
      this.teamDateType = 'Fecha de modificación';
    }
  }


  /**
   * On tableMenu event success
   * @param event
   */
  public onTableMenuEvent(event): void {
    if (event['action'] === 'reset') {
      this._teamsDatatable.reset();
    } else if (event['action'] === 'error') {
    }
  }

  /************************ API METHODS  ************************/

  /**
   * Get all masters
   */
  private getAllMasters(): void {
    this._httpSrv
      .get(
      'teamts/',
      {status: 'COMPLETE'}
      )
      .subscribe(
      success => this.onSuccessGetAllMasters(success)
    );
  }

  /**
   * On succes get all masters
   * @param response
   */
  private onSuccessGetAllMasters(response: any): void {
    this.existMasters = !_.isNil(response.object) && response.object.total_count > 0;
  }

  /**
   * Call to API: all team (GET)
   */
  private getAllTeams(): void {
    this._httpSrv
      .get(
        'teams/',
        {
        page: this.pagination.page,
        elements: this.pagination.rows,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder,
        see_is_deleteable: true,
        statistics: true
      }
    )
    .subscribe(
      success => this.onSuccessGetAllTeams(success)
    );
  }

  /**
   * Listing the team returned for API: all team (GET)
   * @param response
   * @param {boolean} exportToCsv
   */
  private onSuccessGetAllTeams(response: any, exportToCsv: boolean = false): void {
    this._loaderSrv.stopGeneral();

    if (!_.isNil(response.object)) {
      this.teams = [];
      response.object.items.forEach( team => {
        this.teams.push(new TeamModel(team));
      });
      this.pagination.totalRecords = response.object.total_count;
    }

    if (exportToCsv) {
      setTimeout(() => {
        this._teamsDatatable.exportCSV();
        this.getAllTeams();
      }, 300);
    }
  }

  /**
   * Get all persons to export
   */
  private getAllTeamsToExport(): void {
    this._httpSrv.get(
      'teams/',
      {
        page: 1,
        elements: this.pagination.totalRecords,
        orderBy: this.pagination.sortField,
        orderType: this.pagination.sortOrder,
        statistics: true
      },
      'Listado equipos a exportar'
    ).subscribe(
      success => this.onSuccessGetAllTeams(success, true)
    );
  }
}

