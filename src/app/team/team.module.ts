/*GENERAL ASSETS*/
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';


import {TeamComponent} from './team.component';
import {HttpSrvService} from '../shared/services/http.service';
import {ComponentsModule} from '../shared/components/components.module';
import {TreeModule} from 'angular-tree-component';
import {EditTeamComponent} from './edit/edit-team.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    ComponentsModule,
    TreeModule
  ],
  exports: [
  ],
  declarations: [
    TeamComponent,
    EditTeamComponent
  ],
  providers: [
    HttpSrvService
  ]
})

export class TeamModule {
}
