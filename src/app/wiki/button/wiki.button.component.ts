'use strict';

import {Component, OnInit} from '@angular/core';


const _ = require('lodash');
const $ = require('jquery');

@Component({
  selector: 'app-wiki-button-component',
  templateUrl: './wiki.button.component.html',
  styleUrls: ['./wiki.button.component.css']
})

export class WikiButtonComponent {

  private basicConfig: any = {
    button: {
      type: 'button',
      hasText: true,
      text: 'Check this',
      isDisabled: false,
      classes: {
        main: 'primary',
        extra: '',
        isBlockButton: false,
        isLargeButton: false,
        isSmallButton: false,
        hasOutlineBorder: false
      }
    }
  };

  constructor() {
    this.basicConfig = JSON.stringify(this.basicConfig, null, 2);
  }

  public getButtonClass(index: string): string{
    return this.basicConfig.button.classes[index];
  }

}

