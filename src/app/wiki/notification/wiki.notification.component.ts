'use strict';

import {Component, OnInit} from '@angular/core';


const _ = require('lodash');
const $ = require('jquery');

@Component({
  selector: 'app-wiki-notification-component',
  templateUrl: './wiki.notification.component.html',
  styleUrls: ['./wiki.notification.component.css']
})

export class WikiNotificationComponent {

  public showFirstExampleNotification = false;
  public firstExampleButtonConfig = {button: {text: 'Mostrar'}};

  constructor() {
  }

  public showHideFirstExample(): void {
    this.showFirstExampleNotification = !this.showFirstExampleNotification;
  }
}
