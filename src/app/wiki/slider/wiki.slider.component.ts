'use strict';
import { Component, OnInit } from '@angular/core';

const _ = require('lodash');

@Component({
  selector: 'app-wiki-slider-component',
  templateUrl: './wiki.slider.component.html',
  styleUrls: ['./wiki.slider.component.css']
})


export class WikiSliderComponent{


  public sliderRange: number = 1;//[] = [25, 75];

  //exmaple 01
  public sliderRange01: number = 5;
  public min01 = 0;
  public max01 = 50;

  //exmaple 02
  public sliderRange02: number = 50;
  public min02 = 1;
  public max02 = 100;
  public step02 = 0.5;

  //exmaple 03
  public sliderRange03: number = 50;
  public min03 = 0;
  public max03 = 100;
  public step03 = 1;
  public sliderConfig03: any = {
    pips: {
      mode: 'count',
      density: 1,
      values: 11,
      stepped: true
    }
  };

  //exmaple 04
  public sliderRange04 = [5,15];
  public sliderConfig04: any = {
    behaviour: 'drag',
    connect: true,
    step: 1,
    start: this.sliderRange04,
    range: {
      min: 0,
      max: 20
    },
    pips: {
      mode: 'steps',
      density: 5
    }
  };
    // behaviour: 'drag',
    // start: [25, 75],
    // keyboard: true,  // same as [keyboard]="true"
    // margin: 1,
    // step: 1,
    // pageSteps: 10,  // number of page steps, defaults to 10
    // range: {
    //   min: 0,
    //   max: 100
    // }


  constructor() { }


}
