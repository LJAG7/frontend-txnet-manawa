import { Component, OnInit } from '@angular/core';

const _ = require('lodash');

@Component({
  selector: 'app-split-pane',
  templateUrl: './wiki.split-pane.component.html',
  styleUrls: ['./wiki.split-pane.component.css']
})
export class WikiSplitPaneComponent implements OnInit {

  public isMobile = false;
  constructor() { }

  ngOnInit() {
  }

  onResize(event) {
    // console.log('resize: ' + window.innerWidth);
    this.isMobile = window.innerWidth < 769;
  }

}
