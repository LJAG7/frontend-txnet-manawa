'use strict';

import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {HttpSrvService} from '../shared/services/http.service';

const _ = require('lodash');
const $ = require('jquery');

@Component({
  selector: 'app-wiki-component',
  templateUrl: './wiki.component.html',
  providers: [HttpSrvService],
  styleUrls: ['./wiki.component.css']
})

export class WikiComponent {

  private basicConfig: any = {
    button: {
      type: 'button',
      hasText: true,
      text: 'Check this',
      isDisabled: false,
      classes: {
        main: 'primary',
        extra: '',
        isBlockButton: false,
        isLargeButton: false,
        isSmallButton: false,
        hasOutlineBorder: false
      }
    }
  };

  constructor() {
    this.basicConfig = JSON.stringify(this.basicConfig, null, 2);
  }
}

