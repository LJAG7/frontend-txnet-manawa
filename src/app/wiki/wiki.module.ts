/*GENERAL ASSETS*/
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';
/*MODULE ASSETS*/
import {WikiComponent} from './wiki.component';
import {WikiNotificationComponent} from './notification/wiki.notification.component';
import {WikiButtonComponent} from './button/wiki.button.component';
import {WikiSliderComponent} from './slider/wiki.slider.component';
import { WikiSplitPaneComponent } from './split-pane/wiki.split-pane.component';

/*SERVICES*/
import {HttpSrvService} from '../shared/services/http.service';

import {ComponentsModule} from '../shared/components/components.module';
import { NouisliderModule } from 'ng2-nouislider';
import { SplitPaneModule } from 'ng2-split-pane/lib/ng2-split-pane';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    ComponentsModule,
    NouisliderModule,
    SplitPaneModule
  ],
  exports: [
    WikiComponent
  ],
  declarations: [
    WikiComponent,
    WikiNotificationComponent,
    WikiButtonComponent,
    WikiSliderComponent,
    WikiSplitPaneComponent
  ],
  providers: [
    HttpSrvService
  ]
})

export class WikiModule {}
